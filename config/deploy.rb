# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 't4p'
set :repo_url, 'git@bitbucket.org:gollariel/mypet.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
ask :branch, "develop"

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/t4p'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  desc 'Restart application'
  task :php_restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
      execute :sudo, "/usr/sbin/service php5-fpm restart"
    end
  end

    task :bower_install do
      on roles(:app), in: :sequence, wait: 5 do
        # Your restart mechanism here, for example:
        # execute :touch, release_path.join('tmp/restart.txt')

        execute :ln, "-nfs", "/var/www/node/node_modules #{release_path}/app/templates/mypet/node_modules"
        execute "cd #{release_path}/app/templates/mypet && bower install"
      end
    end

        task :npm_install do
          on roles(:app), in: :sequence, wait: 5 do
            # Your restart mechanism here, for example:
            # execute :touch, release_path.join('tmp/restart.txt')

            execute :mkdir, "-p", "/var/www/node"
            execute "cd /var/www/node && bower install"
          end
        end

        task :files_dir_create do
          on roles(:app), in: :sequence, wait: 5 do
              # Your restart mechanism here, for example:
             # execute :touch, release_path.join('tmp/restart.txt')

            execute :mkdir, "-p", "/var/www/files"
          end
        end
end

namespace :deploy do
    before :starting, :map_composer_command do
        on roles(:app) do |server|
            SSHKit.config.command_map[:composer] = "#{shared_path.join("composer.phar")}"
        end
    end
    before :starting, 'composer:install_executable'

        desc "Check if set composer_cache"
        after :updating, :check_composer_cache do
            on roles(:app), in: :parallel do
                execute "if [ -d #{deploy_to}/shared/composer_cache ]; then cp -a #{deploy_to}/shared/composer_cache #{release_path}/vendor; fi"
            end
        end

        desc "Copy vendor folder to composer_cache"
        after :updated, :update_composer_cache do
            on roles(:app), in: :parallel do
                execute "rm -rf #{deploy_to}/shared/composer_cache && cp -a #{release_path}/vendor #{deploy_to}/shared/composer_cache"
            end
        end

end

namespace :app do
    before "deploy:updated", :setup do
        on roles(:app), in: :parallel do
                           if fetch(:config_platform) && fetch(:config_serverConfig)
                               execute :rm, "-rf", "#{release_path}/app/config/platform.php"
                               execute :rm, "-rf", "#{release_path}/app/config/serverConfig.php"
                               execute :ln, "-s", "./platforms/#{fetch(:config_platform)}.php #{release_path}/app/config/platform.php"
                               execute :ln, "-s", "./platforms/#{fetch(:config_serverConfig)}.php #{release_path}/app/config/serverConfig.php"
                               # assets symlink
                               execute :ln, "-nfs", "#{release_path}/app/templates/mypet/resource #{release_path}/app/public/"
                               execute :ln, "-nfs", "/var/www/files #{release_path}/app/public/files"
                               # control panel install
                               execute :php, "#{release_path}/app/console CP/createIndex"
                               execute :php, "#{release_path}/app/console CP/install"
                           else
                               puts "===!!! config_platform, config_serverConfig is not correct !!!==="
                               invoke "deploy:stop"
                           end
        end
    end
end
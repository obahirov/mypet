//function dump(obj) {
//    var out = '';
//    for (var i in obj) {
//        out += i + ": " + obj[i] + "\n";
//    }
//
//    alert(out);
//
//
//    var pre = document.createElement('pre');
//    pre.innerHTML = out;
//    document.body.appendChild(pre)
//
//
//            dump(e.target);
//}

$(function(){
    var centered = $(".sign-up, .login");
    centered.css("opacity", 0);
});

$(window).on('load',function() {

    //  Слайдер  --------------------------------------
    $(".bg-slider").responsiveSlides({
        "speed": 2000
    });

    //  Перемикання мов у футері ----------------------
    $(".langvich").hover(function () {
        $(".switch-languages").stop().fadeIn();
    }, function(){
        $(".switch-languages").stop().fadeOut();
    });

    //  Стилізація форм ----------------------------
    if($('input, select').length){
        $('input, select').styler();
    }

    //  Налаштування .header ----------------------
    $(".settings").hover(function () {
        $(".list-settings").stop().fadeIn();
    }, function(){
        $(".list-settings").stop().fadeOut();
    });

    // Іконки поідомленнь -------------------------
    $(".letters, .alert").hover(function () {
        $(this).children().removeClass("wobble");
        $(this).children().addClass("bounceIn");
    }, function(){
        $(this).children().removeClass("bounceIn");
    });

    //   Кнопка close - закрити модуль в лівому сайдбарі ---
    $(".close").click(function () {
        var close = $(this).parents(".module");
        close.css("opacity", 0)
             .animate({
                height: 0
             }, 500,
             function(){
                close.remove();
             });
    });

    //  lightbox -------------------------------------
    if($(".lightbox").length){
        $(".lightbox").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 460,
                    fitToView	: false,
                    width		: '460px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    wrapCSS     : "spinner",
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
                $('input, select').styler({
                    filePlaceholder: "",
                    fileBrowse: ""
                });
                $(".owner").autocomplete({source: availableOwner});
                $(".birthday").mask("99/99/9999");
                $('textarea').autosize({
                    callback: function(){
                        $.fancybox.update({
                            helpers: {
                                overlay: {
                                    locked: true
                                }
                            }
                        });
                        if($(this).height() > 315){
                            $(this).trigger('autosize.destroy').height(315);
                        }
                    }
                });
            });
            return false;
        });
    }

    if($(".lightbox-2").length){
        $(".lightbox-2").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 990,
                    fitToView	: false,
                    width		: '990px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true
                });
            });
            return false;
        });
    }

    //  lightbox -------------------------------------
    if($(".lightbox-group").length){
        $(".lightbox-group").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 460,
                    fitToView	: false,
                    width		: '460px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    wrapCSS     : "spinner"
                });
                $('input, select').styler({
                    filePlaceholder: "",
                    fileBrowse: ""
                });
                $( ".country" ).autocomplete({source: availableCountry});
                $( ".city" ).autocomplete({source: availableCity});

                $('textarea').autosize({
                    callback: function(){
                        $.fancybox.update({
                            helpers: {
                                overlay: {
                                    locked: true
                                }
                            }
                        });
                        if($(this).height() > 315){
                            $(this).trigger('autosize.destroy').height(315);
                        }
                    }
                });
            });
            return false;
        });
    }

    if($(".add-new-photo").length){
        $(".add-new-photo").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 460,
                    fitToView	: false,
                    width		: '460px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    wrapCSS     : "spinner"
                });
                $('input, select').styler({
                    filePlaceholder: "",
                    fileBrowse: ""
                });
                $(".owner").autocomplete({source: availableOwner});
                $(".birthday").mask("99/99/9999");
                $('textarea').autosize({
                    callback: function(){
                        $.fancybox.update({
                            helpers: {
                                overlay: {
                                    locked: true
                                }
                            }
                        });
                        if($(this).height() > 315){
                            $(this).trigger('autosize.destroy').height(315);
                        }
                    }
                });
            });
            return false;
        });
    }

    //   маска на поле input - день народження --------------
    if($(".birthday").length){
        $(".birthday").mask("99/99/9999");
    }

    //   Кнорка клонування
     $(".add").click(function (e) {
        e.preventDefault();
        var parentAdd = $(this).siblings();
        var item = parentAdd.find(".item").clone(true).eq(0);
        item.find("input").val("");
        var select = item.find('select');
        select.each(function(){
          var $this = $(this),
              name = $this.attr('name'),
              data = $this.html(),
              td = $this.parents('td:first'),
              s = $('<select></select>').attr('name', name).html(data);
            td.html(s);
            s.styler();
        });
        var birthday = item.find('.birthday').removeClass('birthday');
        birthday.unmask().mask("99/99/9999");
        item.appendTo(parentAdd);
    });

    //  Кнорка видалення
    $(".remove").click(function (e) {
        e.preventDefault();
        var quantity =  $(this).parent().parent().find(".item").length;
        if(quantity > 1){
            $(this).parents(".item").remove();
        }
    });

    //   Якщо спеціаліст то показати форму  (форма редагування профілю)
    var sb  = $(".specialist-body"),
        isb = $(".specialist-checkbox input");
        isb.change(function() {
        if($(this).is(':checked')) {
            sb.slideDown();
        } else {
            sb.slideUp();
        }
    });

    //  іконка редагування головного зображення сторінки користувача
    $(".cover-image").hover(function () {
         $(this).find("a").stop().animate({
             opacity: 1
         }, 500);
    }, function(){
        $(this).find("a").stop().animate({
            opacity: 0
        }, 500);
    });

    var availableCountry = [
        "Україна",
        "Білорусь",
        "Хуйлостан"
    ];
    var availableCity = [
        "Львів",
        "Київ",
        "Москвабад"
    ];
    var availableSpecies = [
        "Британська короткошерста",
        "Британська довгошерста"
    ];
    var availableOwner = [
        "Оля",
        "Юля",
        "Роман",
        "Сергій",
        "Володя"
    ];
    var availableBreeder = [
        "Оля",
        "Юля",
        "Роман",
        "Сергій",
        "Володя"
    ];
    $( ".country" ).autocomplete({source: availableCountry});
    $( ".city" ).autocomplete({source: availableCity});
    $(".species").autocomplete({source: availableSpecies});
    $(".owner").autocomplete({source: availableOwner});
    $(".breeder").autocomplete({source: availableBreeder});

    //  tooltip
    if($(".tooltip").length){
        $('.tooltip').tooltipster();
    }

    //  Фотоальбом
    if($(".photo-album, .post-wall").length){
        $(".photo-pop-up").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 780,
                    fitToView	: false,
                    width		: '780px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    helpers: {
                        overlay: {
                            locked: true
                        }
                    }
                });
                $('input, select').styler();
                $('textarea').autosize();
            });
            return false;
        });

        $(".photo-item").hover(function () {
            $(this).find(".bottom").stop().animate({bottom: 0}, 300);
        }, function(){
            $(this).find(".bottom").stop().animate({bottom: -23}, 300);
        });
    }

    //  Автоматично міняємо висоту  textarea
    if($("textarea").length){
        $('textarea').autosize();
    }

    //  Змінюємо висто
    if($(".album-item").length){
        $(".album-item").hover(function () {
            $(this).find(".bottom").stop().animate({
                top: 0
            }, 100);
        }, function(){
            $(this).find(".bottom").stop().animate({
                top: 107
            }, 100);
        });
    }
    if($(".scroll-pane").length){
        $('.scroll-pane').jScrollPane();
    }

//  Сторінка пошуку - вкладки

       var tabs = $('ul.tabs');
       tabs.each(function(i) {
           var storage = localStorage.getItem('tab'+i);
           if (storage) $(this).find('li').eq(storage).addClass('current').siblings().removeClass('current')
               .parents('div.section').find('div.box').hide().eq(storage).show();
       });

       tabs.on('click', 'li:not(.current)', function() {
           $(this).addClass('current').siblings().removeClass('current')
               .parents('div.section').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();
           var ulIndex = $('ul.tabs').index($(this).parents('ul.tabs'));
           localStorage.removeItem('tab'+ulIndex);
           localStorage.setItem('tab'+ulIndex, $(this).index());
       });





});


$(window).on('load resize',function() {
    //  Вирівнювання по центру ".sign-up" на строінках входу, зміни паролю
    var windowH = $(window).height(),
        formBox = $(".form-box"),
        centered = $(".sign-up, .login"),
        centeredH = centered.innerHeight();
        centered.css("margin-top", (centeredH/-2) + "px");
        if(windowH < centeredH + 300){
            formBox.addClass("form-box-css");
            formBox.height(centeredH);
        } else{
            formBox.removeClass("form-box-css");
        }
        setTimeout(function() {
            centered.animate({opacity: 1}, 2000);
        }, 300);
});

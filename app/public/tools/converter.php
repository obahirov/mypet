<?php
use MyPet\FileConverter\Classes\Converter;

require(realpath(__DIR__ . '/../../bootstrap.php'));
$file = 'test.csv';
$convertFormat = 'json';

Converter::run($file, $convertFormat);
<?php
require(realpath(__DIR__ . '/../../bootstrap.php'));

if(\KMCore\Validation\Validate::isFloat(0.7))
{
	header('HTTP/1.1 200 OK');
	die("200 OK");
}
else
{
	header('HTTP/1.1 500 Internal Server Error');
	die("500 Forbidden");
}
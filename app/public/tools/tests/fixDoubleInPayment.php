<?php
set_time_limit(0);

require(realpath(__DIR__ . '/../../../bootstrap.php'));
\MyPet\Payment\Model\PaymentHistory::setSafeWrite(false);
$history = \MyPet\Payment\Model\PaymentHistory::getAllAdvanced(
	[],
	array(),
	false,
	false,
	false,
	false,
	true
);

$transactionId = '';
while ($dbobject = $history->getNext())
{
	$pyment = \MyPet\Payment\Model\PaymentHistory::getAllAdvanced(['transaction_id' => $dbobject['transaction_id']]);
	if(count($pyment) > 1)
	{
		$double = array_shift($pyment);
		\MyPet\Payment\Model\PaymentHistory::deleteAllAdvanced(['_id' => $double['_id']]);
	}
	usleep(10000);
}
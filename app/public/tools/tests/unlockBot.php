<?php
use KMCore\Events\EventDispatcher;
use MyPet\Bots\Model\BotsUser;
use MyPet\SP\History\HistoryModel;
use MyPet\UserService\Users\Classes\Helper;

set_time_limit(0);

require(realpath(__DIR__ . '/../../../bootstrap.php'));

$params = ['lock' => true];
$fields = ['id' => 1];
$sanitize = false;
$sort = false;
$page = false;
$per_page = false;
$onlycursor = true;
$cursor = \MyPet\Bots\Model\BotsUser::getAllAdvanced(
	$params,
	$fields,
	$sanitize,
	$sort,
	$page,
	$per_page,
	$onlycursor
);
$i = 0;
while($data = $cursor->getNext())
{
	$query = array('id' => $data['id']);
	$update = array('$set' => array('lock' => false, 'last_sitin' => 0));
	$fields = ['id' => 1];
	BotsUser::findAndModify($query, $update, $fields);
	$i++;
	print $i.PHP_EOL;
	ob_flush();
	microtime(10000);
}
<?php
require(realpath(__DIR__.'/../../../bootstrap.php'));

/** @var MongoCursor $cursor */
$cursor = \MyPet\Users\Model\User::getAllAdvanced(
	[],
	[
		'id'         => 1,
		'first_name' => 1,
		'last_name'  => 1,
		'photo'      => 1,
		'location'   => 1,
		'phones'     => 1,
		'career'     => 1,

		'profession'     => 1,
		'isProf'         => 1,
		'organizations'  => 1,
		'sites'          => 1,
		'progress'          => 1,
	],
	false,
	false,
	false,
	false,
	true
);
$result = [];
while ($cursor->hasNext())
{
	$data = $cursor->getNext();
	$data['he_invited'] = \MyPet\Referrals\Model\HistoryModel::count(['id' => $data['id']]);
	$data['he_was_invited_to'] = \MyPet\Referrals\Model\HistoryModel::count(['referralId' => $data['id']]);
	$data['pets'] = \MyPet\Pets\Model\PetModel::count(['ownerIds' => $data['id']]);
	$data['friends'] = \MyPet\Friends\Model\FriendsModel::count(['id' => $data['id'], 'status' => 1]);

	$result[] = $data;
}
print json_encode($result);
<?php
//set_time_limit(0);

require(realpath(__DIR__ . '/../../../bootstrap.php'));

$params = '{
	"a": "game_end",
	"gn": "406f7672-6152-4dbe-a8e7-b585f10127aa",
	"croupier": "riu32sv4hk9nu2behno2yyd8xwgv55uw",
	"manager": "manager20",
	"board": [
		"5d",
		"9s",
		"Qs",
		"Jh",
		"4d"
	],
	"stats": {
		"ap": 18360,
		"tvpip": 0.81666666666667
	},
	"seats": [
		{
			"username": "49952df8e67b0cca697048b458c",
			"br": 9400,
			"state": "play",
			"sn": 4
		},
		{
			"username": "49952df8e67b0cca697048b458cBzzz0",
			"br": 9400,
			"state": "play",
			"sn": 4
		}
	],
	"game_log": {
		"rake": 0,
		"players": [
			{
				"username": "49952df8e67b0cca697048b458c",
				"start_br": 100,
				"end_br": 500,
				"bet": 400,
				"pos": "sb",
				"last_act": "call",
				"hole": ["Th", "Ts"],
				"made_hand": [],
				"hand_id": 0,
				"hand_strength": 0
			},
			{
				"username": "49952df8e67b0cca697048b458cBzzz0",
				"start_br": 100,
				"end_br": 50,
				"bet": 400,
				"pos": "sb",
				"last_act": "call",
				"hole": ["9h", "9s"],
				"made_hand": [],
				"hand_id": 0,
				"hand_strength": 0
			}
		]
	}
}';
\MyPet\Rest\Callbacks\GameManager::getInstance('gameEnd', json_decode($params, true))->callback();
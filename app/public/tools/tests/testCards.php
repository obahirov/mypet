<?php
use KMCore\SocialApi\SocialApi;

require(realpath(__DIR__ . '/../../../bootstrap.php'));

ini_set('memory_limit', '1024M');
set_time_limit(0);
session_write_close();
while (ob_get_level())
{
	ob_end_clean();
}

$cache = realpath(APP_PATH.'/cache');

removeCache($cache);
function removeCache($cache)
{
	$iterator = new DirectoryIterator($cache);
	while($iterator->valid()) {
		/** @var $fileinfo DirectoryIterator */
		$fileinfo = $iterator->current();
		if($fileinfo->isFile())
		{
			unlink($fileinfo->getPath().'/'.$fileinfo->getFilename());
		}
		elseif($fileinfo->getFilename() !== '..' && $fileinfo->getFilename() !== '.')
		{
			removeCache($fileinfo->getPath().'/'.$fileinfo->getFilename());
			rmdir($fileinfo->getPath().'/'.$fileinfo->getFilename());
		}
		$iterator->next();
	}
}
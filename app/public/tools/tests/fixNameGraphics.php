<?php

require(realpath(__DIR__ . '/../../../bootstrap.php'));

$old_dir = './images';
/**
 * Class FileExtensionFilter
 */
class FileExtensionFilter extends FilterIterator
{
	// Белый список расширений файлов
	protected $ext = ["png"];

	// Абстрактный метод, который надо реализовать в подклассе
	public function accept() {
		return in_array($this->getExtension(), $this->ext);
	}
}

//Создаем новый итератор
$dir = new FileExtensionFilter(new DirectoryIterator($old_dir));
try {
	foreach ($dir as $item) {
		$info = new SplFileInfo($item);
		$toFilteret = $info->getBasename('.'.$info->getExtension());
		$toFilteret=strtolower($toFilteret);

		$result = explode('.', $toFilteret);
		$parsetData = explode('_', $result[0]);

		$name = '';
		$type = '_';
		$resultName = 'pka_';
		if(isset($parsetData[0]) && $parsetData[0] === 'pka' && isset($parsetData[1]))
		{
			if(!empty($parsetData[1]) && (!empty($parsetData[2] || !empty($parsetData[3]))))
			{
				$type = $parsetData[1];
				if(empty($parsetData[2]))
				{
					$name = $parsetData[3];
				}
				else
				{
					$name = $parsetData[2];
				}
			}
			elseif(empty($parsetData[2]))
			{
				$name = $parsetData[1];
			}
		}
		if(!empty($name))
		{
			$resultName .= $type.'_'.$name.'.'.$info->getExtension();
			print $resultName.PHP_EOL;
			$res = rename(realpath($old_dir.'/'.$item), $resultName);
		}
	}
}
catch (Exception $e) {
	echo get_class($e) . ": " . $e->getMessage();
}
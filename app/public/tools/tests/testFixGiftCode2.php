<?php
use KMCore\DB\GeoLocation\Adapter\MongoAdapter;
use KMCore\Helper\StringHelper;

require(realpath(__DIR__ . '/../../../bootstrap.php'));

$point0 = new \KMCore\DB\GeoLocation\Point(5, 7);
$point1 = new \KMCore\DB\GeoLocation\Point(43, 75);
$point2 = new \KMCore\DB\GeoLocation\Point(21, 32);
$point3 = new \KMCore\DB\GeoLocation\Point(3, 6);
$point4 = new \KMCore\DB\GeoLocation\Point(56, 32);
$line = new \KMCore\DB\GeoLocation\LineString($point0, $point1);

$objectStorage2 = new SplObjectStorage();
$objectStorage = new SplObjectStorage();
$objectStorage->attach($point0);
$objectStorage->attach($point1);
$objectStorage->attach($point2);
$objectStorage->attach($point3);
$objectStorage->attach($point4);
$objectStorage3 = new SplObjectStorage();
$objectStorage3->attach($point4);
$objectStorage3->attach($point1);
$objectStorage3->attach($point2);
$objectStorage3->attach($point3);
$objectStorage3->attach($point0);
$objectStorage3->attach($point2);
$objectStorage2->attach($objectStorage);
$objectStorage2->attach($objectStorage3);
$polygon = new \KMCore\DB\GeoLocation\Polygon($objectStorage2);

$geometruStorage = new SplObjectStorage();
$geometruStorage->attach($point0);
$geometruStorage->attach($line);
$geometruStorage->attach($polygon);
$geometry = new \KMCore\DB\GeoLocation\GeometryCollection($geometruStorage);

$mongo = new \KMCore\DB\GeoLocation\Adapter\MongoAdapter($point2);
//print_r($mongo->getToFind('$centerSphere', 10));

$result = MongoAdapter::parse(['type' => 'Point', 'coordinates' => [6, 7]]);
if($result)
{
	print_r($result->getCoordinates());
}
<?php
set_time_limit(0);
require(realpath(__DIR__ . '/../../../bootstrap.php'));

$oldPrices = \MyPet\PromoCampaign\Model\PromoModel::getAllAdvanced([]);
foreach($oldPrices as $priceData)
{
	foreach($priceData['prices'] as $key=>$prices)
	{
		$price = new \MyPet\Store\Products\Model\ProductModel();
		$price->name = $priceData['name'];
		$price->id   = $priceData['id'] << 16 | $key; //PK
		$price->bundleId = $prices['itunes_id'];
		$price->enable = true;
		$price->priority = $priceData['priority'];
		$price->type = $priceData['name'];
		$price->product = $prices['type'];
		$price->amount = $prices['amount'];
		$price->bonus = isset($prices['bonus'])?$prices['bonus']:0;
		$price->groups = $prices['groups'];
		$price->prices = ['USD' => $prices['price'], 'SN_OK' => $prices['price_sn']['ok'], 'SN_MM' => $prices['price_sn']['mm'], 'SN_VK' => $prices['price_sn']['vk']];
		$price->currency = 'USD';
		$price->description = [];
		$price->exchangeRates = [];
		if($priceData['id'] === 1 || $priceData['id'] === 43)
		{
			$price->isAction = false;
			if($key >= 1 && $key <= 6)
			{
				$price->inMenu = true;
			}
		}
		else
		{
			$price->isAction = true;
		}
		$price->viewType = $priceData['type'];

		if(isset($prices['best']))
		{
			$price->isBest = true;
		}

		if($key == 1000)
		{
			$price->isFlashSale = true;
		}

		if(isset($prices['includes']))
		{
			$price->params['includes'] = $prices['includes'];
		}

		if(isset($prices['avatarId']))
		{
			$price->params['avatarId'] = $prices['avatarId'];
		}

		$price->save();
		$price->clearStaticCache();
	}
}
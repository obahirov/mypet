<?php
use MyPet\Posts\Classes\PostManager;

set_time_limit(0);
require(realpath(__DIR__ . '/../../../bootstrap.php'));

$userId = isset($_REQUEST['userId'])?$_REQUEST['userId']:'245556365736e8520e0058b4572';
$postId = isset($_REQUEST['postId'])?$_REQUEST['postId']:'245556365736e8520e0058b4572';

$users = \MyPet\Users\Model\User::getAllAdvanced([], [], true, true, 0, 4, false);
$pets = \MyPet\Pets\Model\PetModel::getAllAdvanced([], [], true, true, 0, 4, false);
$news = new \MyPet\Posts\Classes\NewsHelper($GLOBALS['serverContainer'], $userId, 'test');
$news->addNews($users);
$news->addNewsAddPet($pets, 10);
$news->addNewsNewUsers($users, 10);
$news->addNewsNewPets($pets, 10);
$news->addNewsPets($pets, 10);

$photos = \MyPet\Photos\Model\PhotoModel::getAllAdvanced([], [], true, true, 0, 4, false);
$post = new \MyPet\Posts\Model\PostModel(['id' => $postId]);
$ancestry = new \MyPet\Pets\Classes\PetManager($GLOBALS['serverContainer']);
$news->addNewsAddWallPost($post->getFieldsSanitized());
//$news->addNewsAddPetWithRel($ancestry->get);
$news->addNewsAddPhoto($photos, 10);
<?php

require(realpath(__DIR__ . '/../../../bootstrap.php'));

$old_dir = './';
/**
 * Class FileExtensionFilter
 */
class FileExtensionFilter extends FilterIterator
{
	// Белый список расширений файлов
	protected $ext = ["png"];

	// Абстрактный метод, который надо реализовать в подклассе
	public function accept() {
		return in_array($this->getExtension(), $this->ext);
	}
}

//Создаем новый итератор
$dir = new FileExtensionFilter(new DirectoryIterator($old_dir));
try {
	foreach ($dir as $item) {
		$info = new SplFileInfo($item);
		$toFilteret = $info->getBasename('.'.$info->getExtension());
		$parsetData = explode('_', $toFilteret);
		$prefix = array_shift($parsetData);
		$type = array_shift($parsetData);
		$name = implode('_', $parsetData);
		$avatar = new \MyPet\Avatar\Model\ListAvatar($name);
		$avatar->_id = $type.'_'.$name;
		$avatar->type = $type;
		$avatar->save();
	}
}
catch (Exception $e) {
	echo get_class($e) . ": " . $e->getMessage();
}
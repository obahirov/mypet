<?php
use MyPet\UserService\Users\Model\User;

set_time_limit(0);
require(realpath(__DIR__ . '/../../../bootstrap.php'));

$fileName = 'parser.csv';
$file = new SplFileObject($fileName);
$file->setFlags(SplFileObject::READ_CSV);
$file->setCsvControl(';', '"');
$i = 0;
$transaction_ids = [];
$doubles = [];
foreach ($file as $row)
{
	if(isset($row[6]))
	{
		$userId = $row[1];
		$transactionId = $row[6];
		$money = $row[4];

		if(isset($transactionIds[$transactionId]))
		{
			isset($doubles[$userId])?$doubles[$userId] += $money:$doubles[$userId] = $money;
		}
		$transactionIds[$transactionId] = $userId;
		$i++;
		printf("%s) transaction_id %s" . PHP_EOL, $i, $transactionId);
		ob_flush();
	}
}
unset($transactionIds);

$i = 0;
foreach($doubles as $userId=>$money)
{
//	User::setSafeWrite(false);
	$i++;
	$user = new User($userId);
	if($user->isLoadedObject())
	{
		$user->money -= $money;
		$user->save();
		printf("%s) %s success!" . PHP_EOL, $i, $userId);
	}
	$user->clearStaticCache();
	unset($doubles[$userId]);
	ob_flush();
}

print count($doubles).PHP_EOL;
<?php
use MyPet\Rest\Model\GameProfile;

set_time_limit(0);

require(realpath(__DIR__ . '/../../../bootstrap.php'));

$cursor = GameProfile::getAllAdvanced(
	['bot' => true],
	array(),
	false,
	false,
	false,
	false,
	true
);
while ($cursor->hasNext())
{
	$bot = $cursor->getNext();
	if ($bot['hand_id'] === 10 || $bot['hand_id'] === 9)
	{
		$hole = [];
		$results = [];
		$made_hand = [];
		$deck = new \MyPet\Cards\CardDeck();

		$hand_id = mt_rand(7, 8);
		if ($hand_id === 7)
		{
			$firstRankCard = [10, 11, 12];
			shuffle($firstRankCard);
			$secondRankCard = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
			shuffle($secondRankCard);
			$suits = [0, 1, 2, 3];

			shuffle($suits);
			$id_rank = array_pop($firstRankCard);
			for ($i = 0; $i <= 2; $i++)
			{
				$id_suit = $suits[$i];
				$id = $deck->getCardIdByRankAndSuit($id_rank, $id_suit);
				$results[] = $deck->getCardById($id);
			}

			shuffle($suits);
			$id_rank = array_pop($secondRankCard);
			for ($i = 0; $i <= 1; $i++)
			{
				$id_suit = $suits[$i];
				$id = $deck->getCardIdByRankAndSuit($id_rank, $id_suit);
				$results[] = $deck->getCardById($id);
			}

			$made_hand = $results;
			$hole[] = array_pop($results);
			$hole[] = array_pop($results);

			$hand_strength = 101163008;
		}
		else
		{
			$firstRankCard = [0, 12];
			shuffle($firstRankCard);
			$suits = [0, 1, 2, 3];

			shuffle($suits);
			$id_rank = array_pop($firstRankCard);
			for ($i = 0; $i <= 3; $i++)
			{
				$id_suit = $suits[$i];
				$id = $deck->getCardIdByRankAndSuit($id_rank, $id_suit);
				$results[] = $deck->getCardById($id);
			}

			$results[] = $deck->getRandomCard();

			$made_hand = $results;
			$hole[] = array_pop($results);
			$hole[] = array_pop($results);

			$hand_strength = 117878784;
		}

		if ($bot['hand_id'] === 10)
		{
			GameProfile::findAndModify(
				['id' => $bot['id']],
				[
				'$set' => [
					'hand_id'       => $hand_id,
					'hand_strength' => $hand_strength,
					'made_hand'     => $made_hand,
					'hole'          => $hole
				]
				]
			);
		}
		elseif ($bot['hand_id'] === 9)
		{
			GameProfile::findAndModify(
				['id' => $bot['id']],
				[
				'$set' => [
					'hand_id'       => $hand_id,
					'hand_strength' => $hand_strength,
					'made_hand'     => $made_hand,
					'hole'          => $hole
				]
				]
			);
		}
	}
	microtime(10000);
}
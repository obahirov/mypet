<?php
use KMCore\SocialApi\SocialApi;

require(realpath(__DIR__ . '/../../../bootstrap.php'));

ini_set('memory_limit', '1024M');
set_time_limit(0);
session_write_close();
while (ob_get_level())
{
	ob_end_clean();
}

$params = [];
$fields = [];
$sanitize = false;
$sort = false;
$page = false;
$per_page = false;
$onlycursor = true;

/** @var MongoCursor $cursor */
$cursor = MyPet\FriendsService\Friends\Model\FriendsModel::getAllAdvanced($params,
	$fields,
	$sanitize,
	$sort,
	$page,
	$per_page,
	$onlycursor);

while($data = $cursor->getNext())
{
	$id = $data['_id'];

	foreach($data['friends'] as $friendId=>$friendData)
	{
		$friend = new \MyPet\Friends\Model\FriendsModel(['id' => $id, 'friendId' => $friendId]);
		$friend2 = new \MyPet\Friends\Model\FriendsModel(['id' => $friendId, 'friendId' => $id]);

		$friend->id = $id;
		$friend->friendId = $friendId;
		$friend->status = ($friendData['status'] === 'waiting')? 3 : 1;
		$friend->type = $friendData['type'];

		$friend2->id = $friendId;
		$friend2->friendId = $id;
		$friend2->status = ($friendData['status'] === 'waiting')? 2 : 1;
		$friend2->type = $friendData['type'];

		$friend->save();
		$friend2->save();

		$friend->clearStaticCache();
		$friend2->clearStaticCache();
		usleep(1000);
	}
	usleep(10000);
}
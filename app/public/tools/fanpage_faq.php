<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<script src="js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript">
		$(function(){
			(function($) {

				var panels = $('.accord > dd').hide();

				$('.accord > dt > a').click(function() {
					panels.slideUp();
					$(this).parent().next().slideDown();
					return false;
				});

			})(jQuery);
		});
	</script>
	<style type="text/css">
		* {
			padding: 0px;
			margin: 0px;
		}
		body {
			font-family: Arial, sans-serif;
		}
		h1 {
			font-size: 25px;
			margin: 30px 0px 30px;
		}
		h2 {
			font-size: 13px;
			padding-top: 8px;
			padding-bottom: 8px;
			padding-left: 10px;
			line-height: 1.4em;
		}
		dd, dt {
			padding: 10px;
			border-top: 1px solid black;
			border-bottom: 0;
		}
		dd {
			border-top: 0;
			font-size: 12px;

		}
		.FAQintro {
			font-size: 12px;
			line-height: 1.5em;
			margin: 0em 0px 1em
		}
		.FAQp {
			font-size: 12px;
			line-height: 1.5em;
			width: 500px;
			margin-left: auto;
			margin-right: auto;
			margin-top: 1em
		}
		.firstp {
			margin-top: 0px
		}
		.ulFaq {
			width: 400px;
			margin-left: auto;
			margin-right: auto;
		}
		.ulFaq li {
			font-size: 12px;
			line-height: 1.5em;
			margin: 10px auto 10px
		}
		.accord {
			max-width: 650px;
			margin: 0px auto;
			padding-bottom: 50px;
			padding-top: 30px;
			background: url('FAQlogo.jpg') center top no-repeat;
		}
		.link {
			text-decoration: none;
			display: block;
			color: #000;
		}
		.link:hover {
			color: #007FFF;
		}
		.bottomPad {
			padding-bottom: 17px;
		}
		.borderBottom {
			height: 1px;
			max-width: 650px;
			background-color: black;
		}
		.wrap {
			height: 100%;
			width: 100%;
			padding-bottom: 140px;
			background: url('FAQfooter.jpg') center bottom no-repeat;
		}
	</style>
	<title></title>
</head>
<body>
<div class="wrap">
	<div class="accord">
		<img src="img/faq_top.png" alt=""/>
		<h1>1. Game rules</h1>

		<p class="FAQintro">Texas Hold 'Em is a very popular and dearly loved type of club poker around the world. It is normally played with a standard 52-card pack. The game is split into five stages, and the winner at the table is the player who manages to complete the highest-ranking combination of five cards by the end of the game. Please, see the complete set of rules for the Texas Hold 'Em below.</p>

		<dt><a href="#" class="link"><h2>Round one – preflop</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">The game starts from the dealer. You can always recognize the player who currently holds this position at the table by a D-marked chip; according to traditional poker rules, the dealer chip moves clockwise around the table after each hand has been played. It is called the Dealer Button. The two players to the left of the dealer make compulsory bets before any cards are dealt: the small and the big blinds.</p>
				<p class="FAQp">Each player then receives 2 cards that are not disclosed to other players (they are called the "hole cards"). Each player assess his/her hole cards and decides whether it is worth betting on them. The players who decided not to fold take turns in betting clockwise. The first player to bet is the one to the left of the big blind, and his/her bet cannot constitute less than that of the big blind's. And if any of the players decides to <b>raise</b> – to increase the bet – then this new higher value becomes the minimal bet for further trading. Now the players can either respond to the bet – make a <b>call</b> – or raise it further up once more (make a <b>re-raise</b>), as well as <b>fold</b> (decline further participation in the game). It is worth noting that you can have 1, 2 or even 3 betting rounds at the preflop stage: it depends on whether any raises were made in between the big blind's betting decisions. If there was no raising the bet, the big blind chooses between a <b>check</b> (skipping the turn), a raise, and a fold, and the game goes on to the next stage.
				</p>
			</div>
		</dd>

		<dt><a href="" class="link"><h2>Round two – flop:</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">The first three community cards, which can be equally used by all the players to form a hand, are dealt face-up on the table. The players make bets again, just as they did at the preflop stage, clockwise, starting from the player to the left of the dealer.</p>
			</div>
		</dd>

		<dt><a href="" class="link"><h2>Round three – turn:</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">The fourth community card is dealt face-up on the table. The players engage in betting again, in the same manner as during the flop.</p>
			</div>
		</dd>

		<dt><a href="" class="link"><h2>Round four – river:</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">The final fifth community card is dealt face-up on the table. The players enter their final betting round in the same manner as before.</p>
				<p class="FAQp">There is one particular rule in poker, amongst the others. The player can only put forward as much money for the game as he/she had originally set for the table (this amount is called the <b>stack</b>). Topping up one's stack is possible only in between the games. Withdrawing from the stack is only possible by leaving the table altogether. If the player has run out of funds during the game, he/she continues to take part without betting any further money – a side pot is formed at the table that is only contested by the remaining players that continue betting money.</p>
			</div>
		</dd>

		<dt><a href="" class="link"><h2>Round five – showdown:</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">Those players who have made it to the final round show their cards and determine the winner: it will be the player who collected the highest-ranking combination of cards at the table.</p>
			</div>
		</dd>

		<dt><a href="" class="link"><h2>Combinations/Hands</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">When learning to play poker, the rules come naturally very soon: just make sure you remember the hierarchy of card combinations, and don't forget that the highest-ranking hand is always the winner.</p>
				<p class="FAQp">Possible card combinations in a descending order are shown below:</p>
				<ol class="ulFaq">
					<li>Royal Flush ("highest of the 	same suit strictly ordered"): the 5 highest-ranking cards (Ace, King, Queen, Jack, Ten) of the same suit, for example: A♥ K♥ Q♥ J♥ 10♥.
					</li><li>Straight Flush ("same suit strictly ordered"): any 5 cards of the same suit ordered one by 	one in rank, for example: 9♠ 8♠ 7♠ 6♠ 5♠.
					</li><li>Four of a Kind, or Quads ("four of the same kind"): 4 cards of the same rank, for example: 3♥ 3♦ 3♣ 3♠ 10♥.
					</li><li>Full House, or Full Boat ("full 	house" says it all, really): 3 cards of the same rank + one pair, for example: 10♥ 10♦ 10♠ 8♣ 8♥.
					</li><li>Flush ("one flush", "one breed"): 5 cards of the same suit, for example: K♠ J♠ 8♠ 	4♠ 3♠.
					</li><li>Straight ("straight order"): 5 cards of any suit ordered one by one in rank, for example: 5♦ 4♥ 3♠ 2♦ A♦. The Ace may either open or close the combination. In this particular example the Ace (A♦) opens an order of 5 cards, 	therefore its rank is likened to 1, and 5♦ is considered the 	highest card.
					</li><li>Set, or Three of a Kind ("set of three"): 3 cards of the same rank, for example: 7♣ 7♥ 7♠ 	K♦ 2♠.
					</li><li>Two Pair ("two card pairs"): 2 pairs of same-ranking cards, for example: 8♣ 8♠ 4♥ 4♣ 2♠.
					</li><li>One Pair ("one pair of cards"): 2 cards of the same rank, for example: 9♥ 9♠ A♣ J♠ 4♥.
					</li><li>High Card, or Kicker: none of the combinations described above, for example (in this case the combination is called "High 	Ace"): A♦ 10♦ 9♠ 5♣ 4♣.
					</li></ol>
				<p class="FAQp">In the case of two essentially matching combinations the highest is deemed to be the one involving cards of a higher rank; for example, the "8♣ 8♠ 4♥ 4♣ 2♠" is higher than the "7♣ 7♠ 5♥ 5♣ K♠" (two 8s are "better" than two 7s). The "6♠ 5♦ 4♥ 3♠ 2♦" combination is higher than "5♦ 4♥ 3♠ 2♦ A♦" (here the rank of the Ace is likened to 1).</p>
				<p class="FAQp">When both the hands and the kicker cards are identical (with the kicker being among the 5 open cards), the pot is split between those players with matching hands. Only one card can serve as a kicker.</p>
				<p class="FAQp"><b>Lucky game!</b></p>
			</div>
		</dd>
		<div class="borderBottom"></div>

		<h1>2. Game currency</h1>

		<dt><a href="" class="link"><h2>Chips</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">Players play only for chips. Everyone can buy extra chips in the shopping menu, win prize chips playing Jackpot Slot, or collect free bonus chips (hourly bonus; chips for winning with a combination, level up, for participation in contests, following the wallpost or email link) or just winning them from other players at the poker table.</p>
			</div>
		</dd>
		<div class="borderBottom"></div>

		<h1>3. Daily leaderbords</h1>

		<dt><a href="" class="link"><h2>Daily leaderbords</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">There are 2 types of ratings in the game: hands won and biggest pot leaderboards. Ratings are updated every 24 hours. Prize pool is shared by first 3 places.</p>
			</div>
		</dd>
		<div class="borderBottom"></div>

		<h1>4. Playing with friends</h1>

		<dt><a href="" class="link"><h2>Playing with friends</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">You can play with your friends at the same table. Join or invite your friend with the help of “Friends” and “Invite” buttons. Make new friends in the game by clicking the button “Add Buddy” in the player's profile.</p>
			</div>
		</dd>
		<div class="borderBottom"></div>

		<h1>5. Types of bonuses</h1>

		<dt><a href="" class="link"><h2><b>5.1. Hourly bonus</b></h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">25 bonus chips appear every 10 minutes. The max. bonus is ready in 4 hours (600 chips). Collect it by entering the shopping menu.</p>
			</div>
		</dd>

		<dt><a href="" class="link"><h2><b>5.2. Daily bonus</b></h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">1 free spin appears every day in Jackpot slot.</p>
			</div>
		</dd>

		<dt><a href="" class="link"><h2><b>5.3. Inbox</b></h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">Find free bonuses in your Inbox for:</p>
				<ol class="ulFaq">
					<li>winning with a combination
					</li><li>for big win
					</li><li>for level-up
					</li><li>for following the gift-link:<br> - fan page link <br> - email link
					</li><li>your friend installed the game (bonus chips are accrued only if you send a request and a friend accepts you invite)
					</li>
				</ol>
			</div>
		</dd>
		<div class="borderBottom"></div>

		<h1>6. Gifts from friends</h1>

		<dt><a href="" class="link"><h2>Gifts from friends</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">Players can exchange free spins with each other. The more friends you have – the more gifts you can send and get back (1 friends – 1 gift per day). Use free spins in Jackpot Slot to win extra chips for the game.</p>
			</div>
		</dd>
		<div class="borderBottom"></div>

		<h1>7. Sit'n'Go tournament</h1>

		<dt><a href="" class="link"><h2>Sit'n'Go tournament</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">Registration is open all day. The tournament begins when 9 players are at the poker table. All players are required to pay the entry payment with an additional fee. The prize pool is divided between first 3 places. Blinds grow every 3 minutes.</p>
			</div>
		</dd>
		<div class="borderBottom"></div>

		<h1>8. Jackpot slot</h1>

		<dt><a href="" class="link"><h2>Jackpot slot</h2></a>
		</dt><dd style="display: none;">
			<div class="bottomPad">
				<p class="FAQp firstp">There are 2 types of spins in Jackpot Slot: free and paid. Get free spins as a daily bonus or as a gift from your friends. Paid spins can be bought in Jackpot Slot. Payouts are higher for such spins.
				</p>
			</div>
		</dd>
		<div class="borderBottom"></div>


	</div>
</div>
</body>
</html>
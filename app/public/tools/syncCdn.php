<?php
if (function_exists('ssh2_connect'))
{
	$host = 'cdn-main.fishsticksgames.com';
	$port = 22;
	$connection = ssh2_connect($host, $port, array('hostkey'=>'ssh-rsa'));
	if (ssh2_auth_pubkey_file($connection, 'deploy',
		'/home/gollariel/.ssh/idrsa1.pub',
		'/home/gollariel/.ssh/idrsa1', 'secret')) {
		echo "Public Key Authentication Successful\n";
	} else {
		die('Public Key Authentication Failed');
	}

	$stream = ssh2_exec($connection, 'sudo /usr/bin/cdn_sync.sh');

	$errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);

// Enable blocking for both streams
	stream_set_blocking($errorStream, true);
	stream_set_blocking($stream, true);

// Whichever of the two below commands is listed first will receive its appropriate output.  The second command receives nothing
	echo "Output: " . stream_get_contents($stream);
	echo "Error: " . stream_get_contents($errorStream);

// Close the streams
	fclose($errorStream);
	fclose($stream);
}
else
{
	print 'Not found ssl2';
}
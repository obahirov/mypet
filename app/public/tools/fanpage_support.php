<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
 <title>TX Poker</title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<h3>Support </h3>
<br />
In order to contact TX Poker Support follow the steps below:<br />
<br />
<ol>
<li>Go to TX Poker fan-page by <a href='//www.facebook.com/pages/TX-Poker/627412183952051' target="_blank">clicking here</a> </li>
<li>Send a private message by clicking the <b>Message</b> button in the upper-right corner of the Page.</li>
<img src='https://cdn5.fishsticksgames.com/poker-fb/images/support/support_fanpage1.png' />
<li>Describe your issue in details and send us a message</li>
<li>A member of our support team will be glad to help you within 12 business hours.</li>
</ol>
<br />
You can also contact TX Poker Support by sending an e-mail to <a href='mailto:txpokersupport@murka.com'>txpokersupport@murka.com</a> or by clicking the <b>"Support"</b> tab while playing the game.<br />
</body>
</html>
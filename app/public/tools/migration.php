<?php
use KMCore\DB\ObjectModel;

require(realpath(__DIR__ . '/../../bootstrap.php'));
/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PagesOld extends ObjectModel
{
	/** @var string Метод обновления записи - full, inc, set, cacheOnly */
	//	protected static $updateMethod = self::UPDATE_METHOD_CACHE_ONLY;
	/** @var string $collection Название модели */
	protected static $collection = 'cms_pages_model';
	protected static $readPreference = \MongoClient::RP_SECONDARY_PREFERRED;
	/** @var string $pk первичный ключ */
	protected static $pk = '_id';
	/** @var array $sort параметры сортировки */
	protected static $sort = ['name' => 1];
	/** @var array $indexes индексы коллекции */
	protected static $indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true
		],
		[
			'keys'   => ['lang' => 1, 'name' => 1],
			'unique' => true
		],
		[
			'keys' => ['tags' => 1]
		]
	];

	/** @var array $fieldsDefault список значений коллекции */
	protected static $fieldsDefault = [
		'_id'        => '',
		'langs'       => '',
		'name'       => '',
		'template'   => '',
		'components' => [],
		'data'       => [],
	];
	/** @var array $fieldsValidate типы значений для валидации */
	protected static $fieldsValidate = [
		'_id'        => self::TYPE_MONGO_ID,
		'lang'       => self::TYPE_STRING,
		'name'       => self::TYPE_STRING,
		'template'   => self::TYPE_STRING,
		'components' => self::TYPE_JSON,
		'data'       => self::TYPE_JSON,
	];

	protected static $fieldsHint = [
		'_id' => 'Идентификатор БД',
	];
}
class ComponentsOld extends ObjectModel
{
	/** @var string Метод обновления записи - full, inc, set, cacheOnly */
	//	protected static $updateMethod = self::UPDATE_METHOD_CACHE_ONLY;
	/** @var string $collection Название модели */
	protected static $collection = 'cms_components_model';
	protected static $readPreference = \MongoClient::RP_SECONDARY_PREFERRED;
	/** @var string $pk первичный ключ */
	protected static $pk = '_id';
	/** @var array $sort параметры сортировки */
	protected static $sort = ['priority' => 1];
	/** @var array $indexes индексы коллекции */
	protected static $indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true
		],
		[
			'keys'   => ['lang' => 1, 'name' => 1],
			'unique' => true
		],
		[
			'keys' => ['position' => 1]
		],
		[
			'keys' => ['tags' => 1]
		]
	];

	/** @var array $fieldsDefault список значений коллекции */
	protected static $fieldsDefault = [
		'_id'      => '',
		'lang'     => '',
		'name'     => '',
		'tags'     => [],
		'template' => '',
		'position' => '',
		'data'     => [],
		'methods'  => [],
		'priority' => 0
	];
	/** @var array $fieldsValidate типы значений для валидации */
	protected static $fieldsValidate = [
		'_id'      => self::TYPE_MONGO_ID,
		'lang'     => self::TYPE_STRING,
		'name'     => self::TYPE_STRING,
		'tags'     => self::TYPE_JSON,
		'template' => self::TYPE_STRING,
		'position' => self::TYPE_STRING,
		'data'     => self::TYPE_JSON,
		'methods'  => self::TYPE_JSON,
		'priority' => self::TYPE_INT,
	];

	protected static $fieldsHint = [
		'_id' => 'Идентификатор БД',
	];
}
//\KMCore\CMS\Model\Pages::drop();
//\KMCore\CMS\Model\Components::drop();
//exit;
/** @var MongoCursor $cursor */
$cursor = PagesOld::getAllAdvanced(['lang' => 'ua'], [], false, true, false, false, true);
var_dump(\KMCore\CMS\Model\Pages::getExistingIndexes());
while($cursor->hasNext())
{
	$data = $cursor->getNext();
	$page = new \KMCore\CMS\Model\Pages();
	$page->langs = ['ua', 'en', 'ru'];
	$page->tags = [$data['name']];
	$page->template = $data['template'];
	$page->components = $data['components'];
	$page->data = $data['data'];
	$page->save();
}
/** @var MongoCursor $cursor */
$cursor = ComponentsOld::getAllAdvanced([], [], false, true, false, false, true);
while($cursor->hasNext())
{
	$data = $cursor->getNext();
	$comp = new \KMCore\CMS\Model\Components();
	$comp->langs = [$data['lang']];
	$comp->name = $data['name'];
	$comp->tags = [];
	$comp->template = $data['template'];
	$comp->position = $data['position'];
	$comp->data = $data['data'];
	$comp->methods = $data['methods'];
	$comp->priority = $data['priority'];
	$comp->save();
}
<?php
set_time_limit(0);
require(realpath(__DIR__ . '/../../bootstrap.php'));
use MyPet\EventService\EventPubService;
use MyPet\UserService\Users\Classes\UserManager;

$userId = strip_tags(trim($_GET['userId']));
if(!empty($userId))
{
	EventPubService::getInstance($userId)->sendEventPub(EventPubService::TYPE_PLACE_SNG, array('place' => 1), 1);
}

print true;

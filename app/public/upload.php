<?php
use KMCore\ServerContainer;
use MyPet\Users\Classes\AuthManager;
require(realpath(__DIR__ . '/../bootstrap.php'));
/** @var ServerContainer $GLOBALS['serverContainer'] */
$GLOBALS['serverContainer']->prepareScript();
$config = $GLOBALS['serverContainer']->getConfig();
$config->loadDb();

if (isset($_FILES['file']['tmp_name']) && !is_array($_FILES['file']['tmp_name'])) {
    $error = $_FILES['file']['error'];
    if ($error > 0)
    {
        echo 'ERROR Return Code: '. $error;
    }
    else
    {
        $allowedExts = array("gif" => 1, "jpeg" => 1, "jpg" => 1, "png" => 1, "GIF" => 1, "JPEG" => 1, "JPG" => 1, "PNG" => 1);
        $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
        $userId = AuthManager::getSessionUserId();
        $temp = explode('.', $_FILES[ 'file' ][ 'name' ]);
        $extension = end($temp);
        if(isset($allowedExts[$extension]) && file_exists($tempPath))
        {
            $fileName = 'post_'.time().mt_rand(0,1000).'.'.$extension;
            $uploadPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $userId;

            if(!file_exists($uploadPath))
            {
                mkdir($uploadPath);
            }
            $uploadPath .= DIRECTORY_SEPARATOR . $fileName;

            move_uploaded_file( $tempPath, $uploadPath );
            $answer = array( 'name' => $fileName, 'url' => $config->get('cdnUrl').'/files/'.$userId.'/'.$fileName );
            $json = json_encode( $answer );
            echo $json;
        }
        else {
            echo 'Error file type';
        }
    }
} else {
    echo 'No files';
}
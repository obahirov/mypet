<?php
/**
 * Точка входа в приложени. Загрузку базового загрузчика и вызов необходимого контроллера.
 * Poker powered by KMCore
 *
 * @author    Maxim Tkach <gollariel@gmail.com>
 */
use KMCore\KMCoreContainer;
use KMCore\Routing\Route;
use KMCore\Routing\Router;
use KMCore\ServerContainer;

require(realpath(__DIR__ . '/../bootstrap.php'));

/**
 * Ищем параметры роутинга
 */
$route = (string)isset($_REQUEST['route'])?$_REQUEST['route']:'';

/** @var KMCoreContainer $server */
$server = $GLOBALS['serverContainer'];
$config = $server->getConfig();

/**
 * Удаляем из запроса параметр route
 * Получить параметр остаеться возможным через $_REQUEST
 */
unset($_GET['route']);
unset($_POST['route']);

$router = new Router();
$router->setDefaultIdent($config->get('defaultIdent'));
$router->setDefaultMethod($config->get('defaultMethod'));
$router->setRootControllerNamespaces([
									 'MyPet',
									 'KMCore',
									 ]);
$router->setUrlMap($config->get('urlMap', null, $config::TYPE_MAIN, []));
$router->setAvailableLanguage($config->get('list', 'lang'));
$route = $router->parseRoute($route, true);

if($route->getLangName())
{
	$server->getContainer('sessionStorage')->set('langName', $route->getLangName());
}

$className = $route->getClassName();
$methodName = $route->getMethodName();

$controller = new $className();
$controller->$methodName();
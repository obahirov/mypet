<?php
namespace MyPet\Pets\Controller;

use KMCore\Helper\GeneratorHelper;
use MyPet\Inbox\Classes\RequestData;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\Pets\Classes\PetException;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\GagModel;
use MyPet\SiteController;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AncestryController extends SiteController
{
	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function addRelation()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$manager = new PetManager($this->server);
		$userId = AuthManager::getSessionUserId();
		$ancestryId = $this->request->getGetOrPost('ancestryId', '');

		$petId = $this->request->getGetOrPost('petId', '');
		$petRelationId = $this->request->getGetOrPost('petRelationId', '');

		if ($petId === $petRelationId || $ancestryId === $petRelationId)
		{
			throw new PetException('Not correct ancestry');
		}

		$type = (string)$this->request->getGetOrPost('type', ''); // cat OR dog
		$parent = $this->request->getGetOrPost('parent', ''); // parent OR child
		$gender = (int)$this->request->getGetOrPost('gender', 0); // 1 OR 2

		if (!empty($type) && !empty($petId) && !empty($petRelationId))
		{
			$userObj = UserManager::getUserModelByUserId($userId);

			$pet = $manager->getPetOrGag($petId);
			if (!$pet->isLoadedObject())
			{
				throw new PetException('Not correct pet');
			}

			$petRelation = $manager->getPetOrGag($petRelationId);

			if ((string)$pet->type !== $petRelation->type)
			{
				throw new PetException('Not correct type');
			}

			if ($gender === 0)
			{
				$gender = (int)$pet->gender;
			}
			elseif ((int)$petRelation->gender !== $gender)
			{
				throw new PetException('Not correct gender: '.$petRelation->gender.' !== '.$gender);
			}

			if ($manager->isOwner($pet, $userId) && $manager->isOwner($petRelation, $userId))
			{
				$manager = new PetManager($this->server);
				$manager->addLink($pet, $petRelation, $parent, $gender);

				$petRelation->save();
				$pet->save();

				$id = 'success_save';
			}
			else
			{
				$ownerId = reset($petRelation->ownerIds);
				$attributes = [
					'userId'        => $userId,
					'petId'         => $petId,
					'petRelationId' => $petRelationId,
					'parent'        => $parent,
					'gender'        => $gender,
					'payload'       => ['user'        => $userObj->getFieldsSanitized(),
					                    'pet'         => $pet->getFieldsSanitized(),
					                    'petRelation' => $petRelation->getFieldsSanitized()
					],
				];
				RequestData::init(new RequestData($ownerId))->setType(
					RequestManager::TYPE_ANCESTRY_ADD_RELATION_CONFIRM
				)->setAttributes($attributes)->setUnique($petId.'|'.$petRelationId)->setPub(true)->update();
				$id = 'send_ancestry_pet_request';
			}
		}
		else
		{
			throw new PetException('Not found type');
		}

		$this->responseByForm($id, '/ancestry/'.$ancestryId);
	}

	public function remRelation()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$manager = new PetManager($this->server);
		$userId = AuthManager::getSessionUserId();
		$ancestryId = $this->request->getGetOrPost('ancestryId', '');

		$petId = $this->request->getGetOrPost('petId', '');
		$petRelationId = $this->request->getGetOrPost('petRelationId', '');

		$type = $this->request->getGetOrPost('type', ''); // cat OR dog
		$parent = $this->request->getGetOrPost('parent', ''); // parent OR child

		if (!empty($type) && !empty($petId) && !empty($petRelationId))
		{
			$userObj = UserManager::getUserModelByUserId($userId);

			$pet = $manager->getPetOrGag($petId);
			if (!$pet->isLoadedObject())
			{
				throw new PetException('Not correct pet');
			}
			$petRelation = $manager->getPetOrGag($petRelationId);
			if (!$petRelation->isLoadedObject())
			{
				throw new PetException('Not correct pet');
			}

			if ($pet->type !== $petRelation->type)
			{
				throw new PetException('Not correct type: '.$pet->type.' !== '.$petRelation->type);
			}
			//			$gender = $petRelation->gender;

			$gender = (int)$pet->gender;

			if ($manager->isOwner($pet, $userId) || $manager->isOwner($petRelation, $userId))
			{
				$manager = new PetManager($this->server);
				$manager->remLink($pet, $petRelation, $parent, $gender);

				$petRelation->save();
				$pet->save();
			}
			else
			{
				$ownerId = reset($petRelation->ownerIds);
				$attributes = [
					'userId'        => $userId,
					'petId'         => $petId,
					'petRelationId' => $petRelationId,
					'parent'        => $parent,
					'gender'        => $gender,
					'payload'       => ['user'        => $userObj->getFieldsSanitized(),
					                    'pet'         => $pet->getFieldsSanitized(),
					                    'petRelation' => $petRelation->getFieldsSanitized()
					],
				];
				RequestData::init(new RequestData($ownerId))->setType(
					RequestManager::TYPE_ANCESTRY_REM_RELATION_CONFIRM
				)->setAttributes($attributes)->setPub(true)->send();
			}
		}
		else
		{
			throw new PetException('Not found type');
		}

		$this->responseByForm('success_save', '/ancestry/'.$ancestryId);
	}

	public function addGag()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$manager = new PetManager($this->server);
		$userId = AuthManager::getSessionUserId();
		$ancestryId = $this->request->getGetOrPost('ancestryId', '');

		$petId = $this->request->getGetOrPost('petId', '');

		$type = $this->request->getGetOrPost('type', ''); // cat OR dog
		$parent = $this->request->getGetOrPost('parent', ''); // parent OR child
		$gender = (int)$this->request->getGetOrPost('gender', 0); // 1 OR 2

		if (!empty($type) && !empty($petId))
		{
			$pet = $manager->getPetOrGag($petId);
			if (!$pet->isLoadedObject())
			{
				throw new PetException('Not correct pet');
			}

			if (!$gender)
			{
				$gender = (int)$pet->gender;
			}

			if ($manager->isOwner($pet, $userId))
			{
				$hNick = $this->request->getGetOrPost('hNick');
				$birthday = $this->request->getGetOrPost('birthday');
				$deathday = $this->request->getGetOrPost('deathday');
				$signs = (array)explode(', ', $this->request->getGetOrPost('signs'));

				$gagModel = new GagModel();
				$gagModel->id = GeneratorHelper::getStringUniqueId();
				$gagModel->hNick = $hNick;
				$gagModel->type = $pet->type;
				$gagModel->breed = $pet->breed;
				$gagModel->gender = $gender;
				$gagModel->ownerIds[] = $userId;
				$gagModel->signs = $signs;

				$gagModel->params = ['owner'      => $this->request->getGetOrPost('owner', '', true),
				                     'ringleader' => $this->request->getGetOrPost('ringleader', '', true)
				];

				$dataArray = explode('/', $birthday);
				$birthD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
				$birthM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
				$birthY = (string)isset($dataArray[2]) ? $dataArray[2] : '';
				if (checkdate((int)$birthM, (int)$birthD, (int)$birthY))
				{
					$gagModel->birth_d = $birthD;
					$gagModel->birth_m = $birthM;
					$gagModel->birth_y = $birthY;
				}

				$dataArray = explode('/', $deathday);
				$deathD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
				$deathM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
				$deathY = (string)isset($dataArray[2]) ? $dataArray[2] : '';
				if (checkdate((int)$deathM, (int)$deathD, (int)$deathY))
				{
					$gagModel->death_d = $deathD;
					$gagModel->death_m = $deathM;
					$gagModel->death_y = $deathY;
				}
				$manager = new PetManager($this->server);
				$manager->addLink($pet, $gagModel, $parent, $gender);

				$gagModel->save();
				$pet->save();
			}
			else
			{
				throw new PetException('Not have permitted');
			}
		}
		else
		{
			throw new PetException('Not correct type');
		}
		$this->responseByForm('success_save', '/ancestry/'.$ancestryId);
	}

	public function remGag()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$manager = new PetManager($this->server);
		$userId = AuthManager::getSessionUserId();
		$gagId = $this->request->getGetOrPost('gagId', '');

		$gag = new GagModel($gagId);
		if (!$gag->isLoadedObject())
		{
			throw new PetException('Not found pet');
		}

		if ($manager->isOwner($gag, $userId))
		{
			$gag->delete();
		}
		$this->responseByForm('success_save', '/');
	}

	public function editGag()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$manager = new PetManager($this->server);
		$userId = AuthManager::getSessionUserId();
		$gagId = $this->request->getGetOrPost('gagId', '');

		$gag = new GagModel($gagId);
		if (!$gag->isLoadedObject())
		{
			throw new \Exception('Not found pet');
		}

		if ($manager->isOwner($gag, $userId))
		{
			$hNick = $this->request->getGetOrPost('hNick');
			$birthday = $this->request->getGetOrPost('birthday');
			$deathday = $this->request->getGetOrPost('deathday');
			$signs = (array)explode(', ', $this->request->getGetOrPost('signs'));
			$gender = $this->request->getGetOrPost('gender', $gag->gender);

			$gag->hNick = $hNick;
			$gag->ownerIds[] = $userId;
			$gag->signs = $signs;
			$gag->gender = $gender;

			$gag->params = ['owner'      => $this->request->getGetOrPost('owner', '', true),
			                'ringleader' => $this->request->getGetOrPost('ringleader', '', true)
			];

			$dataArray = explode('/', $birthday);
			$birthD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
			$birthM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
			$birthY = (string)isset($dataArray[2]) ? $dataArray[2] : '';
			if (checkdate((int)$birthM, (int)$birthD, (int)$birthY))
			{
				$gag->birth_d = $birthD;
				$gag->birth_m = $birthM;
				$gag->birth_y = $birthY;
			}

			$dataArray = explode('/', $deathday);
			$deathD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
			$deathM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
			$deathY = (string)isset($dataArray[2]) ? $dataArray[2] : '';
			if (checkdate((int)$deathM, (int)$deathD, (int)$deathY))
			{
				$gag->death_d = $deathD;
				$gag->death_m = $deathM;
				$gag->death_y = $deathY;
			}
			$gag->save();
		}
		$this->responseByForm('success_save', '/');
	}
} 
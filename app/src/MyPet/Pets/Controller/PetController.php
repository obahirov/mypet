<?php
namespace MyPet\Pets\Controller;

use KMCore\Helper\GeneratorHelper;
use KMCore\Validation\Validate;
use MyPet\Pets\Classes\PetException;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\BreedModel;
use MyPet\Pets\Model\PetModel;
use MyPet\SiteController;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;
use MyPet\Users\Model\ProfessionModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PetController extends SiteController
{
	const EVENT_CONTROLLER_PET_CREATE = 'event.controller.pet.create';
	const EVENT_CONTROLLER_PET_UPDATE = 'event.controller.pet.update';
	const EVENT_CONTROLLER_PET_REMOVE = 'event.controller.pet.remove';

	public function __construct()
	{
		parent::__construct();
		$this->server->on(
			self::EVENT_CONTROLLER_PET_CREATE,
			function (PetModel $pet)
			{
				PetManager::addedToSearch($pet);
			}
		);

		$this->server->on(
			self::EVENT_CONTROLLER_PET_UPDATE,
			function (PetModel $pet)
			{
				PetManager::updateToSearch($pet);
			}
		);

		$this->server->on(
			self::EVENT_CONTROLLER_PET_UPDATE,
			function (PetModel $pet)
			{
				PetManager::removeToSearch($pet);
			}
		);
	}

	public function changeStatus()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$userId = AuthManager::getSessionUserId();
		$petId = $this->request->getGetOrPost('petId');
		if (empty($petId))
		{
			throw new PetException('Not found petId');
		}

		$pet = new PetModel($petId);
		if (!$pet->isLoadedObject())
		{
			throw new PetException('Not correct pet');
		}
		$manager = new PetManager($this->server);
		if ($manager->isOwner($pet, $userId))
		{
			$status = $this->server->getContainer('request')->getGetOrPost('status');
			$pet->status = $status;
			$pet->save();
			$this->server->emit(self::EVENT_CONTROLLER_PET_UPDATE, [$pet]);
		}
		$this->responseByForm('success_update', '/pet/'.$pet->id);
	}

	public function addNewPet()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$userId = AuthManager::getSessionUserId();
		$pet = new PetModel();

		$pet->id = GeneratorHelper::getStringUniqueId();
		$pet->ownerIds[] = $userId;
		$pet->hNick = $this->request->getGetOrPost('hNick');
		$pet->type = $this->request->getGetOrPost('type');
		$pet->gender = (int)$this->request->getGetOrPost('gender');
		$pet->breed = $this->request->getGetOrPost('breed');

		$birthday = $this->request->getGetOrPost('birthday');
		$dataArray = explode('/', $birthday);
		$birthD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
		$birthM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
		$birthY = (string)isset($dataArray[2]) ? $dataArray[2] : '';
		if (checkdate((int)$birthM, (int)$birthD, (int)$birthY))
		{
			$pet->birth_y = $birthY;
			$pet->birth_m = $birthM;
			$pet->birth_d = $birthD;
		}

		$pet->save();
		$pet->clearStaticCache();
		$this->server->emit(self::EVENT_CONTROLLER_PET_CREATE, [$pet]);

		$this->responseByForm('success_update', '/pet/'.$pet->id);
	}

	public function removePet()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$userId = AuthManager::getSessionUserId();
		$petId = $this->request->getGetOrPost('petId');

		$pet = new PetModel($petId);
		if (!$pet->isLoadedObject())
		{
			throw new PetException('Not correct pet');
		}

		$manager = new PetManager($this->server);
		if ($manager->isOwner($pet, $userId))
		{
			$manager->removeAllLink($pet);
			$this->server->emit(self::EVENT_CONTROLLER_PET_REMOVE, [$pet]);
			$pet->delete();
		}
	}

	public function addNewOwner()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$userId = AuthManager::getSessionUserId();
		$petId = $this->request->getGetOrPost('petId');
		if (empty($petId))
		{
			throw new PetException('Not found petId');
		}

		$pet = new PetModel($petId);
		if (!$pet->isLoadedObject())
		{
			throw new PetException('Not correct pet');
		}
		$manager = new PetManager($this->server);
		if ($manager->isOwner($pet, $userId))
		{
			$ownerId = $this->request->getGetOrPost('ownerId');
			$pet->ownerIds[] = $ownerId;
			$pet->save();
		}
		$this->responseByForm('success_update', '/pet/'.$pet->id);
	}

	public function inviteOwner()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$userId = AuthManager::getSessionUserId();
		$email = $this->request->getGetOrPost('email');
		$petId = $this->request->getGetOrPost('petId');
		if (empty($petId))
		{
			throw new PetException('Not found petId');
		}
		$pet = new PetModel($petId);
		if (!$pet->isLoadedObject())
		{
			throw new PetException('Not correct pet');
		}
		$manager = new PetManager($this->server);
		if ($manager->isOwner($pet, $userId) && Validate::isEmail($email))
		{
			$user = UserManager::getUserByEmail($email);
			if ($user->isLoadedObject())
			{
				$ownerId = $user->id;
				$pet->ownerIds[] = $ownerId;
				$pet->save();
			}
			else
			{

			}
		}
		$this->responseByForm('success_update', '/pet/'.$pet->id);
	}

	public function editPetData()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$userId = AuthManager::getSessionUserId();
		$id = $this->request->getGetOrPost('id');

		$pet = new PetModel($id);
		if (!$pet->isLoadedObject())
		{
			throw new PetException('Not correct pet');
		}
		$manager = new PetManager($this->server);
		if ($pet->isLoadedObject() && $manager->isOwner($pet, $userId))
		{
			$hNick = $this->request->getGetOrPost('hNick');
			$type = $this->request->getGetOrPost('type');
			$breed = $this->request->getGetOrPost('breed');
			$birthday = $this->request->getGetOrPost('birthday');
			$deathday = $this->request->getGetOrPost('deathday');
			$country = $this->request->getGetOrPost('country');
			$region = $this->request->getGetOrPost('region');
			$city = $this->request->getGetOrPost('city');
			$signs = (array)explode(', ', $this->request->getGetOrPost('signs'));
			$marks = $this->request->getGetOrPost('marks');

			$nick = $this->request->getGetOrPost('nick');
			$ancestry_code = $this->request->getGetOrPost('ancestry_code');
			$chip = $this->request->getGetOrPost('chip');
			$passport = $this->request->getGetOrPost('passport');
			$color = $this->request->getGetOrPost('color');
			$hair = $this->request->getGetOrPost('hair');
			$size = $this->request->getGetOrPost('size');
			$weight = $this->request->getGetOrPost('weight');
			$about = $this->request->getGetOrPost('about');
			$gender = (int)$this->request->getGetOrPost('gender');

			$owners = (array)$this->request->getGetOrPost('owners');
			$ringleader = $this->request->getGetOrPost('ringleader');

			$specialists = [];
			$realSpecialists = [];
			$professions = ProfessionModel::getAllAdvanced([], [], true, ['priority' => 1]);
			foreach ($professions as $item)
			{
				$id = $item['_id'];
				$specialists[$id] = (array)$this->request->getGetOrPost($id, []);
				$realSpecialists = array_merge($realSpecialists, $specialists[$id]);
			}

			$pet->hNick = $hNick;
			$pet->type = $type;
			$pet->breed = $breed;
			$pet->gender = $gender;

			if (empty($birthday))
			{
				$pet->birth_d = '';
				$pet->birth_m = '';
				$pet->birth_y = '';
			}
			else
			{
				$dataArray = explode('/', $birthday);
				$birthD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
				$birthM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
				$birthY = (string)isset($dataArray[2]) ? $dataArray[2] : '';
				if (checkdate((int)$birthM, (int)$birthD, (int)$birthY))
				{
					$pet->birth_d = $birthD;
					$pet->birth_m = $birthM;
					$pet->birth_y = $birthY;
				}
			}

			if (empty($deathday))
			{
				$pet->death_d = '';
				$pet->death_m = '';
				$pet->death_y = '';
			}
			else
			{
				$dataArray = explode('/', $deathday);
				$deathD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
				$deathM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
				$deathY = (string)isset($dataArray[2]) ? $dataArray[2] : '';
				if (checkdate((int)$deathM, (int)$deathD, (int)$deathY))
				{
					$pet->death_d = $deathD;
					$pet->death_m = $deathM;
					$pet->death_y = $deathY;
				}
			}

			$pet->country = $country;
			$pet->region = $region;
			$pet->city = $city;
			$pet->signs = $signs;
			$pet->marks = $marks;

			$pet->nick = $nick;
			$pet->ancestry_code = $ancestry_code;
			$pet->chip = $chip;
			$pet->passport = $passport;
			$pet->color = $color;
			$pet->hair = $hair;
			$pet->size = $size;
			$pet->weight = $weight;
			$pet->about = $about;

			$double = [];
			$owners[] = $userId;
			foreach ($owners as $i => $item)
			{
				if (empty($item))
				{
					unset($owners[$i]);
				}

				if (isset($double[$item]))
				{
					unset($owners[$i]);
				}
				else
				{
					$double[$item] = 1;
				}
			}

			if (empty($owners))
			{
				$owners[] = $userId;
			}
			$pet->ownerIds = $owners;
			$pet->ringleader = $ringleader;

			foreach ($specialists as $type => &$users)
			{
				if (empty($users))
				{
					unset($specialists[$type]);
				}
				else
				{
					$double = [];
					foreach ($users as $i => $userId)
					{
						if (empty($userId))
						{
							unset($users[$i]);
						}

						if (isset($double[$userId]))
						{
							unset($users[$i]);
						}
						else
						{
							$double[$userId] = 1;
						}
					}
				}
			}

			$pet->specialists = $specialists;
			$pet->realSpecialists = $realSpecialists;
			$pet->save();

			$this->server->emit(self::EVENT_CONTROLLER_PET_UPDATE, [$pet]);
		}
		$this->responseByForm('success_save', '/pet/'.$pet->id);
	}

	public function getBreedsByType()
	{
		$type = $this->request->getGetOrPost('type');

		$breeds = BreedModel::getAllAdvanced(['type' => $type], [], true, ['priority' => 1]);
		$breeds = array_map(
			function ($breed)
			{
				$breed['_id'] = (string)$breed['_id'];

				return $breed;
			},
			$breeds
		);
		$this->response->json(['breeds' => $breeds]);
	}

	public function getPets()
	{
		$petIds = (array)$this->request->getGetOrPost('petIds', []);
		$petIds = array_slice($petIds, 0, 100);

		$pets = PetModel::getAllAdvanced(['id' => ['$in' => $petIds]], [], true);
		$this->response->json(['pets' => $pets]);
	}
} 
<?php
namespace MyPet\Pets\Classes;

use KMCore\DB\Elasticsearch\ElasticsearchManager;
use KMCore\Helper\ArrayDataHelper;
use KMCore\Helper\SessionStorage;
use KMCore\LogSystem\FastLog;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Pets\Model\GagModel;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Model\ProfessionModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PetManager
{
	protected $server;

	/**
	 * @param ServerContainer $server
	 */
	public function __construct(ServerContainer $server)
	{
		$this->server = $server;
	}

	/**
	 * @param PetModel $pet
	 * @param          $ownerId
	 * @return bool
	 */
	public function isOwner(PetModel $pet, $ownerId)
	{
		return (array_search($ownerId, $pet->ownerIds) !== false);
	}

	/**
	 * @param $id
	 * @return GagModel|PetModel
	 * @throws PetException
	 */
	public function getPetOrGag($id)
	{
		$pet = new PetModel($id);
		if (!$pet->isLoadedObject())
		{
			$pet = new GagModel($id);
			if (!$pet->isLoadedObject())
			{
				throw new PetException('Not found pet');
			}
		}

		return $pet;
	}

	/**
	 * @param \MyPet\Pets\Model\PetModel $pet
	 * @return array
	 */
	public function getChildsPet(PetModel $pet)
	{
		return PetModel::getAllAdvanced(['$or' => [['fatherId' => $pet->id], ['motherId' => $pet->id]]], ['id' => 1]);
	}

	/**
	 * @param \MyPet\Pets\Model\PetModel $pet
	 * @return array
	 */
	public function getBrothersPet(PetModel $pet)
	{
		$brothers = [];
		if (!empty($pet->fatherId) && !empty($pet->motherId))
		{
			$brothers = PetModel::getAllAdvanced(
				['$or' => [['fatherId' => $pet->fatherId], ['motherId' => $pet->motherId]]],
				['id' => 1]
			);
		}
		elseif (!empty($pet->fatherId))
		{
			$brothers = PetModel::getAllAdvanced(['fatherId' => $pet->fatherId], ['id' => 1]);
		}
		elseif (!empty($pet->motherId))
		{
			$brothers = PetModel::getAllAdvanced(['motherId' => $pet->motherId], ['id' => 1]);
		}
		foreach ($brothers as $k => $item)
		{
			if ($item['id'] === $pet->id)
			{
				unset($brothers[$k]);
			}
		}

		return $brothers;
	}

	/**
	 * @param PetModel $pet
	 * @internal param bool $includeGags
	 * @return array
	 */
	public function getParentsPet(PetModel $pet)
	{
		if (!empty($pet->fatherId) && !empty($pet->motherId))
		{
			return PetModel::getAllAdvanced(
				['$or' => [['id' => $pet->fatherId], ['id' => $pet->motherId]]],
				['id' => 1]
			);
		}
		elseif (!empty($pet->fatherId))
		{
			return PetModel::getAllAdvanced(['id' => $pet->fatherId], ['id' => 1]);
		}
		elseif (!empty($pet->motherId))
		{
			return PetModel::getAllAdvanced(['id' => $pet->motherId], ['id' => 1]);
		}

		return [];
	}

	/**
	 * @param \MyPet\Pets\Model\PetModel $pet
	 * @return array
	 */
	public function getPartnersPet(PetModel $pet)
	{
		$partners = [];
		if (!empty($pet->partners))
		{
			$partners = PetModel::getAllAdvanced(['id' => ['$in' => $pet->partners]], []);
		}

		return $partners; // Partners
	}

	/**
	 * @param \MyPet\Pets\Model\PetModel $pet
	 * @param bool                       $includeGags
	 * @return array
	 */
	public function getChildsPetData(PetModel $pet, $includeGags = false)
	{
		$childs = PetModel::getAllAdvanced(['$or' => [['fatherId' => $pet->id], ['motherId' => $pet->id]]], []);
		$childs = ArrayDataHelper::arrayByOneKey($childs, 'id');

		if ($includeGags)
		{
			$gagChilds = GagModel::getAllAdvanced(['$or' => [['fatherId' => $pet->id], ['motherId' => $pet->id]]], []);
			$gagChilds = ArrayDataHelper::arrayByOneKey($gagChilds, 'id');
			$childs = array_merge($childs, $gagChilds);
		}

		return $childs; // Child
	}

	/**
	 * @param \MyPet\Pets\Model\PetModel $pet
	 * @param bool                       $includeGags
	 * @return array
	 */
	public function getBrothersPetData(PetModel $pet, $includeGags = false)
	{
		$brothers = [];
		if (!empty($pet->fatherId) && !empty($pet->motherId))
		{
			$brothers = PetModel::getAllAdvanced(
				['$or' => [['fatherId' => $pet->fatherId], ['motherId' => $pet->motherId]]],
				[]
			);
			$brothers = ArrayDataHelper::arrayByOneKey($brothers, 'id');

			if ($includeGags)
			{
				$gagBothers = GagModel::getAllAdvanced(
					['$or' => [['fatherId' => $pet->fatherId], ['motherId' => $pet->motherId]]],
					[]
				);
				$gagBothers = ArrayDataHelper::arrayByOneKey($gagBothers, 'id');
				$brothers = array_merge($brothers, $gagBothers);
			}
		}
		elseif (!empty($pet->fatherId))
		{
			$brothers = PetModel::getAllAdvanced(['fatherId' => $pet->fatherId], []);
			$brothers = ArrayDataHelper::arrayByOneKey($brothers, 'id');

			if ($includeGags)
			{
				$gagBothers = GagModel::getAllAdvanced(['fatherId' => $pet->fatherId], []);
				$gagBothers = ArrayDataHelper::arrayByOneKey($gagBothers, 'id');
				$brothers = array_merge($brothers, $gagBothers);
			}
		}
		elseif (!empty($pet->motherId))
		{
			$brothers = PetModel::getAllAdvanced(['motherId' => $pet->motherId], []);
			$brothers = ArrayDataHelper::arrayByOneKey($brothers, 'id');

			if ($includeGags)
			{
				$gagBothers = GagModel::getAllAdvanced(['motherId' => $pet->motherId], []);
				$gagBothers = ArrayDataHelper::arrayByOneKey($gagBothers, 'id');
				$brothers = array_merge($brothers, $gagBothers);
			}
		}

		unset($brothers[$pet->id]);

		return $brothers;
	}

	/**
	 * @param PetModel $pet
	 * @param bool     $includeGags
	 * @return array
	 */
	public function getParentsPetData(PetModel $pet, $includeGags = false)
	{
		$parents = [];
		if (!empty($pet->fatherId) && !empty($pet->motherId))
		{
			$parents = PetModel::getAllAdvanced(['$or' => [['id' => $pet->fatherId], ['id' => $pet->motherId]]], []);
			$parents = ArrayDataHelper::arrayByOneKey($parents, 'id');

			if ($includeGags)
			{
				$gagParents = GagModel::getAllAdvanced(
					['$or' => [['id' => $pet->fatherId], ['id' => $pet->motherId]]],
					[]
				);
				$gagParents = ArrayDataHelper::arrayByOneKey($gagParents, 'id');
				$parents = array_merge($parents, $gagParents);
			}
		}
		elseif (!empty($pet->fatherId))
		{
			$parents = PetModel::getAllAdvanced(['id' => $pet->fatherId], []);
			$parents = ArrayDataHelper::arrayByOneKey($parents, 'id');

			if ($includeGags)
			{
				$gagParents = GagModel::getAllAdvanced(['id' => $pet->fatherId], []);
				$gagParents = ArrayDataHelper::arrayByOneKey($gagParents, 'id');
				$parents = array_merge($parents, $gagParents);
			}
		}
		elseif (!empty($pet->motherId))
		{
			$parents = PetModel::getAllAdvanced(['id' => $pet->motherId], []);
			$parents = ArrayDataHelper::arrayByOneKey($parents, 'id');

			if ($includeGags)
			{
				$gagParents = GagModel::getAllAdvanced(['id' => $pet->motherId], []);
				$gagParents = ArrayDataHelper::arrayByOneKey($gagParents, 'id');
				$parents = array_merge($parents, $gagParents);
			}
		}

		return $parents;
	}

	/**
	 * @param \MyPet\Pets\Model\PetModel $pet
	 * @param bool                       $includeGags
	 * @return array
	 */
	public function getPartnersPetData(PetModel $pet, $includeGags = false)
	{
		$partners = [];
		if (!empty($pet->partners))
		{
			$partners = PetModel::getAllAdvanced(['id' => ['$in' => $pet->partners]], []);
			$partners = ArrayDataHelper::arrayByOneKey($partners, 'id');
			if ($includeGags)
			{
				$gagPartners = GagModel::getAllAdvanced(['id' => ['$in' => $pet->partners]], []);
				$gagPartners = ArrayDataHelper::arrayByOneKey($gagPartners, 'id');
				$partners = array_merge($partners, $gagPartners);
			}
		}

		return $partners; // Partners
	}

	/**
	 * @param \MyPet\Pets\Model\PetModel $pet
	 * @param                            $fatherId
	 */
	public function addFather(PetModel $pet, $fatherId)
	{
		$pet->fatherId = $fatherId;
	}

	/**
	 * @param \MyPet\Pets\Model\PetModel $pet
	 * @param                            $motherId
	 */
	public function addMother(PetModel $pet, $motherId)
	{
		$pet->motherId = $motherId;
	}

	/**
	 * @param PetModel $pet
	 */
	public function removeFather(PetModel $pet)
	{
		$pet->fatherId = '';
	}

	/**
	 * @param PetModel $pet
	 */
	public function removeMother(PetModel $pet)
	{
		$pet->motherId = '';
	}

	/**
	 * @param $petId
	 * @return PetModel
	 */
	public static function addedPetAndGet($petId)
	{
		$pet = new PetModel($petId);
		$pet->id = $petId;

		return $pet;
	}

	/**
	 * @param PetModel $pet
	 * @return mixed
	 */
	public function getOwersPet(PetModel $pet)
	{
		return $pet->ownerIds;
	}

	/**
	 * @param     $userId
	 * @param int $page
	 * @param int $perPage
	 * @return array
	 */
	public function getPetsByUser($userId, $page = 0, $perPage = 10)
	{
		return PetModel::getAllAdvanced(['ownerIds' => $userId], [], false, true, $page, $perPage);
	}

	/**
	 * @param     $userId
	 * @param int $page
	 * @param int $perPage
	 * @return array
	 */
	public function getPetsBySpecialist($userId, $page = 0, $perPage = 10)
	{
		return PetModel::getAllAdvanced(['realSpecialists' => $userId], [], false, true, $page, $perPage);
	}

	/**
	 * @param $userId
	 * @return int
	 */
	public function countPetsByUser($userId)
	{
		return PetModel::count(['ownerIds' => $userId]);
	}

	/**
	 * @param     $userId
	 * @param int $perPage
	 * @return float
	 */
	public function countPagesPetsByUser($userId, $perPage)
	{
		return (int)ceil($this->countPetsByUser($userId) / $perPage);
	}

	/**
	 * @param array $pet
	 * @return bool
	 */
	public static function isDead(Array $pet)
	{
		return (isset($pet['death_y']) && $pet['death_y'] > 0);
	}

	/**
	 * @param PetModel $pet
	 */
	public function getSpecialist(PetModel $pet)
	{
		$langName = SessionStorage::get('langName');
		$ids = [];
		foreach ($pet->ownerIds as $id)
		{
			if (!empty($id))
			{
				$ids[$id] = $id;
			}
		}
		foreach ($pet->specialists as $users)
		{
			foreach ($users as $id)
			{
				if (!empty($id))
				{
					$ids[$id] = $id;
				}
			}
		}
		$ringleader = $pet->ringleader;
		if (!empty($ringleader))
		{
			$ids[$ringleader] = $ringleader;
		}
		$ids = array_values($ids);
		$profiles = InteractionOfFriends::getProfileSpecialist($ids);
		$professions = ProfessionModel::getAllAdvanced([], [], true, ['priority' => 1]);
		$professions = ArrayDataHelper::arrayByOneKey($professions, '_id');
		$result['ringleader'] = isset($profiles[$ringleader]) ? $profiles[$ringleader] : [];
		$result['ringleader']['profession'] = '_t_ringleader_t_';
		$result['specialists'] = [];
		$result['owners'] = [];

		foreach ($pet->specialists as $type => $ids)
		{
			foreach ($ids as $id)
			{
				$profiles[$id]['profession'] = isset($professions[$type]) ? isset($professions[$type][$langName]) ? $professions[$type][$langName] : $professions[$type]['en'] : $type;
				$result['specialists'][$type][$id] = isset($profiles[$id]) ? $profiles[$id] : [];
			}
		}

		foreach ($pet->ownerIds as $id)
		{
			$result['owners'][$id] = isset($profiles[$id]) ? $profiles[$id] : [];
			$result['owners'][$id]['profession'] = '_t_owner_t_';
		}

		return $result;
	}

	/**
	 * @param PetModel $pet
	 * @param PetModel $petRelation
	 * @param          $type
	 * @param          $gender
	 * @throws PetException
	 */
	public function addLink(PetModel $pet, PetModel $petRelation, $type, $gender)
	{
		switch ($type)
		{
			case 'parent':
				$manager = new PetManager($this->server);
				if ((int)$petRelation->gender > 0 && (int)$petRelation->gender !== (int)$gender)
				{
					throw new PetException('Man not woman: '.$petRelation->gender.' !== '.$gender);
				}
				else
				{
					if ($petRelation->gender === 2)
					{
						$manager->addMother($pet, $petRelation->id);
					}
					else
					{
						$manager->addFather($pet, $petRelation->id);
					}
				}
				break;
			case 'child':
				$manager = new PetManager($this->server);
				if ($pet->gender === 2)
				{
					$manager->addMother($petRelation, $pet->id);
				}
				else
				{
					$manager->addFather($petRelation, $pet->id);
				}
				break;
		}
	}

	/**
	 * @param PetModel $pet
	 * @param PetModel $petRelation
	 * @param          $type
	 * @param          $gender
	 * @throws PetException
	 */
	public function remLink(PetModel $pet, PetModel $petRelation, $type, $gender)
	{
		switch ($type)
		{
			case 'parent':
				$manager = new PetManager($this->server);
				if ((int)$petRelation->gender === 2)
				{
					$manager->removeMother($pet);
				}
				else
				{
					$manager->removeFather($pet);
				}
				break;
			case 'child':
				$manager = new PetManager($this->server);
				if ((int)$pet->gender === 2)
				{
					$manager->removeMother($petRelation);
				}
				else
				{
					$manager->removeFather($petRelation);
				}
				break;
		}
	}

	/**
	 * @param PetModel $pet
	 */
	public function removeAllLink(PetModel $pet)
	{
		/** @var \MongoСursor $cursor */
		$cursor = PetModel::getAllAdvanced(
			['$or' => [['fatherId' => $pet->id], ['motherId' => $pet->id]]],
			['id' => 1],
			false,
			true,
			false,
			false,
			true
		);
		while ($object = $cursor->getNext)
		{
			$id = $object['id'];
			$child = new PetModel($id);
			if ($child->fatherId === $pet->id)
			{
				$child->fatherId = '';
			}
			if ($child->motherId === $pet->id)
			{
				$child->motherId = '';
			}
			$child->save();
			$child->clearStaticCache();
			usleep(100);
		}
	}

	/**
	 * @param PetModel $pet
	 */
	public static function removeToSearch(PetModel $pet)
	{
		$params['id'] = $pet->id;
		$params['index'] = 'mypet';
		$params['type'] = 'pet';

		try
		{
			ElasticsearchManager::getInstance()->delete($params);
		}
		catch (\Exception $e)
		{
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'line'    => $e->getLine(),
					'file'    => $e->getFile(),
					'code'    => $e->getCode()
				],
				false,
				true
			);
		}
	}

	/**
	 * @param PetModel $pet
	 */
	public static function updateToSearch(PetModel $pet)
	{
		$params['id'] = $pet->id;
		$params['index'] = 'mypet';
		$params['type'] = 'pet';
		$params['body']['doc'] = $pet->getFieldsArrayBySearch();
		try
		{
			ElasticsearchManager::getInstance()->update($params);
		}
		catch (\Exception $e)
		{
			if ($e->getCode() === 400 || $e->getCode() === 404)
			{
				static::addedToSearch($pet);
			}
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'line'    => $e->getLine(),
					'file'    => $e->getFile(),
					'code'    => $e->getCode()
				],
				false,
				true
			);
		}
	}

	/**
	 * @param PetModel $pet
	 */
	public static function addedToSearch(PetModel $pet)
	{
		$params['id'] = $pet->id;
		$params['index'] = 'mypet';
		$params['type'] = 'pet';
		$params['body'] = $pet->getFieldsArrayBySearch();
		try
		{
			ElasticsearchManager::getInstance()->create($params);
		}
		catch (\Exception $e)
		{
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'line'    => $e->getLine(),
					'file'    => $e->getFile(),
					'code'    => $e->getCode()
				],
				false,
				true
			);
		}
	}

	public static function createIndex()
	{
		$params = [
			'index' => 'mypet',
			'body'  => [
				'settings' => [
					'analysis' => [
						"filter"   => [
							"ru_stop"    => [
								"type"      => "stop",
								"stopwords" => "_russian_"
							],
							"ru_stemmer" => [
								"type"     => "stemmer",
								"language" => "russian"
							]
						],
						'analyzer' => [
							"default" => [
								"char_filter" => [
									"html_strip"
								],
								"tokenizer"   => "standard",
								"filter"      => [
									"lowercase",
									"ru_stop",
									"ru_stemmer",
									'stop',
									'kstem'
								]
							]
						]
					]
				],
				'mappings' => [
					'pet' => [
						'properties' => [
							'partners' => [
								"type"  => "string",
								"index" => "not_analyzed"
							],
							'signs'    => [
								"type"  => "string",
								"index" => "not_analyzed"
							],
							'locs'     => [
								"type" => "geo_point",
								//								"geohash_prefix" => true,
								//								"geohash_precision" =>  "1km"
							]
						]
					]
				]
			]
		];
		ElasticsearchManager::getInstance()->indices()->create($params);
	}
}
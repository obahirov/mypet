<?php
namespace MyPet\Pets\Model;

use KMCore\DB\ObjectModel;

/**
 * @property mixed  ownerIds
 * @property mixed  id
 * @property string type
 * @property string species
 * @property string hNick
 * @property string nick
 * @property string color
 * @property string hair
 * @property string gender
 * @property int    birth_y
 * @property int    birth_m
 * @property int    birth_d
 * @property int    death_y
 * @property int    death_m
 * @property int    death_d
 * @property string country
 * @property mixed  awards
 * @property string photo
 * @property string nursery
 * @property mixed  motherId
 * @property mixed  fatherId
 * @property mixed  childrens
 * @property mixed  partners
 * @property string size
 * @property string weight
 * @property mixed  signs
 * @property mixed  subjects
 * @property mixed  specialists
 * @property mixed  ringleader
 * @property mixed  region
 * @property mixed  breed
 * @property mixed  city
 * @property array  realSpecialists
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PetModel extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'mp_pets';
	protected static $pk = 'id';
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['ownerIds' => 1]
		],
		[
			'keys' => ['partners' => 1]
		],
		[
			'keys' => ['motherId' => 1]
		],
		[
			'keys' => ['fatherId' => 1]
		],
		[
			'keys' => ['status' => 1]
		],
		[
			'keys' => ['specialists' => 1]
		],
		[
			'keys' => ['realSpecialists' => 1]
		]
	];

	protected static $fieldsDefault = [
		'id'              => '',
		'ownerIds'        => [],
		'type'            => '',
		'hNick'           => '', // Домашня кличка 16 символів
		'nick'            => '', // Кличка (згідно родоводу)
		'chip'            => '',
		'ancestry_code'   => '',
		'passport'        => '',
		'color'           => '',
		'hair'            => '',
		'gender'          => 0,
		'birth_y'         => '',
		'birth_m'         => '',
		'birth_d'         => '',
		'death_y'         => '',
		'death_m'         => '',
		'death_d'         => '',
		'country'         => '',
		'city'            => '',
		'region'          => '',
		'awards'          => [],
		'nursery'         => '',
		'photo'           => '',
		'small_photo'     => '',
		'src_photo'       => '',
		'cover'           => '',
		'motherId'        => '',
		'fatherId'        => '',
		'partners'        => [],
		'size'            => '',
		'weight'          => '',
		'signs'           => [],
		'subjects'        => [],
		'status'          => '',
		'breed'           => '',
		'about'           => '',
		'ancestry'        => '',
		'ringleader'      => '', // Заводчик
		'specialists'     => [],
		'realSpecialists' => [],
		'marks'           => '',
		'locs'            => []
	];

	protected static $fieldsValidate = [
		'id'              => self::TYPE_STRING,
		'ownerIds'        => self::TYPE_JSON,
		'type'            => self::TYPE_STRING,
		'hNick'           => self::TYPE_STRING, // Домашня кличка 16 символів
		'nick'            => self::TYPE_STRING, // Кличка (згідно родоводу)
		'chip'            => self::TYPE_STRING,
		'ancestry_code'   => self::TYPE_STRING,
		'passport'        => self::TYPE_STRING,
		'color'           => self::TYPE_STRING,
		'hair'            => self::TYPE_STRING,
		'gender'          => self::TYPE_UNSIGNED_INT,
		'birth_y'         => self::TYPE_STRING,
		'birth_m'         => self::TYPE_STRING,
		'birth_d'         => self::TYPE_STRING,
		'death_y'         => self::TYPE_STRING,
		'death_m'         => self::TYPE_STRING,
		'death_d'         => self::TYPE_STRING,
		'country'         => self::TYPE_STRING,
		'city'            => self::TYPE_STRING,
		'region'          => self::TYPE_STRING,
		'awards'          => self::TYPE_JSON,
		'nursery'         => self::TYPE_STRING,
		'photo'           => self::TYPE_STRING,
		'small_photo'     => self::TYPE_STRING,
		'src_photo'       => self::TYPE_STRING,
		'cover'           => self::TYPE_STRING,
		'motherId'        => self::TYPE_STRING,
		'fatherId'        => self::TYPE_STRING,
		'partners'        => self::TYPE_JSON,
		'size'            => self::TYPE_STRING,
		'weight'          => self::TYPE_STRING,
		'signs'           => self::TYPE_JSON,
		'subjects'        => self::TYPE_JSON,
		'status'          => self::TYPE_STRING,
		'breed'           => self::TYPE_STRING,
		'about'           => self::TYPE_STRING,
		'ancestry'        => self::TYPE_STRING,
		'ringleader'      => self::TYPE_STRING, // Заводчик
		'specialists'     => self::TYPE_JSON,
		'realSpecialists' => self::TYPE_JSON,
		'marks'           => self::TYPE_STRING,
		'locs'            => self::TYPE_JSON
	];

	public static $fieldsSearch = [
		'id',
		'ownerIds',
		'type',
		'hNick',
		'nick',
		'chip',
		'ancestry_code',
		'passport',
		'color',
		'hair',
		'gender',
		'birth_y',
		'birth_m',
		'birth_d',
		'death_y',
		'death_m',
		'death_d',
		'country',
		'region',
		'city',
		'awards',
		'nursery',
		'photo',
		'motherId',
		'fatherId',
		'partners',
		'signs',
		'subjects',
		'status',
		'breed',
		'about',
		'ancestry',
		'ringleader', // Заводчик
		'specialists',
		'marks',
		'locs'
	];
} 
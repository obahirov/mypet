<?php
namespace MyPet\Pets\Model;

/**
 * @property mixed  ownerIds
 * @property mixed  id
 * @property string type
 * @property string species
 * @property string hNick
 * @property string nick
 * @property string color
 * @property string hair
 * @property string gender
 * @property int    birth_y
 * @property int    birth_m
 * @property int    birth_d
 * @property int    death_y
 * @property int    death_m
 * @property int    death_d
 * @property string country
 * @property mixed  awards
 * @property string photo
 * @property string nursery
 * @property mixed  motherId
 * @property mixed  fatherId
 * @property mixed  childrens
 * @property mixed  partners
 * @property string size
 * @property string weight
 * @property mixed  signs
 * @property mixed  subjects
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class GagModel extends PetModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'mp_gags';
	protected static $pk = 'id';
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['ownerIds' => 1]
		],
		[
			'keys' => ['partners' => 1]
		],
		[
			'keys' => ['motherId' => 1]
		],
		[
			'keys' => ['fatherId' => 1]
		],
		[
			'keys' => ['status' => 1]
		],
		[
			'keys' => ['specialists' => 1]
		]
	];

	protected static $fieldsDefault = [
		'gag'    => '',
		'params' => []
	];

	protected static $fieldsValidate = [
		'gag'    => self::TYPE_STRING,
		'params' => self::TYPE_JSON
	];
} 
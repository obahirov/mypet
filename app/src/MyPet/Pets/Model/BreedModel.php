<?php
namespace MyPet\Pets\Model;

use KMCore\DB\ObjectModel;

/**
 * @property mixed en
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class BreedModel extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'mp_breed';
	protected static $pk = '_id';
	protected static $sort = ['priority' => -1];
	protected static $indexes = [
		[
			'keys' => ['priority' => -1]
		],
		[
			'keys' => ['type' => 1]
		],
	];

	protected static $fieldsDefault = [
		'_id'      => '',
		'type'     => '',
		'en'       => '',
		'ua'       => '',
		'ru'       => '',
		'priority' => 0,
	];

	protected static $fieldsValidate = [
		'_id'      => self::TYPE_MONGO_ID,
		'type'     => self::TYPE_STRING,
		'en'       => self::TYPE_STRING,
		'ua'       => self::TYPE_STRING,
		'ru'       => self::TYPE_STRING,
		'priority' => self::TYPE_UNSIGNED_INT,
	];
} 
<?php
namespace MyPet\Users\Classes;

use KMCore\Helper\SessionStorage;

/**
 * Менеджер для контролю авторизації
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AuthManager
{
	const SESSION_EMAIL = 'email';
	const SESSION_USER_ID = 'userId';
	const SESSION_LOGGED = 'logged';

	/**
	 * Виставлення сессії
	 *
	 * @param int $sessionTimeout
	 */
	public static function setSessionMaxLifetime($sessionTimeout = 86400)
	{
		ini_set('session.gc_maxlifetime', $sessionTimeout);
	}

	/**
	 * Вийти з системи
	 */
	public static function logout()
	{
		SessionStorage::destroySession(SessionStorage::TYPE_BASE);
	}

	/**
	 * Встановлення авторизований чи ні
	 *
	 * @param bool $logged
	 * @return $this
	 */
	public static function setLogged($logged = false)
	{
		SessionStorage::set(self::SESSION_LOGGED, (bool)$logged, SessionStorage::TYPE_BASE);
	}

	/**
	 * Встановлення айдішки користувача
	 *
	 * @param $userId
	 * @return $this
	 */
	public static function setSessionUserId($userId)
	{
		SessionStorage::set(self::SESSION_USER_ID, $userId, SessionStorage::TYPE_BASE);
	}

	/**
	 * Встановлення мейлу користувача
	 *
	 * @param $email
	 */
	public static function setSessionEmail($email)
	{
		SessionStorage::set(self::SESSION_EMAIL, $email, SessionStorage::TYPE_BASE);
	}

	/**
	 * Отримати мейл користувача
	 */
	public static function getSessionEmail()
	{
		return SessionStorage::get(self::SESSION_EMAIL, SessionStorage::TYPE_BASE);
	}

	/**
	 * Чи увійшов користувач в систему
	 *
	 * @return bool
	 */
	public static function loggedIn()
	{
		return (SessionStorage::get(self::SESSION_LOGGED, SessionStorage::TYPE_BASE) === true);
	}

	/**
	 * Чи існує ідентифікатор сесії користувача (перевірка анонімності)
	 *
	 * @return bool
	 */
	public static function issetSessionUserId()
	{
		return SessionStorage::issetSession(self::SESSION_USER_ID, SessionStorage::TYPE_BASE);
	}

	/**
	 * Отримати АйДі користувача
	 *
	 * @return mixed
	 */
	public static function getSessionUserId()
	{
		return SessionStorage::get(self::SESSION_USER_ID, SessionStorage::TYPE_BASE);
	}
} 
<?php
namespace MyPet\Users\Classes;

use KMCore\DB\Elasticsearch\ElasticsearchManager;
use KMCore\Helper\GeneratorHelper;
use KMCore\Helper\SessionStorage;
use KMCore\LogSystem\FastLog;
use KMCore\Request\Request;
use KMCore\ServerContainer;
use MyPet\Friends\Model\FriendsModel;
use MyPet\TokenAuth\SecureTokenManager;
use MyPet\Users\Model\SocUserModel;
use MyPet\Users\Model\User;
use MyPet\Users\Model\WorkModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UserManager
{
	public static $useSession = true;

	/**
	 * @param $userId
	 * @return User
	 */
	public static function getUserModelByUserId($userId)
	{
		$user = new User(['id' => (string)$userId]);
		if (!$user->isLoadedObject())
		{
			$user->id = (string)$userId;
		}

		return $user;
	}

	/**
	 * Генерація гостьового userId
	 */
	public static function initUserId()
	{
		if (!AuthManager::issetSessionUserId())
		{
			$userId = GeneratorHelper::getStringUniqueId();
			AuthManager::setSessionUserId($userId);
		}
	}

	/**
	 * @param       $email
	 * @param       $password
	 * @param array $userFields
	 * @throws \Exception
	 * @return User
	 */
	public static function create($email, $password, Array $userFields = [])
	{
		$email = strtolower($email);
		$user = new User(['email' => $email]);
		if (!$user->isLoadedObject())
		{
			$user->id = (AuthManager::issetSessionUserId()) ? AuthManager::getSessionUserId(
			) : GeneratorHelper::getStringUniqueId();

			$user->email = $email;
			$user->password = $password;
			$user->created = time();
			$user->ip_install = $_SERVER['REMOTE_ADDR'];
			$user->setFieldsArray($userFields);
			$user->save();
		}
		else
		{
			throw new \Exception('double_email');
		}

		if ($user->password === $password)
		{
			static::auth($user);

			return $user;
		}

		return $user;
	}

	/**
	 * @param $email
	 * @param $password
	 * @return null|User
	 */
	public static function login($email, $password)
	{
		$email = strtolower($email);
		$user = new User(['email' => $email]);
		if ($user->password === $password)
		{
			static::auth($user);

			return $user;
		}

		return null;
	}

	public static function logout()
	{
		AuthManager::setLogged(false);
		if (SessionStorage::get('access_token') !== null)
		{
			SessionStorage::unsetSession('access_token');
		}
		AuthManager::logout();
	}

	/**
	 * @param User $user
	 */
	public static function auth(User $user)
	{
		if (static::$useSession)
		{
			AuthManager::setLogged(true);
			AuthManager::setSessionEmail($user->email);
			AuthManager::setSessionUserId($user->id);
		}

		$user->lastlog = time();
		$user->login_count++;
		$user->ip = $_SERVER['REMOTE_ADDR'];
		$ipInfo = Request::getIpInfo($user->ip);
		$user->country = $ipInfo['country_code'];

	}

	/**
	 * @param SocUserModel $socUser
	 * @return User
	 * @throws \Exception
	 */
	public static function getUserBySoc(SocUserModel $socUser)
	{
		$user = new User(['socUserId' => $socUser->id]);
		if (!$user->isLoadedObject())
		{ // Пользователя с таким идентификатором не существует
			$socUser->email = strtolower($socUser->email);
			if (!empty($socUser->email))
			{
				$user = new User(['email' => $socUser->email]);
				if (!$user->isLoadedObject())
				{
					$user->id = (AuthManager::issetSessionUserId()) ? AuthManager::getSessionUserId(
					) : GeneratorHelper::getStringUniqueId();
					$user->email = $socUser->email;
				}
			}
			// Почта нигде не использовалась ранее, смело устанавливаем её
			$user->created = time();
			$user->ip_install = $_SERVER['REMOTE_ADDR'];
			$user->socUserId = $socUser->id;
			$user->save();
		}

		return $user;
	}

	/**
	 * @param SocUserModel $socUser
	 * @throws \Exception
	 * @return mixed|void
	 */
	public static function socialLogin(SocUserModel $socUser)
	{
		$user = static::getUserBySoc($socUser);

		if ($socUser->gender === 'male')
		{
			$gender = 1;
		}
		elseif ($socUser->gender === 'female')
		{
			$gender = 2;
		}
		else
		{
			$gender = 0;
		}

		$userFields = [
			'first_name'   => $socUser->first_name,
			'last_name'    => $socUser->last_name,
			'gender'       => $gender,
			'city'         => $socUser->city,
			'user_country' => $socUser->country,
			'birth_y'      => $socUser->birth_y,
			'birth_m'      => $socUser->birth_m,
			'birth_d'      => $socUser->birth_d,
			'photo'        => $socUser->photo,
			'small_photo'  => $socUser->photo,
		];

		$userFields = [
			'first_name'   => $socUser->first_name,
			'last_name'    => $socUser->last_name,
			'gender'       => $gender,
			'city'         => $socUser->city,
			'user_country' => $socUser->country,
			'birth_y'      => $socUser->birth_y,
			'birth_m'      => $socUser->birth_m,
			'birth_d'      => $socUser->birth_d,
			'photo'        => $socUser->photo,
			'small_photo'  => $socUser->photo,
		];

		$user->setFieldsArray($userFields);
		UserManager::auth($user);
		$user->save();

		return $user;
	}

	/**
	 * @param $email
	 * @return User
	 */
	public static function getUserByEmail($email)
	{
		$email = strtolower($email);
		$user = new User(['email' => $email]);
		return $user;
	}

	/**
	 * @param User $user
	 */
	public static function removeToSearch(User $user)
	{
		$params['id'] = $user->id;
		$params['index'] = 'mypet';
		$params['type'] = 'user';

		try
		{
			ElasticsearchManager::getInstance()->delete($params);
		}
		catch (\Exception $e)
		{
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'line'    => $e->getLine(),
					'file'    => $e->getFile(),
					'code'    => $e->getCode()
				],
				false,
				true
			);
		}
	}

	/**
	 * @param User $user
	 */
	public static function updateToSearch(User $user)
	{
		$params['id'] = $user->id;
		$params['index'] = 'mypet';
		$params['type'] = 'user';
		$params['body']['doc'] = $user->getFieldsArrayBySearch();
		try
		{
			ElasticsearchManager::getInstance()->update($params);
		}
		catch (\Exception $e)
		{
			if ($e->getCode() === 400 || $e->getCode() === 404)
			{
				static::addedToSearch($user);
			}
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'line'    => $e->getLine(),
					'file'    => $e->getFile(),
					'code'    => $e->getCode(),
					'params'  => $params
				],
				false,
				true
			);
		}
	}

	/**
	 * @param User $user
	 */
	public static function addedToSearch(User $user)
	{
		$params['id'] = $user->id;
		$params['index'] = 'mypet';
		$params['type'] = 'user';
		$params['body'] = $user->getFieldsArrayBySearch();

		try
		{
			ElasticsearchManager::getInstance()->create($params);
		}
		catch (\Exception $e)
		{
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'line'    => $e->getLine(),
					'file'    => $e->getFile(),
					'code'    => $e->getCode(),
					'params'  => $params
				],
				false,
				true
			);
		}
	}

	public function removeAllLink(User $user)
	{
		$cursor = FriendsModel::getAllAdvanced(
			['$or' => [['id' => $user->id], ['friendId' => $user->id]]],
			['_id' => 1],
			false,
			true,
			false,
			false,
			true
		);
		while ($object = $cursor->getNext)
		{
			$id = $object['_id'];
			$friend = new FriendsModel($id);
			$friend->delete();
			$friend->clearStaticCache();
			usleep(100);
		}
	}

	public static function createIndex()
	{
		$params = [
			'index' => 'mypet',
			'body'  => [
				'settings' => [
					'analysis' => [
						"filter"   => [
							"ru_stop"    => [
								"type"      => "stop",
								"stopwords" => "_russian_"
							],
							"ru_stemmer" => [
								"type"     => "stemmer",
								"language" => "russian"
							]
						],
						'analyzer' => [
							"default"      => [
								"char_filter" => [
									"html_strip"
								],
								"tokenizer"   => "standard",
								"filter"      => [
									"lowercase",
									"ru_stop",
									"ru_stemmer",
									'stop',
									'kstem'
								]
							],
							'custom_email' => [
								'type'      => 'custom',
								'tokenizer' => 'uax_url_email',
								'filter'    => ['lowercase']
							]
						]
					]
				],
				'mappings' => [
					'user' => [
						'properties' => [
							'email'  => [
								'type'     => 'string',
								'analyzer' => 'custom_email'
							],
							'skills' => [
								"type"  => "string",
								"index" => "not_analyzed"
							],
							'phone'  => [
								"type"  => "string",
								"index" => "not_analyzed"
							],
							'locs'   => [
								"type" => "geo_point",
								//								"geohash_prefix" => true,
								//								"geohash_precision" =>  "1km"
							]
						]
					]
				]
			]
		];
		ElasticsearchManager::getInstance()->indices()->create($params);
	}

	public static function getTokenData(ServerContainer $server, User $user)
	{
		$secure = new SecureTokenManager($server);
		$data = SecureTokenManager::generate(
			$secure,
			[$user->password, $user->login_count, $user->lastlog],
			['id' => $user->id, 'loginCount' => $user->login_count, 'lastlog' => $user->lastlog]
		);
		return $data;
	}
}
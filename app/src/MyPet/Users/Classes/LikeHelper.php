<?php
namespace MyPet\Users\Classes;

use MyPet\Inbox\Classes\RequestData;
use MyPet\Users\Model\User;

/**
 * Class LikeHelper
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class LikeHelper
{
	const TYPE_POST = 'post';
	const TYPE_PHOTO = 'photo';

	public static function request(User $user, User $owner, $id, $type, $inboxType, Array $params = [])
	{
		if ($user->id !== $owner->id)
		{
			$link = '';
			switch ($type)
			{
				case self::TYPE_PHOTO:
					$link = '/photo/'.$user->id;
					break;
				case self::TYPE_POST:
					$link = '/post/'.$user->id;
					break;
			}
			$attributes = [
				'userId'  => $user->id,
				'photo'  => $user->photo,
				'pageId'  => $id,
				'type'  => $type,
				'link'    => $link,
				'payload' => $params,
				'uniqid' => uniqid().mt_rand(0,100)
			];
			$attributes['payload']['id'] = $user->id;
			$attributes['payload']['first_name'] = $user->first_name;
			$attributes['payload']['last_name'] = $user->last_name;
			$attributes['payload']['photourl'] = $user->photo;

			RequestData::init(new RequestData($owner->id))->setType(
				$inboxType
			)->setAttributes($attributes)->setUnique($id.'|'.$owner->id.'|'.$inboxType)->setPub(true)->update();
		}
	}
}
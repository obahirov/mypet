<?php
namespace MyPet\Users\Classes;

use MyPet\Pets\Model\PetModel;
use MyPet\Users\Model\User;
use MyPet\Users\Model\WorkModel;

class ProgressBar
{
	protected $userFields = [
		'first_name' => 5,
		'last_name'  => 5,
		'photo'      => 5,
		'location'    => 1,
		'tags'    => 3
	];
	protected $petFields = ['hNick' => 5, 'gender' => 3, 'breed' => 5, 'photo' => 3];
	protected $specialistFields = ['phones' => 3, 'sites' => 3, 'profession' => 5, 'career' => 3];
	protected $fields = [];
	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
		if ($user->isProf === 'yes')
		{
			$this->fields = array_merge($this->userFields, $this->specialistFields);
		}
		else
		{
			$this->fields = $this->userFields;
		}
	}

	public function calc()
	{
		$maxSum = array_sum($this->fields);
		$userSum = 0;
		foreach ($this->fields as $field => $count)
		{
			if (isset($this->user->{$field}) && (!empty($this->user->{$field}) || $this->user->{$field} === 0))
			{
				$userSum += $count;
			}
		}

		$maxSumPet = 0;
		$userSumPet = 0;
		$pets = PetModel::getAllAdvanced(['ownerIds' => $this->user->id]);
		if (!empty($pets))
		{
			$maxSumPet = array_sum($this->petFields);
			$userSumPet = $this->calcPets($pets);
		}

		$resultMaxSum = $maxSum + $maxSumPet;
		$resultUser = $userSum + $userSumPet;

		$progress = round(($resultUser * 100) / $resultMaxSum);

		return $progress;
	}

	protected function calcPets(Array $pets)
	{
		$userSumPet = 0;
		$countPets = count($pets);

		foreach ($pets as $pet)
		{

			foreach ($this->petFields as $field => $count)
			{
				if (isset($pet[$field]) && (!empty($pet[$field]) || $pet[$field] === 0))
				{
					$userSumPet += $count;
				}
			}
		}

		//var_dump($this->petFields);
		return ceil($userSumPet / $countPets);
	}

	public function getInfoField()
	{
		$okFields = [];
		$failFields = [];
		foreach ($this->fields as $field => $count)
		{
			if (isset($this->user->{$field}) && (!empty($this->user->{$field}) || $this->user->{$field} === 0))
			{
				$okFields[$field] = $field;
			}
			else
			{
				$failFields[$field] = $field;
			}

		}
		$okFieldsPet = [];
		$failFieldsPet = [];
		$pets = PetModel::getAllAdvanced(['ownerIds' => $this->user->id]);
		if (!empty($pets))
		{
			foreach ($pets as $pet)
			{
				foreach ($this->petFields as $field => $count)
				{
					if (isset($pet[$field]) && (!empty($pet[$field]) || $pet[$field] === 0))
					{
						if(!isset($failFieldsPet[$field]))
						{
							$okFieldsPet[$field] = $field;
						}
					}
					else
					{
						$failFieldsPet[$field] = $field;
						unset($okFieldsPet[$field]);
					}
				}
			}
		}

		return ['user' => ['ok' => $okFields, 'cr' => $failFields], 'pet' => ['ok' => $okFieldsPet, 'cr' => $failFieldsPet]];
	}
}

<?php
namespace MyPet\Users\Controller;

use KMCore\Helper\GeneratorHelper;
use MyPet\BaseApiController;
use MyPet\TokenAuth\SecureTokenManager;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;
use MyPet\Users\Model\ProfessionModel;
use MyPet\Users\Model\User;

class AuthApiController extends BaseApiController
{
	public function registration()
	{
		$email = (string)$this->request->getGetOrPost('email', '');
		$email = strtolower($email);
		$password = $this->request->getGetOrPost('password', '');
		$password2 = $this->request->getGetOrPost('password2', '');

		$emaillen = strlen($_POST['email']);
		if ($emaillen < 3 || $emaillen > 64)
		{
			throw new \Exception('Pasword lenght error');
		}

		$passwordlen = strlen($_POST['password']);
		if ($passwordlen < 3 || $passwordlen > 64)
		{
			throw new \Exception('Pasword lenght error');
		}

		if ($password !== $password2)
		{
			throw new \Exception('Pasword not match');
		}

		$userId = GeneratorHelper::getStringUniqueId();
		if (!empty($password))
		{
			$password = password_hash(
				$password,
				PASSWORD_BCRYPT,
				['salt' => $userId]
			);
		}
		else
		{
			throw new \Exception('You must enter pass');
		}

		$user = new User(['email' => $email]);
		if (!$user->isLoadedObject())
		{
			$user->id = $userId;

			$user->email = $email;
			$user->password = $password;
			$user->created = time();
			$user->ip_install = $_SERVER['REMOTE_ADDR'];
			UserManager::auth($user);
			$user->save();
		}
		else
		{
			throw new \Exception('Double email');
		}

		$this->success = true;
		$this->data = UserManager::getTokenData($this->server, $user);
		$this->response();
	}

	public function auth()
	{
		$email = (string)$this->request->getGetOrPost('email', '');
		$email = strtolower($email);
		$password = $this->request->getGetOrPost('password', '');

		$user = new User(['email' => $email]);
		if (!$user->isLoadedObject())
		{
			throw new \Exception('Not found user');
		}

		if (password_verify($password, $user->password))
		{
			UserManager::auth($user);
			$user->save();
		}
		else
		{
			throw new \Exception('Invalid password');
		}

		$this->success = true;
		$this->data = UserManager::getTokenData($this->server, $user);
		$this->response();
	}

	public function returnNewToken()
	{
		$email = (string)$this->request->getGetOrPost('email', '');
		$signedRequest = (string)$this->request->getGetOrPost('signedRequest', '');
		$expiresIn = $this->request->getGetOrPost('expiresIn', '');

		$user = new User(['email' => $email]);
		if (!$user->isLoadedObject())
		{
			throw new \Exception('Not found user');
		}

		$secure = new SecureTokenManager($this->server);
		if (!$secure->validateSignedRequest(
			$signedRequest,
			$user->password,
			$user->login_count,
			$user->lastlog,
			$expiresIn
		))
		{
			throw new \Exception('Error key');
		}

		$this->success = true;
		$this->data = UserManager::getTokenData($this->server, $user);
		$this->response();
	}


	public function getUser()
	{
		$langName = $this->request->getGetOrPost('lang', '');
		$data = [];

		$userId = $this->request->getGetOrPost('id', '');
		if(empty($userId) && AuthManager::loggedIn())
		{
			$userId = AuthManager::getSessionUserId();
		}
		elseif(empty($userId))
		{
			throw new \Exception('Not found userId');
		}

		$user = new User($userId);
		if ($user->isLoadedObject())
		{
			$data = $user->getFieldsSanitized();
			if(isset($data['tags']))
			{
				$data['tags'] = implode(', ', $data['tags']);
			}
		}

		if (!empty($langName))
		{
			$profession = new ProfessionModel($user->profession);
			if ($profession->isLoadedObject())
			{
				$data['profession'] = [];
				$data['profession']['_id'] = (string)$user->profession;
				if (isset($profession->{$langName}))
				{
					$data['profession']['name'] = $profession->{$langName};
				}
				else
				{
					$data['profession']['name'] = $profession->en;
				}
			}
		}

		$this->success = true;
		$this->data['userInfo'] = $data;
		$this->response();
	}

	public function getProfessions()
	{
		$lang = $this->request->getGetOrPost('lang', '');
		$professions = ProfessionModel::getAllAdvanced([], [], true, ['priority' => 1]);

		if (!empty($lang))
		{
			$result = [];
			foreach ($professions as $item)
			{
				if (isset($item[$lang]))
				{
					$result[] = ['_id' => $item['_id'], 'name' => $item[$lang], 'priority' => $item['priority']];
				}
			}
			$professions = $result;
		}
		$this->success = true;
		$this->data['professions'] = $professions;
		$this->response();
	}
}
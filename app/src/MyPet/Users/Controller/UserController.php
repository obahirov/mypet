<?php
namespace MyPet\Users\Controller;

use KMCore\Helper\ArrayDataHelper;
use KMCore\Helper\SessionStorage;
use KMCore\LogSystem\FastLog;
use KMCore\Validation\Validate;
use MyPet\Friends\Classes\FriendsManager;
use MyPet\Friends\Model\FriendsModel;
use MyPet\Inbox\Classes\RequestData;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\Notification\Email\Classes\MessagePrepare;
use MyPet\Notification\Email\Classes\SendEmailManager;
use MyPet\Referrals\Classes\ReferralHandler;
use MyPet\SiteController;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;
use MyPet\Users\Model\HistoryConnectionDevelopers;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UserController extends SiteController
{
	const EVENT_CONTROLLER_USER_CREATE = 'event.controller.user.create';
	const EVENT_CONTROLLER_USER_UPDATE = 'event.controller.user.update';
	const EVENT_CONTROLLER_USER_REMOVE = 'event.controller.user.remove';

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->server->on(
			self::EVENT_CONTROLLER_USER_CREATE,
			function (User $user)
			{
				UserManager::addedToSearch($user);
				ReferralHandler::registration($user->id);
				//FIXME КОСТИЛЬНИЙ ВАРІАНТ
				$userObj = UserManager::getUserByEmail('dmytro.tkach@gmail.com');
				if ($userObj->isLoadedObject())
				{
					$friendsManager = new FriendsManager();
					$friendsManager->addFriend($userObj->id, $user->id);

					$friendsManager->on(
						FriendsManager::EVENT_FRIEND_ADD,
						function (FriendsModel $user) use ($userObj)
						{
							$attributes = [
								'userId'   => $user->id,
								'friendId' => $user->friendId,
								'payload'  => $userObj->getFieldsSanitized(),
							];
							RequestData::init(new RequestData($user->friendId))->setType(
								RequestManager::TYPE_INVITE_CONFIRM
							)->setAttributes($attributes)->setUnique($user->_id)->setPub(true)->update();
						}
					);
				}
			}
		);

		$this->server->on(
			self::EVENT_CONTROLLER_USER_UPDATE,
			function (User $user)
			{
				UserManager::updateToSearch($user);
			}
		);
		$this->server->on(
			self::EVENT_CONTROLLER_USER_REMOVE,
			function (User $user)
			{
				UserManager::removeToSearch($user);
			}
		);
	}

	public function connectionDeveloper()
	{
		$message = $this->request->getGetOrPost('message', null, true);
		$this->responseByForm(false, '/', true, false);

		if ($message)
		{
			$history = new HistoryConnectionDevelopers();
			$history->message = $message;
			$history->date = time(true);
			$history->save();

			if (AuthManager::loggedIn())
			{
				$email = AuthManager::getSessionEmail();
			}
			else
			{
				$email = 'Anonim';
			}
			$sendManager = new SendEmailManager($this->server->getContainer('config'));
			$sendManager->addAddress('info@time4pets.com', $email);
			$sendManager->send('New message', $message, true, '', 3, 50);
		}
	}

	/**
	 * Логин
	 */
	public function login()
	{
		if (isset($_POST['email'], $_POST['password']) && !empty($_POST['email']) && !empty($_POST['password'])
		)
		{
			if (isset($_POST['remember_me']))
			{
				AuthManager::setSessionMaxLifetime(604800);
			}

			$user = UserManager::login(
				$_POST['email'],
				md5((string)$_POST['password'].$this->server->getContainer('config')->get('salt'))
			);
			if ($user !== null)
			{
				$user->save();
				$this->responseByForm(false, '/user/'.$user->id);
			}
		}

		$this->responseByForm('wrong_email_or_password', '/', false);
	}

	/**
	 * Регистрация
	 */
	public function registration()
	{
		if (AuthManager::loggedIn())
		{
			$this->responseByForm('you_logged_user', '/', false);
		}

		if (!isset($_POST['email'], $_POST['password'], $_POST['password2']))
		{
			$this->responseByForm('registration_form_is_invalid', '/', false);
		}

		if (isset($_POST['email']) && !empty($_POST['email']) && !Validate::isEmail($_POST['email']))
		{
			$this->responseByForm('email_is_invalid', '/', false);
		}

		if (empty($_POST['password']))
		{
			$this->responseByForm('empty_password', '/', false);
		}

		$emaillen = strlen($_POST['email']);
		if ($emaillen < 3 || $emaillen > 64)
		{
			$this->responseByForm('email_strlen_error', '/', false);
		}

		$passwordlen = strlen($_POST['password']);
		if ($passwordlen < 3 || $passwordlen > 64)
		{
			$this->responseByForm('password_strlen_error', '/', false);
		}

		if ($_POST['password'] != $_POST['password2'])
		{
			$this->responseByForm('passwords_do_not_match', '/', false);
		}

		if (isset($_POST['first_name']) && (strlen($_POST['first_name']) > 64 || preg_match(
					"/[^0-9A-Za-zА-Яа-яЭэЁёЄєЇїІі’_-]/u",
					$_POST['first_name']
				))
		)
		{
			$this->responseByForm('registration_form_is_invalid', '/', false);
		}
		if (isset($_POST['last_name']) && (strlen($_POST['last_name']) > 64 || preg_match(
					"/[^0-9A-Za-zА-Яа-яЭэЁёЄєЇїІі’_-]/u",
					$_POST['last_name']
				))
		)
		{
			$this->responseByForm('registration_form_is_invalid', '/', false);
		}

		$userFieldsRequired = [
			'first_name' => '',
			'last_name'  => ''
		];

		try
		{
			$userFields = $this->server->getHelper()->getArrayDataHelper()->filterOnlyRequiredParams(
				$_POST,
				$userFieldsRequired
			);

			$user = UserManager::create(
				$_POST['email'],
				md5((string)$_POST['password'].$this->server->getContainer('config')->get('salt')),
				$userFields
			);
			if ($user !== null)
			{
				$user->save();
				//				$this->responseByForm('you_done_login', '/');
				$this->server->emit(self::EVENT_CONTROLLER_USER_CREATE, [$user]);
				$this->responseByForm(false, '/');
			}

			$this->responseByForm('unsuccess_login', '/user/'.$user->id, false);
		}
		catch (\Exception $e)
		{
			FastLog::add(
				'EXCEPTION',
				[
					'code'    => (int)$e->getCode(),
					'message' => (string)$e->getMessage(),
					'file'    => (string)$e->getFile(),
					'line'    => (int)$e->getLine()
				],
				false,
				true
			);
			$this->responseByForm($e->getMessage(), '/');
		}
	}

	public function editProfile()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$email = $this->request->getGetOrPost('email', '');
		if (!empty($email) && !Validate::isEmail($email))
		{
			$this->responseByForm('email_is_invalid', '/', false);
		}

		$emaillen = strlen($email);
		if (!empty($email) && ($emaillen < 3 || $emaillen > 64))
		{
			$this->responseByForm('email_strlen_error', '/', false);
		}

		$password = $this->request->getGetOrPost('password', '');
		$password2 = $this->request->getGetOrPost('password2', '');
		$passlen = strlen($password);
		if (!empty($password) && ($passlen < 4 || $passlen > 64))
		{
			$this->responseByForm('password_is_too_short', '/', false);
		}

		if (!empty($password) && $password != $password2)
		{
			$this->responseByForm('passwords_do_not_match', '/', false);
		}

		try
		{
			$user = UserManager::getUserModelByUserId(AuthManager::getSessionUserId());
			if (!empty($password))
			{
				$user->password = md5((string)$password.$this->server->getContainer('config')->get('salt'));
			}
			if (!empty($email) && $user->email !== $email)
			{
				$userAcc = UserManager::getUserByEmail($email);
				if ($userAcc->isLoadedObject() && $user->email !== $userAcc->email)
				{
					$this->responseByForm('email_already_exist', '/', false);
				}
				$user->email = $email;
			}

			if (isset($_POST['first_name']) && (strlen($_POST['first_name']) > 64 || preg_match(
						"/[^0-9A-Za-zА-Яа-яЭэЁёЄєЇї’_-]/u",
						$_POST['first_name']
					))
			)
			{
				$this->responseByForm('registration_form_is_invalid', '/', false);
			}
			if (isset($_POST['last_name']) && (strlen($_POST['last_name']) > 64 || preg_match(
						"/[^0-9A-Za-zА-Яа-яЭэЁёЄєЇї’_-]/u",
						$_POST['last_name']
					))
			)
			{
				$this->responseByForm('registration_form_is_invalid', '/', false);
			}

			$userFieldsRequired = [
				'first_name' => '',
				'last_name'  => '',
			    'gender' => 0
			];
			$userFields = ArrayDataHelper::filterOnlyRequiredParams(
				$_POST,
				$userFieldsRequired
			);
			//			$userFields['full_name'] = $userFields['last_name'].' '.$userFields['first_name'];
			$user->setFieldsArray($userFields);
			$birthday = $this->request->getGetOrPost('birthday');
			$dataArray = explode('/', $birthday);
			$birthD = (string)isset($dataArray[0]) ? $dataArray[0] : '';
			$birthM = (string)isset($dataArray[1]) ? $dataArray[1] : '';
			$birthY = (string)isset($dataArray[2]) ? $dataArray[2] : '';
			if (checkdate((int)$birthM, (int)$birthD, (int)$birthY))
			{
				$user->birth_y = $birthY;
				$user->birth_m = $birthM;
				$user->birth_d = $birthD;
			}

			$user->save();
			$this->server->emit(self::EVENT_CONTROLLER_USER_UPDATE, [$user]);

			$this->responseByForm('success_update', '/user/'.$user->id);
		}
		catch (\Exception $e)
		{
			FastLog::add(
				'EXCEPTION',
				[
					'code'    => (int)$e->getCode(),
					'message' => (string)$e->getMessage(),
					'file'    => (string)$e->getFile(),
					'line'    => (int)$e->getLine()
				],
				false,
				true
			);
			$this->responseByForm($e->getMessage(), '/', false);
		}
	}

	/**
	 * Выход
	 */
	public function logout()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		UserManager::logout();
		$this->responseByForm(false, '/', true);
	}

	public function recoverPassword()
	{
		if (AuthManager::loggedIn())
		{
			$this->responseByForm('you_logged_user', '/', false);
		}

		$email = $this->request->getGetOrPost('email', '');
		$langName = $this->request->getGetOrPost('langName', '');
		$user = UserManager::getUserByEmail($email);
		if ($user->isLoadedObject())
		{
			$message = new MessagePrepare('recoverPassword', $langName);

			$email = $user->email;
			if (!empty($email) && Validate::isEmail($email))
			{
				// Ответ пользователю
				$this->responseByForm('success_send_password', '/', true, false);

				$password = uniqid();
				$user->password = md5((string)$password.'amxcgu-0)_(MU_XU#_XUR aejrhn37840xnxajhgt78');
				//				$code = sha1(uniqid());

				$sendManager = new SendEmailManager($this->server->getContainer('config'));
				$sendManager->addAddress($email, $user->first_name);
				$userdata = $user->getFieldsSanitized();
				$userdata['password'] = $password;
				$sendManager->send(
					$message->getSubject($userdata),
					$message->getMessage($userdata),
					true,
					'',
					3,
					50
				);

				$user->save();
			}
			else
			{
				$this->responseByForm('not_found_correct_mail', '/', false);
			}
		}
		$this->responseByForm('not_found_correct_user', '/user/'.$user->id, false);
	}

	public function removeUser()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$userId = AuthManager::getSessionUserId();
		$user = UserManager::getUserModelByUserId($userId);
		$manager = new UserManager();
		$manager->removeAllLink($user);
		$this->server->emit(self::EVENT_CONTROLLER_USER_REMOVE, [$user]);

		UserManager::logout();
		$user->delete();
	}
}

<?php
namespace MyPet\Users\Controller;

use KMCore\Helper\ArrayDataHelper;
use KMCore\Helper\ArrayException;
use MyPet\ApiController;
use MyPet\Users\Classes\ProgressBar;
use MyPet\Users\Classes\UserManager;
use MyPet\Users\Model\ProfessionModel;
use MyPet\Users\Model\User;

class ClientApiController extends ApiController
{
	protected $dataFields = [
		'photo'          => '',
		'first_name'     => null,
		'last_name'      => null,
		'profession'     => '',
		'isProf'         => false,
		'organizations'  => [],
		'sites'          => [],
		'phones'         => [],
		'career'         => '',
		'specialization' => '',
		'country'        => '',
		'city'           => '',
		'tags'      => [],
		'location'       => [],
	];

	public function setUser()
	{
		$this->dataFields['organizations'] = $this->dataFields['sites'] = $this->dataFields['phones'] = function ($key, $value)
		{
			if (!is_array($value))
			{
				throw new ArrayException('name must be array');
			}
			foreach ($value as $k => $item)
			{
				if (empty($item['name']))
				{
					unset($value[$k]);
				}
			}
			$value = array_values($value);

			return $value;
		};
		$this->dataFields['profession'] = function ($key, $value)
		{
			if (isset($value['_id']))
			{
				return (string)$value['_id'];
			}

			return '';
		};
		$this->dataFields['location'] = function ($key, $value)
		{
			if (isset($value['formated'], $value['detailes']['geometry']))
			{
				return ['formated' => $value['formated'], 'detailes' => ['geometry' => $value['detailes']['geometry']]];
			}

			return [];
		};
		$this->dataFields['tags'] = function ($key, $value)
		{
			$value = trim($value);
			$result = explode(',', $value);
			foreach($result as &$item)
			{
				$item = trim($item);
			}

			return $result;
		};

		$userInfo = $this->request->getGetOrPost('userInfo', []);
		if(isset($userInfo['interests']))
		{
			$userInfo['tags'] = $userInfo['interests'];
		}
		$userInfo = ArrayDataHelper::filterOnlyRequiredParams($userInfo, $this->dataFields);

		if(isset($userInfo['detailes']['geometry']['location']) && is_array($userInfo['detailes']['geometry']['location']))
		{
			$userInfo['locs'] = array_values($userInfo['detailes']['geometry']['location']);
		}

		$this->user->setFieldsArray($userInfo);

		// FIXME ProgressBar
		$progress = new ProgressBar($this->user);
		$this->user->progress = $progress->calc();

		$this->user->save();

		UserManager::updateToSearch($this->user);


		$this->success = true;
		$this->response();
	}

	public function updateProgress()
	{
		$progress = new ProgressBar($this->user);
		$this->user->progress = $progress->calc();
		$this->user->save();

		$this->response->json(['progress' => $this->user->progress]);

		$this->success = true;
		$this->data['progress'] = $this->user->progress;
		$this->response();
	}

	public function getProgressBarInfo()
	{
		$progress = new ProgressBar($this->user);
		$progressInfo = $progress->getInfoField();
		$this->success = true;
		$this->data['info'] = $progressInfo;
		$this->response();
	}

	public function getUsers()
	{
		$userIds = (array)$this->request->getGetOrPost('userIds', []);
		$userIds = array_slice($userIds, 0, 100);

		$this->success = true;
		$this->data['users'] = User::getAllAdvanced(['id' => ['$in' => $userIds]]);
		$this->response();
	}
}
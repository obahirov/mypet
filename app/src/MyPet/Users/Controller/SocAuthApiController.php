<?php
namespace MyPet\Users\Controller;

use KMCore\Helper\GeneratorHelper;
use KMCore\Helper\SessionStorage;
use KMCore\LogSystem\FastLog;
use KMCore\SocialApi\Controller\BaseLogin;
use KMCore\SocialApi\SocialNetworks\AuthBase;
use KMCore\SocialApi\SocialUser;
use MyPet\Referrals\Classes\ReferralHandler;
use MyPet\TokenAuth\SecureTokenManager;
use MyPet\Users\Classes\UserManager;
use MyPet\Users\Model\SocUserModel;
use MyPet\Users\Model\User;

class SocAuthApiController extends BaseLogin
{
	public function __construct()
	{
		parent::__construct();

		$this->server->on(
			AuthBase::EVENT_GET_ACCESS_TOKEN,
			function ($token, $params, $sn)
			{
				SessionStorage::set('socAuthData', $params, $sn);
				SessionStorage::set('accessToken', $token);
				FastLog::add('DEBUG', [$params], false, true);
			}
		);

		$this->server->on(
			BaseLogin::EVENT_AUTH_READY,
			function (SocialUser $socialUser, $sn)
			{
				$socUser = new SocUserModel(['sn' => $sn, 'socId' => $socialUser->getSocId()]);
				if (!$socUser->isLoadedObject())
				{
					$socUser->id = GeneratorHelper::getStringUniqueId();
					$socUser->sn = $sn;
					$socUser->socId = $socialUser->getSocId();
				}

				$socUser->email = $socialUser->getEmail();
				$socUser->first_name = $socialUser->getFirstName();
				$socUser->last_name = $socialUser->getLastName();
				$socUser->gender = $socialUser->getGender();
				$socUser->city = $socialUser->getCity();
				$socUser->country = $socialUser->getCountry();
				$socUser->birth_y = $socialUser->getBirthY();
				$socUser->birth_m = $socialUser->getBirthM();
				$socUser->birth_d = $socialUser->getBirthD();
				$socUser->locale = $socialUser->getLocale();
				$socUser->timezone = $socialUser->getTimezone();
				$socUser->photo = $socialUser->getPhoto();
				$socUser->save();

				$user = new User(['socUserId' => $socUser->id]);
				if (!$user->isLoadedObject())
				{ // Пользователя с таким идентификатором не существует
					$socUser->email = strtolower($socUser->email);
					if (!empty($socUser->email))
					{
						$user = new User(['email' => $socUser->email]);
						if (!$user->isLoadedObject())
						{
							$user->id = GeneratorHelper::getStringUniqueId();
							$user->email = $socUser->email;
						}
					}
					// Почта нигде не использовалась ранее, смело устанавливаем её
					$user->created = time();
					$user->ip_install = $_SERVER['REMOTE_ADDR'];
					$user->socUserId = $socUser->id;
				}

				if ($socUser->gender === 'male')
				{
					$gender = 1;
				}
				elseif ($socUser->gender === 'female')
				{
					$gender = 2;
				}
				else
				{
					$gender = 0;
				}

				$userFields = [
					'first_name' => $socUser->first_name,
					'last_name' => $socUser->last_name,
					'gender' => $gender,
					'city' => $socUser->city,
					'user_country' => $socUser->country,
					'birth_y' => $socUser->birth_y,
					'birth_m' => $socUser->birth_m,
					'birth_d' => $socUser->birth_d,
					'photo' => $socUser->photo,
					'small_photo' => $socUser->photo,
				];

				$user->setFieldsArray($userFields);
				UserManager::auth($user);
				$user->save();

				ReferralHandler::registration($user->id);

				UserManager::getTokenData($this->server, $user);
			}
		);
	}
}
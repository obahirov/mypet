<?php
namespace MyPet\Users\Controller;

use MyPet\ApiController;
use MyPet\Users\Model\Online;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class OnlineController extends ApiController
{
	public function ping()
	{
		$online = new Online($this->user->id);
		if (!$online->isLoadedObject())
		{
			$online->id = $this->user->id;
		}
		$online->online = true;
		$online->time = time();
		$online->save();

		$this->success = true;
		$this->response();
	}
}
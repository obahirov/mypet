<?php
namespace MyPet\Users\Model;

use KMCore\DB\ObjectModel;

/**
 * Модель пользователя, наследуеться от базовой модели
 *
 * @property mixed online
 * @property mixed time
 * @package MyPet\UserService\Users\Model
 */
class Online extends ObjectModel
{
	protected static $collection = 'mp_online_model';
	protected static $safeWrite = true;
	protected static $sort = ['id' => 1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['id' => 1, 'online' => 1],
		],
		[
			'keys' => ['time' => 1],
		],
	];

	protected static $fieldsDefault = [
		'id'     => '',
		'online' => false,
		'time'   => 0,
	];

	protected static $fieldsValidate = [
		'id'     => self::TYPE_STRING,
		'online' => self::TYPE_BOOL,
		'time'   => self::TYPE_TIMESTAMP,
	];

	/**
	 * @param array $ids
	 * @return int
	 */
	public static function getOnlineUsersByArray(Array $ids)
	{
		return Online::count(['id' => ['$in' => $ids], 'online' => true]);
	}
}
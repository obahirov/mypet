<?php
namespace MyPet\Users\Model;

use KMCore\DB\ObjectModel;

/**
 * Модель пользователя, наследуеться от базовой модели
 *
 * @property mixed id
 * @property mixed login_count
 * @property mixed country
 * @property mixed lastlog
 * @property mixed ip
 * @property mixed password
 * @property mixed email
 * @property mixed created
 * @property mixed ip_install
 * @property mixed username
 * @property mixed socUserId
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed gender
 * @property mixed user_country
 * @property mixed city
 * @property mixed birth_y
 * @property mixed birth_m
 * @property mixed birth_d
 * @property mixed balance
 * @property mixed ref_install
 * @property mixed isSpecialist
 * @property mixed message
 * @property mixed date
 */
class HistoryConnectionDevelopers extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'connection_developers';
	protected static $pk = '_id';
	protected static $indexes = [
	];

	protected static $fieldsDefault = [
		'_id'     => '',
		'message' => '',
		'date'    => 0,
	];

	protected static $fieldsValidate = [
		'_id'     => self::TYPE_MONGO_ID,
		'message' => self::TYPE_STRING,
		'date'    => self::TYPE_TIMESTAMP,
	];
}
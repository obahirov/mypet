<?php
namespace MyPet\Users\Model;

use KMCore\DB\ObjectModel;

/**
 * Модель пользователя, наследуеться от базовой модели
 *
 * @property mixed id
 * @property mixed login_count
 * @property mixed country
 * @property mixed lastlog
 * @property mixed ip
 * @property mixed password
 * @property mixed email
 * @property mixed created
 * @property mixed ip_install
 * @property mixed username
 * @property mixed socUserId
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed gender
 * @property mixed user_country
 * @property mixed city
 * @property mixed birth_y
 * @property mixed birth_m
 * @property mixed birth_d
 * @property mixed balance
 * @property mixed ref_install
 * @property mixed en
 */
class ProfessionModel extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'mp_profession';
	protected static $pk = '_id';
	protected static $sort = ['priority' => -1];
	protected static $indexes = [
		[
			'keys' => ['priority' => -1]
		],
	];

	protected static $fieldsDefault = [
		'_id'      => '',
		'en'       => '',
		'ua'       => '',
		'ru'       => '',
		'priority' => 0,
	];

	protected static $fieldsValidate = [
		'_id'      => self::TYPE_STRING,
		'en'       => self::TYPE_STRING,
		'ua'       => self::TYPE_STRING,
		'ru'       => self::TYPE_STRING,
		'priority' => self::TYPE_UNSIGNED_INT,
	];
}
<?php
namespace MyPet\Users\Model;

use KMCore\DB\ObjectModel;

/**
 * @property mixed id
 * @property mixed socId
 * @property mixed sn
 * @property mixed email
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed gender
 * @property mixed city
 * @property mixed country
 * @property mixed birth_y
 * @property mixed birth_m
 * @property mixed birth_d
 * @property mixed photo
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SocUserModel extends ObjectModel
{
	protected static $collection = 'hr_soc_user';
	protected static $pk = 'id';
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys'   => ['sn' => 1, 'socId' => 1],
			'unique' => true,
		],
		[
			'keys' => ['socId' => 1],
		]
	];

	protected static $fieldsDefault = [
		'id'         => '',
		'sn'         => '',
		'socId'      => '',
		'email'      => '',
		'first_name' => '',
		'last_name'  => '',
		'gender'     => '',
		'city'       => '',
		'country'    => '',
		'birth_y'    => '',
		'birth_m'    => '',
		'birth_d'    => '',
		'username'   => '',
		'locale'     => '',
		'timezone'   => '',
		'photo'      => '',
	];

	protected static $fieldsValidate = [
		'id'         => self::TYPE_STRING,
		'sn'         => self::TYPE_STRING,
		'socId'      => self::TYPE_STRING,
		'email'      => self::TYPE_STRING,
		'first_name' => self::TYPE_STRING,
		'last_name'  => self::TYPE_STRING,
		'gender'     => self::TYPE_STRING,
		'city'       => self::TYPE_STRING,
		'country'    => self::TYPE_STRING,
		'birth_y'    => self::TYPE_STRING,
		'birth_m'    => self::TYPE_STRING,
		'birth_d'    => self::TYPE_STRING,
		'username'   => self::TYPE_STRING,
		'locale'     => self::TYPE_STRING,
		'birthday'   => self::TYPE_STRING,
		'timezone'   => self::TYPE_STRING,
		'photo'      => self::TYPE_STRING,
	];
}
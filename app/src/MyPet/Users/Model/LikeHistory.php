<?php
namespace MyPet\Users\Model;

use KMCore\DB\ObjectModel;

/**
 * Модель пользователя, наследуеться от базовой модели
 *
 * @property mixed id
 * @property mixed login_count
 * @property mixed country
 * @property mixed lastlog
 * @property mixed ip
 * @property mixed password
 * @property mixed email
 * @property mixed created
 * @property mixed ip_install
 * @property mixed username
 * @property mixed socUserId
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed gender
 * @property mixed user_country
 * @property mixed city
 * @property mixed birth_y
 * @property mixed birth_m
 * @property mixed birth_d
 * @property mixed phone
 * @property mixed balance
 * @property mixed ref_install
 * @property mixed isSpecialist
 * @property mixed photo
 * @property mixed region
 * @property mixed likes
 * @property mixed profession
 */
class LikeHistory extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'like_history';
	protected static $pk = '_id';
	protected static $indexes = [
		[
			'keys'   => ['id' => 1, 'likeId' => 1],
			'unique' => true,
		],
		[
			'keys' => ['likeId' => 1],
		],
		[
			'keys' => ['type' => 1],
		]
	];

	protected static $fieldsDefault = [
		'_id'    => '',
		'id'     => '',
		'likeId' => '',
		'type'   => ''
	];

	protected static $fieldsValidate = [
		'_id'    => self::TYPE_MONGO_ID,
		'id'     => self::TYPE_STRING,
		'likeId' => self::TYPE_STRING,
		'type'   => self::TYPE_STRING,
	];
}
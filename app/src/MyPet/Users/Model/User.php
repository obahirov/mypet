<?php
namespace MyPet\Users\Model;

use KMCore\DB\ObjectModel;

/**
 * Модель пользователя, наследуеться от базовой модели



*
*@property mixed id
 * @property mixed login_count
 * @property mixed country
 * @property mixed lastlog
 * @property mixed ip
 * @property mixed password
 * @property mixed email
 * @property mixed created
 * @property mixed ip_install
 * @property mixed username
 * @property mixed socUserId
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed gender
 * @property mixed user_country
 * @property mixed city
 * @property mixed birth_y
 * @property mixed birth_m
 * @property mixed birth_d
 * @property mixed phone
 * @property mixed balance
 * @property mixed ref_install
 * @property mixed isSpecialist
 * @property mixed photo
 * @property mixed region
 * @property mixed likes
 * @property mixed profession
 * @property mixed isProf
*/
class User extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'site_user';
	protected static $pk = 'id';
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['email' => 1],
		],
		[
			'keys' => ['socUserId' => 1],
		],
		[
			'keys' => ['profession' => 1],
		],
		[
			'keys' => ['locs' => "2d"],
		],
		[
			'keys' => ['tags' => 1],
		]
	];

	protected static $fieldsDefault = [
		'id'             => '',
		'photo'          => '',
		'first_name'     => '',
		'last_name'      => '',
		'profession'     => '',
		'isProf'         => '',
		'organizations'  => [],
		'sites'          => [],
		'phones'         => [],
		'career'         => '',
		'country'        => '',
		'city'           => '',
		'tags'      => [],
		'location'       => [],
		'email'          => '',

		'created'        => 0,
		'lastlog'        => 0,
		'login_count'    => 0,
		'ip'             => '',
		'ip_install'     => '',
		'password'       => '',
		'isDev'          => false,

		'socUserId'      => '',
		'gender'         => '',

		'small_photo'    => '', //45x45
		'src_photo'      => '',
		'cover'          => '',
		'src_cover'      => '',

		'birth_y'        => '',
		'birth_m'        => '',
		'birth_d'        => '',
		'progress'       => 0,
		'locs'           => [],
	];

	protected static $fieldsValidate = [
		'id'             => self::TYPE_STRING,
		'photo'          => self::TYPE_STRING,
		'first_name'     => self::TYPE_STRING,
		'last_name'      => self::TYPE_STRING,
		'profession'     => self::TYPE_STRING,
		'isProf'         => self::TYPE_STRING,
		'organizations'  => self::TYPE_JSON,
		'sites'          => self::TYPE_JSON,
		'phones'         => self::TYPE_JSON,
		'career'         => self::TYPE_STRING,
		'country'        => self::TYPE_STRING,
		'city'           => self::TYPE_STRING,
		'tags'      => self::TYPE_JSON,
		'location'       => self::TYPE_JSON,
		'email'          => self::TYPE_STRING,

		'created'        => self::TYPE_TIMESTAMP,
		'lastlog'        => self::TYPE_TIMESTAMP,
		'login_count'    => self::TYPE_UNSIGNED_INT,
		'ip'             => self::TYPE_STRING,
		'ip_install'     => self::TYPE_STRING,
		'password'       => self::TYPE_STRING,
		'isDev'          => self::TYPE_BOOL,

		'socUserId'      => self::TYPE_STRING,
		'gender'         => self::TYPE_STRING,

		'small_photo'    => self::TYPE_STRING, //45x45
		'src_photo'      => self::TYPE_STRING,
		'cover'          => self::TYPE_STRING,
		'src_cover'      => self::TYPE_STRING,

		'birth_y'        => self::TYPE_STRING,
		'birth_m'        => self::TYPE_STRING,
		'birth_d'        => self::TYPE_STRING,
		'progress'       => self::TYPE_UNSIGNED_INT,
		'locs'           => self::TYPE_JSON,
	];

	public static $fieldsSearch = [
		'id',
		'email',
		'first_name',
		'last_name',
		'gender',
		'sites',
		'phones',
		'photo',
		'small_photo',
		'tags',
		'profession',
		'locs',
		'location'
	];

	public static $fieldsPrivate = [
		'src_photo',
		'src_cover',
		'password',
		'isDev'
	];
}
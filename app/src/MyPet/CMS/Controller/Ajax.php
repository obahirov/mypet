<?php
namespace MyPet\CMS\Controller;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Controller\JsonController;
use KMCore\Config\Config;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;
use MyPet\Users\Model\User;

/**
 * Контроллер по умолчанию
 */
class Ajax extends JsonController
{
	const EVENT_CONTROLLER_AFTER_USERID = 'event.controller.after.userId';

	/**
	 * Инициализация гостя
	 */
	public function __construct()
	{
		parent::__construct();
		if (!(bool)$this->server->getContainer('config')->get('enable', 'cms'))
		{
			exit('Site is disable');
		}
		UserManager::initUserId();
	}

	/**
	 * @param array  $data
	 * @param string $renderFile
	 * @return string
	 */
	protected function response(Array $data = [], $renderFile = '')
	{
		$data['loggedIn'] = AuthManager::loggedIn();
		$data['userId'] = AuthManager::getSessionUserId();
		$data['email'] = AuthManager::getSessionEmail();

		$data['messages'] = $this->server->getContainer('helperFactory')->getRedirectHelper()->showMessage();
		$data['errors'] = $this->server->getContainer('helperFactory')->getRedirectHelper()->showErrors();

		$user = new User(['id' => (string)$data['userId']]);
		if ($user->isLoadedObject())
		{
			$data['user']['first_name'] = $user->first_name;
			$data['user']['last_name'] = $user->last_name;
			$data['user']['photo'] = $user->photo;
			$data['user']['id'] = $user->id;
			$data['user']['profession'] = $user->profession;
		}

		$userPageId = $this->request->getRequest('userId', '');
		if (empty($userPageId))
		{
			$userPageId = $this->request->getRequest('type', '');
		}
		$data['isYou'] = (empty($userPageId) || $data['userId'] === $userPageId);

		if (!empty($userPageId))
		{
			$user = new User(['id' => (string)$userPageId]);
			if ($user->isLoadedObject())
			{
				$data['userPageId'] = $userPageId;
				$data['author']['first_name'] = $user->first_name;
				$data['author']['last_name'] = $user->last_name;
				$data['author']['photo'] = $user->photo;
				$data['author']['id'] = $user->id;
				$data['author']['profession'] = $user->profession;
			}
		}

		foreach ($data['messages'] as &$item)
		{
			$item = $this->server->getContainer('config')->loadFile('translate')->get(
				PageVariable::getLangName(),
				$item,
				Config::TYPE_MAIN,
				$item
			);
		}
		foreach ($data['errors'] as &$item)
		{
			$item = $this->server->getContainer('config')->loadFile('translate')->get(
				PageVariable::getLangName(),
				$item,
				Config::TYPE_MAIN,
				$item
			);
		}

		parent::response($data, $renderFile);
	}
}

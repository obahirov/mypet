<?php
namespace MyPet\CMS\Controller;

use KMCore\BaseController;
use KMCore\DB\Elasticsearch\Common\Exceptions\UnexpectedValueException;
use KMCore\DB\Elasticsearch\ElasticsearchManager;
use KMCore\Helper\ArrayDataHelper;
use MyPet\CMS\Classes\Plugins\Page\AboutPet;
use MyPet\Location\Model\City;
use MyPet\Pets\Model\BreedModel;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SearchHelper extends BaseController
{
	public function findMixed()
	{
		if (!AuthManager::loggedIn())
		{
			throw new UnexpectedValueException('Not auth user');
		}

		$userId = AuthManager::getSessionUserId();

		$langName = $this->server->getContainer('sessionStorage')->get('langName');

		$perPage = 10;
		$from = (int)$this->request->getGetOrPost('from', 0);
		$query = $this->request->getGetOrPost('query', '', true);
		$params = [];
		$params['index'] = 'mypet';
		$params['size'] = $perPage;
		$params['from'] = $from;
		$query_string['query'] = $query;
		$query_string['fields'] = ["nick", "hNick", "email", "*_name"];
		$params['body']['query']['query_string'] = $query_string;

		$data = ElasticsearchManager::getInstance()->search($params);

		$breeds = [];
		if (isset($data['hits']['hits']))
		{
			foreach ($data['hits']['hits'] as &$item)
			{
				if ($item['_type'] === 'pet')
				{
					$item['_source']['age'] = AboutPet::getAgeToRender($item['_source'], $langName);
					if (isset($item['_source']['breed']) && !empty($item['_source']['breed']))
					{
						try
						{
							$breeds[] = new \MongoId($item['_source']['breed']);
						}
						catch (\Exception $e)
						{

						}
					}

				}
			}

			$breeds = BreedModel::getAllAdvanced(['_id' => ['$in' => $breeds]]);
			$breeds = ArrayDataHelper::arrayByOneKey($breeds, '_id');

			foreach ($data['hits']['hits'] as $k => &$item)
			{
				if ($item['_type'] === 'user')
				{
					$item['sort'] = $k;
				}
				elseif ($item['_type'] === 'pet')
				{
					$item['_source']['render_breed'] = '';
					if (isset($item['_source']['breed']))
					{
						$breed = $item['_source']['breed'];
						if (isset($breeds[$breed][$langName]))
						{
							$item['_source']['render_breed'] = $breeds[$breed][$langName];
						}
						elseif (isset($breeds[$breed]))
						{
							$item['_source']['render_breed'] = $breeds[$breed]['en'];
						}
					}

					$isYourPet = (array_search($userId, $item['_source']['ownerIds']) !== false);
					$item['sort'] = $k;
					if ($isYourPet)
					{
						$item['sort'] += 1000;
					}
				}
			}
			ArrayDataHelper::keyMultiSort($data['hits']['hits'], 'sort', true, false, false);
		}
		$this->response->json($data);
	}
} 
<?php
namespace MyPet\CMS\Classes\Plugins\Module;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\RedirectManager;
use KMCore\ServerContainer;
use MyPet\Inbox\Model\Requests;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class CheckStatus implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		if(AuthManager::loggedIn())
		{
			RedirectManager::redirect('/user');
		}
	}
} 
<?php
namespace MyPet\CMS\Classes\Plugins\Module;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Message implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$friendId = $server->getContainer('request')->getRequest('friendId');
		$dialogId = $server->getContainer('request')->getRequest('dialogId');
		$component['data']['dialogId'] = $dialogId;
		$component['data']['friendId'] = $friendId;

		$user = new User(['id' => (string)$friendId]);
		if (!$user->isLoadedObject())
		{
			throw new PageNotFound();
		}
		$component['data']['profile'] = $user->getFieldsSanitized();
	}
} 
<?php
namespace MyPet\CMS\Classes\Plugins\Module;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Location\Model\Country;
use MyPet\Users\Model\ProfessionModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Search implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$component['data']['countrys'] = Country::getAllAdvanced([], [], true, true);
		$component['data']['langName'] = PageVariable::getLangName();
		$component['data']['professions'] = ProfessionModel::getAllAdvanced([], [], true, ['priority' => 1]);
	}
} 
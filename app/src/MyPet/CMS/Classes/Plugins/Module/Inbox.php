<?php
namespace MyPet\CMS\Classes\Plugins\Module;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Inbox\Model\Requests;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Inbox implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = AuthManager::getSessionUserId();
		$component['data']['count_inbox'] = Requests::count(
			['type' => ['$ne' => 'new_message'], 'collected' => false, 'userId' => $userId]
		);
		$component['data']['count_message'] = Requests::count(
			['type' => 'new_message', 'collected' => false, 'userId' => $userId]
		);
	}
} 
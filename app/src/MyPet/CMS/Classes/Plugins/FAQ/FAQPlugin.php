<?php
namespace MyPet\CMS\Classes\Plugins\FAQ;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FAQPlugin implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$langName = PageVariable::getLangName();
		$component['data']['faqs'] = FAQModel::getAllAdvanced(['lang' => $langName], [], false, true);
	}
} 
<?php
namespace MyPet\CMS\Classes\Plugins\FAQ;

use KMCore\DB\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FAQModel extends ObjectModel
{
	/** @var string $collection Название модели */
	protected static $collection = 'cms_faq_model';
	protected static $slaveOkay = true;
	/** @var string $pk первичный ключ */
	protected static $pk = '_id';
	/** @var array $sort параметры сортировки */
	protected static $sort = ['priority' => -1];
	/** @var array $indexes индексы коллекции */
	protected static $indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true
		],
		[
			'keys' => ['lang' => 1],
		],
		[
			'keys' => ['priority' => -1]
		],
		[
			'keys' => ['type' => 1]
		],
		[
			'keys' => ['group' => 1]
		]
	];

	/** @var array $fieldsDefault список значений коллекции */
	protected static $fieldsDefault = [
		'_id'      => '',
		'lang'     => '',
		'priority' => 0,
		'title'    => '',
		'text'     => '',
		'type'     => [],
	];
	/** @var array $fieldsValidate типы значений для валидации */
	protected static $fieldsValidate = [
		'_id'      => self::TYPE_MONGO_ID,
		'lang'     => self::TYPE_STRING,
		'priority' => self::TYPE_UNSIGNED_INT,
		'title'    => self::TYPE_STRING,
		'text'     => self::TYPE_STRING,
		'type'     => self::TYPE_JSON,
	];

	protected static $fieldsHint = [
		'_id' => 'Идентификатор БД',
	];
} 
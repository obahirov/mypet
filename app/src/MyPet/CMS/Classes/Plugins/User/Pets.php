<?php
namespace MyPet\CMS\Classes\Plugins\User;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\ArrayDataHelper;
use KMCore\ServerContainer;
use MyPet\CMS\Classes\Plugins\Page\AboutPet;
use MyPet\Pets\Classes\PetManager;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Pets implements PluginsInterface
{
	/**
	 * @param ServerContainer         $server
	 * @param array                   $component
	 * @param \KMCore\CMS\Model\Pages $pageModel
	 * @param bool                    $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = $server->getContainer('request')->getRequest('userId');
		if (!isset($userId) || $userId === 'me')
		{
			$userId = AuthManager::getSessionUserId();
		}

		$petManager = new PetManager($server);

		$component['data']['isYou'] = ($userId === AuthManager::getSessionUserId());
		$pets = $petManager->getPetsByUser($userId);
		//		$component['data']['pets'] = array_slice($component['data']['pets'], 0, 4);

		$i = 0;
		$deads = [];
		foreach ($pets as $k => &$pet)
		{
			$pet['age'] = AboutPet::getAgeToRender($pet, PageVariable::getLangName());

			$pet['isDead'] = $petManager::isDead($pet);
			$pet['sort'] = $k;
			if ($pet['isDead'])
			{
				$pet['sort'] += 1000;
			}

			if ($pet['isDead'] === false)
			{
				$i++;
				$component['data']['pets'][$k] = $pet;
			}
			else
			{
				$deads[] = $pet;
			}

			if ($i >= 4)
			{
				break;
			}
		}

		if ($i < 4)
		{
			while ($i < 4 && !empty($deads))
			{
				$component['data']['pets'][] = array_shift($deads);
				$i++;
			}
		}

		if (!empty($component['data']['pets']) && is_array($component['data']['pets']))
		{
			ArrayDataHelper::keyMultiSort($component['data']['pets'], 'sort', false, false, false);
		}
	}
} 
<?php
namespace MyPet\CMS\Classes\Plugins\User;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Owners implements PluginsInterface
{
	/**
	 * @param ServerContainer         $server
	 * @param array                   $component
	 * @param \KMCore\CMS\Model\Pages $pageModel
	 * @param bool                    $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = $server->getContainer('request')->getRequest('userId');
		if (!isset($userId) || $userId === 'me')
		{
			$userId = AuthManager::getSessionUserId();
		}

		$petId = $server->getContainer('request')->getRequest('petId');

		if ($petId)
		{
			$pet = new PetModel($petId);
			if ($pet->isLoadedObject())
			{

				$petManager = new PetManager($server);
				$userIds = $petManager->getOwersPet($pet);
				$userIds = array_slice($userIds, 0, 4);

				$component['data']['pet'] = $pet->getFieldsArray();
				$component['data']['owners'] = InteractionOfFriends::getProfile($userIds);
				$component['data']['isYourPet'] = $petManager->isOwner($pet, $userId);
			}
		}

		if (isset($component['data']['owners']))
		{
			foreach ($component['data']['owners'] as $k => &$owner)
			{
				if (isset($owner['id']))
				{
					$owner['url'] = '/user/'.$owner['id'];
				}
				else
				{
					unset($component['data']['owners'][$k]);
				}
			}
		}
	}
} 
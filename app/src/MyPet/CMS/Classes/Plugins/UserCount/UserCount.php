<?php
namespace MyPet\CMS\Classes\Plugins\UserCount;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\DB\RedisDatabase;
use KMCore\ServerContainer;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UserCount implements PluginsInterface
{
	/**
	 * @param ServerContainer         $server
	 * @param array                   $component
	 * @param \KMCore\CMS\Model\Pages $pageModel
	 * @param bool                    $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$component['data']['online'] = mt_rand(571, 582);
		if (class_exists('Redis') && (bool)$server->getContainer('config')->get('realPeopleCount', 'page'))
		{
			$redis = RedisDatabase::getInstance();
			$redis->zAdd('online', time(), session_id());
			$component['data']['online'] = $redis->zCount('online', time() - (15 * 60), time());
		}
		$component['data']['online'] = number_format($component['data']['online'], 0, '.', ' ');
		//		REMRANGEBYSCORE guys_online -inf (<unix_timestamp-15*60>
	}

} 
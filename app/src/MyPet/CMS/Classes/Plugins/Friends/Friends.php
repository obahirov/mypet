<?php
namespace MyPet\CMS\Classes\Plugins\Friends;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\FriendsManager;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Friends implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = $server->getContainer('request')->getRequest('userId');
		if (!isset($userId) || $userId === 'me')
		{
			$userId = AuthManager::getSessionUserId();
		}

		$petId = $server->getContainer('request')->getRequest('petId');
		$petManager = new PetManager($server);
		$pet = new PetModel($petId);
		if (isset($petId) && $pet->isLoadedObject())
		{
			$component['data']['specialists'] = [];
			$specialists = $petManager->getSpecialist($pet);
			$component['data']['count'] = 0;
			foreach ($specialists['specialists'] as $element)
			{
				foreach ($element as $item)
				{
					if (isset($item['id']))
					{
						$component['data']['specialists'][$item['id']] = $item;
						$component['data']['specialists'][$item['id']]['icon'] = 1;
						$component['data']['count']++;
					}
				}
			}

//			foreach ($specialists['owners'] as $value)
//			{
//				if (isset($value['id']))
//				{
//					$component['data']['specialists'][$value['id']] = $value;
//					$component['data']['count']++;
//				}
//			}

			if (isset($specialists['ringleader']['id']))
			{
				$component['data']['specialists'][$specialists['ringleader']['id']] = $specialists['ringleader'];
				$component['data']['count']++;
			}
		}
		else
		{
			/**
			 * @var FriendsManager $friendsManager
			 */
			$friendsManager = new FriendsManager();
			$component['data']['count'] = $friendsManager->countFriends($userId, 1);
			$component['data']['friends'] = InteractionOfFriends::getProfileFriends(
				$userId,
				$friendsManager->getFriends($userId, 0, 4, 1)
			);
		}
		$component['data']['userId'] = $userId;
		$component['data']['petId'] = $petId;
		$component['data']['loggedIn'] = AuthManager::loggedIn();
	}
} 
<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\ArrayDataHelper;
use KMCore\ServerContainer;
use KMCore\SocialApi\SocialVariables;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\BreedModel;
use MyPet\Pets\Model\GagModel;
use MyPet\Pets\Model\PetModel;
use MyPet\Referrals\Classes\LinkGenerator;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Invite implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$linkGenerator = new LinkGenerator($server);
		$link = $linkGenerator->getLink(AuthManager::getSessionUserId());
		$component['data']['link'] = $link;
		$serverConfig = $server->getContainer('config');
		$component['data']['appId'] = $serverConfig->get('socApiId', SocialVariables::SN_CODE_FB, $serverConfig::TYPE_MAIN);
		$component['data']['baseUrl'] = $serverConfig->get('baseUrl');
	}
} 
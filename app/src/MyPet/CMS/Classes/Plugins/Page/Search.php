<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\DB\Elasticsearch\ElasticsearchManager;
use KMCore\Helper\ArrayDataHelper;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Location\Model\City;
use MyPet\Pets\Model\BreedModel;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Model\ProfessionModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Search implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		if (!AuthManager::loggedIn())
		{
			throw new PageNotFound('Not auth user');
		}
		$userId = AuthManager::getSessionUserId();
		$perPage = 10;
		$page = $server->getContainer('request')->getRequest('page', 0);
		$type = $server->getContainer('request')->getRequest('type', 'user');
		$query = $server->getContainer('request')->getRequest('query', '*', true);
		$component['data']['professions'] = ProfessionModel::getAllAdvanced([], [], true, ['priority' => 1]);
		$component['data']['professions'] = ArrayDataHelper::arrayByOneKey($component['data']['professions'], '_id');

		if ($query)
		{
			$params = [];
			switch ($type)
			{
				case 'user':
					$params['index'] = 'mypet';
					$params['type'] = 'user';
					$params['size'] = $perPage;
					$params['from'] = $page;
					$query_string['query'] = $query;
					$query_string['fields'] = ["email", "*_name"];
					$params['body']['query']['query_string'] = $query_string;
					break;
				case 'pet':
					$params['index'] = 'mypet';
					$params['type'] = 'pet';
					$params['size'] = $perPage;
					$params['from'] = $page;
					$query_string['query'] = $query;
					$query_string['fields'] = ["hNick", "nick"];
					$params['body']['query']['query_string'] = $query_string;
					break;
			}

			$searchResult = ElasticsearchManager::getInstance()->search($params);
			switch ($type)
			{
				case 'user':
					$users = [];
					$citys = [];
					if (isset($searchResult['hits']['hits']))
					{
						$langName = PageVariable::getLangName();
						foreach ($searchResult['hits']['hits'] as &$user)
						{
							if (isset($user['_source']))
							{
								$users[] = $user['_source'];
							}
						}

						$component['data']['users'] = $users;
						$pets = InteractionOfFriends::getPetsData($users);
						foreach ($component['data']['users'] as &$user)
						{
							foreach ($pets as $pet)
							{
								if (array_search($user['id'], $pet['ownerIds']) !== false)
								{
									$user['pets'][] = $pet;
								}
							}
						}
					}

					break;
				case 'pet':
					$pets = [];
					$citys = [];
					if (isset($searchResult['hits']['hits']))
					{
						$breeds = [];
						foreach ($searchResult['hits']['hits'] as &$pet)
						{
							if (isset($pet['_source']))
							{
								if (isset($pet['_source']['breed']) && !empty($pet['_source']['breed']))
								{
									try
									{
										$breeds[] = new \MongoId($pet['_source']['breed']);
									}
									catch (\Exception $e)
									{

									}
								}

								$pets[] = $pet['_source'];
								$citys[] = $pet['_source']['city'];
							}
						}
						$breeds = BreedModel::getAllAdvanced(['_id' => ['$in' => $breeds]]);
						$breeds = ArrayDataHelper::arrayByOneKey($breeds, '_id');
						$langName = PageVariable::getLangName();
						foreach ($pets as &$pet)
						{
							$pet['age'] = AboutPet::getAgeToRender($pet, PageVariable::getLangName());
							if (isset($pet['breed']))
							{
								$breed = $pet['breed'];
								if (isset($breeds[$breed][$langName]))
								{
									$pet['breed'] = $breeds[$breed][$langName];
								}
								elseif (isset($breeds[$breed]))
								{
									$pet['breed'] = $breeds[$breed]['en'];
								}
							}
						}

						$citys = City::getAllAdvanced(['_id' => ['$in' => $citys]]);
						foreach ($citys as $city)
						{
							$id = (string)$city['_id'];
							$citys[$id] = isset($city[$langName]) ? $city[$langName] : $city['en'];
						}

						$component['data']['pets'] = $pets;
						$users = InteractionOfFriends::getUsersData($pets);
						foreach ($component['data']['pets'] as $k => &$pet)
						{
							$city = $pet['city'];
							if (isset($citys[$city]))
							{
								$pet['render_city'] = $citys[$city];
							}

							foreach ($pet['ownerIds'] as $id)
							{
								if (isset($users[$id]))
								{
									$pet['users'][] = $users[$id];
								}
							}

							$isYourPet = (array_search($userId, $pet['ownerIds']) !== false);
							$pet['sort'] = $k;
							if ($isYourPet)
							{
								$pet['sort'] += 1000;
							}
						}
						ArrayDataHelper::keyMultiSort($component['data']['pets'], 'sort', true, false, false);
					}

					break;
			}

			$component['data']['count'] = $searchResult['hits']['total'];
			$component['data']['pages'] = (int)ceil($component['data']['count'] / $perPage);
			$component['data']['type'] = $type;
			$component['data']['query'] = $query;
			$component['data']['page'] = $page;
		}
	}
} 
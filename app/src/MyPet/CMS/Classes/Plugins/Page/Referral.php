<?php
namespace MyPet\CMS\Classes\Plugins\Page;
use KMCore\ServerContainer;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\SocialApi\SocialVariables;
use MyPet\Referrals\Classes\LinkGenerator;
use MyPet\Users\Classes\AuthManager;

/**
 * Class Referral
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Referral implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$linkGenerator = new LinkGenerator($server);
		$link = $linkGenerator->getLink(AuthManager::getSessionUserId());
		$component['data']['link'] = $link;
		$serverConfig = $server->getContainer('config');
		$component['data']['appId'] = $serverConfig->get('socApiId', SocialVariables::SN_CODE_FB, $serverConfig::TYPE_MAIN);
		$component['data']['baseUrl'] = $serverConfig->get('baseUrl');
	}
}
<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\PetModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AboutOwners implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$id = $server->getContainer('request')->getRequest('petId');
		if (isset($id))
		{
			$pet = new PetModel($id);
			if (!$pet->isLoadedObject())
			{
				throw new PageNotFound();
			}


			$petManager = new PetManager($server);

			$component['data'] = array_merge($component['data'], $petManager->getSpecialist($pet));
		}
	}
} 
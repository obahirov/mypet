<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\FriendsManager;
use MyPet\Location\Model\City;
use MyPet\Location\Model\Country;
use MyPet\Location\Model\Region;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Model\ProfessionModel;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AboutUser implements PluginsInterface
{
	protected static $guestComponents = [
		'mp_popup_settings'              => 1,
		'mp_module_progress_bar'         => 1,
		'mp_module_profile_main_actions' => 1
	];

	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = $server->getContainer('request')->getRequest('userId');
		if (!isset($userId) || $userId === 'me')
		{
			$userId = AuthManager::getSessionUserId();
		}

		$user = new User(['id' => (string)$userId]);
		if (!$user->isLoadedObject() && !isset(static::$guestComponents[$component['name']]))
		{
			throw new PageNotFound();
		}

		$langName = PageVariable::getLangName();

		$component['data']['profile'] = $user->getFieldsSanitized();
		$component['data']['profile']['friendId'] = $userId;
		$component['data']['isYouFriend'] = false;
		$component['data']['loggedIn'] = AuthManager::loggedIn();

		if (AuthManager::getSessionUserId() !== $userId)
		{
			$friendManager = new FriendsManager();
			$friend = $friendManager->getFriend(AuthManager::getSessionUserId(), $userId);
			$component['data']['isYouFriend'] = $friend->isLoadedObject();
			$component['data']['profile']['status'] = $friend->status;
			$component['data']['profile']['isYou'] = false;
		}
		else
		{
			$component['data']['profile']['isYou'] = true;
		}

		$profession = new ProfessionModel($component['data']['profile']['profession']);
		if ($profession->isLoadedObject())
		{
			if (isset($profession->$langName))
			{
				$component['data']['profile']['profession_render'] = $profession->$langName;
			}
			else
			{
				$component['data']['profile']['profession_render'] = $profession->en;
			}
		}
	}

	/**
	 * @param array $user
	 * @param       $lang
	 * @return string
	 */
	public static function getAgeToRender(Array $user, $lang)
	{
		$ageRender = '';
		if (isset($user['birth_y'], $user['birth_m'], $user['birth_d']) && $user['birth_y'] > 0)
		{
			$ageRender = $user['birth_d'].'/'.$user['birth_m'].'/'.$user['birth_y'];
		}

		return $ageRender;
	}
} 
<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AboutMe implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = AuthManager::getSessionUserId();

		$user = UserManager::getUserModelByUserId($userId);

		$component['data']['profile'] = $user->getFieldsSanitized();
		$component['data']['profile']['age'] = AboutUser::getAgeToRender(
			$component['data']['profile'],
			PageVariable::getLangName()
		);
		$component['data']['profile']['isYou'] = true;
	}
} 
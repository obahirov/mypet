<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\ArrayDataHelper;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\BreedModel;
use MyPet\Pets\Model\GagModel;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Ancestry implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = AuthManager::getSessionUserId();
		$petId = $server->getContainer('request')->getRequest('petId');
		$type = $server->getContainer('request')->getRequest('type', 'parent');
		if (!$petId)
		{
			throw new PageNotFound();
		}
		$pet = new PetModel($petId);
		if (!$pet->isLoadedObject())
		{
			throw new PageNotFound();
		}
		$me = $pet->getFieldsArray();

		$petManager = new PetManager($server);
		$component['data']['isYou'] = (int)$petManager->isOwner($pet, $userId);

		$langName = PageVariable::getLangName();
		$breeds = [];
		if (!empty($me['breed']))
		{
			try
			{
				$breeds[] = new \MongoId($me['breed']);
			}
			catch (\Exception $e)
			{

			}
		}

		$userIds = [];

		$userIds = array_merge($userIds, $pet->ownerIds);
		$userIds[] = $pet->ringleader;
		if ($type === 'child')
		{
			$childs = $petManager->getChildsPetData($pet, true);
			foreach ($childs as &$child)
			{
				if (!empty($child['breed']))
				{
					try
					{
						$breeds[] = new \MongoId($child['breed']);
					}
					catch (\Exception $e)
					{

					}
				}
				$userIds = array_merge($userIds, $child['ownerIds']);
				$userIds[] = $child['ringleader'];
			}
			$breeds = BreedModel::getAllAdvanced(['_id' => ['$in' => $breeds]]);
			$breeds = ArrayDataHelper::arrayByOneKey($breeds, '_id');
			foreach ($childs as &$child)
			{
				$breed = $child['breed'];
				if (isset($breeds[$breed][$langName]))
				{
					$child['breed_translate'] = $breeds[$breed][$langName];
				}
				elseif (isset($breeds[$breed]))
				{
					$child['breed_translate'] = $breeds[$breed]['en'];
				}
				$child['age'] = AboutPet::getAgeToRender($child, $langName);
				$child['signs'] = implode(', ', $child['signs']);
			}
			$component['data']['childs'] = $childs;
			$component['template'] = 'page_ancestry_child.html.twig';
		}
		else
		{
			$fatherParents = [];
			$motherParents = [];
			$parents = $petManager->getParentsPetData($pet, true);
			$father = new PetModel($pet->fatherId);
			if (!$father->isLoadedObject())
			{
				$father = new GagModel($pet->fatherId);
			}
			if ($father->isLoadedObject())
			{
				$fatherParents = $petManager->getParentsPetData($father, true);
			}

			$mother = new PetModel($pet->motherId);
			if (!$mother->isLoadedObject())
			{
				$mother = new GagModel($pet->motherId);
			}
			if ($mother->isLoadedObject())
			{
				$motherParents = $petManager->getParentsPetData($mother, true);
			}
			foreach ($parents as &$parent)
			{
				if (!empty($parent['breed']))
				{
					try
					{
						$breeds[] = new \MongoId($parent['breed']);
					}
					catch (\Exception $e)
					{

					}
				}
				$userIds = array_merge($userIds, $parent['ownerIds']);
				$userIds[] = $parent['ringleader'];
			}
			foreach ($fatherParents as &$fatherParent)
			{
				if (!empty($fatherParent['breed']))
				{
					try
					{
						$breeds[] = new \MongoId($fatherParent['breed']);
					}
					catch (\Exception $e)
					{

					}
				}
				$userIds = array_merge($userIds, $fatherParent['ownerIds']);
				$userIds[] = $fatherParent['ringleader'];
			}
			foreach ($motherParents as &$motherParent)
			{
				if (!empty($motherParent['breed']))
				{
					try
					{
						$breeds[] = new \MongoId($motherParent['breed']);
					}
					catch (\Exception $e)
					{

					}
				}
				$userIds = array_merge($userIds, $motherParent['ownerIds']);
				$userIds[] = $motherParent['ringleader'];
			}
			$breeds = BreedModel::getAllAdvanced(['_id' => ['$in' => $breeds]]);
			$breeds = ArrayDataHelper::arrayByOneKey($breeds, '_id');
			foreach ($parents as &$parent)
			{
				$breed = $parent['breed'];
				if (isset($breeds[$breed][$langName]))
				{
					$parent['breed_translate'] = $breeds[$breed][$langName];
				}
				elseif (isset($breeds[$breed]))
				{
					$parent['breed_translate'] = $breeds[$breed]['en'];
				}
				$parent['age'] = AboutPet::getAgeToRender($parent, $langName);
				$parent['signs'] = implode(', ', $parent['signs']);
			}
			foreach ($fatherParents as &$fatherParent)
			{
				$breed = $fatherParent['breed'];
				if (isset($breeds[$breed][$langName]))
				{
					$fatherParent['breed_translate'] = $breeds[$breed][$langName];
				}
				elseif (isset($breeds[$breed]))
				{
					$fatherParent['breed_translate'] = $breeds[$breed]['en'];
				}
				$fatherParent['age'] = AboutPet::getAgeToRender($fatherParent, $langName);
				$fatherParent['signs'] = implode(', ', $fatherParent['signs']);
			}

			foreach ($motherParents as &$motherParent)
			{
				$breed = $motherParent['breed'];
				if (isset($breeds[$breed][$langName]))
				{
					$motherParent['breed_translate'] = $breeds[$breed][$langName];
				}
				elseif (isset($breeds[$breed]))
				{
					$motherParent['breed_translate'] = $breeds[$breed]['en'];
				}
				$motherParent['age'] = AboutPet::getAgeToRender($motherParent, $langName);
				$motherParent['signs'] = implode(', ', $motherParent['signs']);
			}
			$component['data']['fatherParents'] = $fatherParents;
			$component['data']['motherParents'] = $motherParents;
			$component['data']['parents'] = $parents;
		}
		$breed = $me['breed'];
		if (isset($breeds[$breed][$langName]))
		{
			$me['breed_translate'] = $breeds[$breed][$langName];
		}
		elseif (isset($breeds[$breed]))
		{
			$me['breed_translate'] = $breeds[$breed]['en'];
		}
		$me['age'] = AboutPet::getAgeToRender($me, $langName);
		$me['signs'] = implode(', ', $me['signs']);

		$component['data']['profiles'] = InteractionOfFriends::getProfile($userIds);

		$component['data']['me'] = $me;
	}
}
<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\ArrayDataHelper;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Messages\Classes\DialogManager;
use MyPet\Messages\Classes\MessageManager;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Message implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$perPage = 20;
		$page = $server->getContainer('request')->getRequest('page', 0);
		$dialogId = $server->getContainer('request')->getRequest('dialogId');
		if (strlen($dialogId) <= 5)
		{
			$page = $dialogId;
			$dialogId = null;
		}

		$userId = AuthManager::getSessionUserId();
		if (isset($dialogId) && $dialogId)
		{
			$dialoManager = new DialogManager($server);
			$dialog = $dialoManager->get($dialogId, $userId);
			if (isset($dialog['users']))
			{
				$users = $dialog['users'];
				$usersToRequest = [];
				foreach ($users as $friendId)
				{
					$usersToRequest[] = ['friendId' => $friendId];
				}
				$component['data']['users'] = InteractionOfFriends::getProfileFriends($userId, $usersToRequest);
				$component['data']['users'] = ArrayDataHelper::arrayByOneKey($component['data']['users'], 'userId');

				$messageManager = new MessageManager($server);
				$component['data']['messages'] = $messageManager->getMessagesByDialog($dialogId, $page, $perPage);
				$component['data']['pages'] = $messageManager->countPage($dialogId, $perPage);
				$component['data']['page'] = $page;
				$component['data']['count'] = $messageManager->count($dialogId);
				$component['data']['dialogId'] = $dialogId;
				$component['data']['isMessages'] = true;
			}
			else
			{
				throw new PageNotFound();
			}
		}
		else
		{
			$messageManager = new MessageManager($server);
			$dialoManager = new DialogManager($server);
			$component['data']['dialogs'] = $dialoManager->getDialogs($userId, $page, $perPage);
			$component['data']['pages'] = $dialoManager->countPage($userId, $perPage);
			$component['data']['count'] = $dialoManager->count($userId);
			$component['data']['page'] = $page;
			$component['data']['isDialogs'] = true;

			$usersToRequest = [];
			foreach ($component['data']['dialogs'] as &$dialog)
			{
				if (!empty($dialog['users']))
				{
					foreach ($dialog['users'] as $friendId)
					{
						if ($friendId !== $userId)
						{
							$dialog['user'] = InteractionOfFriends::getProfileFriends(
								$userId,
								[['friendId' => $friendId]]
							);
							break;
						}
					}
					if (isset($dialog['user'][0]))
					{
						$dialog['user'] = $dialog['user'][0];
					}
				}
				$dialog['messages'] = $messageManager->getMessagesByDialog($dialog['_id'], 0, 1);
				foreach ($dialog['messages'] as $msg)
				{
					$usersToRequest[] = ['friendId' => $msg['userId']];
				}
			}
			$component['data']['users'] = InteractionOfFriends::getProfileFriends($userId, $usersToRequest);
			$component['data']['users'] = ArrayDataHelper::arrayByOneKey($component['data']['users'], 'userId');
		}
	}
} 
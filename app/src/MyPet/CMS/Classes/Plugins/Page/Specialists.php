<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Specialists implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = AuthManager::getSessionUserId();

		$petId = $server->getContainer('request')->getRequest('petId');
		$petManager = new PetManager($server);
		$pet = new PetModel($petId);
		if (isset($petId) && $pet->isLoadedObject())
		{
			if ($petManager->isOwner($pet, $userId))
			{
				$component['data']['isYourPet'] = true;
			}
			else
			{
				$component['data']['isYourPet'] = false;
			}

			$component['data']['userId'] = $userId;

			$component['data']['specialists'] = [];
			$specialists = $petManager->getSpecialist($pet);
			$component['data']['count'] = 0;
			foreach ($specialists['specialists'] as $element)
			{
				foreach ($element as $item)
				{
					if (isset($item['id']))
					{
						$component['data']['specialists'][$item['id']] = $item;
						$component['data']['count']++;
					}
				}
			}
//			foreach ($specialists['owners'] as $value)
//			{
//				if (isset($value['id']))
//				{
//					$component['data']['specialists'][$value['id']] = $value;
//					$component['data']['count']++;
//				}
//			}
			if (isset($specialists['ringleader']['id']))
			{
				$component['data']['specialists'][$specialists['ringleader']['id']] = $specialists['ringleader'];
				$component['data']['count']++;
			}

			$component['data']['userId'] = $userId;
		}
		else
		{
			throw new PageNotFound('Not found pet');
		}
	}
} 
<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\ArrayDataHelper;
use KMCore\ServerContainer;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\BreedModel;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PetsProfile implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$perPage = 10;
		$page = $server->getContainer('request')->getRequest('page', 0);
		$userId = $server->getContainer('request')->getRequest('userId');
		if (!isset($userId) || $userId === 'me')
		{
			$userId = AuthManager::getSessionUserId();
		}

		$petManager = new PetManager($server);
		$component['data']['userId'] = $userId;
		$component['data']['pets'] = $petManager->getPetsByUser($userId, $page, $perPage);
		$component['data']['pages'] = $petManager->countPagesPetsByUser($userId, $perPage);
		$component['data']['page'] = $page;

		$breeds = [];
		$petsIds = [];
		foreach ($component['data']['pets'] as &$pet)
		{
			if (!empty($pet['fatherId']) || !empty($pet['motherId']))
			{
				$pet['ancestry'] = true;
			}
			else
			{
				$petsIds[] = $pet['id'];
			}

			$pet['isDead'] = $petManager::isDead($pet);
			$pet['isYourPet'] = (array_search($userId, $pet['ownerIds']) !== false);
			$pet['age'] = AboutPet::getAgeToRender($pet, PageVariable::getLangName());
			if (!empty($pet['breed']))
			{
				try
				{
					$breeds[] = new \MongoId($pet['breed']);
				}
				catch (\Exception $e)
				{

				}
			}
		}

		$childs = PetModel::getAllAdvanced(
			['$or' => [['fatherId' => ['$in' => $petsIds]], ['motherId' => ['$in' => $petsIds]]]],
			['id' => 1, 'fatherId' => 1, 'motherId' => 1]
		);
		$childsFather = ArrayDataHelper::arrayByOneKey($childs, 'fatherId');
		$childsMother = ArrayDataHelper::arrayByOneKey($childs, 'motherId');

		$breeds = BreedModel::getAllAdvanced(['_id' => ['$in' => $breeds]]);
		$breeds = ArrayDataHelper::arrayByOneKey($breeds, '_id');
		$langName = PageVariable::getLangName();
		foreach ($component['data']['pets'] as $k => &$pet)
		{
			$breed = $pet['breed'];
			if (isset($breeds[$breed][$langName]))
			{
				$pet['breed'] = $breeds[$breed][$langName];
			}
			elseif (isset($breeds[$breed]))
			{
				$pet['breed'] = $breeds[$breed]['en'];
			}

			if (isset($childsFather[$pet['id']]))
			{
				$pet['ancestry'] = true;
			}
			elseif (isset($childsMother[$pet['id']]))
			{
				$pet['ancestry'] = true;
			}

			$pet['sort'] = $k;
			if ($pet['isDead'])
			{
				$pet['sort'] += 1000;
			}
		}

		if (!empty($component['data']['pets']) && is_array($component['data']['pets']))
		{
			ArrayDataHelper::keyMultiSort($component['data']['pets'], 'sort', false, false, false);
		}
	}
} 
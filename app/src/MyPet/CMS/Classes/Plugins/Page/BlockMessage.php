<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\ArrayDataHelper;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Messages\Classes\DialogManager;
use MyPet\Messages\Classes\MessageManager;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class BlockMessage implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$perPage = 100;
		$page = $server->getContainer('request')->getRequest('page', 0);
		$dialogId = $server->getContainer('request')->getRequest('dialogId');
		$userId = AuthManager::getSessionUserId();
		if (isset($dialogId))
		{
			$dialoManager = new DialogManager($server);
			$dialog = $dialoManager->get($dialogId, $userId);
			if (isset($dialog['users']))
			{
				$users = $dialog['users'];
				$usersToRequest = [];
				foreach ($users as $friendId)
				{
					$usersToRequest[] = ['friendId' => $friendId];
				}
				$component['data']['users'] = InteractionOfFriends::getProfileFriends($userId, $usersToRequest);
				$component['data']['users'] = ArrayDataHelper::arrayByOneKey($component['data']['users'], 'userId');

				$messageManager = new MessageManager($server);
				$component['data']['messages'] = $messageManager->getMessagesByDialog($dialogId, $page, $perPage);
				$component['data']['pages'] = $messageManager->countPage($dialogId, $perPage);
				$component['data']['count'] = $messageManager->count($dialogId);
				$component['data']['dialogId'] = $dialogId;
				$component['data']['isMessages'] = true;
			}
			else
			{
				throw new PageNotFound();
			}
		}
	}
} 
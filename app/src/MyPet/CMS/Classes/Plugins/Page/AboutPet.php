<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\PluralHelper;
use KMCore\ServerContainer;
use MyPet\Location\Model\City;
use MyPet\Location\Model\Country;
use MyPet\Location\Model\Region;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\BreedModel;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Model\ProfessionModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AboutPet implements PluginsInterface
{
	public static $cache = [];

	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$userId = AuthManager::getSessionUserId();
		$id = $server->getContainer('request')->getRequest('petId');
		$langName = PageVariable::getLangName();

		if (isset($id))
		{
			$pet = new PetModel($id);
			if (!$pet->isLoadedObject())
			{
				throw new PageNotFound();
			}
			$action = $server->getContainer('request')->getRequest('action');
			$manager = new PetManager($server);
			if ($action === 'edit' && $component['template'] === 'page_about_pet.html.twig' && $manager->isOwner(
					$pet,
					$userId
				)
			)
			{
				$component['data']['countrys'] = Country::getAllAdvanced([], [], true, true);
				if ($pet->country)
				{
					$component['data']['regions'] = Region::getAllAdvanced(
						['country' => $pet->country],
						[],
						true,
						true
					);
				}
				if ($pet->region)
				{
					$component['data']['cities'] = City::getAllAdvanced(['region' => $pet->region], [], true, true);
				}

				$component['data']['langName'] = $langName;
				if ($pet->type)
				{
					$component['data']['breeds'] = BreedModel::getAllAdvanced(
						['type' => $pet->type],
						[],
						true,
						['priority' => 1]
					);
				}

				$component['data']['professions'] = ProfessionModel::getAllAdvanced([], [], true, ['priority' => 1]);
				$component['template'] = 'page_about_pet_edit.html.twig';
			}

			$component['data']['isYourPet'] = $manager->isOwner($pet, $userId);
			$component['data']['pet'] = $pet->getFieldsArray();

			$component['data']['pet']['country_render'] = (new Country($pet->country))->$langName;
			$component['data']['pet']['region_render'] = (new Region($pet->region))->$langName;

			$cityModel = new City($pet->city);
			if ($cityModel->isLoadedObject())
			{
				$component['data']['pet']['city_render'] = (new City($pet->city))->$langName;
			}
			else
			{
				$component['data']['pet']['city_render'] = $pet->city;
			}

			$component['data']['pet']['isDead'] = PetManager::isDead($component['data']['pet']);
			$component['data']['pet']['age'] = AboutPet::getAgeToRender(
				$component['data']['pet'],
				PageVariable::getLangName()
			);

			$langName = PageVariable::getLangName();

			$breed = new BreedModel($component['data']['pet']['breed']);
			if ($breed->isLoadedObject())
			{
				if (isset($breed->$langName))
				{
					$component['data']['pet']['breed_translate'] = $breed->$langName;
				}
				else
				{
					$component['data']['pet']['breed_translate'] = $breed->en;
				}
			}

			$component['data']['pet']['signs'] = implode(', ', $component['data']['pet']['signs']);
			if ((int)$component['data']['pet']['gender'] === 2)
			{
				$component['data']['pet']['gender_name'] = 'she';
			}
			elseif ((int)$component['data']['pet']['gender'] === 1)
			{
				$component['data']['pet']['gender_name'] = 'he';
			}
		}
		else
		{
			throw new PageNotFound();
		}
	}

	/**
	 * @param array $pet
	 * @param       $lang
	 * @return string
	 */
	public static function getAgeToRender(Array $pet, $lang)
	{
		$ageRender = '';
		if (isset($pet['death_y']) && $pet['death_y'] > 0)
		{
			if (isset($pet['death_y']) && $pet['birth_y'] > 0)
			{
				$ageRender = sprintf(
					'(%d/%d/%d - %d/%d/%d)',
					$pet['birth_d'],
					$pet['birth_m'],
					$pet['birth_y'],
					$pet['death_d'],
					$pet['death_m'],
					$pet['death_y']
				);
			}
			else
			{
				$ageRender = sprintf('( - %d/%d/%d)', $pet['death_d'], $pet['death_m'], $pet['death_y']);
			}
		}
		elseif (isset($pet['birth_y'], $pet['birth_m'], $pet['birth_d']) && $pet['birth_y'] > 0)
		{
			$date_user = new \DateTime();
			$date_user->setDate((int)$pet['birth_y'], (int)$pet['birth_m'], (int)$pet['birth_d']);

			$date_server = new \DateTime();
			$interval = $date_user->diff($date_server);

			$year = $interval->format('%y%');
			$month = $interval->format('%m%');
			$day = $interval->format('%d%');

			if ($year > 0)
			{
				switch ($lang)
				{
					case 'en':
						$ageRender = $year.' '.PluralHelper::pluralLatin($year, 'year').PHP_EOL;
						break;
					case 'ru':
						$ageRender = $year.' '.PluralHelper::pluralCirilic($year, ['год', 'года', 'лет']).PHP_EOL;
						break;
					default:
						$ageRender = $year.' '.PluralHelper::pluralCirilic($year, ['рік', 'роки', 'років']).PHP_EOL;
						break;
				}
			}
			elseif ($month > 0)
			{
				switch ($lang)
				{
					case 'en':
						$ageRender = $month.' '.PluralHelper::pluralLatin($month, 'month').PHP_EOL;
						break;
					case 'ru':
						$ageRender = $month.' '.PluralHelper::pluralCirilic(
								$month,
								['месяц', 'месяцев', 'месяцев']
							).PHP_EOL;
						break;
					default:
						$ageRender = $month.' '.PluralHelper::pluralCirilic(
								$month,
								['місяць', 'місяців', 'місяців']
							).PHP_EOL;
						break;
				}
			}
			elseif ($day > 0)
			{
				switch ($lang)
				{
					case 'en':
						$ageRender = $day.' '.PluralHelper::pluralLatin($day, 'day').PHP_EOL;
						break;
					case 'ru':
						$ageRender = $day.' '.PluralHelper::pluralCirilic($day, ['день', 'дней', 'дней']).PHP_EOL;
						break;
					default:
						$ageRender = $day.' '.PluralHelper::pluralCirilic($day, ['день', 'днів', 'днів']).PHP_EOL;
						break;
				}
			}
		}

		return $ageRender;
	}
} 
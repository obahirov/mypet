<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Friends\Classes\FriendsManager;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Friends implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$perPage = 10;
		$page = $server->getContainer('request')->getRequest('page', 0);
		$userId = $server->getContainer('request')->getRequest('userId');
		if (!isset($userId) || $userId === 'me')
		{
			$userId = AuthManager::getSessionUserId();
		}
		$component['data']['loggedIn'] = AuthManager::loggedIn();
		if (AuthManager::getSessionUserId() !== $userId)
		{
			$component['data']['isYou'] = false;
		}
		else
		{
			$component['data']['isYou'] = true;
		}

		$container = $server->getContainer();

		/**
		 * @var FriendsManager $friendsManager
		 */
		$friendsManager = $container['friendsManager'];
		$component['data']['userId'] = $userId;
		$component['data']['count'] = $friendsManager->countFriends($userId, 1);
		$component['data']['friends'] = InteractionOfFriends::getProfileFriends(
			$userId,
			$friendsManager->getFriends($userId, $page, $perPage, 1)
		);
		$component['data']['pages'] = $friendsManager->countPagesFriend($userId, $perPage, 1);
		$component['data']['page'] = $page;

	}
} 
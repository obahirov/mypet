<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Inbox implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		if (!AuthManager::loggedIn())
		{
			throw new PageNotFound('Not auth user');
		}

		$perPage = 50;
		$page = $server->getContainer('request')->getRequest('page', 0);
		$userId = AuthManager::getSessionUserId();

		$requestManager = new RequestManager();
		$component['data']['requests'] = $requestManager->getAllRequests($userId, $page, $perPage);
	}
} 
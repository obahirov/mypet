<?php
namespace MyPet\CMS\Classes\Plugins\Page;

use KMCore\CMS\Classes\PageNotFound;
use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\PetModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Familys
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @throws \KMCore\CMS\Classes\PageNotFound
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$id = $server->getContainer('request')->getRequest('petId');
		$langName = PageVariable::getLangName();

		if (isset($id))
		{
			$petModel = new PetModel($id);
			if (!$petModel->isLoadedObject())
			{
				throw new PageNotFound();
			}


			$petManager = new PetManager($server);

			$parentsPetData = $petManager->getParentsPetData($petModel, false);
			$brothersPetData = $petManager->getBrothersPetData($petModel, false);
			$childsPetData = $petManager->getChildsPetData($petModel, false);
			$component['data']['pets'] = array_merge($parentsPetData, $brothersPetData, $childsPetData);
			$component['data']['count'] = count($component['data']['pets']);

			foreach ($component['data']['pets'] as &$pet)
			{
				$pet['isDead'] = $petManager::isDead($pet);
				$pet['age'] = AboutPet::getAgeToRender($pet, PageVariable::getLangName());
				if (!empty($pet['breed']))
				{
					try
					{
						$breeds[] = new \MongoId($pet['breed']);
					}
					catch (\Exception $e)
					{

					}
				}

				if ($pet['id'] === $petModel->fatherId)
				{
					$pet['family'] = '_t_father_t_';
				}
				elseif ($pet['id'] === $petModel->motherId)
				{
					$pet['family'] = '_t_mother_t_';
				}
				elseif ($pet['fatherId'] === $petModel->fatherId || $pet['motherId'] === $petModel->motherId)
				{
					if ($pet['gender'] === 2)
					{
						$pet['family'] = '_t_brother_t_';
					}
					elseif ($pet['gender'] === 1)
					{
						$pet['family'] = '_t_sister_t_';
					}
					else
					{
						$pet['family'] = '_t_brother_or_sister_t_';
					}
				}
				elseif ($pet['fatherId'] === $petModel->id || $pet['motherId'] === $petModel->id)
				{
					if ($pet['gender'] === 2)
					{
						$pet['family'] = '_t_daughter_t_';
					}
					elseif ($pet['gender'] === 1)
					{
						$pet['family'] = '_t_son_t_';
					}
					else
					{
						$pet['family'] = '_t_child_t_';
					}
				}
			}

			foreach ($component['data']['pets'] as &$pet)
			{
				$breed = $pet['breed'];
				if (isset($breeds[$breed][$langName]))
				{
					$pet['breed'] = $breeds[$breed][$langName];
				}
				elseif (isset($breeds[$breed]))
				{
					$pet['breed'] = $breeds[$breed]['en'];
				}
			}
		}
		else
		{
			throw new PageNotFound();
		}
	}
} 
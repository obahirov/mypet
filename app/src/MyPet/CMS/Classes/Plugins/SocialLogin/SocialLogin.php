<?php
namespace MyPet\CMS\Classes\Plugins\SocialLogin;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use KMCore\SocialApi\SocialApiSelector;
use KMCore\SocialApi\SocialConfig;
use KMCore\SocialApi\SocialVariables;
use MyPet\SocialLogin\Classes\AbstractLogin;
use MyPet\SocialLogin\Classes\LoginInstance;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SocialLogin implements PluginsInterface
{
	/**
	 * @param ServerContainer         $server
	 * @param array                   $component
	 * @param \KMCore\CMS\Model\Pages $pageModel
	 * @param bool                    $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$serverConfig = $server->getContainer('config');
		$config = new SocialConfig(
			$serverConfig->get('socApiId', SocialVariables::SN_CODE_FB, $serverConfig::TYPE_MAIN),
			$serverConfig->get('socApiSecret', SocialVariables::SN_CODE_FB, $serverConfig::TYPE_MAIN),
			$serverConfig->get('socAppNamespace', SocialVariables::SN_CODE_FB, $serverConfig::TYPE_MAIN, ''),
			$serverConfig->get('socApiUrl', SocialVariables::SN_CODE_FB, $serverConfig::TYPE_MAIN, ''),
			$serverConfig->get('socApiKey', SocialVariables::SN_CODE_FB, $serverConfig::TYPE_MAIN, ''),
			$serverConfig->get('baseUrl', $serverConfig::TYPE_MAIN, '')
		);
		$component['data']['fbLogin'] = SocialApiSelector::getAuth(SocialVariables::SN_CODE_FB, $config)->getUrlLogin();

		$config = new SocialConfig(
			$serverConfig->get('socApiId', SocialVariables::SN_CODE_GL, $serverConfig::TYPE_MAIN),
			$serverConfig->get('socApiSecret', SocialVariables::SN_CODE_GL, $serverConfig::TYPE_MAIN),
			$serverConfig->get('socAppNamespace', SocialVariables::SN_CODE_GL, $serverConfig::TYPE_MAIN, ''),
			$serverConfig->get('socApiUrl', SocialVariables::SN_CODE_GL, $serverConfig::TYPE_MAIN, ''),
			$serverConfig->get('socApiKey', SocialVariables::SN_CODE_GL, $serverConfig::TYPE_MAIN, ''),
			$serverConfig->get('baseUrl', $serverConfig::TYPE_MAIN, '')
		);
		$component['data']['glLogin'] = SocialApiSelector::getAuth(SocialVariables::SN_CODE_GL, $config)->getUrlLogin();
	}
} 
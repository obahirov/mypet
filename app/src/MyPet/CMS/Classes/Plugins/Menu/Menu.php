<?php
namespace MyPet\CMS\Classes\Plugins\Menu;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Menu implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$route = $server->getContainer('request')->getGetOrPost('route', $server->getContainer('config')->get('route'));
		$userId = $server->getContainer('request')->getRequest('userId');
		$petId = $server->getContainer('request')->getRequest('petId');

		if (!isset($userId) || $userId === 'me')
		{
			$userId = AuthManager::getSessionUserId();
		}

		if (PageVariable::getPageName() === 'user' || PageVariable::getPageName() === 'pet')
		{
			$component['data']['current'] = 'index';
		}
		else
		{
			$component['data']['current'] = PageVariable::getPageName();
		}

		if (isset($component['data']['menu']))
		{
			foreach ($component['data']['menu'] as &$item)
			{
				if (isset($item['name']) && strpos($route, $item['name']) !== false)
				{
					$component['data']['current'] = $item['name'];
				}
				$item['href'] = str_replace('{userId}', $userId, $item['href']);
				$item['href'] = str_replace('{petId}', $petId, $item['href']);
			}
		}
	}
} 
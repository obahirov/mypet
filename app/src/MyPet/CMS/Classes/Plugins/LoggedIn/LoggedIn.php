<?php
namespace MyPet\CMS\Classes\Plugins\LoggedIn;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class LoggedIn implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$component['data']['langName'] = PageVariable::getLangName();
		$component['data']['loggedIn'] = AuthManager::loggedIn();
		$component['data']['pageName'] = PageVariable::getPageName();
	}
} 
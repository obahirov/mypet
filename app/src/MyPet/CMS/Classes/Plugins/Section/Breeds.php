<?php
namespace MyPet\CMS\Classes\Plugins\Section;

use KMCore\CMS\Classes\PageVariable;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Pets\Model\BreedModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Breeds implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$component['data']['langName'] = PageVariable::getLangName();
		$component['data']['breeds'] = BreedModel::getAllAdvanced(['type' => 'cat'], [], true, ['priority' => 1]);
	}
} 
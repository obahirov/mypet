<?php
namespace MyPet\CMS\Classes\Plugins\Section;
use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\ServerContainer;
use KMCore\CMS\Model\Pages;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;

/**
 * Class SystemInfo
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SystemInfo implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{

	}
}
<?php
namespace MyPet\CMS\Classes\Plugins\Section;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;
use MyPet\Pets\Model\GagModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Gag implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$gagId = $server->getContainer('request')->getGetOrPost('gagId');
		if ($gagId)
		{
			$gag = new GagModel($gagId);
			if ($gag->isLoadedObject())
			{
				$gag = $gag->getFieldsArray();
				$gag['signs'] = implode(', ', $gag['signs']);
				$component['data']['gag'] = $gag;
			}
		}
	}
} 
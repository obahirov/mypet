<?php
namespace MyPet\CMS\Classes\Plugins\Section;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Ancestry implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$petId = $server->getContainer('request')->getGetOrPost('petId');
		$gender = $server->getContainer('request')->getGetOrPost('gender');
		$gagId = $server->getContainer('request')->getGetOrPost('gagId');

		$component['data']['petId'] = $petId;
		$component['data']['gagId'] = $gagId;
		$component['data']['gender'] = $gender;
	}
} 
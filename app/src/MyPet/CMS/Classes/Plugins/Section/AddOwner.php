<?php
namespace MyPet\CMS\Classes\Plugins\Section;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\ServerContainer;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AddOwner implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$petId = $server->getContainer('request')->getGetOrPost('petId');
		$component['data']['petId'] = $petId;
	}
} 
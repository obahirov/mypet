<?php
namespace MyPet\CMS\Classes\Plugins\Section;

use KMCore\CMS\Classes\Plugins\PluginsInterface;
use KMCore\CMS\Model\Pages;
use KMCore\Helper\SessionStorage;
use KMCore\ServerContainer;
use MyPet\Pets\Model\PetModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Status implements PluginsInterface
{
	/**
	 * @param ServerContainer $server
	 * @param array           $component
	 * @param Pages           $pageModel
	 * @param bool            $rendering
	 * @return array|void
	 */
	public static function run(ServerContainer $server, Array &$component, Pages &$pageModel, $rendering = false)
	{
		$petId = $server->getContainer('request')->getGetOrPost('petId');
		SessionStorage::set('petId', $petId);
		$component['data']['petId'] = $petId;
		$component['data']['status'] = (new PetModel($petId))->status;
	}
} 
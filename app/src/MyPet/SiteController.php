<?php
namespace MyPet;

use KMCore\Helper\RedirectManager;
use KMCore\Helper\SessionStorage;
use KMCore\RestfulController;

/**
 * Контроллер з базовим методом відповіді через форми
 *
 * @package Poker
 */
class SiteController extends RestfulController
{
	/**
	 * @param        $message
	 * @param string $url
	 * @param bool   $success
	 * @param bool   $terminate
	 */
	protected function responseByForm($message, $url = '/', $success = true, $terminate = true)
	{
		$this->response();

		if ($this->request->getIsAjaxRequest())
		{
			$config = $this->server->getContainer('config');
			$baseLang = $config->get('base', 'lang');
			$langName = $this->server->getContainer('sessionStorage')->get(
				'langName',
				SessionStorage::TYPE_BASE,
				$baseLang
			);
			$message = $config->loadFile('translate')->get($langName, $message, $config::TYPE_MAIN, $message);
			$this->response->json(['message' => $message, 'success' => $success], 200, []);
			if ($terminate)
			{
				exit();
			}
		}
		else
		{
			RedirectManager::redirectWithMessage($message, $url, $terminate);
		}
	}
}

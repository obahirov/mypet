<?php
namespace MyPet\Inbox\Controller;

use KMCore\Helper\SessionStorage;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\ApiController;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class RequestController extends ApiController
{
	protected $manger;

	/**
	 * Инициализация менеджера, вызов родительского конструктора
	 */
	public function __construct()
	{
		parent::__construct();
		$this->manger = new RequestManager();
	}

	/**
	 * Получить бонус
	 */
	public function collectRequest()
	{
		$giftIds = (string)$this->request->getGetOrPost('giftIds', '');
		$giftIds = explode(',', $giftIds);

		$giftIds = array_slice(
			$giftIds,
			0,
			RequestManager::MAX_REQUEST_MODIFY
		); // Ограничиваем количество обрабатываемых реквестов одним запросом
		foreach ($giftIds as $giftId)
		{
			if (!empty($giftId))
			{
				$this->manger->collectRequest($giftId, $this->user->id);
			}
		}

		// TODO Метод возвращения всех записей
		$perPage = RequestManager::MAX_REQUEST_RETURN;
		$page = (int)$this->request->getGetOrPost('page', 0);
		$this->data['requests'] = $this->manger->getAllRequests($this->user->id, $page, $perPage);
		$this->data['pages'] = $this->manger->countPagesRequest($this->user->id, $perPage);
		$this->data['perPage'] = $perPage;
		$this->data['count'] = $this->manger->countRequest($this->user->id, $page, $perPage);
		$this->data['lastRead'] = SessionStorage::get('lastRead');
		SessionStorage::set('lastRead', time());
		$this->success = true;
		$this->response();
	}

	public function removeRequest()
	{
		$giftIds = (string)$this->request->getGetOrPost('giftIds', '');
		$giftIds = explode(',', $giftIds);

		$giftIds = array_slice(
			$giftIds,
			0,
			RequestManager::MAX_REQUEST_MODIFY
		); // Ограничиваем количество обрабатываемых реквестов одним запросом
		foreach ($giftIds as $giftId)
		{
			if (!empty($giftId))
			{
				$this->manger->removeRequest($giftId, $this->user->id);
			}
		}
		$this->success = true;
		$this->response();
	}

	/**
	 * Получить все входящие сообщения с Inbox
	 */
	public function getAllRequests()
	{
		$perPage = RequestManager::MAX_REQUEST_RETURN;
		$page = (int)$this->request->getGetOrPost('page', 0);

		$this->data['requests'] = $this->manger->getAllRequests($this->user->id, $page, $perPage);
		$this->data['pages'] = $this->manger->countPagesRequest($this->user->id, $perPage);
		$this->data['perPage'] = $perPage;
		$this->data['count'] = $this->manger->countRequest($this->user->id);
		$this->data['lastRead'] = SessionStorage::get('lastRead');
		SessionStorage::set('lastRead', time());
		$this->success = true;
		$this->response();
	}
}
<?php
namespace MyPet\Inbox\Classes;

use MyPet\Inbox\Model\Requests;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class RequestManager extends BaseRequestManager
{
	const TYPE_ANCESTRY_ADD_RELATION_CONFIRM = 'ancestry_add_relation_confirm';
	const TYPE_ANCESTRY_REM_RELATION_CONFIRM = 'ancestry_rem_relation_confirm';
	const TYPE_INVITE_CONFIRM = 'invite_confirm';
	const TYPE_NEW_MESSAGE = 'new_message';

	const TYPE_LIKE = 'like';
	const TYPE_COMMENT = 'comment';
	const TYPE_SHARE = 'share';

	protected static $callbacks = [
		'ancestry_add_relation_confirm' => ['\MyPet\Inbox\Classes\RequestHelper', 'confirmAddRelationAncestry'],
		'ancestry_rem_relation_confirm' => ['\MyPet\Inbox\Classes\RequestHelper', 'confirmRemRelationAncestry'],
		'invite_confirm'                => ['\MyPet\Inbox\Classes\RequestHelper', 'confirmCallbackFriend'],
		'new_message'                   => ['\MyPet\Inbox\Classes\RequestHelper', 'noneAction'],
		'like'                          => ['\MyPet\Inbox\Classes\RequestHelper', 'noneAction'],
		'comment'                       => ['\MyPet\Inbox\Classes\RequestHelper', 'noneAction'],
		'share'                         => ['\MyPet\Inbox\Classes\RequestHelper', 'noneAction'],
	];

	/**
	 * @param Requests $model
	 * @param          $userId
	 */
	public function sendPub(Requests $model, $userId)
	{
		//		ZMQEvent::run(ListEvents::EVENT_INBOX_ADD, $model->attributes);
	}
}
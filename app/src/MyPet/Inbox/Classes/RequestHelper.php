<?php
namespace MyPet\Inbox\Classes;

use MyPet\Friends\Classes\FriendsManager;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\PetModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class RequestHelper
{
	/**
	 * @param        $userId
	 * @param string $friendId
	 * @param        $payload
	 * @param bool   $cancel Параметр отвечающий за то был принят реквест или отвергнут. Обычно параметра несуществует,
	 *                       так как подарки и бонусы отвергунть нельзя
	 */
	public static function confirmCallbackFriend($userId, $friendId, $payload, $cancel = false)
	{
		$friendManager = new FriendsManager();
		if ($cancel === true)
		{
			$friendManager->remFriend($userId, $friendId);
		}
		else
		{
			$friendManager->addFriend($friendId, $userId);
		}
	}

	/**
	 * @param      $userId
	 * @param      $petId
	 * @param      $petRelationId
	 * @param      $parent
	 * @param      $gender
	 * @param      $payload
	 * @param bool $cancel
	 */
	public static function confirmAddRelationAncestry($userId, $petId, $petRelationId, $parent, $gender, $payload, $cancel = false)
	{
		$pet = new PetModel($petId);
		$petRelation = new PetModel($petRelationId);
		if ($cancel === false)
		{
			$manager = new PetManager($GLOBALS['serverContainer']);
			$manager->addLink($pet, $petRelation, $parent, $gender);
			$pet->save();
			$petRelation->save();
		}
	}

	/**
	 * @param      $userId
	 * @param      $petId
	 * @param      $petRelationId
	 * @param      $parent
	 * @param      $gender
	 * @param      $payload
	 * @param bool $cancel
	 */
	public static function confirmRemRelationAncestry($userId, $petId, $petRelationId, $parent, $gender, $payload, $cancel = false)
	{
		$pet = new PetModel($petId);
		$petRelation = new PetModel($petRelationId);
		if ($cancel === false)
		{
			$manager = new PetManager($GLOBALS['serverContainer']);
			$manager->remLink($pet, $petRelation, $parent, $gender);
			$pet->save();
			$petRelation->save();
		}
	}

	public static function noneAction()
	{

	}
}
<?php
namespace MyPet\Inbox\Classes;

use KMCore\Helper\GeneratorHelper;
use MyPet\Inbox\Model\Requests;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class BaseRequestManager
{
	const MAX_REQUEST_MODIFY = 50;
	const MAX_REQUEST_RETURN = 100;

	protected static $callbacks = [

	];

	/**
	 * @param $type
	 * @return array
	 */
	public static function getCallbackByType($type)
	{
		if (isset(static::$callbacks[$type]))
		{
			return static::$callbacks[$type];
		}

		return [];
	}

	/**
	 * @param        $userId
	 * @param string $type
	 * @param array  $attributes
	 * @param string $unique
	 * @return Requests
	 */
	public static function addRequest($userId, $type = '', Array $attributes = [], $unique = '')
	{
		if (empty($unique))
		{
			$unique = GeneratorHelper::getStringUniqueId();
		}

		$model = new Requests(['id' => $unique, 'userId' => $userId]);
		if (!$model->isLoadedObject())
		{
			$model->userId = $userId;
			$model->id = $unique;
			$model->type = $type;
			$model->date = time();
			$model->callback = static::getCallbackByType($type);
			$model->attributes = $attributes;
			$model->collected = false;
			$model->save();
		}

		return $model;
	}

	/**
	 * @param $userId
	 * @param $guestId
	 */
	public static function mergeAccount($userId, $guestId)
	{
		$all_requests = Requests::getAllAdvanced(['collected' => false, 'userId' => $guestId]);
		foreach ($all_requests as $request)
		{
			$model = new Requests(['id' => $request['id'], 'userId' => $userId]);
			if (!$model->isLoadedObject())
			{
				$model->id = $request['id'];
				$model->userId = $userId;
				$model->type = $request['type'];
				$model->attributes = $request['attributes'];
				$model->date = $request['date'];
				$model->callback = $request['callback'];
				$model->collected = $request['collected'];
				$model->save();
			}
			else
			{
				if ($request['collected'])
				{
					$model->collected = $request['collected'];
				}
				$model->save();
			}
		}
	}

	/**
	 * @param RequestData $request
	 * @return Requests
	 */
	public static function requestByRequestData(RequestData $request)
	{
		$model = new Requests(['id' => $request->getUnique(), 'userId' => $request->getUserId()]);
		if (!$model->isLoadedObject())
		{
			$model->userId = $request->getUserId();
			$model->id = $request->getUnique();
			$model->type = $request->getType();
			$model->date = time();
			$model->callback = static::getCallbackByType($request->getType());
			$model->attributes = $request->getAttributes();
			$model->collected = false;
			$model->save();
		}

		return $model;
	}

	/**
	 * @param $giftId
	 * @param $userId
	 * @return mixed
	 */
	public function collectRequest($giftId, $userId)
	{
		$query = ['id' => $giftId, 'userId' => $userId, 'collected' => false];
		//$update = array('$set' => array('collected' => true));
		$update = [];
		$fields = [
			'id'         => 1,
			'userId'     => 1,
			'type'       => 1,
			'attributes' => 1,
			'date'       => 1,
			'collected'  => 1,
			'callback'   => 1
		];
		// Видаляємо реквести після збору, для уникнення перенаповнення БД
		$options = [
			'remove' => true
		];
		$model = Requests::findAndModify($query, $update, $fields, $options);

		if (isset($model['callback']) && is_callable($model['callback']) && is_array($model['attributes']))
		{
			call_user_func_array($model['callback'], $model['attributes']);
		}

		$model['collected'] = true;

		return $model;
	}

	/**
	 * @param $giftId
	 * @param $userId
	 * @return void
	 */
	public function removeRequest($giftId, $userId)
	{
		$query = ['id' => $giftId, 'userId' => $userId];
		$update = [];
		$fields = [
			'id'         => 1,
			'callback'   => 1,
			'attributes' => 1
		];
		// Видаляємо реквести після збору, для уникнення перенаповнення БД
		$options = [
			'remove' => true
		];
		$model = Requests::findAndModify($query, $update, $fields, $options);

		$model['attributes']['cancel'] = true;

		if (isset($model['callback']) && is_callable($model['callback']) && is_array($model['attributes']))
		{
			call_user_func_array($model['callback'], $model['attributes']);
		}
	}

	/**
	 * @param     $userId
	 * @param int $page
	 * @param int $perPage
	 * @return array
	 */
	public function getAllRequests($userId, $page = 0, $perPage = self::MAX_REQUEST_RETURN)
	{
		return Requests::getAllAdvanced(
			['collected' => false, 'userId' => $userId],
			['id' => 1, 'userId' => 1, 'type' => 1, 'attributes' => 1, 'date' => 1, 'collected' => 1],
			false,
			true,
			$page,
			$perPage
		);
	}

	/**
	 * @param RequestData $request
	 * @return Requests
	 */
	public static function updateRequest(RequestData $request)
	{
		$model = new Requests(['id' => $request->getUnique(), 'userId' => $request->getUserId()]);
		if ($model->isLoadedObject())
		{
			$model->date = time();
			$model->collected = false;
			$model->save();
		}
		else
		{
			$model->userId = $request->getUserId();
			$model->id = $request->getUnique();
			$model->type = $request->getType();
			$model->date = time();
			$model->callback = static::getCallbackByType($request->getType());
			$model->attributes = $request->getAttributes();
			$model->collected = false;
			$model->save();
		}

		return $model;
	}

	public static function deleteAllCollected()
	{
		Requests::deleteAllAdvanced(['collected' => true, 'date' => ['$lte' => strtotime('tomorrow')]]);
	}

	/**
	 * @param      $userId
	 * @return int
	 */
	public static function countRequest($userId)
	{
		return Requests::count(['collected' => false, 'userId' => $userId]);
	}

	/**
	 * @param      $userId
	 * @param int  $perPage
	 * @return float
	 */
	public function countPagesRequest($userId, $perPage = self::MAX_REQUEST_RETURN)
	{
		return (int)ceil(static::countRequest($userId) / $perPage);
	}
}
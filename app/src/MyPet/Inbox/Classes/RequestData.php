<?php
namespace MyPet\Inbox\Classes;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class RequestData
{
	protected $userId = '';
	protected $type = '';
	protected $attributes = [];
	protected $unique = '';
	protected $pub = false;

	/**
	 * @param RequestData $request
	 * @return RequestData
	 */
	public static function init(RequestData $request)
	{
		return $request;
	}

	/**
	 * @param $userId
	 */
	public function __construct($userId = '')
	{
		if (!empty($userId))
		{
			$this->setUserId($userId);
		}
	}

	/**
	 * @param $userId
	 * @return $this
	 */
	public function setUserId($userId)
	{
		$this->userId = (string)$userId;

		return $this;
	}

	/**
	 * @param string $type
	 * @return $this
	 */
	public function setType($type = '')
	{
		$this->type = (string)$type;

		return $this;
	}

	/**
	 * @param array $attributes
	 * @return $this
	 */
	public function setAttributes(Array $attributes = [])
	{
		$this->attributes = (array)$attributes;

		return $this;
	}

	/**
	 * @param string $unique
	 * @return $this
	 */
	public function setUnique($unique = '')
	{
		$this->unique = (string)$unique;

		return $this;
	}

	/**
	 * @param bool $pub
	 * @return $this
	 */
	public function setPub($pub = false)
	{
		$this->pub = (bool)$pub;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * @return string
	 */
	public function getUnique()
	{
		if (empty($this->unique))
		{
			$genericId = new \MongoId();
			$prefix = substr((string)mt_rand(1000, 1999), -3);
			$genericId = $prefix.(string)$genericId;
			$this->unique = $genericId;
		}

		return $this->unique;
	}

	/**
	 * @return bool
	 */
	public function getPub()
	{
		return $this->pub;
	}

	/**
	 * @return bool
	 */
	public function send()
	{
		$userId = $this->getUserId();
		$type = $this->getType();
		$attributes = $this->getAttributes();
		$unique = $this->getUnique();
		$pub = $this->getPub();

		if (!empty($userId) && !empty($type) && !empty($attributes) && !empty($unique))
		{
			$model = RequestManager::requestByRequestData($this);

			if ($pub)
			{
				$manager = new RequestManager();
				$manager->sendPub($model, $userId);
			}

			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function update()
	{
		$userId = $this->getUserId();
		$type = $this->getType();
		$attributes = $this->getAttributes();
		$unique = $this->getUnique();
		$pub = $this->getPub();

		if (!empty($userId) && !empty($type) && !empty($attributes) && !empty($unique))
		{
			$model = RequestManager::updateRequest($this);
			if ($pub)
			{
				$manager = new RequestManager();
				$manager->sendPub($model, $userId);
			}

			return true;
		}

		return false;
	}
}
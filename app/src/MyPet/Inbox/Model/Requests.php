<?php
namespace MyPet\Inbox\Model;

use KMCore\DB\ObjectModel;

/**
 * @property mixed  userId
 * @property string id
 * @property string type
 * @property int    amount
 * @property mixed  collected
 * @property array  callback
 * @property mixed  date
 * @property array  attributes
 * @property string unique
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Requests extends ObjectModel
{
	protected static $collection = 'pk_requests_model';
	protected static $pk = '_id';
	protected static $sort = ['date' => -1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1, 'userId' => 1],
			'unique' => true,
		],
		[
			'keys' => ['userId' => 1, 'collected' => 1, 'id' => 1],
		],
		[
			'keys' => ['type' => 1, 'userId' => 1, 'collected' => 1],
		],
		[
			'keys' => ['date' => -1, 'collected' => 1],
		],
	];

	protected static $fieldsDefault = [
		'_id'        => '',
		'id'         => '',
		'userId'     => '',
		'type'       => '',
		'attributes' => [],
		'date'       => 0,
		'callback'   => [],
		'collected'  => false,
	];

	protected static $fieldsValidate = [
		'_id'        => self::TYPE_MONGO_ID,
		'id'         => self::TYPE_STRING,
		'userId'     => self::TYPE_STRING,
		'type'       => self::TYPE_STRING,
		'attributes' => self::TYPE_JSON,
		'date'       => self::TYPE_TIMESTAMP,
		'callback'   => self::TYPE_JSON,
		'collected'  => self::TYPE_BOOL,
	];
}
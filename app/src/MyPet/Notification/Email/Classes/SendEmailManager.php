<?php
namespace MyPet\Notification\Email\Classes;

use KMCore\Config\Config;
use KMCore\LogSystem\FastLog;
use KMCore\Validation\Validate;
use PHPMailer;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SendEmailManager
{
	/**
	 * @var Config
	 */
	protected $config;
	protected $mail;

	/**
	 * @param Config $config
	 */
	public function __construct(Config $config)
	{
		$this->config = $config;
		$this->mail = new PHPMailer;

		/**
		 * The character set of the message.
		 *
		 * @type string
		 */
		$this->mail->CharSet = $this->config->get('CharSet', 'config', 'email', 'iso-8859-1');

		/**
		 * The MIME Content-type of the message.
		 *
		 * @type string
		 */
		$this->mail->ContentType = $this->config->get('ContentType', 'config', 'email', 'text/plain');

		/**
		 * The message encoding.
		 * Options: "8bit", "7bit", "binary", "base64", and "quoted-printable".
		 *
		 * @type string
		 */
		$this->mail->Encoding = $this->config->get('Encoding', 'config', 'email', '8bit');

		/**
		 * The From email address for the message.
		 *
		 * @type string
		 */
		$this->mail->From = $this->config->get('From', 'config', 'email', 'root@localhost');

		/**
		 * The From name of the message.
		 *
		 * @type string
		 */
		$this->mail->FromName = $this->config->get('FromName', 'config', 'email', 'Root User');

		/**
		 * The Sender email (Return-Path) of the message.
		 * If not empty, will be sent via -f to sendmail or as 'MAIL FROM' in smtp mode.
		 *
		 * @type string
		 */
		$this->mail->Sender = $this->config->get('Sender', 'config', 'email', '');

		/**
		 * Which method to use to send mail.
		 * Options: "mail", "sendmail", or "smtp".
		 *
		 * @type string
		 */
		$this->mail->Mailer = $this->config->get('Mailer', 'config', 'email', 'mail');

		/**
		 * The path to the sendmail program.
		 *
		 * @type string
		 */
		$this->mail->Sendmail = $this->config->get('Sendmail', 'config', 'email', '/usr/sbin/sendmail');

		/**
		 * Whether mail() uses a fully sendmail-compatible MTA.
		 * One which supports sendmail's "-oi -f" options.
		 *
		 * @type boolean
		 */
		$this->mail->UseSendmailOptions = $this->config->get('UseSendmailOptions', 'config', 'email', true);

		/**
		 * The email address that a reading confirmation should be sent to.
		 *
		 * @type string
		 */
		$this->mail->ConfirmReadingTo = $this->config->get('ConfirmReadingTo', 'config', 'email', '');

		/**
		 * The hostname to use in Message-Id and Received headers
		 * and as default HELO string.
		 * If empty, the value returned
		 * by SERVER_NAME is used or 'localhost.localdomain'.
		 *
		 * @type string
		 */
		$this->mail->Hostname = $this->config->get('Hostname', 'config', 'email', '');

		/**
		 * An ID to be used in the Message-Id header.
		 * If empty, a unique id will be generated.
		 *
		 * @type string
		 */
		$this->mail->MessageID = $this->config->get('MessageID', 'config', 'email', '');

		/**
		 * The message Date to be used in the Date header.
		 * If empty, the current date will be added.
		 *
		 * @type string
		 */
		$this->mail->MessageDate = $this->config->get('MessageDate', 'config', 'email', '');

		/**
		 * SMTP hosts.
		 * Either a single hostname or multiple semicolon-delimited hostnames.
		 * You can also specify a different port
		 * for each host by using this format: [hostname:port]
		 * (e.g. "smtp1.example.com:25;smtp2.example.com").
		 * You can also specify encryption type, for example:
		 * (e.g. "tls://smtp1.example.com:587;ssl://smtp2.example.com:465").
		 * Hosts will be tried in order.
		 *
		 * @type string
		 */
		$this->mail->Host = $this->config->get('Host', 'config', 'email', 'localhost');

		/**
		 * The default SMTP server port.
		 *
		 * @type integer
		 * @TODO Why is this needed when the SMTP class takes care of it?
		 */
		$this->mail->Port = $this->config->get('Port', 'config', 'email', 25);

		/**
		 * The SMTP HELO of the message.
		 * Default is $Hostname.
		 *
		 * @type string
		 * @see PHPMailer::$Hostname
		 */
		$this->mail->Helo = $this->config->get('Helo', 'config', 'email', '');

		/**
		 * The secure connection prefix.
		 * Options: "", "ssl" or "tls"
		 *
		 * @type string
		 */
		$this->mail->SMTPSecure = $this->config->get('SMTPSecure', 'config', 'email', '');

		/**
		 * Whether to use SMTP authentication.
		 * Uses the Username and Password properties.
		 *
		 * @type boolean
		 * @see PHPMailer::$Username
		 * @see PHPMailer::$Password
		 */
		$this->mail->SMTPAuth = $this->config->get('SMTPAuth', 'config', 'email', false);

		/**
		 * SMTP username.
		 *
		 * @type string
		 */
		$this->mail->Username = $this->config->get('Username', 'config', 'email', '');

		/**
		 * SMTP password.
		 *
		 * @type string
		 */
		$this->mail->Password = $this->config->get('Password', 'config', 'email', '');

		/**
		 * SMTP auth type.
		 * Options are LOGIN (default), PLAIN, NTLM, CRAM-MD5
		 *
		 * @type string
		 */
		$this->mail->AuthType = $this->config->get('AuthType', 'config', 'email', '');

		/**
		 * SMTP realm.
		 * Used for NTLM auth
		 *
		 * @type string
		 */
		$this->mail->Realm = $this->config->get('Realm', 'config', 'email', '');

		/**
		 * SMTP workstation.
		 * Used for NTLM auth
		 *
		 * @type string
		 */
		$this->mail->Workstation = $this->config->get('Workstation', 'config', 'email', '');

		/**
		 * The SMTP server timeout in seconds.
		 * Default of 5 minutes (300sec) is from RFC2821 section 4.5.3.2
		 *
		 * @type integer
		 */
		$this->mail->Timeout = $this->config->get('Timeout', 'config', 'email', 300);

		/**
		 * SMTP class debug output mode.
		 * Debug output level.
		 * Options:
		 * * `0` No output
		 * * `1` Commands
		 * * `2` Data and commands
		 * * `3` As 2 plus connection status
		 * * `4` Low-level data output
		 *
		 * @type integer
		 * @see SMTP::$do_debug
		 */
		$this->mail->SMTPDebug = $this->config->get('SMTPDebug', 'config', 'email', 0);

		/**
		 * How to handle debug output.
		 * Options:
		 * * `echo` Output plain-text as-is, appropriate for CLI
		 * * `html` Output escaped, line breaks converted to `<br>`, appropriate for browser output
		 * * `error_log` Output to error log as configured in php.ini
		 * Alternatively, you can provide a callable expecting two params: a message string and the debug level:
		 * <code>
		 * $this->mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";};
		 * </code>
		 *
		 * @type string|callable
		 * @see SMTP::$Debugoutput
		 */
		$this->mail->ebugoutput = $this->config->get('ebugoutput', 'config', 'email', 'echo');

		/**
		 * Whether to keep SMTP connection open after each message.
		 * If this is set to true then to close the connection
		 * requires an explicit call to smtpClose().
		 *
		 * @type boolean
		 */
		$this->mail->SMTPKeepAlive = $this->config->get('SMTPKeepAlive', 'config', 'email', false);

		/**
		 * Whether to split multiple to addresses into multiple messages
		 * or send them all in one message.
		 *
		 * @type boolean
		 */
		$this->mail->SingleTo = $this->config->get('SingleTo', 'config', 'email', false);

		/**
		 * Whether to generate VERP addresses on send.
		 * Only applicable when sending via SMTP.
		 *
		 * @link http://en.wikipedia.org/wiki/Variable_envelope_return_path
		 * @link http://www.postfix.org/VERP_README.html Postfix VERP info
		 * @type boolean
		 */
		$this->mail->do_verp = $this->config->get('do_verp', 'config', 'email', false);

		/**
		 * Whether to allow sending messages with an empty body.
		 *
		 * @type boolean
		 */
		$this->mail->AllowEmpty = $this->config->get('AllowEmpty', 'config', 'email', false);

		/**
		 * The default line ending.
		 *
		 * @note  The default remains "\n". We force CRLF where we know
		 *        it must be used via self::CRLF.
		 * @type string
		 */
		$this->mail->LE = $this->config->get('LE', 'config', 'email', "\n");

		/**
		 * DKIM selector.
		 *
		 * @type string
		 */
		$this->mail->DKIM_selector = $this->config->get('DKIM_selector', 'config', 'email', '');

		/**
		 * DKIM Identity.
		 * Usually the email address used as the source of the email
		 *
		 * @type string
		 */
		$this->mail->DKIM_identity = $this->config->get('DKIM_identity', 'config', 'email', '');

		/**
		 * DKIM passphrase.
		 * Used if your key is encrypted.
		 *
		 * @type string
		 */
		$this->mail->DKIM_passphrase = $this->config->get('DKIM_passphrase', 'config', 'email', '');

		/**
		 * DKIM signing domain name.
		 *
		 * @example 'example.com'
		 * @type string
		 */
		$this->mail->DKIM_domain = $this->config->get('DKIM_domain', 'config', 'email', '');

		/**
		 * DKIM private key file path.
		 *
		 * @type string
		 */
		$this->mail->DKIM_private = $this->config->get('DKIM_private', 'config', 'email', '');

		/**
		 * Callback Action function name.
		 * The function that handles the result of the send email action.
		 * It is called out by send() for each email sent.
		 * Value can be any php callable: http://www.php.net/is_callable
		 * Parameters:
		 *   boolean $result        result of the send action
		 *   string  $to            email address of the recipient
		 *   string  $cc            cc email addresses
		 *   string  $bcc           bcc email addresses
		 *   string  $subject       the subject
		 *   string  $body          the email body
		 *   string  $from          email address of sender
		 *
		 * @type string
		 */
		$this->mail->action_function = $this->config->get('action_function', 'config', 'email', '');
	}

	/**
	 * @param        $subject
	 * @param        $body
	 * @param bool   $isHtml
	 * @param string $altBody
	 * @param int    $priority
	 * @param int    $wordWrap
	 * @param string $iCal
	 * @throws SendEmailException
	 */
	public function send($subject, $body, $isHtml = false, $altBody = '', $priority = 3, $wordWrap = 0, $iCal = '')
	{
		if ($priority < 1 || $priority > 5)
		{
			$priority = 3;
		}

		/**
		 * Email priority.
		 * Options: 1 = High, 3 = Normal, 5 = low.
		 *
		 * @type integer
		 */
		$this->mail->Priority = $priority;

		/**
		 * The Subject of the message.
		 *
		 * @type string
		 */
		$this->mail->Subject = $subject;

		/**
		 * An HTML or plain text message body.
		 * If HTML then call isHTML(true).
		 *
		 * @type string
		 */
		$this->mail->Body = $body;

		/**
		 * The plain-text message body.
		 * This body can be read by mail clients that do not have HTML email
		 * capability such as mutt & Eudora.
		 * Clients that can read HTML will view the normal Body.
		 *
		 * @type string
		 */
		$this->mail->AltBody = $altBody;

		/**
		 * An iCal message part body.
		 * Only supported in simple alt or alt_inline message types
		 * To generate iCal events, use the bundled extras/EasyPeasyICS.php class or iCalcreator
		 *
		 * @link http://sprain.ch/blog/downloads/php-class-easypeasyics-create-ical-files-with-php/
		 * @link http://kigkonsult.se/iCalcreator/
		 * @type string
		 */
		$this->mail->Ical = $iCal;

		/**
		 * Word-wrap the message body to this number of chars.
		 *
		 * @type integer
		 */
		$this->mail->WordWrap = $wordWrap;

		/**
		 * What to use in the X-Mailer header.
		 * Options: null for default, whitespace for none, or a string to use
		 *
		 * @type string
		 */

		$this->mail->isHTML($isHtml);                                  // Set email format to HTML

		//  $options["ssl"]=array("verify_peer"=>false,"verify_peer_name"=>false,"allow_self_signed"=>true);
		if (!$this->mail->send())
		{
			FastLog::add('error_mail', ['ErrorInfo' => $this->mail->ErrorInfo], false, true);
		}
		else
		{
			FastLog::add('info_mail', ['ErrorInfo' => 'Message has been sent']);
		}
	}

	/**
	 * @param $email
	 * @param $name
	 * @throws SendEmailException
	 */
	public function addAddress($email, $name)
	{
		if (!Validate::isEmail($email))
		{
			throw new SendEmailException('Email is invalid');
		}
		$this->mail->addAddress($email, $name);
	}
} 
<?php
namespace MyPet\Notification\Email\Classes;

use KMCore\Parsing\Parser;
use MyPet\Notification\Email\Model\SendEmailModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class MessagePrepare
{
	protected $template;

	public function __construct($id, $lang)
	{
		$this->template = new SendEmailModel(['id' => $id, 'lang' => $lang]);
		if (!$this->template->isLoadedObject())
		{
			throw new \Exception('Not found templates');
		}
	}

	public function getMessage(Array $userdata = [])
	{
		return Parser::parseUserData($this->template->message, $userdata);
	}

	public function getSubject(Array $userdata = [])
	{
		return Parser::parseUserData($this->template->subject, $userdata);
	}
} 
<?php
namespace MyPet\Notification\Email\Classes;

use Exception;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SendEmailException extends Exception
{

} 
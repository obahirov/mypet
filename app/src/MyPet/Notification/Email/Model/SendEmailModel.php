<?php
namespace MyPet\Notification\Email\Model;

use KMCore\DB\ObjectModel;

/**
 * @property mixed message
 * @property mixed subject
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SendEmailModel extends ObjectModel
{
	protected static $pk = '_id';
	protected static $collection = 'mp_email_templates_model';
	protected static $slaveOkay = true;
	protected static $safeWrite = true;
	protected static $sort = ['_id' => 1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1, 'lang' => 1],
			'unique' => true
		],
		[
			'keys' => ['type' => 1]
		],
	];

	protected static $fieldsDefault = [
		'_id'     => '',
		'id'      => '',
		'lang'    => '',
		'type'    => [],
		'message' => '',
		'subject' => ''
	];

	protected static $fieldsValidate = [
		'_id'     => self::TYPE_MONGO_ID,
		'id'      => self::TYPE_STRING,
		'lang'    => self::TYPE_STRING,
		'type'    => self::TYPE_JSON,
		'message' => self::TYPE_STRING,
		'subject' => self::TYPE_STRING
	];
} 
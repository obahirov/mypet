<?php
namespace MyPet\Friends\Model;

use KMCore\DB\ObjectModel;

/**
 * Модель хранения друзей
 *
 * @property mixed  id
 * @property mixed  friends
 * @property mixed  online
 * @property mixed  timeout
 * @property string _id
 * @property mixed  payload
 * @property mixed  status
 * @property mixed  type
 * @property string friendId
 */
class FriendsModel extends ObjectModel
{
	protected static $pk = '_id';
	protected static $collection = 'mp_friends_model';
	protected static $slaveOkay = true;
	protected static $safeWrite = true;
	protected static $sort = ['status' => 1, '_id' => 1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1, 'friendId' => 1],
			'unique' => true
		],
		[
			'keys' => ['id' => 1, 'status' => 1],
		],
		[
			'keys' => ['status' => 1, '_id' => 1]
		]
	];

	protected static $fieldsDefault = [
		'_id'      => '',
		'id'       => '',
		'friendId' => '',
		'status'   => 0,
		'type'     => '',
		'timeout'  => 0,
	];

	protected static $fieldsValidate = [
		'_id'      => self::TYPE_MONGO_ID,
		'id'       => self::TYPE_STRING,
		'friendId' => self::TYPE_STRING,
		'status'   => self::TYPE_UNSIGNED_INT,
		'type'     => self::TYPE_STRING,
		'timeout'  => self::TYPE_TIMESTAMP,
	];
}
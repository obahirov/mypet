<?php
namespace MyPet\Friends\Classes;

use KMCore\CMS\Classes\PageVariable;
use KMCore\Helper\ArrayDataHelper;
use MyPet\CMS\Classes\Plugins\Page\AboutUser;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Model\Online;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class InteractionOfFriends
{
	/**
	 * @param $userId
	 * @param $sn
	 * @param $socIds
	 */
	public static function syncSocialFriends($userId, $sn, Array $socIds)
	{
		$friendManager = new FriendsManager();

		if (!empty($socIds))
		{
			$data = User::getAllAdvanced(
				['sn' => $sn, 'socId' => ['$in' => $socIds]],
				['id' => 1, 'socId' => 1],
				false,
				true,
				false,
				false,
				true
			);
			unset($socIds);
			while ($data->hasNext())
			{
				$friendData = $data->getNext();
				if (!empty($friendData['id']))
				{
					$friendManager->syncFriend($userId, $friendData['id'], $sn);
				}
				usleep(10); // 0.0001 s
			}
		}
	}

	public static function getUsersData($pets)
	{
		$ownerIds = [];
		foreach ($pets as $pet)
		{
			$ownerIds = array_merge($pet['ownerIds'], $ownerIds);
		}

		$cursor = User::getAllAdvanced(
			['id' => ['$in' => $ownerIds]],
			[
				'id'         => 1,
				'photo'      => 1,
				'first_name' => 1,
				'last_name'  => 1
			],
			false,
			true,
			false,
			false,
			true
		);

		$profile = [];
		while ($cursor->hasNext())
		{
			$data = $cursor->getNext();
			$id = isset($data['id']) ? $data['id'] : '';
			$profile[$id]['id'] = $id;
			$profile[$id]['photo'] = isset($data['photo']) ? $data['photo'] : '';
			$profile[$id]['first_name'] = isset($data['first_name']) ? $data['first_name'] : '';
			$profile[$id]['last_name'] = isset($data['last_name']) ? $data['last_name'] : '';
		}

		return $profile;
	}

	public static function getPetsData($owners)
	{
		$ownerIds = [];
		foreach ($owners as $owner)
		{
			$ownerIds[] = $owner['id'];
		}

		$cursor = PetModel::getAllAdvanced(
			['ownerIds' => ['$in' => $ownerIds]],
			[
				'id'       => 1,
				'photo'    => 1,
				'hNick'    => 1,
				'ownerIds' => 1,
				'type'     => 1,
			],
			false,
			true,
			false,
			false,
			true
		);

		$profile = [];
		while ($cursor->hasNext())
		{
			$data = $cursor->getNext();
			$id = isset($data['id']) ? $data['id'] : '';
			$profile[$id]['id'] = $id;
			$profile[$id]['photo'] = isset($data['photo']) ? $data['photo'] : '';
			$profile[$id]['hNick'] = isset($data['hNick']) ? $data['hNick'] : '';
			$profile[$id]['ownerIds'] = isset($data['ownerIds']) ? $data['ownerIds'] : [];
			$profile[$id]['type'] = isset($data['type']) ? $data['type'] : '';
		}

		return $profile;
	}

	/**
	 * @param       $userId
	 * @param array $friends
	 * @return array
	 */
	public static function getProfileFriends($userId, Array $friends)
	{
		$friendIds = [];
		foreach ($friends as $friend)
		{
			$friendIds[] = $friend['friendId'];
		}

		$profile = [];

		$cursor = User::getAllAdvanced(
			['id' => ['$in' => $friendIds]],
			[
				'id'           => 1,
				'created'      => 1,
				'photo'        => 1,
				'first_name'   => 1,
				'last_name'    => 1,
				'birth_y'      => 1,
				'isSpecialist' => 1,
				'status'       => 1,
			],
			false,
			true,
			false,
			false,
			true
		);

		while ($cursor->hasNext())
		{
			$data = $cursor->getNext();
			$id = isset($data['id']) ? $data['id'] : '';
			$profile[$id]['id'] = $id;
			$profile[$id]['created'] = isset($data['created']) ? $data['created'] : 0;
			$profile[$id]['photo'] = isset($data['photo']) ? $data['photo'] : '';
			$profile[$id]['first_name'] = isset($data['first_name']) ? $data['first_name'] : '';
			$profile[$id]['last_name'] = isset($data['last_name']) ? $data['last_name'] : '';
			$profile[$id]['age'] = AboutUser::getAgeToRender($data, PageVariable::getLangName());
			$profile[$id]['isSpecialist'] = isset($data['isSpecialist']) ? $data['isSpecialist'] : false;
			$profile[$id]['status'] = isset($data['status']) ? $data['status'] : '';
		}

		$cursor = Online::getAllAdvanced(
			['id' => ['$in' => $friendIds]],
			['id' => 1, 'online' => 1],
			false,
			true,
			false,
			false,
			true
		);

		while ($cursor->hasNext())
		{
			$data = $cursor->getNext();
			$id = isset($data['id']) ? $data['id'] : '';
			$profile[$id]['online'] = false;
			if (isset($data['time']) && $data['time'] + 900 >= time())
			{
				$profile[$id]['online'] = $data['online'];
			}
		}

		$pets = PetModel::getAllAdvanced(
			['ownerIds' => ['$in' => $friendIds]],
			[
				'id'       => 1,
				'photo'    => 1,
				'ownerIds' => 1,
				'type'     => 1,
			],
			false,
			true,
			false,
			false,
			true
		);

		foreach ($pets as $data)
		{
			foreach ($data['ownerIds'] as $id)
			{
				$petId = isset($data['id']) ? $data['id'] : '';
				$profile[$id]['pets_photo'][$petId]['photo'] = isset($data['photo']) ? $data['photo'] : '';
				$profile[$id]['pets_photo'][$petId]['type'] = isset($data['type']) ? $data['type'] : 'cat';
				if (count($profile[$id]['pets_photo']) >= 3)
				{
					continue;
				}
			}
		}

		$required_profile = [
			'id'           => '',
			'created'      => 0,
			'photo'        => '',
			'first_name'   => '',
			'last_name'    => '',
			'pets_photo'   => [],
			'age'          => '',
			'isSpecialist' => false,
			'status'       => '',
			'online'       => false,
		];

		$results = [];
		foreach ($friends as $friend)
		{
			$result['userId'] = $friend['friendId'];
			$result['status'] = isset($friend['status']) ? $friend['status'] : FriendsManager::STATUS_CONFIRMED;
			$result['profile'] = isset($profile[$friend['friendId']]) ? ArrayDataHelper::filterRequiredParams(
				$profile[$friend['friendId']],
				$required_profile
			) : [];
			$result['type'] = isset($friend['type']) ? $friend['type'] : FriendsManager::TYPE_BASE;
			$result['timeout'] = isset($friend['timeout']) ? $friend['timeout'] : 0;
			$results[] = $result;
			usleep(10);
		}

		return $results;
	}

	/**
	 * @param array $friendIds
	 * @return array
	 */
	public static function getProfile(Array $friendIds)
	{
		$profile = [];

		$cursor = User::getAllAdvanced(
			['id' => ['$in' => $friendIds]],
			[
				'id'         => 1,
				'created'    => 1,
				'photo'      => 1,
				'first_name' => 1,
				'last_name'  => 1,
				'profession' => 1,
			],
			false,
			true,
			false,
			false,
			true
		);

		while ($cursor->hasNext())
		{
			$data = $cursor->getNext();
			$id = isset($data['id']) ? $data['id'] : '';
			$profile[$id]['id'] = $id;
			$profile[$id]['created'] = isset($data['created']) ? $data['created'] : 0;
			$profile[$id]['photo'] = isset($data['photo']) ? $data['photo'] : '';
			$profile[$id]['first_name'] = isset($data['first_name']) ? $data['first_name'] : '';
			$profile[$id]['last_name'] = isset($data['last_name']) ? $data['last_name'] : '';
			$profile[$id]['profession'] = isset($data['profession']) ? $data['profession'] : '';
		}

		$required_profile = [
			'id'         => '',
			'created'    => 0,
			'photo'      => '',
			'first_name' => '',
			'last_name'  => '',
			'profession' => '',
		];

		$results = [];
		foreach ($friendIds as $friendId)
		{
			$results[$friendId] = isset($profile[$friendId]) ? ArrayDataHelper::filterRequiredParams(
				$profile[$friendId],
				$required_profile
			) : [];
			usleep(10);
		}

		return $results;
	}

	/**
	 * @param array $friendIds
	 * @return array
	 */
	public static function getProfileSpecialist(Array $friendIds)
	{
		$profile = [];

		$cursor = User::getAllAdvanced(
			['id' => ['$in' => $friendIds]],
			[
				'id'         => 1,
				'created'    => 1,
				'photo'      => 1,
				'first_name' => 1,
				'last_name'  => 1,
				'profession' => 1,
			],
			false,
			true,
			false,
			false,
			true
		);

		while ($cursor->hasNext())
		{
			$data = $cursor->getNext();
			$id = isset($data['id']) ? $data['id'] : '';
			$profile[$id]['id'] = $id;
			$profile[$id]['created'] = isset($data['created']) ? $data['created'] : 0;
			$profile[$id]['photo'] = isset($data['photo']) ? $data['photo'] : '';
			$profile[$id]['first_name'] = isset($data['first_name']) ? $data['first_name'] : '';
			$profile[$id]['last_name'] = isset($data['last_name']) ? $data['last_name'] : '';
			$profile[$id]['profession'] = isset($data['profession']) ? $data['profession'] : '';
		}

		$required_profile = [
			'id'         => '',
			'created'    => 0,
			'photo'      => '',
			'first_name' => '',
			'last_name'  => '',
			'profession' => '',
		];

		$results = [];
		foreach ($friendIds as $friendId)
		{
			$results[$friendId] = isset($profile[$friendId]) ? ArrayDataHelper::filterRequiredParams(
				$profile[$friendId],
				$required_profile
			) : ['first_name' => $friendId, 'domId' => uniqid()];
			usleep(10);
		}

		return $results;
	}
} 
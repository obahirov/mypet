<?php
namespace MyPet\Friends\Classes;

use Evenement\EventEmitter;
use MyPet\Friends\Model\FriendsModel;

/**
 * Класс для управления списками друзей, и их статусами.
 */
class FriendsManager extends EventEmitter
{
	const EVENT_FRIEND_ADD = 'event.friend.add';
	const EVENT_FRIEND_CONFIRM = 'event.friend.confirm';

	const FRIENDS_PER_PAGE = 100;
	const PROFILES_PER_PAGE = 20;
	const STATUS_WAITS = 3;
	const STATUS_RECEIVED = 2;
	const STATUS_CONFIRMED = 1;

	const TYPE_BASE = 'tx';

	const TIMEOUT_FRIEND = 1800;

	/**
	 * @param        $id
	 * @param        $friendId
	 * @param string $type
	 */
	public function syncFriend($id, $friendId, $type = self::TYPE_BASE)
	{
		$user = new FriendsModel(['id' => $id, 'friendId' => $friendId]);
		$friend = new FriendsModel(['id' => $friendId, 'friendId' => $id]);

		if (!$user->isLoadedObject())
		{
			$user->id = $id;
			$user->friendId = $friendId;
		}
		if (!$friend->isLoadedObject())
		{
			$friend->id = $friendId;
			$friend->friendId = $id;
		}
		$user->status = self::STATUS_CONFIRMED;
		$friend->status = self::STATUS_CONFIRMED;
		$user->type = $type;
		$friend->type = $type;

		$user->save();
		$friend->save();
		$user->clearStaticCache();
		$friend->clearStaticCache();
	}

	/**
	 * @param $id
	 * @param $friendId
	 * @return FriendsModel
	 */
	public function getFriend($id, $friendId)
	{
		$user = new FriendsModel(['id' => $id, 'friendId' => $friendId]);

		return $user;
	}

	/**
	 * Метод добавления друга
	 *
	 * @param        $id
	 * @param string $friendId
	 * @throws FriendsException
	 * @return $this
	 */
	public function addFriend($id, $friendId)
	{
		if ($id === $friendId)
		{
			throw new FriendsException('$id === $friendId: '.$id);
		}

		$user = new FriendsModel(['id' => $id, 'friendId' => $friendId]);
		$friend = new FriendsModel(['id' => $friendId, 'friendId' => $id]);

		if (!$user->isLoadedObject())
		{
			$user->id = $id;
			$user->friendId = $friendId;
		}
		if (!$friend->isLoadedObject())
		{
			$friend->id = $friendId;
			$friend->friendId = $id;
		}

		if ($friend->status === self::STATUS_WAITS || $friend->status === self::STATUS_CONFIRMED)
		{
			$user->status = self::STATUS_CONFIRMED;
			$friend->status = self::STATUS_CONFIRMED;
			$this->emit(self::EVENT_FRIEND_CONFIRM, [$user]);
		}
		else
		{
			$user->status = self::STATUS_WAITS;
			$friend->status = self::STATUS_RECEIVED;

			if ($user->timeout <= time())
			{
				$this->emit(self::EVENT_FRIEND_ADD, [$user]);
				$user->timeout = time() + self::TIMEOUT_FRIEND;
			}
		}

		$user->type = self::TYPE_BASE;
		$friend->type = self::TYPE_BASE;

		$user->save();
		$friend->save();
		$user->clearStaticCache();
		$friend->clearStaticCache();
	}

	/**
	 * Метод добавления друга
	 *
	 * @param        $id
	 * @param string $friendId
	 * @return $this
	 */
	public function remFriend($id, $friendId)
	{
		$user = new FriendsModel(['id' => $id, 'friendId' => $friendId]);
		$friend = new FriendsModel(['id' => $friendId, 'friendId' => $id]);
		$user->delete();
		$friend->delete();
	}

	/**
	 * Возвращает всех друзей пользователя
	 *
	 * @param      $id
	 * @param int  $page
	 * @param int  $perPage
	 * @param null $status
	 * @return array
	 */
	public function getFriends($id, $page = 0, $perPage = self::FRIENDS_PER_PAGE, $status = null)
	{
		if ($status === null)
		{
			return FriendsModel::getAllAdvanced(
				['id' => $id],
				[],
				false,
				true,
				$page,
				$perPage
			);
		}
		else
		{
			return FriendsModel::getAllAdvanced(
				['id' => $id, 'status' => $status],
				[],
				false,
				true,
				$page,
				$perPage
			);
		}
	}

	/**
	 * @param array $query
	 * @return array
	 */
	public function getFriendsCursor(Array $query)
	{
		$cursor = FriendsModel::getAllAdvanced(
			$query,
			[],
			false,
			true,
			false,
			false,
			true
		);

		return $cursor;
	}

	/**
	 * @param      $id
	 * @param null $status
	 * @return int
	 */
	public static function countFriends($id, $status = null)
	{
		if ($status === null)
		{
			return FriendsModel::count(['id' => $id]);
		}
		else
		{
			return FriendsModel::count(['id' => $id, 'status' => $status]);
		}
	}

	/**
	 * @param      $id
	 * @param int  $perPage
	 * @param null $status
	 * @return float
	 */
	public function countPagesFriend($id, $perPage = self::FRIENDS_PER_PAGE, $status = null)
	{
		return (int)ceil(static::countFriends($id, $status) / $perPage);
	}

	/**
	 * @param $id
	 * @param $friendId
	 * @return bool
	 */
	public static function isFriend($id, $friendId)
	{
		$user = new FriendsModel(['id' => $id, 'friendId' => $friendId]);

		return ($user->isLoadedObject() && $user->status === self::STATUS_CONFIRMED);
	}
}
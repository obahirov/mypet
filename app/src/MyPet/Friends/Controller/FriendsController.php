<?php
namespace MyPet\Friends\Controller;

use MyPet\Friends\Classes\FriendsManager;
use MyPet\Friends\Classes\InteractionOfFriends;
use MyPet\Friends\Model\FriendsModel;
use MyPet\Inbox\Classes\RequestData;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\ApiController;

/**
 * Контроллер управления друзьями
 *
 * @api
 */
class FriendsController extends ApiController
{
	/**
	 * @var FriendsManager
	 */
	protected $friendsManager;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->friendsManager = new FriendsManager();
	}

	public function add()
	{
		$id = (string)$this->user->id;
		$friendId = (string)$this->request->getGetOrPost('friendId', '');

		$userObj = $this->user;
		$this->friendsManager->on(
			FriendsManager::EVENT_FRIEND_ADD,
			function (FriendsModel $user) use ($userObj)
			{
				$attributes = [
					'userId'   => $user->id,
					'friendId' => $user->friendId,
					'payload'  => $userObj->getFieldsSanitized(),
				];
				RequestData::init(new RequestData($user->friendId))->setType(
					RequestManager::TYPE_INVITE_CONFIRM
				)->setAttributes($attributes)->setUnique($user->_id)->setPub(true)->update();
			}
		);

		$this->friendsManager->on(
			FriendsManager::EVENT_FRIEND_CONFIRM,
			function (FriendsModel $user) use ($userObj)
			{
				$manger = new RequestManager();
				$manger->collectRequest((string)$user->_id, $this->user->id);
			}
		);

		if (!empty($friendId))
		{
			$this->friendsManager->addFriend($id, $friendId);
		}

		$this->data = ['success' => true];
		$this->success = true;
		$this->response();
	}

	public function delete()
	{
		$id = (string)$this->user->id;
		$friendId = (string)$this->request->getGetOrPost('friendId', '');

		$this->friendsManager->remFriend($id, $friendId);

		$this->data = ['success' => true];
		$this->success = true;
		$this->response();
	}

	public function count()
	{
		$id = (string)$this->user->id;
		$count = $this->friendsManager->countFriends($id);

		$this->data['count'] = $count;
		$this->success = true;
		$this->response();
	}

	public function getFriends()
	{
		$id = (string)$this->user->id;
		$page = (int)$this->request->getGetOrPost('page', 0);
		$results = $this->friendsManager->getFriends($id, $page, FriendsManager::FRIENDS_PER_PAGE);
		$pages = $this->friendsManager->countPagesFriend($id, FriendsManager::FRIENDS_PER_PAGE);
		$count = $this->friendsManager->countFriends($id);

		$this->data['friends'] = $results;
		$this->data['pages'] = $pages;
		$this->data['perPage'] = FriendsManager::FRIENDS_PER_PAGE;
		$this->data['count'] = $count;
		$this->success = true;
		$this->response();
	}

	public function getFriendProfiles()
	{
		$id = (string)$this->user->id;
		$page = (int)$this->request->getGetOrPost('page', 0);
		$results = $this->friendsManager->getFriends($id, $page, FriendsManager::PROFILES_PER_PAGE);
		$pages = $this->friendsManager->countPagesFriend($id, FriendsManager::PROFILES_PER_PAGE);
		$count = $this->friendsManager->countFriends($id);

		$this->data['profiles'] = InteractionOfFriends::getProfileFriends($id, $results);
		$this->data['pages'] = $pages;
		$this->data['perPage'] = FriendsManager::PROFILES_PER_PAGE;
		$this->data['count'] = $count;
		$this->success = true;
		$this->response();
	}

	public function getUserProfile()
	{
		$id = (string)$this->user->id;
		$userIds = (array)$this->request->getGetOrPost('userIds', []);
		$userIds = array_slice($userIds, 0, 100);
		$ids = [];
		foreach ($userIds as $id)
		{
			if (!empty($id))
			{
				$ids[] = ['friendId' => $id];
			}
		}

		$this->data['profiles'] = InteractionOfFriends::getProfileFriends($id, $ids);
		$this->success = true;
		$this->response();
	}

	public function getProfile()
	{
		$id = (string)$this->user->id;
		$userId = (string)$this->request->getGetOrPost('friendId', '');

		$this->data['profiles'][] = InteractionOfFriends::getProfile($id, [$userId]);
		$this->success = true;
		$this->response();
	}

	public function isFriend()
	{
		$id = (string)$this->user->id;
		$friendId = (string)$this->request->getGetOrPost('friendId', '');

		$this->data['isFriend'] = $this->friendsManager->isFriend($id, $friendId);

		$this->success = true;
		$this->response();
	}
}

<?php
namespace MyPet\Location\Controller;

use MyPet\Location\Model\City;
use MyPet\Location\Model\Region;
use MyPet\SiteController;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class LocationController extends SiteController
{
	public function getRegionsByCountry()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$country = $this->request->getGetOrPost('country');

		$regions = Region::getAllAdvanced(['country' => $country], [], true, true, 0, 1000);
		$this->response->json(['regions' => $regions]);
	}

	public function getCitiesByRegion()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$region = $this->request->getGetOrPost('region');

		$cities = City::getAllAdvanced(['region' => $region], [], true, true, 0, 1000);
		$this->response->json(['cities' => $cities]);
	}
} 
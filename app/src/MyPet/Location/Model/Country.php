<?php
namespace MyPet\Location\Model;

use KMCore\DB\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Country extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'mp_country';
	protected static $pk = '_id';
	protected static $sort = ['priority' => -1];
	protected static $indexes = [
		[
			'keys' => ['priority' => 1],
		],
		//		[
		//			'keys'   => ['id' => 1],
		//		]
	];

	protected static $fieldsDefault = [
		'_id'      => '',
		'en'       => '',
		'ua'       => '',
		'ru'       => '',
		'priority' => 0,
		//		'id'       => 0
	];

	protected static $fieldsValidate = [
		'_id'      => self::TYPE_STRING,
		'en'       => self::TYPE_STRING,
		'ua'       => self::TYPE_STRING,
		'ru'       => self::TYPE_STRING,
		'priority' => self::TYPE_UNSIGNED_INT,
		//		'id'       => self::TYPE_UNSIGNED_INT
	];
} 
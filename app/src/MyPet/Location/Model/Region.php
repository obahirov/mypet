<?php
namespace MyPet\Location\Model;

use KMCore\DB\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Region extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $collection = 'mp_region';
	protected static $pk = '_id';
	protected static $sort = ['priority' => -1];
	protected static $indexes = [
		[
			'keys' => ['country' => 1],
		],
		[
			'keys' => ['priority' => -1],
		],
		//		[
		//			'keys'   => ['id' => 1],
		//		]
	];

	protected static $fieldsDefault = [
		'_id'      => '',
		'en'       => '',
		'ua'       => '',
		'ru'       => '',
		'country'  => '',
		'priority' => 0,
		//		'id'       => 0
	];

	protected static $fieldsValidate = [
		'_id'      => self::TYPE_STRING,
		'en'       => self::TYPE_STRING,
		'ua'       => self::TYPE_STRING,
		'ru'       => self::TYPE_STRING,
		'country'  => self::TYPE_STRING,
		'priority' => self::TYPE_UNSIGNED_INT,
		//		'id'       => self::TYPE_UNSIGNED_INT
	];
} 
<?php
namespace MyPet\Search\Controller;

use KMCore\DB\Elasticsearch\Common\Exceptions\UnexpectedValueException;
use KMCore\DB\Elasticsearch\ElasticsearchManager;
use KMCore\Helper\ArrayDataHelper;
use MyPet\ApiController;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SearchController extends ApiController
{
	/**
	 * imple query string syntaxedit
	 * The simple_query_string supports the following special characters:
	 * + signifies AND operation
	 * | signifies OR operation
	 * - negates a single token
	 * " wraps a number of tokens to signify a phrase for searching
	 * at the end of a term signifies a prefix query
	 * ( and ) signify precedence
	 * ~N after a word signifies edit distance (fuzziness)
	 * ~N after a phrase signifies slop amount
	 */
	public function findUsers()
	{
		if (!AuthManager::loggedIn())
		{
			throw new UnexpectedValueException('Not auth user');
		}

		$perPage = 10;
		$from = (int)$this->request->getGetOrPost('from', 0);
		$query = $this->request->getGetOrPost('query', '', true);
		$params = [];
		$params['index'] = 'mypet';
		$params['type'] = 'user';
		$params['size'] = $perPage;
		$params['from'] = $from;
		$query_string['query'] = $query;
		$query_string['fields'] = ["email", "*_name"];
		$params['body']['query']['query_string'] = $query_string;

		$data = ElasticsearchManager::getInstance()->search($params);

		$this->data = $data;
		$this->success = true;
		$this->response();
	}

	public function findPets()
	{
		if (!AuthManager::loggedIn())
		{
			throw new UnexpectedValueException('Not auth user');
		}
		$userId = AuthManager::getSessionUserId();

		$perPage = 10;
		$from = (int)$this->request->getGetOrPost('from', 0);
		$query = $this->request->getGetOrPost('query', '', true);
		$params = [];
		$params['index'] = 'mypet';
		$params['type'] = 'pet';
		$params['size'] = $perPage;
		$params['from'] = $from;
		$query_string['query'] = $query;
		$query_string['fields'] = ["hNick", "nick", "breed", "passport"];
		$params['body']['query']['query_string'] = $query_string;

		$data = ElasticsearchManager::getInstance()->search($params);

		foreach ($data['hits']['hits'] as $k => &$item)
		{
			$isYourPet = (array_search($userId, $item['_source']['ownerIds']) !== false);
			$item['sort'] = $k + $item['sort'];
			if ($isYourPet)
			{
				$item['sort'] = 0;
			}
		}
		ArrayDataHelper::keyMultiSort($data['hits']['hits'], 'sort', true, false, false);

		$this->data = $data;
		$this->success = true;
		$this->response();
	}

	public function findMixed()
	{
		if (!AuthManager::loggedIn())
		{
			throw new UnexpectedValueException('Not auth user');
		}

		$perPage = 10;
		$from = (int)$this->request->getGetOrPost('from', 0);
		$query = $this->request->getGetOrPost('query', '', true);
		$params = [];
		$params['index'] = 'mypet';
		$params['size'] = $perPage;
		$params['from'] = $from;
		$query_string['query'] = $query;
		$query_string['fields'] = ["nick", "hNick", "breed", "passport", "email", "*_name"];
		$params['body']['query']['query_string'] = $query_string;

		$this->data = ElasticsearchManager::getInstance()->search($params);
		$this->success = true;
		$this->response();
	}

	public function getUsersByUserIds()
	{
		$users = $this->request->getGetOrPost('users', []);
		$usersData = [];
		if(!empty($users))
		{
			$usersData = User::getAllAdvanced(['id' => ['$in' => $users]], [], true, true);
		}
		$this->response->json($usersData);
	}

	public function getPetsByUserIds()
	{
		$users = $this->request->getGetOrPost('users', []);
		$petsData = [];
		if(!empty($users))
		{
			$petsData = PetModel::getAllAdvanced(['ownerIds' => ['$in' => $users]], [], true, true);
		}
		$this->response->json($petsData);
	}
} 
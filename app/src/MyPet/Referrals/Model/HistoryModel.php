<?php
namespace MyPet\Referrals\Model;
use KMCore\DB\ObjectModel;

/**
 * Class History
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class HistoryModel extends ObjectModel
{
	protected static $collection = 'mp_rf_history_model';
	protected static $sort = ['id' => 1];
	protected static $indexes = [
		[
			'keys' => ['id' => 1, 'referral' => 1],
		]
	];

	protected static $fieldsDefault = [
		'_id'        => '',
		'id'        => '',
		'referralId'     => '',
	];

	protected static $fieldsValidate = [
		'_id'        => self::TYPE_MONGO_ID,
		'id'     => self::TYPE_STRING,
		'referralId'     => self::TYPE_STRING,
	];
}
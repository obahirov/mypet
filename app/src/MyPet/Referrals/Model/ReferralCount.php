<?php
namespace MyPet\Referrals\Model;

use KMCore\DB\ObjectModel;

/**
 * @property mixed id
 * @property mixed email
 * @property mixed genId
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ReferralCount extends ObjectModel
{
	protected static $collection = 'mp_rf_count_model';
	protected static $sort = ['id' => 1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['count' => 1],
		]
	];

	protected static $fieldsDefault = [
		'id'    => '',
		'count' => 0,
	];

	protected static $fieldsValidate = [
		'id'    => self::TYPE_STRING,
		'count' => self::TYPE_UNSIGNED_INT,
	];
} 
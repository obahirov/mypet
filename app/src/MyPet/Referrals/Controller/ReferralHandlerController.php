<?php
namespace MyPet\Referrals\Controller;

use KMCore\BaseController;
use KMCore\Helper\RedirectManager;
use KMCore\Helper\SessionStorage;
use MyPet\Referrals\Classes\LinkGenerator;
use MyPet\Referrals\Classes\ReferralHandler;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ReferralHandlerController extends BaseController
{
	protected $linkGenerator;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->linkGenerator = new LinkGenerator($this->server);
	}

	public function compleate()
	{
		if (AuthManager::loggedIn())
		{
			RedirectManager::redirect('/');
			throw new \Exception('You auth user');
		}

		$code = $this->request->getGetOrPost('code');
		$id = $this->linkGenerator->parseCode($code);
		$referral = new ReferralHandler($id);
		$referral->run();

		RedirectManager::redirect('/');
//		$this->response->json(['id' => $id, 'redirect' => '/#referral']);
	}

	public function getLink()
	{
		if (!AuthManager::loggedIn())
		{
			throw new \Exception('You not auth user');
		}
		$linkGenerator = new LinkGenerator($this->server);
		$link = $linkGenerator->getLink(AuthManager::getSessionUserId());

		$this->response->json(['link' => $link]);
	}
} 
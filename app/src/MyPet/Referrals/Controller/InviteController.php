<?php
namespace MyPet\Referrals\Controller;

use KMCore\Helper\RedirectManager;
use KMCore\Helper\SessionStorage;
use KMCore\SocialApi\SocialApiSelector;
use KMCore\SocialApi\SocialConfig;
use KMCore\Validation\Validate;
use MyPet\Notification\Email\Classes\MessagePrepare;
use MyPet\Notification\Email\Classes\SendEmailManager;
use MyPet\Referrals\Classes\LinkGenerator;
use MyPet\SiteController;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\UserManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class InviteController extends SiteController
{
	protected $linkGenerator;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->linkGenerator = new LinkGenerator($this->server);
	}

	public function invite()
	{
		if (!AuthManager::loggedIn())
		{
			$this->responseByForm('you_not_logged_user', '/', false);
		}

		$userId = AuthManager::getSessionUserId();
		$friendEmails = $this->request->getGetOrPost('friendEmails', '');
		$friendEmails = explode(',', $friendEmails);
		$friendEmails = array_slice($friendEmails, 0, 100);

		$user = UserManager::getUserModelByUserId($userId);
		$link = $this->linkGenerator->getLink($userId);
		foreach ($friendEmails as $friendEmail)
		{
			$friendEmail = trim($friendEmail);
			if (empty($friendEmail) || !Validate::isEmail($friendEmail))
			{
				$this->responseByForm('not_correct_email', '/', false);
			}

			$friend = UserManager::getUserByEmail($friendEmail);
			if (!$friend->isLoadedObject())
			{
				$langName = $this->request->getGetOrPost('langName', SessionStorage::get('langName'));
				$message = new MessagePrepare('inviteFriend', $langName);

				$sendManager = new SendEmailManager($this->server->getContainer('config'));
				$sendManager->addAddress($friendEmail, $friendEmail);
				$sendManager->send($message->getSubject([]), $message->getMessage(['link' => $link, 'first_name' => $user->first_name, 'last_name' => $user->last_name]), true, '', 3, 50);
			}
			usleep(10);
		}
	}
} 
<?php
namespace MyPet\Referrals\Classes;
use KMCore\Helper\SessionStorage;
use MyPet\Referrals\Model\HistoryModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ReferralHandler
{
	protected $id;

	/**
	 * @param $id
	 */
	public function __construct($id)
	{
		$this->id = $id;
	}

	public function run()
	{
		if(!SessionStorage::issetSession('id_ref'))
		{
			SessionStorage::set('id_ref', $this->id);
		}
	}

	public static function registration($userId)
	{
		if(SessionStorage::issetSession('id_ref'))
		{
			$id = SessionStorage::get('id_ref');
			$referral = $userId;

			$history = new HistoryModel(['id' => $id, 'referralId' => $referral]);
			if(!$history->isLoadedObject())
			{
				$history->_id = new \MongoId();
				$history->id = $id;
				$history->referralId = $referral;
				$history->save();
			}
		}
	}
} 
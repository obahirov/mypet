<?php
namespace MyPet\Referrals\Classes;

use KMCore\Helper\GeneratorHelper;
use KMCore\ServerContainer;
use MyPet\Referrals\Model\LinkStorage;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class LinkGenerator
{
	const LINK_REFERRAL_COMPLEATE_PATH = 'referral/compleate';
	protected $server;
	protected $model;

	/**
	 * @param ServerContainer $server
	 * @param                 $id
	 */
	public function __construct(ServerContainer $server, $id = null)
	{
		$this->server = $server;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function generateCode($id)
	{
		$salt = $this->server->getContainer('config')->get('salt', 'xor');
		$pass = $this->server->getContainer('config')->get('pass', 'xor');
		$code = urlencode(base64_encode(GeneratorHelper::xorCode($id, $pass, $salt)));

		return $code;
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function getLink($id)
	{
		$code = $this->generateCode($id);
		$baseUrl = $this->server->getContainer('config')->get('baseUrl');

		return rtrim($baseUrl, '/').'/'.self::LINK_REFERRAL_COMPLEATE_PATH.'?code='.$code;
	}

	/**
	 * @param $code
	 * @return int
	 */
	public function parseCode($code)
	{
		$salt = $this->server->getContainer('config')->get('salt', 'xor');
		$pass = $this->server->getContainer('config')->get('pass', 'xor');
		$id = GeneratorHelper::xorCode(base64_decode(urldecode($code)), $pass, $salt);

		return $id;
	}
} 
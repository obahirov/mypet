<?php
namespace MyPet\FileUpload\Classes;

use Imagine\Image\Box;
use Imagine\Image\Point;
use KMCore\ServerContainer;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ImageUpload extends FileUpload
{
	protected $allowedExts = ["gif"  => 1,
	                          "jpeg" => 1,
	                          "jpg"  => 1,
	                          "png"  => 1,
	                          "GIF"  => 1,
	                          "JPEG" => 1,
	                          "JPG"  => 1,
	                          "PNG"  => 1
	];
	protected $imagine;

	/**
	 * @param ServerContainer $server
	 * @throws \UnexpectedValueException
	 * @throws \Exception
	 */
	public function __construct(ServerContainer $server)
	{
		parent::__construct($server);

		if (class_exists('Gmagick'))
		{
			$this->imagine = new \Imagine\Gmagick\Imagine();
		}
		elseif (class_exists('Imagick'))
		{
			$this->imagine = new \Imagine\Imagick\Imagine();
		}
		elseif (function_exists('gd_info'))
		{
			$this->imagine = new \Imagine\Gd\Imagine();
		}
		else
		{
			throw new \Exception('Not found graphic lib');
		}
	}

	/**
	 * @param $filename
	 * @param $name
	 * @param $error
	 * @return array
	 * @throws FileUploadException
	 */
	public function prepare($name)
	{
		$image = $this->imagine->open($this->pathToUserUploadDir.'/'.$name);
		$size = $image->getSize();
		$width = $size->getWidth();
		$height = $size->getHeight();

		if (static::$errorToSmallImage === false || $this->getMinHeight() <= $height && $this->getMinWidth() <= $width)
		{
			$size = abs(($width - $height));
			if ($size > 0)
			{
				$size = round($size / 2);
				if ($width > $height)
				{
					$point1 = $size;
					$point2 = 0;
					$line = $height;
				}
				elseif ($width < $height)
				{
					$point1 = 0;
					$point2 = $size;
					$line = $width;
				}
			}
			else
			{
				$point1 = 0;
				$point2 = 0;
				$line = $width;
			}

			if ($height > $width)
			{
				$medium = $image->thumbnail(new Box($this->getMinHeight(), $this->getMinWidth()));
			}
			else
			{
				$medium = $image->thumbnail(new Box($this->getMinWidth(), $this->getMinHeight()));
			}

			$image->crop(
				new Point(
					$point1, $point2
				),
				new Box($line, $line)
			);
			$thumbnail = $image->thumbnail(new Box($this->getMinThumbnail(), $this->getMinThumbnail()));

			$options = ['format' => 'jpeg', 'quality' => $this->getQuality()];
			$image->save($this->pathToUserUploadDir.'/'.$name, $options);
			$medium->save($this->pathToUserUploadDir.'/'.'medium_'.$name, $options);
			$thumbnail->save($this->pathToUserUploadDir.'/'.'thumbnail_'.$name, $options);

			$response = [
				'status'           => 'success',
				'url'              => $this->urlToUserUploadDir.'/'.$name,
				'url_medium'       => $this->urlToUserUploadDir.'/'.'medium_'.$name,
				'url_thumbnail'    => $this->urlToUserUploadDir.'/'.'thumbnail_'.$name,
				'name'             => $name,
				'name_medium'      => 'medium_'.$name,
				'name_thumbnail'   => 'thumbnail_'.$name,
				'width'            => $width,
				'height'           => $height,
				'width_medium'     => $this->getMinWidth(),
				'height_medium'    => $this->getMinHeight(),
				'width_thumbnail'  => $this->getMinThumbnail(),
				'height_thumbnail' => $this->getMinThumbnail()
			];

			$this->server->emit(self::UPLOAD_AFTER_MOVE, ['image', $response]);
		}
		else
		{
			throw new FileUploadException('Min size: ('.$this->getMinHeight().'x'.$this->getMinWidth().')');
		}

		return $response;
	}

	protected $minHeight = 480;
	protected $minWidth = 640;
	protected $thumbnail = 155;
	protected $quality = 100;

	protected function getQuality()
	{
		return $this->quality;
	}

	protected function getMinThumbnail()
	{
		return $this->thumbnail;
	}

	/**
	 * @return int
	 */
	protected function getMinHeight()
	{
		return $this->minHeight;
	}

	/**
	 * @return int
	 */
	protected function getMinWidth()
	{
		return $this->minWidth;
	}
} 
<?php
namespace MyPet\FileUpload\Classes;

use KMCore\LogSystem\FastLog;
use KMCore\ServerContainer;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FileUpload
{
	const BASE_KEY = 'file';
	const UPLOAD_AFTER_MOVE = 'upload.after_move';
	protected $allowedExts = [];
	protected $server;
	protected $key = self::BASE_KEY;
	protected static $multiFileUpload = false;
	protected static $errorToSmallImage = false;

	/**
	 * @param ServerContainer $server
	 * @throws \UnexpectedValueException
	 * @throws \Exception
	 */
	public function __construct(ServerContainer $server)
	{
		if (!AuthManager::loggedIn())
		{
			throw new \UnexpectedValueException('Not auth user');
		}
		$this->server = $server;
		$userId = AuthManager::getSessionUserId();
		$this->pathToFile = PUBLIC_PATH.'/files';
		$this->pathToUserUploadDir = $this->pathToFile.'/'.$userId;
		$this->urlToUserUploadDir = $this->server->getContainer('config')->get('cdnUrl').'/files/'.$userId;
		if (!file_exists($this->pathToUserUploadDir))
		{
			mkdir($this->pathToUserUploadDir);
		}
	}

	public function getKey()
	{
		if (!empty($this->key))
		{
			return $this->key;
		}

		return self::BASE_KEY;
	}

	public function run()
	{
		$result = [];
		$key = $this->getKey();
		if (isset($_FILES[$key]['tmp_name']))
		{
			if (is_array($_FILES[$key]['tmp_name']))
			{
				if (static::$multiFileUpload)
				{
					throw new FileUploadException('Sorry! Multi file upload is disable');
				}

				foreach ($_FILES[$key]['tmp_name'] as $key => $tmp_name)
				{
					$name = $_FILES[$key]['name'][$key];
					$error = $_FILES[$key]['error'][$key];
					$response = $this->upload($tmp_name, $name, $error);
					if ($response)
					{
						$result[] = $response;
					}
				}
			}
			else
			{
				$tmp_name = $_FILES[$key]['tmp_name'];
				$name = $_FILES[$key]['name'];
				$error = $_FILES[$key]['error'];
				$response = $this->upload($tmp_name, $name, $error);
				if ($response)
				{
					$result[] = $response;
				}
			}
		}

		return $result;
	}

	public function upload($tmp_name, $name, $error)
	{
		try
		{
			if ($error > 0)
			{
				throw new FileUploadException('ERROR Return Code: '.$error);
			}
			else
			{
				$temp = explode('.', $name);
				$extension = end($temp);
				if (isset($this->allowedExts[$extension]) && file_exists($tmp_name))
				{
					$name = $this->getName($extension, 'upload_');
					move_uploaded_file($tmp_name, $this->pathToUserUploadDir.'/'.$name);

					return $this->prepare($name);
				}
				else
				{
					throw new FileUploadException('extension error');
				}
			}
		}
		catch (FileUploadException $e)
		{
			FastLog::add(
				'error',
				[
					'message' => $e->getMessage(),
					'line'    => $e->getLine(),
					'file'    => $e->getFile(),
					'code'    => $e->getCode()
				],
				false,
				true
			);
		}

		return null;
	}

	/**
	 * @param $name
	 * @throws FileUploadException
	 */
	public function prepare($name)
	{
		$response = [
			'status' => 'success',
			'url'    => $this->urlToUserUploadDir.'/'.$name,
			'name'   => $name
		];

		$this->server->emit(self::UPLOAD_AFTER_MOVE, ['file', $response]);

		return $response;
	}

	/**
	 * @param        $extension
	 * @param string $prefix
	 * @return string
	 */
	protected function getName($extension, $prefix = '')
	{
		return $prefix.mt_rand(0, 1000).time(true).mt_rand(0, 1000).'.'.$extension;
	}
}
<?php
namespace MyPet\FileUpload\Classes;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FileUploadException extends \Exception
{
}
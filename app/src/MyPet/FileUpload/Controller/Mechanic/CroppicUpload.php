<?php
namespace MyPet\FileUpload\Controller\Mechanic;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class CroppicUpload extends ImageUpload
{
	public function upload()
	{
		$response = parent::upload();
		print json_encode($response);
	}

	public function crop()
	{

		$response = parent::crop();
		print json_encode($response);
	}
}
<?php
namespace MyPet\FileUpload\Controller\Mechanic;

use Imagine\Image\Box;
use Imagine\Image\Point;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class ImageUpload extends FileUpload
{
	const EVENT_CONTROLLER_UPLOAD_AFTER_MOVE = 'event.controller.upload.after_move';
	const EVENT_CONTROLLER_UPLOAD_AFTER_CROP = 'event.controller.upload.after_crop';
	protected $minHeight;
	protected $minWidth;
	protected $allowedExts = [
		"gif"  => 1,
		"jpeg" => 1,
		"jpg"  => 1,
		"png"  => 1,
		"GIF"  => 1,
		"JPEG" => 1,
		"JPG"  => 1,
		"PNG"  => 1
	];

	abstract protected function getMinHeight();

	abstract protected function getMinWidth();

	abstract protected function listener();

	public function __construct()
	{
		parent::__construct();

		if (class_exists('Gmagick'))
		{
			$this->imagine = new \Imagine\Gmagick\Imagine();
		}
		elseif (class_exists('Imagick'))
		{
			$this->imagine = new \Imagine\Imagick\Imagine();
		}
		elseif (function_exists('gd_info'))
		{
			$this->imagine = new \Imagine\Gd\Imagine();
		}
		else
		{
			throw new \Exception('Not found graphic lib');
		}

		$this->listener();
	}

	public function upload()
	{
		if (isset($_FILES["img"]["name"]))
		{
			$filename = $_FILES["img"]["tmp_name"];
			$temp = explode(".", $_FILES["img"]["name"]);
			$extension = end($temp);

			if (isset($this->allowedExts[$extension]))
			{
				if ($_FILES["img"]["error"] > 0)
				{
					$response = [
						"status"  => 'error',
						"message" => 'ERROR Return Code: '.$_FILES["img"]["error"],
					];
				}
				else
				{
					$image = $this->imagine->open($filename);
					$size = $image->getSize();
					$width = $size->getWidth();
					$height = $size->getHeight();

					if ($this->getMinHeight() <= $height && $this->getMinWidth() <= $size->getWidth())
					{
						$name = $this->getName($extension, 'source_');
						move_uploaded_file($filename, $this->pathToUserUploadDir.'/'.$name);
						$this->server->emit(
							self::EVENT_CONTROLLER_UPLOAD_AFTER_MOVE,
							[$this->getType(), $this->urlToUserUploadDir.'/'.$name]
						);

						$response = [
							"status" => 'success',
							"url"    => $this->urlToUserUploadDir.'/'.$name,
							"width"  => $width,
							"height" => $height
						];
					}
					else
					{
						$response = [
							"status"  => 'error',
							"message" => 'Min size: ('.$this->getMinHeight().'x'.$this->getMinWidth().')',
						];
					}
				}
			}
			else
			{
				$response = [
					"status"  => 'error',
					"message" => 'extension error',
				];
			}
		}
		else
		{
			$response = [
				"status"  => 'error',
				"message" => 'something went wrong',
			];
		}

		return $response;
	}

	public function crop()
	{
		$imgUrl = $this->request->getPost('imgUrl');
		$data = parse_url($imgUrl);
		if (!isset($data['path']))
		{
			throw new \Exception('Not found path');
		}
		$temp = explode('/', $data['path']);
		if (empty($temp))
		{
			throw new \Exception('Not found temp');
		}
		$tmp = end($temp);
		if (empty($tmp))
		{
			throw new \Exception('Not found tmp');
		}
		$temp = explode('.', $tmp);
		if (!isset($temp[0], $temp[1]))
		{
			throw new \Exception('Not found img');
		}
		$nameBase = $temp[0];
		$extension = $temp[1];

		$imgW = $this->request->getPost('imgW');
		$imgH = $this->request->getPost('imgH');
		$imgY1 = $this->request->getPost('imgY1');
		$imgX1 = $this->request->getPost('imgX1');
		$cropW = $this->request->getPost('cropW');
		$cropH = $this->request->getPost('cropH');

		$jpegQuality = 100;

		$type = 'jpeg';

		$name = $this->getName($type, '');

		$outputFilename = $this->pathToUserUploadDir.'/'.$name;
		$outputUrl = $this->urlToUserUploadDir.'/'.$name;
		$image = $this->imagine->open($this->pathToUserUploadDir.'/'.$nameBase.'.'.$extension);

		$image->resize(new Box($imgW, $imgH));
		$image->crop(
			new Point(
				$imgX1, $imgY1
			),
			new Box($cropW, $cropH)
		);
		$image->resize(new Box($cropW, $cropH));
		$image->save($outputFilename, ['format' => $type, 'quality' => $jpegQuality]);

		$smallPhotoUrl = '';
		if ($this->getType() === 'avatar')
		{
			$thumbnailSmall = $image->thumbnail(new Box(45, 45));
			$smallPhotoUrl = $this->urlToUserUploadDir.'/'.'small_'.$name;
			$smallPhoto = $this->pathToUserUploadDir.'/'.'small_'.$name;
			$thumbnailSmall->save($smallPhoto, ['format' => 'jpeg', 'quality' => $jpegQuality]);
		}

		$this->server->emit(self::EVENT_CONTROLLER_UPLOAD_AFTER_CROP, [$this->getType(), $outputUrl, $smallPhotoUrl]);
		$response = [
			"status" => 'success',
			"url"    => $outputUrl
		];

		return $response;
	}
}
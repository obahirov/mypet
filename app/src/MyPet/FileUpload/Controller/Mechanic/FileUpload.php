<?php
namespace MyPet\FileUpload\Controller\Mechanic;

use KMCore\BaseController;
use KMCore\DB\Elasticsearch\Common\Exceptions\UnexpectedValueException;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FileUpload extends BaseController
{
	protected $pathToImage;
	protected $pathToUserUploadDir;
	protected $urlToUserUploadDir;
	protected $allowedExts;
	protected $imagine;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		if (!AuthManager::loggedIn())
		{
			throw new UnexpectedValueException('Not auth user');
		}
		$userId = AuthManager::getSessionUserId();

		$this->pathToImage = PUBLIC_PATH.'/files';
		$this->pathToUserUploadDir = $this->pathToImage.'/'.$userId;
		$this->urlToUserUploadDir = $this->server->getContainer('config')->get('cdnUrl').'/files/'.$userId;
		if (!file_exists($this->pathToUserUploadDir))
		{
			mkdir($this->pathToUserUploadDir);
		}
	}

	protected function getType()
	{
		return 'file';
	}

	protected function getName($extension, $prefix = '')
	{
		return $prefix.rand(0, 1000).time(true).rand(0, 1000).'.'.$extension;
	}
}
<?php
namespace MyPet\FileUpload\Controller\Type;

use MyPet\FileUpload\Controller\Mechanic\CroppicUpload;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class AvatarUpload extends CroppicUpload
{
	protected $minHeight = 140;
	protected $minWidth = 140;

	protected function getMinHeight()
	{
		return $this->minHeight;
	}

	protected function getMinWidth()
	{
		return $this->minWidth;
	}

	protected function getName($extension, $prefix = '')
	{
		return $prefix.'avatar_'.time(true).'.'.$extension;
	}

	protected function getType()
	{
		return 'avatar';
	}
}
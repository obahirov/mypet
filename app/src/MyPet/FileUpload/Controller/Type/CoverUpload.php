<?php
namespace MyPet\FileUpload\Controller\Type;

use MyPet\FileUpload\Controller\Mechanic\CroppicUpload;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class CoverUpload extends CroppicUpload
{
	protected $minHeight = 730;
	protected $minWidth = 420;

	protected function getMinHeight()
	{
		return $this->minHeight;
	}

	protected function getMinWidth()
	{
		return $this->minWidth;
	}

	protected function getName($extension, $prefix = '')
	{
		return $prefix.'cover_'.time(true).'.'.$extension;
	}

	protected function getType()
	{
		return 'cover';
	}
}
<?php
namespace MyPet\FileUpload\Controller;

use MyPet\FileUpload\Controller\Type\AvatarUpload;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\ProgressBar;
use MyPet\Users\Classes\UserManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UserAvatarUpload extends AvatarUpload
{
	public function listener()
	{
		$this->server->on(
			self::EVENT_CONTROLLER_UPLOAD_AFTER_CROP,
			function ($type, $url, $small_photo = '')
			{
				$userId = AuthManager::getSessionUserId();
				$user = UserManager::getUserModelByUserId($userId);
				switch ($type)
				{
					case 'avatar':
						$user->small_photo = $small_photo;
						$user->photo = $url;
						break;
					case 'cover':
						$user->cover = $url;
						break;
				}
				// FIXME
				$progress = new ProgressBar($user);
				$user->progress = $progress->calc();
				UserManager::updateToSearch($user);
				$user->save();
			}
		);
		$this->server->on(
			self::EVENT_CONTROLLER_UPLOAD_AFTER_MOVE,
			function ($type, $url)
			{
				$userId = AuthManager::getSessionUserId();
				$user = UserManager::getUserModelByUserId($userId);
				switch ($type)
				{
					case 'avatar':
						$user->src_photo = $url;
						break;
					case 'cover':
						$user->src_cover = $url;
						break;
				}
				$user->save();
			}
		);
	}
}
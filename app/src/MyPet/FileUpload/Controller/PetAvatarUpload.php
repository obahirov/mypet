<?php
namespace MyPet\FileUpload\Controller;

use MyPet\FileUpload\Controller\Type\AvatarUpload;
use MyPet\Pets\Classes\PetManager;
use MyPet\Pets\Model\GagModel;
use MyPet\Pets\Model\PetModel;
use MyPet\Users\Classes\AuthManager;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PetAvatarUpload extends AvatarUpload
{
	public function listener()
	{
		$petId = $this->server->getContainer('request')->getGetOrPost('petId');
		$pet = new PetModel($petId);
		if (!$pet->isLoadedObject())
		{
			$pet = new GagModel($petId);
		}

		$userId = AuthManager::getSessionUserId();
		if (array_search($userId, $pet->ownerIds) !== false)
		{
			$this->server->on(
				self::EVENT_CONTROLLER_UPLOAD_AFTER_CROP,
				function ($type, $url, $small_photo = '') use ($pet)
				{
					switch ($type)
					{
						case 'avatar':
							$pet->small_photo = $small_photo;
							$pet->photo = $url;
							break;
						case 'cover':
							$pet->cover = $url;
							break;
					}
					PetManager::updateToSearch($pet);
					$pet->save();
				}
			);
			$this->server->on(
				self::EVENT_CONTROLLER_UPLOAD_AFTER_MOVE,
				function ($type, $url) use ($pet)
				{
					switch ($type)
					{
						case 'avatar':
							$pet->src_photo = $url;
							break;
						case 'cover':
							$pet->src_photo = $url;
							break;
					}
					$pet->save();
				}
			);
		}
		else
		{
			throw new \Exception('not_have_pemission');
		}
	}
}
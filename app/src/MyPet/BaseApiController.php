<?php
namespace MyPet;

use KMCore\Events\EventDispatcher;
use KMCore\RestfulController;
use MyPet\Events\Subscriber\EventSubscriber;

/**
 * Контроллер з базовим методом відповіді через форми
 */
class BaseApiController extends RestfulController
{
	/**
	 * @var array Массив данных возвращаемых клиенту
	 */
	public $data = [];

	/**
	 * @var bool Обозначение, выполнен запрос или нет
	 */
	public $success = false;

	/**
	 * @var string Подпись запроса от клиента
	 */
	protected $snsig = '';

	/**
	 * @var bool Повторный запрос
	 */
	public $snsigFound = false;

	/**
	 * @var bool
	 */
	protected $responseStatus = false;

	/**
	 * Инициализация приложения
	 */
	public function __construct()
	{
		/**
		 * Инициализируем родительский контроллер
		 */
		parent::__construct();

		/**
		 * Проверка дублей запросов-ответов
		 */
		$this->snsig = (string)$this->request->getGetOrPost('snsig', '');
		if (!isset($_SESSION['response_data']))
		{
			$_SESSION['response_data'] = []; // хранилище запросов клиента в сессии
		}
		else
		{
			if (!empty($this->snsig) && isset($_SESSION['response_data'][$this->snsig])) // этот запрос повторный
			{
				$this->snsigFound = true;
				exit();
			}
		}
	}

	public function response()
	{
		parent::response();

		$this->responseStatus = true;

		if (ob_get_level())
		{
			ob_clean();
		}

		/**
		 * Вывод данных, если запрос повторный
		 */
		if ($this->snsigFound) // получен повторный запрос
		{
			$this->response->json($_SESSION['response_data'][$this->snsig], 304, [], true);
		}
		else
		{
//			$this->data['snsig'] = md5(implode(',', $_REQUEST));

			/**
			 * Хранить все аргументы ответа
			 */
			if (!isset($_SESSION['response_data'][$this->snsig]))
			{
				$_SESSION['response_data'][$this->snsig] = [];
			}

			/**
			 * Хранилище запросов клиента в сессии
			 */
			if (!empty($this->snsig) && $this->success && !isset($this->data['error']))
			{
				$_SESSION['response_data'][$this->snsig] = $this->data;
			}

			/**
			 * Сохраняем ответ на запрос клиента
			 */
			if (count($_SESSION['response_data']) > 5)
			{
				array_shift($_SESSION['response_data']);
			}

			$this->response->json($this->data, 200, [], $this->success);
		}
	}

	public function __destruct()
	{
		parent::__destruct();

		if ($this->responseStatus === false)
		{ // Нужно для совместимости легаси кода
			$this->response();
		}
	}
}

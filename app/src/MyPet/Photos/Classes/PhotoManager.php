<?php
namespace MyPet\Photos\Classes;

use KMCore\ServerContainer;
use MyPet\Photos\Model\PhotoModel;
use MyPet\Users\Model\LikeHistory;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PhotoManager
{
	const EVENT_LIKE_PHOTO = 'event.like.photo';
	const EVENT_UNLIKE_PHOTO = 'event.unlike.photo';
	protected $server;

	/**
	 * @param ServerContainer $server
	 */
	public function __construct(ServerContainer $server)
	{
		$this->server = $server;
	}

	/**
	 * Действия по лайку поста
	 *
	 * @param $id
	 * @return mixed
	 */
	public function like($id, LikeHistory $like)
	{
		$query = ['id' => $id];
		$update = ['$inc' => ['likes' => 1]];
		$fields = [
			'id'    => 1,
			'likes' => 1
		];
		$data = PhotoModel::findAndModify($query, $update, $fields);

		$data['likes'] = $data['likes'] + 1;
		$this->server->emit(self::EVENT_LIKE_PHOTO, [$id, $like]);

		return $data;
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function unlike($id, LikeHistory $like)
	{
		$query = ['id' => $id];
		$update = ['$inc' => ['likes' => -1]];
		$fields = [
			'id'    => 1,
			'likes' => 1
		];
		$data = PhotoModel::findAndModify($query, $update, $fields);

		$this->server->emit(self::EVENT_UNLIKE_PHOTO, [$id, $like]);
		$data['likes'] = $data['likes'] - 1;

		return $data;
	}
} 
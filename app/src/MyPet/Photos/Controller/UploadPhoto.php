<?php
namespace MyPet\Photos\Controller;

use MyPet\FileUpload\Classes\ImageUpload;
use MyPet\ApiController;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UploadPhoto extends ApiController
{
	public function upload()
	{
		$file = new ImageUpload($this->server);
		$result = $file->run();
		$this->response->json(['result' => $result]);
	}
} 
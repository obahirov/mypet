<?php
namespace MyPet\Photos\Controller;

use KMCore\Helper\GeneratorHelper;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\Photos\Classes\PhotoManager;
use MyPet\Photos\Model\AlbumModel;
use MyPet\Photos\Model\PhotoModel;
use MyPet\ApiController;
use MyPet\Users\Classes\LikeHelper;
use MyPet\Users\Model\LikeHistory;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PhotoController extends ApiController
{
	protected $photoManager;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->photoManager = new PhotoManager($this->server);
	}

	public function createAlbum()
	{
		$name = (string)$this->request->getGetOrPost('name', '');
		$album = new AlbumModel();
		$album->id = GeneratorHelper::getStringUniqueId();
		$album->name = $name;
		$album->time = time();
		$album->userId = $this->user->id;
		$album->save();

		$this->data = $album->getFieldsArray();
		$this->success = true;
		$this->response();
	}

	public function deleteAlbum()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$album = new AlbumModel($id);
		if ($album->userId !== $this->user->id)
		{
			throw new \Exception('Error album');
		}
		$album->delete();
		PhotoModel::deleteAllAdvanced(['albumId' => $id]);

		$this->data = [];
		$this->success = true;
		$this->response();
	}

	public function appendPhoto()
	{
		$photos = (array)isset($_POST['photos']) ? $_POST['photos'] : [];
		$photos = array_slice($photos, 0, 100);
		PhotoModel::disableCache();
		foreach ($photos as $photo)
		{
			if (isset($photo['photo']['url']) && !empty($photo['photo']['url']))
			{
				$photoData = (array)$photo['photo'];
				$body = isset($photo['body']) ? (string)$photo['body'] : '';
				$albumId = isset($photo['albumId']) ? (string)$photo['albumId'] : '';
				$tags = isset($photo['tags']) ? (array)$photo['tags'] : [];

				if (!empty($albumId))
				{
					$album = new AlbumModel($albumId);
					if ($album->userId !== $this->user->id)
					{
						throw new \Exception('Error album');
					}
				}
				else
				{
					$albumId = $this->user->id;
				}

				$photo = new PhotoModel();
				$photo->id = GeneratorHelper::getStringUniqueId();
				$photo->albumId = $albumId;
				$photo->time = time();
				$photo->body = $body;
				$photo->tags = $tags;
				$photo->photo = $photoData;
				$photo->userId = $this->user->id;
				$photo->save();
			}
		}

		$this->data = [];
		$this->success = true;
		$this->response();
	}

	public function like()
	{
		$user = $this->user;
		$this->server->on(
			PhotoManager::EVENT_LIKE_PHOTO,
			function ($id, LikeHistory $like) use ($user)
			{
				$like->id = $user->id;
				$like->likeId = $id;
				$like->type = LikeHelper::TYPE_PHOTO;
				$like->save();

				$photo = new PhotoModel($id);
				if ($photo->isLoadedObject())
				{
					$owner = new User($photo->userId);
					LikeHelper::request(
						$this->user,
						$owner,
						$photo->id,
						LikeHelper::TYPE_PHOTO,
						RequestManager::TYPE_LIKE,
						['photo' => $photo->photo]
					);
				}
			}
		);
		$this->server->on(
			PhotoManager::EVENT_UNLIKE_PHOTO,
			function ($id, LikeHistory $like) use ($user)
			{
				$like->delete();
			}
		);

		$id = (string)$this->request->getGetOrPost('id', '');

		$like = new LikeHistory(['id' => $user->id, 'likeId' => $id]);
		if (!$like->isLoadedObject())
		{
			$data = $this->photoManager->like($id, $like);
			$success = true;
		}
		else
		{
			$data = $this->photoManager->unlike($id, $like);
			$success = true;
		}

		$this->data = $data;
		$this->success = $success;
		$this->response();
	}

	public function detachPhoto()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$photo = new PhotoModel($id);

		if ($photo->userId === $this->user->id)
		{
			$photo->delete();
		}

		$this->data = [];
		$this->success = true;
		$this->response();
	}

	public function getAllPhotoInAlbum()
	{
		$albumId = (string)$this->request->getGetOrPost('albumId', '');
		if (empty($albumId))
		{
			$albumId = $this->user->id;
		}
		$photos = PhotoModel::getAllAdvanced(['albumId' => $albumId]);

		$this->data = $photos;
		$this->success = true;
		$this->response();
	}

	public function getAllAlbums()
	{
		$albums = AlbumModel::getAllAdvanced(['userId' => $this->user->id]);

		$this->data = $albums;
		$this->success = true;
		$this->response();
	}
} 
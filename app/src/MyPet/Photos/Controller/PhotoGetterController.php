<?php
namespace MyPet\Photos\Controller;

use KMCore\Helper\GeneratorHelper;
use MyPet\BaseApiController;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\Photos\Classes\PhotoManager;
use MyPet\Photos\Model\AlbumModel;
use MyPet\Photos\Model\PhotoModel;
use MyPet\ApiController;
use MyPet\Users\Classes\LikeHelper;
use MyPet\Users\Model\LikeHistory;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PhotoGetterController extends BaseApiController
{
	protected $photoManager;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->photoManager = new PhotoManager($this->server);
	}

	public function getAllPhotoInAlbum()
	{
		$albumId = (string)$this->request->getGetOrPost('albumId', '');
		if (empty($albumId))
		{
			$albumId = $this->user->id;
		}
		$photos = PhotoModel::getAllAdvanced(['albumId' => $albumId]);

		$this->data = $photos;
		$this->success = true;
		$this->response();
	}

	public function getAllAlbums()
	{
		$albums = AlbumModel::getAllAdvanced(['userId' => $this->user->id]);

		$this->data = $albums;
		$this->success = true;
		$this->response();
	}
} 
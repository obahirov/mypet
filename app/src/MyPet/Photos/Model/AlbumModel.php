<?php
namespace MyPet\Photos\Model;

use KMCore\DB\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AlbumModel extends ObjectModel
{
	protected static $pk = 'id';
	protected static $collection = 'mp_album_model';
	protected static $readPreference = \MongoClient::RP_SECONDARY_PREFERRED;
	protected static $writeConcernMode = 0;
	protected static $sort = ['time' => -1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['time' => -1],
		]
	];

	protected static $fieldsDefault = [
		'id'     => '',
		'name'   => '',
		'time'   => 0,
		'userId' => 0
	];

	protected static $fieldsValidate = [
		'id'     => self::TYPE_STRING,
		'name'   => self::TYPE_STRING,
		'time'   => self::TYPE_TIMESTAMP,
		'userId' => self::TYPE_STRING,
	];
}
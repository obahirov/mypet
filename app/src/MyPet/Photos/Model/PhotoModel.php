<?php
namespace MyPet\Photos\Model;

use KMCore\DB\ObjectModel;

/**
 * @property string userId
 * @property  id
 * @property  photo
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PhotoModel extends ObjectModel
{
	protected static $pk = 'id';
	protected static $collection = 'mp_photo_model';
	protected static $readPreference = \MongoClient::RP_SECONDARY_PREFERRED;
	protected static $writeConcernMode = 0;
	protected static $sort = ['time' => -1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['albumId' => 1],
		],
		[
			'keys' => ['tags' => 1],
		],
		[
			'keys' => ['time' => -1],
		]
	];

	protected static $fieldsDefault = [
		'id'      => '',
		'albumId' => '',
		'time'    => 0,
		'tags'    => [],
		'body'    => '',
		'photo'   => [],
		'userId'  => '',
		'likes'   => 0
	];

	protected static $fieldsValidate = [
		'id'      => self::TYPE_STRING,
		'albumId' => self::TYPE_STRING,
		'time'    => self::TYPE_TIMESTAMP,
		'tags'    => self::TYPE_JSON,
		'body'    => self::TYPE_STRING,
		'photo'   => self::TYPE_JSON,
		'userId'  => self::TYPE_STRING,
		'likes'   => self::TYPE_UNSIGNED_INT
	];
} 
<?php
namespace MyPet\ShortLinks\Classes;
use Guzzle\Common\Exception\ExceptionCollection;
use KMCore\Helper\GeneratorHelper;
use KMCore\Validation\Validate;
use MyPet\ShortLinks\Model\Link;

/**
 * Class ShortLinkGenerator
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ShortLinkGenerator 
{
	public function generateName($len = 7, $attempts = 3)
	{
		Link::disableCache();
		$i = 0;
		do
		{
			if($i > $attempts)
			{
				throw new \Exception('Not found name');
			}
			$name = GeneratorHelper::generatePassword($len);
			$link = new Link($name);
			$i++;
		}while($link->isLoadedObject());
		return $name;
	}

	public function generateLink($url, $name)
	{
		$len = strlen($name);
		if($len < 5 || $len > 64 || preg_match(
				"/[^0-9A-Za-z_-]/u",
				$name
			))
		{
			throw new \Exception('Incorect name');
		}

		if(!Validate::isUri($url))
		{
			throw new \Exception('Incorect uri');
		}

		$link = new Link($name);
		if($link->isLoadedObject())
		{
			throw new \Exception('Double name');
		}

		$link->id = $name;
		$link->url = $url;
		$link->save();

		return $name;
	}
}
<?php
namespace MyPet\ShortLinks\Controller;
use KMCore\BaseController;
use MyPet\ShortLinks\Model\Link;
use KMCore\Helper\RedirectManager;

/**
 * Class RedirectController
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ShortLinksController extends BaseController
{
	public function r()
	{
		$code = $this->request->getRequest('code');
		$link = new Link($code);
		if(!$link->isLoadedObject())
		{
			throw new \Exception('Not found link by code');
		}

		RedirectManager::redirect($link->url);
	}
}
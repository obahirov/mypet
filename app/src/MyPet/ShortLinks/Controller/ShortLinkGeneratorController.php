<?php
namespace MyPet\ShortLinks\Controller;
use KMCore\BaseController;
use MyPet\ShortLinks\Classes\ShortLinkGenerator;

/**
 * Class RedirectController
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ShortLinkGeneratorController extends BaseController
{
	const URL_COMPONENT = 'sht';

	public function getLink()
	{
		$url = $this->request->getGetOrPost('url');
		$name = $this->request->getGetOrPost('name', '');
		$len = $this->request->getGetOrPost('len', 5);

		if(empty($url))
		{
			throw new \Exception('Url is empty');
		}

		if($len < 5 || $len > 64)
		{
			$len = 7;
		}

		$manager = new ShortLinkGenerator();
		if(empty($name))
		{
			$name = $manager->generateName($len);
		}
		$name = $manager->generateLink($url, $name);

		$baseUrl = rtrim($this->server->getContainer('config')->get('baseUrl'), '/').'/'.self::URL_COMPONENT.'/'.$name;
		$this->response->json(['url' => $baseUrl]);
	}
}
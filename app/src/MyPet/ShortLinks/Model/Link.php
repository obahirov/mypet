<?php
namespace MyPet\ShortLinks\Model;
use KMCore\DB\ObjectModel;

/**
 * Class Link
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Link extends ObjectModel
{
	protected static $collection = 'mp_link_model';
	protected static $sort = ['id' => 1];
	protected static $indexes = [
		[
			'keys' => ['id' => 1],
			'unique' => true,
		]
	];

	protected static $fieldsDefault = [
		'id'        => '',
		'url'     => '',
	];

	protected static $fieldsValidate = [
		'id'     => self::TYPE_STRING,
		'url'     => self::TYPE_STRING,
	];
}
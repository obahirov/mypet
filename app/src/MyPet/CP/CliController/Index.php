<?php
namespace MyPet\CP\CliController;

use KMCore\Admin\Classes\Main;
use KMCore\BaseController;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Основной контроллер работы с админкой через консоль

 */
class Index extends BaseController
{
	/**
	 * @var array Возвращаемый массив
	 */
	private $result = [];

	/**
	 * Развертывание приложения
	 *
	 * @return array
	 */
	public function install()
	{
		$this->result = [];
		$fileSystem = new Filesystem();

		$fileSystem->mkdir(PUBLIC_PATH.'/admin', 0755);
		$this->result[] = 'Create admin resources path 0755';

		$fileSystem->symlink(
			VENDOR_PATH.'/gollariel/km-core/src/KMCore/Admin/Resources/css',
			PUBLIC_PATH.'/admin/css'
		);
		$this->result[] = 'Create admin css symlink';

		$fileSystem->symlink(
			VENDOR_PATH.'/gollariel/km-core/src/KMCore/Admin/Resources/js',
			PUBLIC_PATH.'/admin/js'
		);
		$this->result[] = 'Create admin js symlink';

		$fileSystem->symlink(
			VENDOR_PATH.'/gollariel/km-core/src/KMCore/Admin/Resources/img',
			PUBLIC_PATH.'/admin/img'
		);
		$this->result[] = 'Create admin img symlink';

		$fileSystem->mkdir(APP_PATH.'/cache', 0777);
		$this->result[] = 'Create cache path 0777';

		$menu = (array)$this->server->getContainer('config')->loadFile('adminMenu')->get('menu');
		$this->processingMenu($menu);

		return $this->result;
	}

	public function createIndex()
	{
		$menu = (array)$this->server->getContainer('config')->loadFile('adminMenu')->get('menu');
		$this->createIndexByMenu($menu);
	}

	private function createIndexByMenu(Array $value)
	{
		foreach ($value as $v)
		{
			if (is_array($v))
			{
				$this->createIndexByMenu($v);
			}
			else
			{
				$modelClassName = '\\'.str_replace('_', '\\', $v);
				if (class_exists($modelClassName) && method_exists($modelClassName, 'ensureIndexes'))
				{
					try
					{
						$modelClassName::ensureIndexes();
					}
					catch (\Exception $e)
					{
						$message = 'Невозможно создать индекс для класса '.$modelClassName.'. Ошибка: '.$e->getMessage(
							);
						echo "\033[1;37;41m".$message."\033[0m\n";
					}
				}
			}
		}
	}

	/**
	 * Обработка моделей БД (создание индексов) по пунктам меню
	 *
	 * @param $value
	 */
	private function processingMenu(Array $value)
	{
		foreach ($value as $v)
		{
			if (is_array($v))
			{
				$this->processingMenu($v);
			}
			else
			{
				$modelClassName = '\\'.str_replace('_', '\\', $v);
				if (class_exists($modelClassName))
				{
					Main::getEntityIndexes($modelClassName, true, true);
				}
			}
		}
	}
}

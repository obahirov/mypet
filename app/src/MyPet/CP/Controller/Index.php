<?php
namespace MyPet\CP\Controller;

use KMCore\Admin\Controller\BaseAdminController;

/**
 * Основной контроллер работы с админкой
 *
 * @author Vasiliy Khvostik <vasyakhvostik@gmail.com>
 */
class Index extends BaseAdminController
{

}

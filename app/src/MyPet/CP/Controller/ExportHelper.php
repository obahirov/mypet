<?php
namespace MyPet\CP\Controller;

use Exception;
use KMCore\Admin\Controller\AdminController;
use KMCore\DB\ObjectModel;
use KMCore\ExportHelper\ExportConfig;
use KMCore\ExportHelper\ExportConfigByCache;
use KMCore\ExportHelper\ExportHistory;
use KMCore\ExportHelper\ExportManager;

/**
 * Управление пользователем и ботами в админке
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ExportHelper extends AdminController
{
	/**
	 * Обработка моделей БД (создание индексов) по пунктам меню
	 *
	 * @param $value
	 * @return array
	 */
	private function processingMenu($value)
	{
		$models = [];
		if (is_array($value))
		{
			foreach ($value as $name => $v)
			{
				if (is_array($v))
				{
					$models = array_merge($models, $this->processingMenu($v));
				}
				else
				{
					$modelClassName = '\\'.str_replace('_', '\\', $v);
					if (class_exists($modelClassName))
					{
						$models[] = ['model' => $modelClassName, 'title' => $name];
					}
				}
			}
		}
		else
		{
			$modelClassName = '\\'.str_replace('_', '\\', $value);
			if (class_exists($modelClassName))
			{
				$models[] = ['model' => $modelClassName, 'title' => 'main'];
			}
		}

		return $models;
	}

	public function getModelFields()
	{
		$fields = [];
		/** @var ObjectModel $model */
		$model = (string)$this->request->getGetOrPost('model', '');
		if (class_exists($model) && method_exists($model, 'getFieldsDefault'))
		{
			$fields = $model::getFieldsDefault();
			$fields = array_keys($fields);
		}

		$this->response->json($fields);
	}

	public function getModelFieldsType()
	{
		$fields = [];
		/** @var ObjectModel $model */
		$model = (string)$this->request->getGetOrPost('model', '');
		if (class_exists($model) && method_exists($model, 'getFieldsValidate'))
		{
			$fields = $model::getFieldsValidate();
			$fields = array_keys($fields);
		}

		$this->response->json($fields);
	}

	public function checkId()
	{
		$checkId = $this->request->getGetOrPost('checkId');
		$result = ExportHistory::isCompleate($checkId);
		$this->response->json(['result' => $result]);
	}

	public function exportById()
	{
		$id = $this->request->getGetOrPost('id');
		$config = new ExportConfigByCache($id);
		$export = new ExportManager($this->server, $config, true);
		$export->run();
	}

	/**
	 *
	 */
	public function export()
	{
		$model = $this->request->getGetOrPost('model', '');
		$userFields = (array)$this->request->getGetOrPost('fields', []);
		$userFilters = (array)$this->request->getGetOrPost('filter', []);
		$params = (array)json_decode($this->request->getGetOrPost('params', '[]'), true);
		$isExport = (bool)$this->request->getGetOrPost('isExport', false);
		$onlyUrl = (bool)$this->request->getGetOrPost('onlyUrl', false);

		$fields = [];
		foreach ($userFields as $field)
		{
			$fields[] = $field;
		}
		foreach ($userFilters as $key => $filter)
		{
			if (!in_array($key, $fields) || empty($filter['val']))
			{
				unset($userFilters[$key]);
			}
		}

		if ($onlyUrl)
		{
			$url = http_build_query(
				[
					'model'    => $model,
					'fields'   => $userFields,
					'filter'   => $userFilters,
					'params'   => $params,
					'isExport' => $isExport
				]
			);
			$url = base64_encode(urlencode($url));
			$this->request->redirect('/CP/ExportHelper/index?exportUrl='.$url, false);

			return;
		}

		$filters = [];
		foreach ($userFilters as $key => $filter)
		{
			if (isset($filter['val']) && !empty($filter['val']))
			{
				if (isset($filter['oper']) && !empty($filter['oper']))
				{
					$filters[$key] = [$filter['oper'] => $filter['val']];
				}
				else
				{
					$filters[$key] = $filter['val'];
				}
			}
		}

		$fields = [];
		foreach ($userFields as $field)
		{
			$fields[$field] = 1;
		}

		$config = new ExportConfig($model, $filters, $params, $fields);
		$export = new ExportManager($this->server, $config, $isExport);

		if ($isExport === false)
		{
			$this->request->redirect('/CP/ExportHelper/index?exportId='.$export->getExportId(), false);
		}

		$export->run();
	}

	/**
	 * Метод возвращающий форму управления ботами в игре
	 *
	 * @throws \Exception
	 * @return string Готовая html страница
	 */
	public function index()
	{
		$exportId = $this->request->getGetOrPost('exportId', '');
		$exportUrl = urldecode(base64_decode($this->request->getGetOrPost('exportUrl', '')));
		if ($exportUrl)
		{
			$baseUrl = $this->server->getContainer('config')->get('baseUrl');
			$exportUrl = rtrim($baseUrl, '/').'/CP/ExportHelper/export?'.$exportUrl;
		}

		$urlMap = $this->server->getContainer('config')->loadFile('adminMenu')->get('menu');
		$models = $this->processingMenu($urlMap);

		return $this->render(
			'CP/views/form_api/exportHelper.html.twig',
			[
				'models'    => $models,
				'exportId'  => $exportId,
				'exportUrl' => $exportUrl
			]
		);
	}
}

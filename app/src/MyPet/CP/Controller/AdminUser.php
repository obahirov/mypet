<?php
namespace MyPet\CP\Controller;

use KMCore\Admin\Classes\Main;
use KMCore\Admin\Controller\AdminController;
use KMCore\Helper\RedirectManager;

/**
 * Страница администраторов
 *
 * @author Vasiliy Khvostik
 */
class AdminUser extends AdminController
{
	/**
	 * Страница генерации паролей администраторами
	 *
	 * @return string
	 */
	public function generator()
	{
		$data = (string)$this->request->getGetOrPost('data', '');
		$hash = (string)$this->request->getGetOrPost('hash', '');
		$action = (string)$this->request->getGetOrPost('action', '');

		if (!empty($data) && !empty($hash))
		{
			if ($hash == "password")
			{
				$result = md5($data.$this->server->getContainer('config')->get("authSalt"));
			}
			else
			{
				$result = hash($hash, $data);
			}

			RedirectManager::showMessage('Успешно сгенерирован!');
		}
		else
		{
			if ($action == 'generate')
			{
				RedirectManager::showErrors('Исходные данные не должны быть пустыми!');
			}
			$result = "";
		}

		return $this->render('generator.html.twig', ['data' => $data, 'hash' => $hash, 'result' => $result]);
	}

	public function createIndex()
	{
		if (isset($_SESSION['auth']['isRoot']))
		{
			$value = $this->server->getContainer('config')->loadFile('adminMenu')->get('menu');
			$this->processingMenu($value);
		}
		RedirectManager::redirect('/CP/KMCore_Admin_Model_AdminUser');
	}

	/**
	 * @param $value
	 */
	private function processingMenu($value)
	{
		if (is_array($value))
		{
			foreach ($value as $v)
			{
				if (is_array($v))
				{
					$this->processingMenu($v);
				}
				else
				{
					$modelClassName = '\\'.str_replace('_', '\\', $v);
					if (class_exists($modelClassName))
					{
						Main::getEntityIndexes($modelClassName, true, true);
					}
				}
			}
		}
		else
		{
			$modelClassName = '\\'.str_replace('_', '\\', $value);
			if (class_exists($modelClassName))
			{
				Main::getEntityIndexes($modelClassName, true, true);
			}
		}
	}
}

<?php
namespace MyPet\CP\Controller;

use KMCore\Admin\Controller\AdminController;
use KMCore\Helper\RedirectManager;

/**
 * Страница администраторов
 */
class Pages extends AdminController
{
	/**
	 * Страница генерации паролей администраторами
	 *
	 * @return string
	 */
	public function generator()
	{
		$data = (string)$this->request->getGetOrPost('data', '');
		$result = '';
		if (!empty($data))
		{
			$result = json_encode($data);
			RedirectManager::showMessage('Успешно сгенерирован!');
		}

		return $this->render('prepare.html.twig', ['data' => $data, 'result' => $result]);
	}
}

<?php
namespace MyPet\Posts\Classes;

use KMCore\ServerContainer;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UserAggregate
{
	protected $server;

	public function __construct(ServerContainer $server)
	{
		$this->server = $server;
	}

	/**
	 * @param array $posts
	 * @return array
	 */
	public function run(Array &$posts)
	{
		$userIds = [];
		foreach ($posts as $post)
		{
			if (isset($post['userId']))
			{
				$userIds[] = $post['userId'];
			}
		}

		/** @var \MongoCursor $cursor */
		$cursor = User::getAllAdvanced(
			['id' => ['$in' => $userIds]],
			[
				'id'         => 1,
				'photo'      => 1,
				'first_name' => 1,
				'last_name'  => 1,
				'profession' => 1
			],
			false,
			true,
			false,
			false,
			true
		);

		$profile = [];
		while ($cursor->hasNext())
		{
			$data = $cursor->getNext();
			if (isset($data['id']))
			{
				$id = $data['id'];
				$profile[$id]['id'] = $id;
				$profile[$id]['photo'] = isset($data['photo']) ? $data['photo'] : '';
				$profile[$id]['first_name'] = isset($data['first_name']) ? $data['first_name'] : '';
				$profile[$id]['last_name'] = isset($data['last_name']) ? $data['last_name'] : '';
				$profile[$id]['profession'] = isset($data['profession']) ? $data['profession'] : '';
			}
			usleep(10);
		}

		foreach ($posts as &$post)
		{
			if (isset($post['userId']))
			{
				$id = $post['userId'];
				if (isset($profile[$id]))
				{
					$post['attr']['author'] = $profile[$id];
				}
				else
				{
					$post['attr']['author'] = [];
				}
			}
		}
	}
} 
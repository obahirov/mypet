<?php
namespace MyPet\Posts\Classes;

use KMCore\ServerContainer;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class NewsHelper
{
	protected $server;
	protected $userId;
	protected $tags = [];
	protected $type;
	protected $body;
	protected $owner;

	public function __construct(ServerContainer $server, $userId, $body, $owner = [])
	{
		$this->server = $server;
		$this->userId = $userId;
		$this->tags[] = 'news';
		$this->type = 'news';
		$this->body = $body;
		if (empty($owner) || !is_array($owner))
		{
			$owner = [];
			$owner[] = $userId;
		}
		$this->owner = $owner;
	}

	public function addNews($users)
	{
		$data['users'] = $users;
		$postManager = new PostManager($GLOBALS['serverContainer']);
		$model = $postManager->add($this->userId, $this->body, $this->type, $this->tags, $data, $this->owner);
		$model->save();

		return $model->getFieldsSanitized();
	}

	public function addNewsPets($pets, $petsCount)
	{
		$this->tags[] = 'pets';
		$data['pets'] = $pets;
		$data['petsCount'] = $petsCount;
		$postManager = new PostManager($GLOBALS['serverContainer']);
		$model = $postManager->add($this->userId, $this->body, $this->type, $this->tags, $data, $this->owner);
		$model->save();

		return $model->getFieldsSanitized();
	}

	public function addNewsNewUsers($users, $usersCount)
	{
		$this->tags[] = 'users';
		$data['users'] = $users;
		$data['usersCount'] = $usersCount;
		$postManager = new PostManager($GLOBALS['serverContainer']);
		$model = $postManager->add($this->userId, $this->body, $this->type, $this->tags, $data, $this->owner);
		$model->save();

		return $model->getFieldsSanitized();
	}

	public function addNewsNewPets($pets, $petsCount)
	{
		$this->tags[] = 'new_pets';
		$data['pets'] = $pets;
		$data['petsCount'] = $petsCount;
		$postManager = new PostManager($GLOBALS['serverContainer']);
		$model = $postManager->add($this->userId, $this->body, $this->type, $this->tags, $data, $this->owner);
		$model->save();

		return $model->getFieldsSanitized();
	}

	public function addNewsAddPhoto($photos, $photosCount)
	{
		$this->tags[] = 'add_photo';
		$data['photos'] = $photos;
		$data['photosCount'] = $photosCount;
		$postManager = new PostManager($GLOBALS['serverContainer']);
		$model = $postManager->add($this->userId, $this->body, $this->type, $this->tags, $data, $this->owner);
		$model->save();

		return $model->getFieldsSanitized();
	}

	public function addNewsAddPet($pets, $petsCount)
	{
		$this->tags[] = 'add_pet';
		$data['pets'] = $pets;
		$data['petsCount'] = $petsCount;
		$postManager = new PostManager($GLOBALS['serverContainer']);
		$model = $postManager->add($this->userId, $this->body, $this->type, $this->tags, $data, $this->owner);
		$model->save();

		return $model->getFieldsSanitized();
	}

	public function addNewsAddWallPost($post)
	{
		$this->tags[] = 'add_wall_post';
		$data['post'] = $post;
		$postManager = new PostManager($GLOBALS['serverContainer']);
		$model = $postManager->add($this->userId, $this->body, $this->type, $this->tags, $data, $this->owner);
		$model->save();

		return $model->getFieldsSanitized();
	}
}
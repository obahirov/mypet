<?php
namespace MyPet\Posts\Classes;

use KMCore\Helper\GeneratorHelper;
use KMCore\ServerContainer;
use MyPet\Posts\Model\PostModel;
use MyPet\Users\Model\LikeHistory;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PostManager
{
	const EVENT_LIKE_POST = 'event.like.post';
	const EVENT_UNLIKE_POST = 'event.unlike.post';
	protected $server;

	/**
	 * @param ServerContainer $server
	 */
	public function __construct(ServerContainer $server)
	{
		$this->server = $server;
	}

	/**
	 * @param           $ownerId
	 * @param PostModel $post
	 * @return bool
	 */
	public function isAuthor($ownerId, PostModel $post)
	{
		return ($ownerId === $post->userId);
	}

	/**
	 * @param           $ownerId
	 * @param PostModel $post
	 * @return bool
	 */
	public function isYouPage($ownerId, PostModel $post)
	{
		return ($ownerId === $post->type);
	}

	/**
	 * @param           $ownerId
	 * @param PostModel $post
	 * @return bool
	 */
	public function isOwner($ownerId, PostModel $post)
	{
		return (array_search($ownerId, $post->owners) !== false);
	}

	/**
	 * @param           $ownerId
	 * @param PostModel $post
	 * @return bool
	 */
	public function isAdmin($ownerId, PostModel $post)
	{
		return ($this->isOwner($ownerId, $post) || $this->isAuthor($ownerId, $post) || $this->isYouPage(
				$ownerId,
				$post
			));
	}

	/**
	 * @param       $userId
	 * @param       $body
	 * @param       $type
	 * @param array $tags
	 * @param array $data
	 * @param array $owners
	 * @return \MyPet\Posts\Model\PostModel
	 */
	public function add($userId, $body, $type, Array $tags, Array $data = [], Array $owners = [])
	{
		$post = new PostModel();
		$post->id = GeneratorHelper::getStringUniqueId();
		$post->userId = $userId;
		$post->body = $body;
		$post->type = $type;
		$post->tags = $tags;
		$post->data = $data;
		$post->time = time();
		$post->owners = $owners;

		return $post;
	}

	/**
	 * @param                              $userId
	 * @param \MyPet\Posts\Model\PostModel $post
	 */
	public function remove($userId, PostModel $post)
	{
		if ($this->isAdmin($userId, $post))
		{
			$post->delete();
		}
	}

	/**
	 * @param                              $userId
	 * @param \MyPet\Posts\Model\PostModel $post
	 * @param                              $body
	 * @param                              $type
	 * @param array                        $tags
	 * @param array                        $data
	 * @param array                        $owners
	 * @return \MyPet\Posts\Model\PostModel|null
	 */
	public function update($userId, PostModel $post, $body, $type, Array $tags, Array $data = [], Array $owners = null)
	{
		if ($this->isAdmin($userId, $post))
		{
			$post->userId = $userId;
			$post->body = $body;
			$post->type = $type;
			$post->tags = $tags;
			$post->data = $data;
			if ($owners)
			{
				$post->owners = $owners;
			}

			return true;
		}

		return false;
	}

	/**
	 * @param           $userId
	 * @param PostModel $post
	 * @param array     $owners
	 * @return PostModel
	 */
	public function share($userId, PostModel $post, Array $owners = null)
	{
		$share = new PostModel();
		$share->id = GeneratorHelper::getStringUniqueId();
		$share->userId = $userId;
		$share->body = $post->body;
		$share->type = $userId;
		$share->tags = $post->tags;
		$share->data = $post->data;
		$share->srcId = $post->id;
		$share->time = time();
		if ($owners)
		{
			$share->owners = $owners;
		}

		return $share;
	}

	/**
	 * @param PostModel $post
	 * @param string    $ownerId
	 */
	public function addOwner(PostModel $post, $ownerId)
	{
		$post->owners[] = $ownerId;
	}

	/**
	 * @param PostModel $post
	 * @param string    $ownerId
	 */
	public function remOwner(PostModel $post, $ownerId)
	{
		$key = array_search($ownerId, $post->owners);
		if (isset($post->owners[$key]))
		{
			unset($post->owners[$key]);
		}
	}

	/**
	 * @param PostModel $post
	 * @param           $owners
	 */
	public function setOwners(PostModel $post, $owners)
	{
		$post->owners = $owners;
	}

	/**
	 * Действия по лайку поста
	 *
	 * @param $id
	 * @return mixed
	 */
	public function like($id, LikeHistory $like)
	{
		$query = ['id' => $id];
		$update = ['$inc' => ['likes' => 1]];
		$fields = [
			'id'    => 1,
			'likes' => 1
		];
		$data = PostModel::findAndModify($query, $update, $fields);

		$data['likes'] = $data['likes'] + 1;
		$this->server->emit(self::EVENT_LIKE_POST, [$id, $like]);

		return $data;
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function unlike($id, LikeHistory $like)
	{
		$query = ['id' => $id];
		$update = ['$inc' => ['likes' => -1]];
		$fields = [
			'id'    => 1,
			'likes' => 1
		];
		$data = PostModel::findAndModify($query, $update, $fields);

		$this->server->emit(self::EVENT_UNLIKE_POST, [$id, $like]);
		$data['likes'] = $data['likes'] - 1;

		return $data;
	}

	/**
	 * @param $tags
	 * @param $page
	 * @param $perPage
	 * @return array
	 */
	public function getPostsByTags(Array $tags, $page = 0, $perPage = 10)
	{
		return PostModel::getAllAdvanced(['tags' => ['$in' => $tags]], [], false, true, $page, $perPage);
	}

	/**
	 * @param $tags
	 * @param $page
	 * @param $perPage
	 * @return array
	 */
	public function getPostsByTypes(Array $types, $page = 0, $perPage = 10)
	{
		return PostModel::getAllAdvanced(['type' => ['$in' => $types]], [], false, true, $page, $perPage);
	}

	/**
	 * @param $type
	 * @param $page
	 * @param $perPage
	 * @return array
	 */
	public function getPostsByType($type, $page = 0, $perPage = 10)
	{
		return PostModel::getAllAdvanced(['type' => $type], [], false, true, $page, $perPage);
	}

	public function getAllComments(Array $posts, $page = 0, $perPage = 3)
	{
		$ids = [];
		foreach ($posts as $post)
		{
			$ids[] = $post['id'];
		}

		return $this->getPostsByTypes($ids, $page, $perPage);
	}
} 
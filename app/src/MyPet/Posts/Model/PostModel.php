<?php
namespace MyPet\Posts\Model;

use KMCore\DB\ObjectModel;

/**
 * @property mixed owners
 * @property mixed userId
 * @property mixed body
 * @property mixed type
 * @property mixed tags
 * @property mixed data
 * @property mixed time
 * @property mixed id
 * @property mixed srcId
 * @property mixed attr
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PostModel extends ObjectModel
{
	protected static $pk = 'id';
	protected static $collection = 'mp_post_model';
	protected static $readPreference = \MongoClient::RP_SECONDARY_PREFERRED;
	protected static $writeConcernMode = 0;
	protected static $sort = ['time' => -1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['type' => 1],
		],
		[
			'keys' => ['tags' => 1],
		],
		[
			'keys' => ['time' => -1],
		],
		[
			'keys' => ['owners' => 1],
		]
	];

	protected static $fieldsDefault = [
		'id'     => '',
		'time'   => 0,
		'tags'   => [],
		'type'   => '',
		'body'   => '',
		'data'   => [],
		'userId' => '',
		'owners' => [],
		'srcId'  => '',
		'likes'  => 0,
		'attr'   => [],
	];

	protected static $fieldsValidate = [
		'id'     => self::TYPE_STRING,
		'time'   => self::TYPE_TIMESTAMP,
		'tags'   => self::TYPE_JSON,
		'type'   => self::TYPE_STRING,
		'body'   => self::TYPE_STRING,
		'data'   => self::TYPE_JSON,
		'userId' => self::TYPE_STRING,
		'owners' => self::TYPE_JSON,
		'srcId'  => self::TYPE_STRING,
		'likes'  => self::TYPE_UNSIGNED_INT,
		'attr'   => self::TYPE_JSON,
	];

	protected static $fieldsHint = [
		'id'     => 'Идентификатор записи',
		'time'   => 'Время добавления',
		'type'   => 'Тип записи',
		'body'   => 'Сожержимое записи',
		'data'   => 'Дополнительный данные (прикрепленные фотографии, и так далее)',
		'userId' => 'Идентификатор пользовател, который добавил запись',
		'srcId'  => 'Исходная запись',
		'likes'  => 'Количество лайков записи',
		'attr'   => 'Атрибути поста',
	];
} 
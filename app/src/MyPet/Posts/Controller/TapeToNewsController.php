<?php
namespace MyPet\Posts\Controller;

use MyPet\BaseApiController;
use MyPet\Posts\Classes\PostManager;
use MyPet\Posts\Classes\UserAggregate;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class TapeToNewsController extends BaseApiController
{
	const MAX_PER_PAGE = 100;
	const DEFAULT_PER_PAGE = 100;
	public static $aggregateUser = true;

	public function getPostsByTypes()
	{
		$types = (array)$this->request->getGetOrPost('types', []);
		$page = (int)$this->request->getGetOrPost('page', 0);
		$perPage = (int)$this->request->getGetOrPost('perPage', self::DEFAULT_PER_PAGE);
		if ($perPage > self::MAX_PER_PAGE)
		{
			$perPage = self::MAX_PER_PAGE;
		}

		$postManager = new PostManager($this->server);
		$posts = [];
		if (!empty($type))
		{
			$posts = $postManager->getPostsByTypes($types, $page, $perPage);
			if (static::$aggregateUser)
			{
				$aggregate = new UserAggregate($this->server);
				$aggregate->run($posts);
			}
		}

		$this->success = true;
		$this->data = $posts;
		$this->response();
	}

	public function getPostsByType()
	{
		$type = (string)$this->request->getGetOrPost('type', '');
		$page = (int)$this->request->getGetOrPost('page', 0);
		$perPage = (int)$this->request->getGetOrPost('perPage', self::DEFAULT_PER_PAGE);
		if ($perPage > self::MAX_PER_PAGE)
		{
			$perPage = self::MAX_PER_PAGE;
		}

		$postManager = new PostManager($this->server);
		$posts = [];
		if (!empty($type))
		{
			$posts = $postManager->getPostsByType($type, $page, $perPage);
			if (static::$aggregateUser)
			{
				$aggregate = new UserAggregate($this->server);
				$aggregate->run($posts);
			}
		}

		$this->success = true;
		$this->data = $posts;
		$this->response();
	}

	public function getPostsByTags()
	{
		$tags = (array)$this->request->getGetOrPost('tags', []);
		$page = (int)$this->request->getGetOrPost('page', 0);
		$perPage = (int)$this->request->getGetOrPost('perPage', self::DEFAULT_PER_PAGE);
		if ($perPage > self::MAX_PER_PAGE)
		{
			$perPage = self::MAX_PER_PAGE;
		}
		$postManager = new PostManager($this->server);
		$posts = [];
		if (!empty($tags))
		{
			$posts = $postManager->getPostsByTags($tags, $page, $perPage);
			if (static::$aggregateUser)
			{
				$aggregate = new UserAggregate($this->server);
				$aggregate->run($posts);
			}
		}

		$this->success = true;
		$this->data = $posts;
		$this->response();
	}
} 
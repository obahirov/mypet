<?php
namespace MyPet\Posts\Controller;

use MyPet\Groups\Model\GroupModel;
use MyPet\Groups\Model\TopicModel;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\Photos\Model\PhotoModel;
use MyPet\Posts\Classes\PostManager;
use MyPet\Posts\Model\PostModel;
use MyPet\ApiController;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Classes\LikeHelper;
use MyPet\Users\Model\LikeHistory;
use MyPet\Users\Model\User;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PostController extends ApiController
{
	protected $postManager;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->postManager = new PostManager($this->server);
	}

	public function add()
	{
		$userId = AuthManager::getSessionUserId();
		$body = (string)$this->request->getGetOrPost('body', '');
		$data = (array)$this->request->getGetOrPost('data', []);

		$isGroup = (bool)$this->request->getGetOrPost('isGroup', false);
		$isTopic = (bool)$this->request->getGetOrPost('isTopic', false);

		if($isGroup)
		{
			$type = (string)$this->request->getGetOrPost('type', '');
			$group = new GroupModel($type);
			if(!$group->isLoadedObject())
			{
				throw new \Exception('Not found group');
			}

			if(array_search($group->users, $this->user->id) === false && array_search($group->owners, $this->user->id) === false)
			{
				throw new \Exception('Not have permission');
			}
		}
		elseif($isTopic)
		{
			$type = (string)$this->request->getGetOrPost('type', '');
			$topic = new TopicModel($type);
			if(!$topic->isLoadedObject())
			{
				throw new \Exception('Not found group');
			}
			$group = new GroupModel($topic->groupId);
			if(!$group->isLoadedObject())
			{
				throw new \Exception('Not found group');
			}

			if(array_search($group->users, $this->user->id) === false && array_search($group->owners, $this->user->id) === false && array_search($topic->owners, $this->user->id) === false )
			{
				throw new \Exception('Not have permission');
			}
		}

		if (!empty($data) || !empty($body))
		{
			$type = (string)$this->request->getGetOrPost('type', '');
			$tags = (array)$this->request->getGetOrPost('tags', []);
			$owners = (array)$this->request->getGetOrPost('owners', []);
			$post = $this->postManager->add($userId, $body, $type, $tags, $data, $owners);
			$post->attr['author']['id'] = $this->user->id;
			$post->attr['author']['first_name'] = $this->user->first_name;
			$post->attr['author']['last_name'] = $this->user->last_name;
			$post->attr['author']['photo'] = $this->user->photo;
			$post->attr['author']['profession'] = $this->user->profession;

			$post->save();
			$this->success = true;

			$isComment = (bool)$this->request->getGetOrPost('isComment', false);
			if ($isComment)
			{
				$parentPost = new PostModel($type);
				if ($parentPost->isLoadedObject())
				{
					$owner = new User($parentPost->userId);
					LikeHelper::request(
						$this->user,
						$owner,
						$post->id,
						LikeHelper::TYPE_POST,
						RequestManager::TYPE_COMMENT,
						['body' => $body]
					);
				}
				else
				{
					$parentPhoto = new PhotoModel($type);
					if ($parentPhoto->isLoadedObject())
					{
						$owner = new User($parentPhoto->userId);
						LikeHelper::request(
							$this->user,
							$owner,
							$post->id,
							LikeHelper::TYPE_POST,
							RequestManager::TYPE_COMMENT,
							['body' => $body, 'photo' => $parentPhoto->photo]
						);
					}
				}
			}
		}

		$this->response();
	}

	public function remove()
	{
		$success = false;
		$userId = AuthManager::getSessionUserId();
		$id = (string)$this->request->getGetOrPost('id', '');
		$post = new PostModel($id);
		if ($post->isLoadedObject())
		{
			$this->postManager->remove($userId, $post);
			$success = true;
		}

		$this->success = $success;
		$this->response();
	}

	public function update()
	{
		$success = false;
		$userId = AuthManager::getSessionUserId();
		$id = (string)$this->request->getGetOrPost('id', '');
		$post = new PostModel($id);
		$body = (string)$this->request->getGetOrPost('body', $post->body);
		$data = (array)$this->request->getGetOrPost('data', $post->data);
		if ($post->isLoadedObject() && (!empty($data) || !empty($body)))
		{
			$type = (string)$this->request->getGetOrPost('type', $post->type);
			$tags = (array)$this->request->getGetOrPost('tags', $post->tags);
			$owners = $this->request->getGetOrPost('owners', $post->owners);
			if (is_array($owners))
			{
				$save = $this->postManager->update($userId, $post, $body, $type, $tags, $data, $owners);
			}
			else
			{
				$save = $this->postManager->update($userId, $post, $body, $type, $tags, $data);
			}
			if ($save)
			{
				if ($this->postManager->isAuthor($userId, $post))
				{
					$post->attr['author']['id'] = $this->user->id;
					$post->attr['author']['first_name'] = $this->user->first_name;
					$post->attr['author']['last_name'] = $this->user->last_name;
					$post->attr['author']['photo'] = $this->user->photo;
					$post->attr['author']['profession'] = $this->user->profession;
				}
				$post->save();
				$success = true;
			}
		}

		$this->success = $success;
		$this->response();
	}

	public function share()
	{
		$success = false;
		$userId = AuthManager::getSessionUserId();
		$id = (string)$this->request->getGetOrPost('id', '');
		$post = new PostModel($id);
		if ($post->isLoadedObject())
		{
			$share = $this->postManager->share($userId, $post);
			$share->save();
			$success = true;

			$owner = new User($post->userId);
			LikeHelper::request(
				$this->user,
				$owner,
				$post->id,
				LikeHelper::TYPE_POST,
				RequestManager::TYPE_SHARE,
				$post->getFieldsSanitized()
			);
		}

		$this->success = $success;
		$this->response();
	}

	public function like()
	{
		$user = $this->user;
		$this->server->on(
			PostManager::EVENT_LIKE_POST,
			function ($id, LikeHistory $like) use ($user)
			{
				$like->id = $user->id;
				$like->likeId = $id;
				$like->type = LikeHelper::TYPE_POST;
				$like->save();

				$post = new PostModel($id);
				if ($post->isLoadedObject())
				{
					$owner = new User($post->userId);
					LikeHelper::request(
						$this->user,
						$owner,
						$post->id,
						LikeHelper::TYPE_POST,
						RequestManager::TYPE_LIKE,
						['body' => $post->body]
					);
				}
			}
		);
		$this->server->on(
			PostManager::EVENT_UNLIKE_POST,
			function ($id, LikeHistory $like) use ($user)
			{
				$like->delete();
			}
		);

		$id = (string)$this->request->getGetOrPost('id', '');

		$like = new LikeHistory(['id' => $user->id, 'likeId' => $id]);
		if (!$like->isLoadedObject())
		{
			$data = $this->postManager->like($id, $like);
			$success = true;
		}
		else
		{
			$data = $this->postManager->unlike($id, $like);
			$success = true;
		}

		$this->data = $data;
		$this->success = $success;
		$this->response();
	}

	public function addOwner()
	{
		$success = false;
		$id = (string)$this->request->getGetOrPost('id', '');
		$post = new PostModel($id);
		if ($post->isLoadedObject())
		{
			$ownerId = (string)$this->request->getGetOrPost('ownerId', '');
			$user = new User($ownerId);
			if ($user->isLoadedObject())
			{
				$this->postManager->addOwner($post, $ownerId);
				$post->save();
				$success = true;
			}
		}

		$this->success = $success;
		$this->response();
	}

	public function remOwner()
	{
		$success = false;
		$id = (string)$this->request->getGetOrPost('id', '');
		$post = new PostModel($id);
		if ($post->isLoadedObject())
		{
			$ownerId = (string)$this->request->getGetOrPost('ownerId', '');
			$user = new User($ownerId);
			if ($user->isLoadedObject())
			{
				$this->postManager->remOwner($post, $ownerId);
				$post->save();
				$success = true;
			}
		}

		$this->success = $success;
		$this->response();
	}

	public function setOwners()
	{
		$success = false;
		$id = (string)$this->request->getGetOrPost('id', '');
		$post = new PostModel($id);
		if ($post->isLoadedObject())
		{
			$owners = (array)$this->request->getGetOrPost('owners', []);
			$this->postManager->setOwners($post, $owners);
			$post->save();
			$success = true;
		}

		$this->success = $success;
		$this->response();
	}
} 
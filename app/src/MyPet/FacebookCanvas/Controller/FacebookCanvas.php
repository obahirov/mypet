<?php
namespace MyPet\FacebookCanvas\Controller;
use KMCore\BaseController;

/**
 * Class FacebookCanvas
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FacebookCanvas extends BaseController
{
	public function app()
	{
		$content = 'Hello world!';

		$view = realpath(__DIR__.'/..');
		$output = $this->server->getContainer('render')->render($view.'/Views/facebook_canvas.html.twig', ['content' => $content]);
		$this->response->html($output);
	}
}
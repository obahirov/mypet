<?php
namespace MyPet\TokenAuth;

use KMCore\Helper\SessionStorage;
use KMCore\ServerContainer;

class SecureTokenManager
{
	protected $server;

	public static function generate(SecureTokenManager $secure, Array $userData, Array $params)
	{
		$expiresIn = time() + 6400;
		$signedRequest = $secure->getSignedRequest($userData, $expiresIn);
		$params['expiresIn'] = $expiresIn;
		$data['accessToken'] = $secure->encode($params, $signedRequest);
		$data['signedRequest'] = $signedRequest;
		$data['expiresIn'] = $expiresIn;
		SessionStorage::set('currentAuthData', $data);

		//		CookieStorage::set('currentAuthData', $data, $expiresIn);
		return $data;
	}

	public function __construct(ServerContainer $server)
	{
		$this->server = $server;
	}

	public function getSignedRequest(Array $userData, $expiresIn)
	{
		$userData = implode('', $userData);

		return sha1(md5($userData).$expiresIn);
	}

	public function validateSignedRequest($signedRequest, Array $userData, $expiresIn)
	{
		$validSignedRequest = $this->getSignedRequest($userData, $expiresIn);

		return ($validSignedRequest === $signedRequest);
	}

	public function encode($params, $signedRequest)
	{
		if (is_array($params) || is_object($params))
		{
			$params = json_encode($params);
		}

		if (!is_string($params))
		{
			$params = (string)$params;
		}

		$salt = $this->server->getContainer('config')->get('sessionXorData', 'salt');
		$pass = $this->server->getContainer('config')->get('sessionXorData', 'pass');
		$pass = $signedRequest.$pass;

		$accessToken = urlencode(base64_encode(\KMCore\Helper\GeneratorHelper::xorCode($params, $pass, $salt)));

		return $accessToken;
	}

	public function decode($signedRequest, $accessToken)
	{
		$salt = $this->server->getContainer('config')->get('sessionXorData', 'salt');
		$pass = $this->server->getContainer('config')->get('sessionXorData', 'pass');
		$pass = $signedRequest.$pass;

		$userId = \KMCore\Helper\GeneratorHelper::xorCode(base64_decode(urldecode($accessToken)), $pass, $salt);

		return $userId;
	}
}
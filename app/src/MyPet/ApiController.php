<?php
namespace MyPet;

use MyPet\TokenAuth\SecureTokenManager;
use MyPet\Users\Classes\AuthManager;
use MyPet\Users\Model\User;
use UnexpectedValueException;

/**
 * Контроллер з базовим методом відповіді через форми
 */
class ApiController extends BaseApiController
{
	/**
	 * @var User Пользователь
	 */
	public $user;

	/**
	 * @var array Массив данных пользователя, до обработки
	 */
	protected $userBefore;

	/**
	 * Инициализируем приложение
	 */
	public function __construct()
	{
		parent::__construct();

		$secure = new SecureTokenManager($this->server);
		$accessToken = (string)$this->request->getGetOrPost('accessToken', '');
		$signedRequest = (string)$this->request->getGetOrPost('signedRequest', '');

		if (!empty($accessToken) && !empty($signedRequest))
		{
			$userId = (string)$this->request->getGetOrPost('userId', '');
			$data = $secure->decode($signedRequest, $accessToken);
			if (!empty($data) && isset($data['id'], $data['login_count'], $data['expiresIn'], $data['lastlog']) && $userId === $data['id'])
			{
				if ($data['expiresIn'] < time())
				{
					throw new UnexpectedValueException('Expire timeout');
				}
				$userId = $data['id'];
			}
			else
			{
				throw new UnexpectedValueException('Not auth user');
			}
		}
		else
		{
			if (!AuthManager::loggedIn())
			{
				throw new UnexpectedValueException('Not auth user');
			}

			$userId = AuthManager::getSessionUserId();
		}

		if (empty($userId))
		{
			throw new UnexpectedValueException('Invalid userId!');
		}

		$user = new User($userId);
		if (!$user->isLoadedObject())
		{
			throw new UnexpectedValueException('User: '.$userId.' not found!');
		}

		if (isset($data) && !empty($accessToken) && !empty($signedRequest))
		{
			if (!$secure->validateSignedRequest(
					$signedRequest,
					[$user->password, $user->login_count, $user->lastlog],
					$data['expiresIn']
				) || $data['login_count'] !== $user->login_count || $data['lastlog'] !== $user->lastlog
			)
			{
				throw new UnexpectedValueException('Error session!');
			}
		}

		$this->user = $user;
		$this->userBefore = $user->getFieldsArray();
	}
}

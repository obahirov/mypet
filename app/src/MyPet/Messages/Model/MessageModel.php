<?php
namespace MyPet\Messages\Model;

use KMCore\DB\ObjectModel;

/**
 * Модель хранения друзей
 *
 * @property mixed time
 * @property mixed message
 * @property mixed dialogId
 * @property mixed userId
 * @property mixed _id
 */
class MessageModel extends ObjectModel
{
	protected static $pk = '_id';
	protected static $collection = 'mp_message_model';
	protected static $slaveOkay = true;
	protected static $safeWrite = true;
	protected static $sort = ['dialogId' => 1, 'time' => -1];
	protected static $indexes = [
		[
			'keys' => ['dialogId' => 1, 'time' => -1],
		],
		[
			'keys' => ['userId' => 1],
		],
	];

	protected static $fieldsDefault = [
		'_id'      => '',
		'dialogId' => '',
		'time'     => 0,
		'message'  => '',
		'userId'   => '',
	];

	protected static $fieldsValidate = [
		'_id'      => self::TYPE_MONGO_ID,
		'dialogId' => self::TYPE_STRING,
		'time'     => self::TYPE_TIMESTAMP,
		'message'  => self::TYPE_STRING,
		'userId'   => self::TYPE_STRING,
	];
}
<?php
namespace MyPet\Messages\Model;

use KMCore\DB\ObjectModel;

/**
 * Модель хранения друзей
 *
 * @property mixed  users
 * @property mixed  time
 * @property mixed  _id
 * @property string description
 * @property string title
 */
class DialogModel extends ObjectModel
{
	protected static $pk = '_id';
	protected static $collection = 'mp_dialog_model';
	protected static $slaveOkay = true;
	protected static $safeWrite = true;
	protected static $sort = ['time' => -1];
	protected static $indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true
		],
		[
			'keys' => ['users' => 1],
		],
		[
			'keys' => ['time' => -1],
		],
	];

	protected static $fieldsDefault = [
		'_id'         => '',
		'title'       => '',
		'description' => '',
		'users'       => [],
		'time'        => 0,
	];

	protected static $fieldsValidate = [
		'_id'         => self::TYPE_STRING,
		'title'       => self::TYPE_STRING,
		'description' => self::TYPE_STRING,
		'users'       => self::TYPE_JSON,
		'time'        => self::TYPE_TIMESTAMP,
	];
}
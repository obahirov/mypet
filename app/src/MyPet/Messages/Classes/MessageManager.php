<?php
namespace MyPet\Messages\Classes;

use KMCore\ServerContainer;
use MyPet\Messages\Model\MessageModel;

/**
 * Класс для управления списками друзей, и их статусами.
 */
class MessageManager
{
	const EVENT_MESSAGE_SEND = 'event.message.send';
	const EVENT_MESSAGE_EDIT = 'event.message.edit';
	const EVENT_MESSAGE_DELETE = 'event.message.delete';

	const MESSAGE_PER_PAGE = 20;

	/**
	 * @var ServerContainer
	 */
	protected $server;

	/**
	 * @param ServerContainer $server
	 */
	public function __construct(ServerContainer $server)
	{
		$this->server = $server;
	}

	/**
	 * @param      $userId
	 * @param      $dialogId
	 * @param      $text
	 * @param bool $sendRequest
	 * @return array
	 */
	public function send($userId, $dialogId, $text, $sendRequest = true)
	{
		$message = new MessageModel();
		$message->userId = $userId;
		$message->dialogId = $dialogId;
		$message->message = $text;
		$message->time = time();
		$message->save();

		if($sendRequest)
		{
			$this->server->emit(self::EVENT_MESSAGE_SEND, [$message]);
		}

		return $message->getFieldsSanitized();
	}

	/**
	 * @param $userId
	 * @param $_id
	 * @param $text
	 * @return array
	 */
	public function edit($userId, $_id, $text)
	{
		$message = new MessageModel(new \MongoId($_id));
		if ($message->isLoadedObject() && $message->userId === $userId)
		{
			$message->message = $text;
			$message->save();
			$this->server->emit(self::EVENT_MESSAGE_EDIT, [$message]);
		}

		return $message->getFieldsSanitized();
	}

	/**
	 * @param $userId
	 * @param $_id
	 */
	public function delete($userId, $_id)
	{
		$message = new MessageModel(new \MongoId($_id));
		if ($message->isLoadedObject() && $message->userId === $userId)
		{
			$message->delete();
			$this->server->emit(self::EVENT_MESSAGE_DELETE, [$message]);
		}
	}

	/**
	 * @param     $dialogId
	 * @param int $page
	 * @param int $perPage
	 * @return array
	 */
	public function getMessagesByDialog($dialogId, $page = 0, $perPage = self::MESSAGE_PER_PAGE)
	{
		return MessageModel::getAllAdvanced(
			['dialogId' => $dialogId],
			[],
			false,
			true,
			$page,
			$perPage
		);
	}

	/**
	 * @param $dialogId
	 * @return int
	 */
	public static function count($dialogId)
	{
		return MessageModel::count(['dialogId' => $dialogId]);
	}

	/**
	 * @param     $dialogId
	 * @param int $perPage
	 * @return float
	 */
	public function countPage($dialogId, $perPage = self::MESSAGE_PER_PAGE)
	{
		return ceil(static::count($dialogId) / $perPage);
	}
}
<?php
namespace MyPet\Messages\Classes;

use KMCore\Helper\GeneratorHelper;
use KMCore\ServerContainer;
use MyPet\Messages\Model\DialogModel;
use MyPet\Messages\Model\MessageModel;

/**
 * Класс для управления списками друзей, и их статусами.
 */
class DialogManager
{
	const EVENT_DIALOG_CREATE = 'event.dialog.create';
	const EVENT_DIALOG_ADD_USER = 'event.dialog.add.user';
	const EVENT_DIALOG_REM_USER = 'event.dialog.remove.user';
	const EVENT_DIALOG_DELETE = 'event.dialog.delete';

	const DIALOG_PER_PAGE = 10;

	/**
	 * @var ServerContainer
	 */
	protected $server;

	/**
	 * @param ServerContainer $server
	 */
	public function __construct(ServerContainer $server)
	{
		$this->server = $server;
	}

	/**
	 * @param        $userId
	 * @param        $friendId
	 * @param string $title
	 * @param string $description
	 * @return mixed
	 */
	public function foundOrCreate($userId, $friendId, $title = '', $description = '')
	{
		$result = DialogModel::getAllAdvanced(
			['$or' => [['users' => [$userId, $friendId]], ['users' => [$friendId, $userId]]]]
		);
		if (isset($result[0]))
		{
			return $result[0];
		}
		else
		{
			return $this->create($userId, $friendId, $title, $description);
		}
	}

	/**
	 * @param        $userId
	 * @param        $friendId
	 * @param string $title
	 * @param string $description
	 * @return array
	 */
	public function create($userId, $friendId, $title = '', $description = '')
	{
		$dialog = new DialogModel();
		$dialog->_id = GeneratorHelper::getStringUniqueId();
		$dialog->title = $title;
		$dialog->description = $description;
		$dialog->users[] = $userId;
		$dialog->users[] = $friendId;
		$dialog->time = time();
		$dialog->save();

		$this->server->emit(self::EVENT_DIALOG_CREATE, [$dialog]);

		return $dialog->getFieldsSanitized();
	}

	/***
	 * @param        $userId
	 * @param        $dialogId
	 * @param string $title
	 * @param string $description
	 * @return array
	 */
	public function setTitleAndDescription($userId, $dialogId, $title = '', $description = '')
	{
		$dialog = new DialogModel($dialogId);
		if ($dialog->isLoadedObject() && array_search($userId, $dialog->users) !== false)
		{
			$dialog->title = $title;
			$dialog->description = $description;
			$dialog->save();
		}

		return $dialog->getFieldsSanitized();
	}

	/**
	 * @param      $dialogId
	 * @param      $userId
	 * @param      $friendId
	 * @return array
	 */
	public function addUser($dialogId, $userId, $friendId)
	{
		$dialog = new DialogModel($dialogId);
		if ($dialog->isLoadedObject() && array_search($userId, $dialog->users) !== false && array_search(
				$friendId,
				$dialog->users
			) === false
		)
		{
			$dialog->users[] = $friendId;
			$dialog->time = time();
			$dialog->save();

			$this->server->emit(self::EVENT_DIALOG_ADD_USER, [$userId, $dialog]);
		}

		return $dialog->getFieldsSanitized();
	}

	/**
	 * @param $dialogId
	 * @param $userId
	 * @param $friendId
	 * @return array
	 */
	public function removeUser($dialogId, $userId, $friendId)
	{
		$dialog = new DialogModel($dialogId);
		if ($dialog->isLoadedObject() && array_search($userId, $dialog->users) !== false)
		{
			$key = array_search($friendId, $dialog->users);
			if ($key && isset($dialog->users[$key]))
			{
				unset($dialog->users[$key]);
				$dialog->time = time();
				$dialog->save();

				$this->server->emit(self::EVENT_DIALOG_REM_USER, [$userId, $dialog]);
			}
		}

		return $dialog->getFieldsSanitized();
	}

	/**
	 * @param $dialogId
	 * @param $userId
	 */
	public function delete($dialogId, $userId)
	{
		$dialog = new DialogModel($dialogId);
		if ($dialog->isLoadedObject() && array_search($userId, $dialog->users) !== false)
		{
			MessageModel::deleteAllAdvanced(['dialogId' => $dialogId]);
			$dialog->delete();
			$this->server->emit(self::EVENT_DIALOG_DELETE, [$dialog]);
		}
	}

	/**
	 * @param     $userId
	 * @param int $page
	 * @param     $perPage
	 * @return array
	 */
	public function getDialogs($userId, $page = 0, $perPage = self::DIALOG_PER_PAGE)
	{
		return DialogModel::getAllAdvanced(
			['users' => $userId],
			[],
			false,
			true,
			$page,
			$perPage
		);
	}

	/**
	 * @param $dialogId
	 * @param $userId
	 * @return array
	 */
	public function get($dialogId, $userId)
	{
		$dialog = new DialogModel($dialogId);
		if (array_search($userId, $dialog->users) !== false)
		{
			return $dialog->getFieldsSanitized();
		}

		return [];
	}

	/**
	 * @param $userId
	 * @return int
	 */
	public static function count($userId)
	{
		return DialogModel::count(['users' => $userId]);
	}

	/**
	 * @param $userId
	 * @param $perPage
	 * @return float
	 */
	public function countPage($userId, $perPage = self::DIALOG_PER_PAGE)
	{
		return ceil(static::count($userId) / $perPage);
	}
}
<?php
namespace MyPet\Messages\Controller;

use MyPet\Inbox\Classes\RequestData;
use MyPet\Inbox\Classes\RequestManager;
use MyPet\Messages\Classes\MessageManager;
use MyPet\Messages\Model\DialogModel;
use MyPet\Messages\Model\MessageModel;
use MyPet\ApiController;
use MyPet\Users\Classes\UserManager;

/**
 * Контроллер управления друзьями
 *
 * @api
 */
class MessageController extends ApiController
{
	/**
	 * @var MessageManager
	 */
	protected $messageManager;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->messageManager = new MessageManager($this->server);

		$this->server->on(
			MessageManager::EVENT_MESSAGE_DELETE,
			function (MessageModel $message)
			{
			}
		);
		$this->server->on(
			MessageManager::EVENT_MESSAGE_EDIT,
			function (MessageModel $message)
			{
			}
		);
		$this->server->on(
			MessageManager::EVENT_MESSAGE_SEND,
			function (MessageModel $message)
			{
				$dialog = new DialogModel($message->dialogId);
				foreach ($dialog->users as $userId)
				{
					if ($userId !== $message->userId)
					{
						$attributes = [
							'userId' => $message->userId,
							'dialogId' => $message->dialogId,
							'payload' => UserManager::getUserModelByUserId($message->userId)->getFieldsSanitized(),
							'text' => $message->message,
							'time' => $message->time,
							'users' => $dialog->users,
						];

						RequestData::init(new RequestData($userId))->setType(
							RequestManager::TYPE_NEW_MESSAGE
						)->setAttributes($attributes)->setUnique($message->_id)->setPub(true)->update();
					}
				}
			}
		);
	}

	public function send()
	{
		$dialogId = (string)$this->request->getGetOrPost('dialogId', '');
		$text = (string)$this->request->getGetOrPost('text', '', true, '<span>');
		$sendRequest = (bool)$this->request->getGetOrPost('sendRequest', false);

		$result = [];
		if (!empty($text))
		{
			$result = $this->messageManager->send($this->user->id, $dialogId, $text, $sendRequest);
		}

		$this->data = $result;
		$this->success = true;
		$this->response();
	}

	public function edit()
	{
		$_id = (string)$this->request->getGetOrPost('_id', '');
		$text = (string)$this->request->getGetOrPost('text', '', true, '<span>');

		$result = [];
		if (!empty($text))
		{
			$result = $this->messageManager->edit($this->user->id, $_id, $text);
		}

		$this->data = $result;
		$this->success = true;
		$this->response();
	}

	public function delete()
	{
		$_id = (string)$this->request->getGetOrPost('_id', '');
		$this->messageManager->delete($this->user->id, $_id);

		$this->data = ['success' => true];
		$this->success = true;
		$this->response();
	}

	public function get()
	{
		$dialogId = (string)$this->request->getGetOrPost('dialogId', '');
		$page = (int)$this->request->getGetOrPost('page', 0);
		$results = $this->messageManager->getMessagesByDialog($dialogId, $page, MessageManager::MESSAGE_PER_PAGE);
		$pages = $this->messageManager->countPage($dialogId, MessageManager::MESSAGE_PER_PAGE);
		$count = $this->messageManager->count($dialogId);

		$this->data['perPage'] = MessageManager::MESSAGE_PER_PAGE;
		$this->data['count'] = $count;
		$this->data['messages'] = $results;
		$this->data['pages'] = $pages;
		$this->success = true;
		$this->response();
	}
}

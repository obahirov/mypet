<?php
namespace MyPet\Messages\Controller;

use MyPet\Messages\Classes\DialogManager;
use MyPet\ApiController;

/**
 * Контроллер управления друзьями
 *
 * @api
 */
class DialogController extends ApiController
{
	/**
	 * @var DialogManager
	 */
	protected $dialogManager;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->dialogManager = new DialogManager($this->server);

		$this->server->on(
			DialogManager::EVENT_DIALOG_ADD_USER,
			function ()
			{
			}
		);
		$this->server->on(
			DialogManager::EVENT_DIALOG_CREATE,
			function ()
			{
			}
		);
		$this->server->on(
			DialogManager::EVENT_DIALOG_DELETE,
			function ()
			{
			}
		);
		$this->server->on(
			DialogManager::EVENT_DIALOG_REM_USER,
			function ()
			{
			}
		);
	}

	public function create()
	{
		$userId = $this->user->id;
		$friendId = (string)$this->request->getGetOrPost('friendId', '');
		$title = (string)$this->request->getGetOrPost('title', '', true);
		$description = (string)$this->request->getGetOrPost('description', '', true);
		$result = $this->dialogManager->foundOrCreate($userId, $friendId, $title, $description);

		$this->data = $result;
		$this->success = true;
		$this->response();
	}

	public function setTitleAndDescription()
	{
		$dialogId = (string)$this->request->getGetOrPost('dialogId', '');
		$title = (string)$this->request->getGetOrPost('title', '', true);
		$description = (string)$this->request->getGetOrPost('description', '', true);
		$result = $this->dialogManager->setTitleAndDescription($$dialogId, $title, $description);

		$this->data = $result;
		$this->success = true;
		$this->response();
	}

	public function addUser()
	{
		$dialogId = (string)$this->request->getGetOrPost('dialogId', '');
		$friendId = (string)$this->request->getGetOrPost('friendId', '');
		$result = $this->dialogManager->addUser($dialogId, $this->user->id, $friendId);

		$this->data = $result;
		$this->success = true;
		$this->response();
	}

	public function removeUser()
	{
		$dialogId = (string)$this->request->getGetOrPost('dialogId', '');
		$friendId = (string)$this->request->getGetOrPost('friendId', '');
		$result = $this->dialogManager->removeUser($dialogId, $this->user->id, $friendId);

		$this->data = $result;
		$this->success = true;
		$this->response();
	}

	public function delete()
	{
		$dialogId = (string)$this->request->getGetOrPost('dialogId', '');
		$this->dialogManager->delete($dialogId, $this->user->id);

		$this->data = ['success' => true];
		$this->success = true;
		$this->response();
	}

	public function get()
	{
		$page = (int)$this->request->getGetOrPost('page', 0);
		$results = $this->dialogManager->getDialogs($this->user->id, $page, DialogManager::DIALOG_PER_PAGE);
		$pages = $this->dialogManager->countPage($this->user->id, DialogManager::DIALOG_PER_PAGE);
		$count = $this->dialogManager->count($this->user->id);

		$this->data['perPage'] = DialogManager::DIALOG_PER_PAGE;
		$this->data['count'] = $count;
		$this->data['dialogs'] = $results;
		$this->data['pages'] = $pages;
		$this->success = true;
		$this->response();
	}
}

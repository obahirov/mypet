<?php
namespace MyPet\WebSockets\Events;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ListEvents
{
	const EVENT_CHAT_MESSAGE = 'event.chat.message';
	const EVENT_CHAT_CREATE = 'event.chat.create';
	const EVENT_CHAT_DELETE = 'event.chat.delete';
	const EVENT_FRIEND_ADD = 'event.friend.add';
	const EVENT_FRIEND_CONFIRM = 'event.friend.confirm';
	const EVENT_FRIEND_PET_CONFIRM = 'event.friend.pet.confirm';
	const EVENT_NEWS_ADD = 'event.news.add';
	const EVENT_INBOX_ADD = 'event.inbox.add';
} 
<?php
namespace MyPet\Groups\Controller;
use MyPet\ApiController;
use MyPet\Groups\Classes\GroupManager;
use MyPet\Groups\Model\GroupModel;
use MyPet\Groups\Model\TopicModel;

/**
 * Class GroupController
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class GroupController extends ApiController
{
	protected $groupManager;
	protected $perPage = 100;

	/**
	 * Create GroupManager
	 */
	public function __construct()
	{
		parent::__construct();
		$this->groupManager = new GroupManager($this->server);
	}

	public function attachUser()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$group = new GroupModel($id);
		if(array_search($group->users, $this->user->id) === false)
		{
			$group->users[] = $this->user->id;
			$group->save();
		}

		$this->data = $group->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function detachUser()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$group = new GroupModel($id);
		$key = array_search($group->users, $this->user->id);
		if($key !== false)
		{
			unset($group->users[$key]);
			$group->save();
		}
		$this->data = $group->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function getAllUserGroup()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$page = (int)$this->request->getGetOrPost('page ', 0);
		$this->data['groups'] = GroupModel::getAllAdvanced(['id' => $id], ['users' => 1], true, true, $page, $this->perPage);
		$this->data['pages'] = ceil(GroupModel::count(['id' => $id])/$this->perPage);
		$this->data['perPage'] = $this->perPage;
		$this->success = true;
		$this->response();
	}

	public function getAllOwnerGroup()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$page = (int)$this->request->getGetOrPost('page ', 0);
		$this->data['group'] = GroupModel::getAllAdvanced(['id' => $id], ['owners' => 1], true, true, $page, $this->perPage);
		$this->data['pages'] = ceil(GroupModel::count(['id' => $id])/$this->perPage);
		$this->data['perPage'] = $this->perPage;
		$this->success = true;
		$this->response();
	}
	public function getAllGroupUser()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$page = (int)$this->request->getGetOrPost('page ', 0);
		$this->data['group'] = GroupModel::getAllAdvanced(['users' => $id], true, true, $page, $this->perPage);
		$this->data['pages'] = ceil(GroupModel::count(['users' => $id])/$this->perPage);
		$this->data['perPage'] = $this->perPage;
		$this->success = true;
		$this->response();
	}

	public function getAllGroupOwner()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$page = (int)$this->request->getGetOrPost('page ', 0);
		$this->data['group'] = GroupModel::getAllAdvanced(['owners' => $id], true, true, $page, $this->perPage);
		$this->data['pages'] = ceil(GroupModel::count(['owners' => $id])/$this->perPage);
		$this->data['perPage'] = $this->perPage;
		$this->success = true;
		$this->response();
	}

	public function get()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$group = new GroupModel($id);
		$this->data = $group->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function create()
	{
		$name = (string)$this->request->getGetOrPost('name', '');
		$body = (string)$this->request->getGetOrPost('body', '');
		$site = (string)$this->request->getGetOrPost('site', '');
		$marks = (array)$this->request->getGetOrPost('marks', []);
		$photo = (string)$this->request->getGetOrPost('photo', '');
		$cover = (string)$this->request->getGetOrPost('cover', '');

		$group = new GroupModel();
		$this->groupManager->create($group, $name, $body, $site, $marks, $this->user->id, $photo, $cover);
		$this->data = $group->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function delete()
	{
		$id = (string)$this->request->getGetOrPost('id', '');

		$group = new GroupModel($id);
		if($this->groupManager->isOwnerGroup($group, $this->user->id))
		{
			$this->groupManager->delete($group);
		}

		$this->success = true;
		$this->response();
	}

	public function edit()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$name = (string)$this->request->getGetOrPost('name', '');
		$body = (string)$this->request->getGetOrPost('body', '');
		$site = (string)$this->request->getGetOrPost('site', '');
		$marks = (array)$this->request->getGetOrPost('marks', []);
		$photo = (string)$this->request->getGetOrPost('photo', '');
		$cover = (string)$this->request->getGetOrPost('cover', '');

		$group = new GroupModel($id);
		if($this->groupManager->isOwnerGroup($group, $this->user->id))
		{
			$this->groupManager->edit($group, $name, $body, $site, $marks, $photo, $cover);
		}
		$this->data = $group->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function getTopics()
	{
		$groupId = (string)$this->request->getGetOrPost('groupId', '');
		$page = (int)$this->request->getGetOrPost('page ', 0);
		$this->data = $this->groupManager->getTopics($groupId, $page);
		$this->data['pages'] = ceil($this->groupManager->countTopic($groupId)/$this->perPage);
		$this->data['perPage'] = $this->perPage;
		$this->success = true;
		$this->response();
	}

	public function createTopic()
	{
		$groupId = (string)$this->request->getGetOrPost('groupId', '');
		$name = (string)$this->request->getGetOrPost('name', '');
		$body = (string)$this->request->getGetOrPost('body', '');
		$owner = (string)$this->request->getGetOrPost('owner', '');

		$group = new GroupModel($groupId);
		if($this->groupManager->isOwnerGroup($group, $this->user->id))
		{
			$topic = new TopicModel();
			$this->groupManager->createTopic($topic, $groupId, $name, $body, $owner);
		}

		$this->success = true;
		$this->response();
	}

	public function editTopic()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$groupId = (string)$this->request->getGetOrPost('groupId', '');
		$name = (string)$this->request->getGetOrPost('name', '');
		$body = (string)$this->request->getGetOrPost('body', '');
		$owner = (string)$this->request->getGetOrPost('owner', '');

		$topic = new TopicModel($id);
		if($this->groupManager->isOwnerTopic($topic, $this->user->id))
		{
			$this->groupManager->editTopic($topic, $groupId, $name, $body, $owner);
		}
		$this->data = $topic->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function deleteTopic()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$topic = new TopicModel($id);
		if($this->groupManager->isOwnerTopic($topic, $this->user->id))
		{
			$this->groupManager->deleteTopic($topic);
		}

		$this->success = true;
		$this->response();
	}

	public function addOwnerGroup()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$owner = (string)$this->request->getGetOrPost('owner', '');
		$group = new GroupModel($id);
		if($this->groupManager->isOwnerGroup($group, $this->user->id))
		{
			$this->groupManager->addOwnerGroup($group, $owner);
		}
		$this->data = $group->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function addOwnerTopic()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$owner = (string)$this->request->getGetOrPost('owner', '');
		$topic = new TopicModel($id);
		if($this->groupManager->isOwnerTopic($topic, $this->user->id))
		{
			$this->groupManager->addOwnerTopic($topic, $owner);
		}
		$this->data = $topic->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}


	public function remOwnerGroup()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$owner = (string)$this->request->getGetOrPost('owner', '');
		$group = new GroupModel($id);
		if($this->groupManager->isOwnerGroup($group, $this->user->id))
		{
			$this->groupManager->remOwnerGroup($group, $owner);
		}
		$this->data = $group->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function remOwnerTopic()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$owner = (string)$this->request->getGetOrPost('owner', '');
		$topic = new TopicModel($id);
		if($this->groupManager->isOwnerTopic($topic, $this->user->id))
		{
			$this->groupManager->remOwnerTopic($topic, $owner);
		}
		$this->data = $topic->getFieldsSanitized();
		$this->success = true;
		$this->response();
	}

	public function isOwnerTopic()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$owner = (string)$this->request->getGetOrPost('owner', '');
		$topic = new TopicModel($id);
		$this->data['owner'] = $this->groupManager->isOwnerTopic($topic, $owner);

		$this->success = true;
		$this->response();
	}

	public function isOwnerGroup()
	{
		$id = (string)$this->request->getGetOrPost('id', '');
		$owner = (string)$this->request->getGetOrPost('owner', '');
		$group = new GroupModel($id);
		$this->data['owner'] = $this->groupManager->isOwnerGroup($group, $owner);

		$this->success = true;
		$this->response();
	}
}
<?php
namespace MyPet\Groups\Model;

use KMCore\DB\ObjectModel;

/**
 * @property array owners
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class TopicModel extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $readPreference = \MongoClient::RP_SECONDARY_PREFERRED;
	//protected static $writeConcernMode = 0;
	protected static $collection = 'mp_topics';
	protected static $pk = 'id';
	protected static $sort = ['time' => -1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys'   => ['tags' => 1],
		],
		[
			'keys'   => ['owners' => 1],
		],
		[
			'keys' => ['groupId' => 1],
		],
		[
			'keys'   => ['time' => -1],
		],
	];

	protected static $fieldsDefault = [
		'id'     => '',
		'time'   => 0,
		'tags'   => [],
		'owners'   => [],
		'groupId'   => '',
		'name'   => '',
		'body'   => '',
	];

	protected static $fieldsValidate = [
		'id'     => self::TYPE_STRING,
		'time'   => self::TYPE_TIMESTAMP,
		'tags'   => self::TYPE_JSON,
		'owners'   => [],
		'groupId'   => self::TYPE_STRING,
		'name'   => self::TYPE_STRING,
		'body'   => self::TYPE_STRING
	];
} 
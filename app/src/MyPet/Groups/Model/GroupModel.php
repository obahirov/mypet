<?php
namespace MyPet\Groups\Model;

use KMCore\DB\ObjectModel;

/**
 * @property array owners
 * @property string id
 * @property string name
 * @property string body
 * @property array  marks
 * @property string site
 * @property array users
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class GroupModel extends ObjectModel
{
	protected static $updateMethod = self::UPDATE_METHOD_SET;
	protected static $readPreference = \MongoClient::RP_SECONDARY_PREFERRED;
	protected static $collection = 'mp_groups';
	protected static $pk = 'id';
	protected static $sort = ['time' => -1];
	protected static $indexes = [
		[
			'keys'   => ['id' => 1],
			'unique' => true,
		],
		[
			'keys'   => ['tags' => 1],
		],
		[
			'keys'   => ['owners' => 1],
		],
		[
			'keys'   => ['users' => 1],
		],
		[
			'keys'   => ['time' => -1],
		],
	];

	protected static $fieldsDefault = [
		'id'   => '',
		'time'   => 0,
		'tags' => [],
		'owners' => [],
		'users' => [],
		'name' => '',
		'body' => '',
		'site' => '',
		'marks' => [],
		'photo' => '',
		'cover' => '',
	];

	protected static $fieldsValidate = [
		'id'   => self::TYPE_STRING,
		'time'   => self::TYPE_TIMESTAMP,
		'tags' => self::TYPE_JSON,
		'owners' => self::TYPE_JSON,
		'users' => self::TYPE_JSON,
		'name' => self::TYPE_STRING,
		'body' => self::TYPE_STRING,
		'site' => self::TYPE_STRING,
		'marks' => self::TYPE_JSON,
		'photo' => self::TYPE_STRING,
		'cover' => self::TYPE_STRING,
	];
} 
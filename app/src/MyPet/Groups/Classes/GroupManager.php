<?php
namespace MyPet\Groups\Classes;
use KMCore\ServerContainer;
use MyPet\Groups\Model\GroupModel;
use KMCore\Helper\GeneratorHelper;
use MyPet\Groups\Model\TopicModel;

/**
 * Class GroupManager
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class GroupManager 
{
	protected $server;

	/**
	 * @param ServerContainer $server
	 */
	public function __construct(ServerContainer $server)
	{
		$this->server = $server;
	}

	/**
	 * @param GroupModel $group
	 * @param            $owner
	 * @return bool
	 */
	public function isOwnerGroup(GroupModel $group, $owner)
	{
		return (array_search($owner, $group->owners) !== false);
	}

	/**
	 * @param TopicModel $topic
	 * @param            $owner
	 * @return bool
	 */
	public function isOwnerTopic(TopicModel $topic, $owner)
	{
		return (array_search($owner, $topic->owners) !== false);
	}

	/**
	 * @param GroupModel $group
	 * @param            $name
	 */
	public function create(GroupModel $group, $name, $body, $site, Array $marks, $owner, $photo, $cover)
	{
		$group->id = GeneratorHelper::getStringUniqueId();
		$group->name = $name;
		$group->body = $body;
		$group->site = $site;
		$group->marks = $marks;
		$group->owners[] = $owner;
		$group->photo = $photo;
		$group->cover = $cover;
		$group->save();
	}

	/**
	 * @param GroupModel $group
	 */
	public function delete(GroupModel $group)
	{
		if($group->isLoadedObject())
		{
			$group->delete();
		}
	}

	/**
	 * @param GroupModel $group
	 * @param            $name
	 */
	public function edit(GroupModel $group, $name, $body, $site, Array $marks, $photo, $cover)
	{
		if($group->isLoadedObject())
		{
			$group->name = $name;
			$group->body = $body;
			$group->site = $site;
			$group->marks = $marks;
			$group->photo = $photo;
			$group->cover = $cover;
			$group->save();
		}
	}

	/**
	 * @param $id
	 * @return array
	 */
	public function getTopics($id, $page = 0)
	{
		return TopicModel::getAllAdvanced(['groupId' => $id]);
	}
	public function countTopic($id)
	{
		return TopicModel::count(['groupId' => $id]);
	}
	/**
	 * @param TopicModel $topic
	 * @param            $id
	 */
	public function createTopic(TopicModel $topic, $id, $name, $body, $owner)
	{
		$topic->id = GeneratorHelper::getStringUniqueId();
		$topic->groupId = $id;
		$topic->name = $name;
		$topic->body = $body;
		$topic->owners[] = $owner;
		$topic->save();
	}

	/**
	 * @param TopicModel $topic
	 */
	public function editTopic(TopicModel $topic, $id, $name, $body)
	{
		if($topic->isLoadedObject())
		{
			$topic->groupId = $id;
			$topic->name = $name;
			$topic->body = $body;
			$topic->save();
		}
	}

	/**
	 * @param TopicModel $topic
	 */
	public function deleteTopic(TopicModel $topic)
	{
		if($topic->isLoadedObject())
		{
			$topic->delete();
		}
	}

	/**
	 * @param GroupModel $group
	 * @param            $owner
	 */
	public function addOwnerGroup(GroupModel $group, $owner)
	{
		if(array_search($owner, $group->owners) === false)
		{
			$group->owners[] = $owner;
			$group->save();
		}
	}

	/**
	 * @param TopicModel $topic
	 * @param            $owner
	 */
	public function addOwnerTopic(TopicModel $topic, $owner)
	{
		if(array_search($owner, $topic->owners) === false)
		{
			$topic->owners[] = $owner;
			$topic->save();
		}
	}

	/**
	 * @param GroupModel $group
	 * @param            $owner
	 */
	public function remOwnerGroup(GroupModel $group, $owner)
	{
		$key = array_search($owner, $group->owners);
		if($key !== false && count($group->owners) > 1)
		{
			unset($group->owners[$key]);
			$group->save();
		}
	}

	/**
	 * @param TopicModel $topic
	 * @param            $owner
	 */
	public function remOwnerTopic(TopicModel $topic, $owner)
	{
		$key = array_search($owner, $topic->owners);
		if($key !== false && count($topic->owners) > 1)
		{
			unset($topic->owners[$key]);
			$topic->save();
		}
	}
}
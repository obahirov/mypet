<?php
use KMCore\KMCoreContainer;
use KMCore\LogSystem\HandlerRegistry;
use KMCore\ServerContainer;
use Pimple\Container;

/**
 * Всевозможные пути
 */
define('REAL_PATH', realpath(__DIR__ . '/../'));
define('SRC_PATH', REAL_PATH . '/app/src');
define('VENDOR_PATH', REAL_PATH . '/vendor');
define('APP_PATH', REAL_PATH . '/app');
define('CONFIG_PATH', REAL_PATH . '/app/config');
define('PUBLIC_PATH', REAL_PATH . '/app/public');

/**
 * Информация о приложнии
 */
define('APP_VERSION', '20141108');
define('APP_STATUS', 'BUILD 1 (Matilda)');
define('APP_NAME', 'MyPet');

/**
 * Подключение автолоадера
 */
if (file_exists(VENDOR_PATH . '/autoload.php'))
{
	require_once VENDOR_PATH . '/autoload.php';
}
else
{
	header('HTTP/1.1 403 Forbidden');
	die('Autoloader is not found');
}

$route = (string)isset($_REQUEST['route'])?$_REQUEST['route']:'';
$GLOBALS['serverContainer'] = new \KMCore\KMCoreContainer(new Container());
/** @var KMCoreContainer $server */
$server = $GLOBALS['serverContainer'];
$server->registerHandler(new HandlerRegistry($GLOBALS['serverContainer']->getContainer('fastLog'), $GLOBALS['serverContainer']->getContainer('response')));
$server->enableSession()->startOutputBuffer()->displayError(true);
$config = $server->getConfig();
$config->loadDb();
$config->set('isTestHost', null, $config->get('mode', 'isTestHost', $config::TYPE_MAIN, $config->get('isTestHost')), $config::TYPE_MAIN);
$config->set('route', null, $route, $config::TYPE_MAIN);
$config->set('startTime', null, microtime(true), $config::TYPE_MAIN);
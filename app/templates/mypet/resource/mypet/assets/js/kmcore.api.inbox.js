/**
 * @author Maxim Tkach
 */
var globtitle = document.title;
var lockedIds = [];
var sounds = [];
kmcore.api.inbox = {
    intervalId: false,
    interval: 5000,
    currentPage: 0,
    verifyInboxStart: function () {
        kmcore.api.inbox.getAllRequests();
        if (kmcore.api.inbox.intervalId == false) {
            kmcore.api.inbox.intervalId = setInterval(function () {
                kmcore.api.inbox.getAllRequests();
            }, kmcore.api.inbox.interval);
        }
    },
    verifyInboxStop: function () {
        if (kmcore.api.inbox.intervalId) {
            clearInterval(kmcore.api.inbox.intervalId);
        }
    },
    collectRequest: function (giftIds, callback) {
        kmcore.api.send("/requestApi/collectRequest", {"giftIds": giftIds}, function (data) {
            if (typeof callback == 'function') {
                callback(data);
            }
        });
    },
    removeRequest: function (giftIds, callback) {
        kmcore.api.send("/requestApi/removeRequest", {"giftIds": giftIds}, function (data) {
            if (typeof callback == 'function') {
                callback(data);
            }
        });
    },
    getAllRequests: function (callback) {
        kmcore.api.send("/requestApi/getAllRequests", {}, function (data) {
            if (data.success) {
                var countMessages = 0;
                var countInbox = 0;
                var pages = data.response.pages;
                var lastRead = data.response.lastRead;
                var requests = data.response.requests;

                for (var key in requests) {
                    value = requests[key];

                    if (!kmcore.inArray(value.id, lockedIds)) {
                        if (value.type == 'new_message') {
                            if (kmcore.pageName == 'message' && value.attributes.dialogId === $("#dialogId").val()) {
                                //							$("#block_message").prepend('<div class="item_post dialogue-item"><div class="avatar"><a href="/user/{{ item.userId }}"><img src="{% if users[item.userId].profile.photo is empty %}{{ cdnUrl }}/resource/mypet/assets/img/avatars/big/2.png{% else %}{{ users[item.userId].profile.photo }}{% endif %}" alt=""/></a></div><div class="dialogue-body"><div class="dialogue-content"><h3><a href="/user/{{ item.userId }}">{{ users[item.userId].profile.last_name }} {{ users[item.userId].profile.first_name }}</a></h3><div class="content-message"><p class="txt" id="{{ item._id }}">{{ item.message|raw }}</p></div></div><div class="dialogue-date-time" id="time_{{ item._id }}"></div><script>var timestamp = {{ item.time }}000;var date = new Date();date.setTime(timestamp);$("#time_{{ item._id }}").append(\'<div class="date-time">\'+date.getDate()+\'/\'+(date.getMonth()+1)+\'/\'+date.getFullYear()+\'<span>\'+date.getHours()+\':\'+date.getMinutes()+\'</span></div>\');</script></div></div>');
                                kmcore.send("/json/" + kmcore.pageToLoadComponent + "?additionalComponents={\"mp_block_message\":1}&rendering=1&dialogId=" + value.attributes.dialogId, [], function (data) {
                                    data = data.response;
                                    if (($.isArray(data.result) && data.result.length > 0) || !$.isArray(data.result)) {
                                        for (var resultKey in data.result) {
                                            domId = resultKey;
                                            value = data.result[resultKey];
                                            kmcore.fastAnimate(domId, value);
                                        }
                                    }
                                });
                                kmcore.api.inbox.collectRequest(value.id);
                            }
                            else
                            {
                                if (kmcore.pageName == 'message') {
                                    var dialogId = value.attributes.dialogId;
                                    $("#" + dialogId).addClass("new-message");
                                    $("#" + dialogId).find(".content-description").html(value.attributes.text);
                                }

                                if (value.attributes.payload.hasOwnProperty("photo") && value.attributes.payload.photo) {
                                    var photo = '<img class="search-avatar"  alt="" src="' + value.attributes.payload.photo + '">';
                                }
                                else {
                                    var photo = '';
                                }

                                var id = value.id;
                                var text = value.attributes.text;
                                var dialogId = value.attributes.dialogId;
                                var first_name = value.attributes.payload.first_name;
                                var last_name = value.attributes.payload.last_name;
                                var userId = value.attributes.userId;
                                var html = '';

                                if ($('#' + id).length == 0) {

                                }
                                else {
                                    html += '<li id="' + id + '">';
                                    html += '<div class="row">';
                                    html += '<div class="img">';
                                    html += '<a href="#">';
                                    html += '<div class="search-avatar avatar-user">' + photo + '</div>';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="noty-body">';
                                    html += '<table>';
                                    html += '<tr>';
                                    html += '<td class="noty-content">';
                                    html += '<p><h3>' + first_name + ' ' + last_name + '</h3></p>';
                                    html += '<p>' + text + '</p>';
                                    html += '<a href="/message/' + dialogId + '" class="toTranslate">_t_details_t_</a>';
                                    html += '</td>';
                                    html += '</tr>';
                                    html += '</table>';
                                    html += '</div>';
                                    html += '<div class="close close-btn" onclick="lockedIds.push(\'' + value.id + '\');kmcore.api.inbox.collectRequest(\'' + value.id + '\', function(){});"><i class="icons"></i></div>';
                                    html += '</div>';
                                    html += '<div class="row">';
                                    //							html += '<a href="#" onclick="kmcore.api.inbox.collectRequest(\''+value.id+'\', function(){location.reload();});" class="button green close-btn">Прочитано</a>';
                                    html += '</div>';
                                    html += '</li>';
                                    $("#inbox_requests").append(html);

                                }
                                countMessages++;
                            }
                        }
                        else {
                            if (value.type == 'invite_confirm') {
                                var html = '';

                                if (value.attributes.payload.hasOwnProperty("photo") && value.attributes.payload.photo) {
                                    var photo = '<div class="search-avatar avatar-user"><img class="search-avatar"  alt="" src="' + value.attributes.payload.photo + '"></div>';
                                }
                                else {
                                    var photo = '<img src="/resource/mypet/assets/img/noty.png" alt=""/>';
                                }

                                var id = value.id;
                                var first_name = value.attributes.payload.first_name;
                                var last_name = value.attributes.payload.last_name;
                                var text = mypet_translate.translateByKey("_t_" + value.type + "_t_");
                                text = sprintf(text, value.attributes.payload);
                                if ($('#' + id).length) {

                                }
                                else {
                                    html += '<li id="' + id + '">';
                                    html += '<div class="row">';
                                    html += '<div class="img">';
                                    html += '<a href="#">';
                                    html += photo;
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="noty-body">';
                                    html += '<table>';
                                    html += '<tr>';
                                    html += '<td class="noty-content">';
                                    html += '<p>' + text + '</p>';
                                    //								html += '<a href="/message/'+dialogId+'" class="toTranslate">_t_details_t_</a>';
                                    html += '</td>';
                                    html += '</tr>';
                                    html += '</table>';
                                    html += '</div>';
                                    html += '<div class="close close-btn"  onclick="lockedIds.push(\'' + value.id + '\');kmcore.api.inbox.removeRequest(\'' + value.id + '\', function(){});"><i class="icons"></i></div>';
                                    html += '</div>';
                                    html += '<div class="row">';
                                    html += '<a href="#" onclick="kmcore.api.inbox.collectRequest(\'' + value.id + '\', function(){location.reload();});" class="button green close-btn toTranslate">_t_confirm_t_</a>';
                                    html += '<a href="#" onclick="kmcore.api.inbox.removeRequest(\'' + value.id + '\', function(){location.reload();});" class="button grey close-btn toTranslate">_t_reject_t_</a>';
                                    html += '</div>';
                                    html += '</li>';
                                    $("#inbox_requests").append(html);
                                }
                            }
                            else if (value.type == 'ancestry_rem_relation_confirm' || value.type == 'ancestry_add_relation_confirm') {
                                var html = '';

                                if (value.attributes.payload.user.hasOwnProperty("photo") && value.attributes.payload.user.photo) {
                                    var photo = '<div class="search-avatar avatar-user"><img class="search-avatar"  alt="" src="' + value.attributes.payload.user.photo + '"></div>';
                                }
                                else {
                                    var photo = '<img src="/resource/mypet/assets/img/noty.png" alt=""/>';
                                }

                                var id = value.id;
                                var text = mypet_translate.translateByKey("_t_" + value.type + "_t_");

                                var petId = value.attributes.payload.pet.id;
                                var petRelationId = value.attributes.payload.petRelation.id;
                                if (value.attributes.payload.pet.hasOwnProperty("gag")) {
                                    value.attributes.payload.pet.href = '#';
                                }
                                else {
                                    value.attributes.payload.pet.href = '/pet/' + petId;
                                }

                                if (value.attributes.payload.petRelation.hasOwnProperty("gag")) {
                                    value.attributes.payload.petRelation.href = '#';
                                }
                                else {
                                    value.attributes.payload.petRelation.href = '/pet/' + petRelationId;
                                }

                                text = sprintf(text, value.attributes.payload);
                                if ($('#' + id).length) {

                                }
                                else {
                                    html += '<li id="' + id + '">';
                                    html += '<div class="row">';
                                    html += '<div class="img">';
                                    html += '<a href="#">';
                                    html += photo;
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="noty-body">';
                                    html += '<table>';
                                    html += '<tr>';
                                    html += '<td class="noty-content">';
                                    html += '<p>' + text + '</p>';
                                    //								html += '<a href="/message/'+dialogId+'" class="toTranslate">_t_details_t_</a>';
                                    html += '</td>';
                                    html += '</tr>';
                                    html += '</table>';
                                    html += '</div>';
                                    html += '<div class="close close-btn" onclick="lockedIds.push(\'' + value.id + '\');"><i class="icons"></i></div>';
                                    html += '</div>';
                                    html += '<div class="row">';
                                    html += '<a href="#" onclick="kmcore.api.inbox.collectRequest(\'' + value.id + '\', function(){location.reload();});" class="button green close-btn toTranslate">_t_confirm_t_</a>';
                                    html += '<a href="#" onclick="kmcore.api.inbox.removeRequest(\'' + value.id + '\', function(){location.reload();});" class="button grey close-btn toTranslate">_t_reject_t_</a>';
                                    html += '</div>';
                                    html += '</li>';
                                    $("#inbox_requests").append(html);
                                }
                            }

                            countInbox++
                        }

	                    if (!kmcore.inArray(value.id, sounds))
	                    {
		                    audio.play();
		                    sounds.push(value.id);
	                    }

                        mypet_translate.findAndTranslate();
                    }
                }

                if (countMessages > 0) {
                    $("#letters_count").show().html(countMessages);
	                document.title = '('+countMessages+') '+globtitle;
                }
                else {
                    $("#letters_count").hide();
	                document.title = globtitle;
                }

                if (countInbox > 0) {
                    $("#inbox_count").show().html(countInbox);
                }
                else {
                    $("#inbox_count").hide();
                }

                var noty = $(".noty");
                var notyLi = noty.find("li");
                notyLi.find(".close-btn").click(function () {
                    var p = $(this).parents("li");
                    p.slideUp({
                        duration: 300,
                        easing: "easeInOutQuad",
                        complete: function () {
                            p.remove();
                        }
                    });
                });
            }

            if (typeof callback == 'function') {
                callback(data);
            }
        });
    }
}
function mpRegistration(form) {
	var id = form.id;

	if (id) {
		var params = {}
		params.email = $("#" + id + " input[name=email]").val();
		params.last_name = $("#" + id + " input[name=last_name]").val();
		params.first_name = $("#" + id + " input[name=first_name]").val();
		params.password = $("#" + id + " input[name=password]").val();
		params.password2 = $("#" + id + " input[name=password2]").val();

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#section_form_box_dialog").empty();
					$("#section_form_box_dialog").html(data.message);
					$("#section_form_box_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#section_form_box_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#section_form_box_dialog").empty()
							});
						}, 5000)
					});
				}

				//kmcore.components.closeComponent('section_form_box');
				//kmcore.page.load('user', function (status) {
				//}, function () {
				//	loadResize();
				//	updateStyler();
				//});
				//kmcore.loggedIn = true;

                kmcore.api.send("/clientApi/updateProgress", {}, function(data){window.location.replace("/user");});
				//window.location.replace("/user");
			}
			else {
				$("#section_form_box_dialog").empty();
				$("#section_form_box_dialog").html(data.message);
				$("#section_form_box_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#section_form_box_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#section_form_box_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}


function mpLogin(form) {
	var id = form.id;

	if (id) {
		var params = {}
		params.email = $("#" + id + " input[name=email]").val();
		params.password = $("#" + id + " input[name=password]").val();
//		params.remember = $("#" + id + " input[name=remember] :checked").val();

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#section_form_box_dialog").empty();
					$("#section_form_box_dialog").html(data.message);
					$("#section_form_box_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#section_form_box_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#section_form_box_dialog").empty()
							});
						}, 5000)
					});
				}

				kmcore.components.closeComponent('section_form_box');
				kmcore.page.load('user', function (status) {
				}, function () {
					loadResize();
					updateStyler();
				});
				kmcore.loggedIn = true;
				window.location.replace("/user");
			}
			else {
				$("#section_form_box_dialog").empty();
				$("#section_form_box_dialog").html(data.message);
				$("#section_form_box_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#section_form_box_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#section_form_box_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function mpLogout() {
	var params = {}
	kmcore.send("/userApi/logout", params, function (data) {
		data = data.response;
		if (data.success) {
			if (data.message) {
				$("#section_form_box_dialog").empty();
				$("#section_form_box_dialog").html(data.message);
				$("#section_form_box_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#section_form_box_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#section_form_box_dialog").empty()
						});
					}, 5000)
				});
			}

			kmcore.components.closeComponent('section_content');
			kmcore.page.load('login', function (status) {
			}, function () {
				loadResize();
				updateStyler();
			});
			kmcore.loggedIn = false;
			window.location.replace("/");
		}
		else {
			$("#section_form_box_dialog").empty();
			$("#section_form_box_dialog").html(data.message);
			$("#section_form_box_dialog").animate({
				opacity: 1
			}, 500, 'swing', function () {
				setTimeout(function () {
					$("#section_form_box_dialog").animate({
						opacity: 0
					}, 500, 'swing', function () {
						$("#section_form_box_dialog").empty()
					});
				}, 5000)
			});
		}
	});
}

function mpEditProfile(form) {
	var id = form.id;

	if (id) {
		var params = {}
		params.first_name = $("#" + id + " input[name=first_name]").val();
		params.email = $("#" + id + " input[name=email]").val();
        params.gender = $("#" + id + " select[name=gender] :selected").val();
        params.birthday = $("#" + id + " input[name=birthday]").val();
		params.password = $("#" + id + " input[name=password]").val();
		params.password2 = $("#" + id + " input[name=password2]").val();
		params.last_name = $("#" + id + " input[name=last_name]").val();

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#popup_settings_dialog").empty();
					$("#popup_settings_dialog").removeClass("error-msg");
					$("#popup_settings_dialog").addClass("success-msg");
					$("#popup_settings_dialog").html(data.message);
					$("#popup_settings_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#popup_settings_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#popup_settings_dialog").empty()
							});
						}, 5000)
					});
				}

                kmcore.api.send("/clientApi/updateProgress", {}, function(data){location.reload();});
//				openComponent("mp_container_pet_page_avatar");
//				location.reload();
			}
			else {
				$("#popup_settings_dialog").empty();
				$("#popup_settings_dialog").removeClass("success-msg");
				$("#popup_settings_dialog").addClass("error-msg");
				$("#popup_settings_dialog").html(data.message);
				$("#popup_settings_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#popup_settings_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#popup_settings_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function mpRecoverPassword(form) {
	var id = form.id;

	if (id) {
		var params = {}
		params.email = $("#" + id + " input[name=email]").val();
		params.langName = $("#" + id + " input[name=langName]").val();

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#section_form_box_dialog").empty();
					$("#section_form_box_dialog").html(data.message);
					$("#section_form_box_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#section_form_box_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#section_form_box_dialog").empty()
							});
						}, 5000)
					});
				}

//				kmcore.components.closeComponent('section_form_box_dialog');
			}
			else
			{
				$("#section_form_box_dialog").empty();
				$("#section_form_box_dialog").html(data.message);
				$("#section_form_box_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#section_form_box_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#section_form_box_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

$(window).bind('hashchange', function () {
	kmcore.hash.component(function (status) {
	}, function () {
		loadResize();
		updateStyler();
	});
});

var langDatepicker = 'uk';
function updateStyler()
{
	$('input, select').styler();
	$("form").find(".dateForm").each(function()
	{
		if($(this).hasClass( "birthday" ))
		{
			$(this).datepicker({
				showButtonPanel: true,
				changeMonth: true,
				changeYear: true,
				format: "dd/mm/yyyy",
				language: langDatepicker,
				endDate: '+0d'
			});
		}
		else
		{
			$(this).datepicker({
				showButtonPanel: true,
				changeMonth: true,
				changeYear: true,
				format: "dd/mm/yyyy",
				language: langDatepicker
			});
		}
	});

	$("form").find(".dateRangeFrom").each(function()
	{
		var from_date = $(this).find('.from_date');
		var to_date = $(this).find('.to_date');

		from_date.datepicker({
			endDate: '+0d',
			showButtonPanel: true,
			changeMonth: true,
			changeYear: true,
			format: "dd/mm/yyyy",
			language: langDatepicker
		})
			.on('changeDate', function(selected){
				var startDate = new Date(selected.date.valueOf());
				to_date.datepicker('setStartDate', startDate);
			});
		to_date
			.datepicker({
				endDate: '+0d',
				showButtonPanel: true,
				changeMonth: true,
				changeYear: true,
				format: "dd/mm/yyyy",
				language: langDatepicker
			})
			.on('changeDate', function(selected){
				var FromEndDate = new Date(selected.date.valueOf());
				from_date.datepicker('setEndDate', FromEndDate);
			});
	});

//	$('.from_date').datepicker({
//		endDate: '+0d',
//		showButtonPanel: true,
//		changeMonth: true,
//		changeYear: true,
//		format: "dd/mm/yyyy",
//		language: langDatepicker
//	})
//		.on('changeDate', function(selected){
//			var startDate = new Date(selected.date.valueOf());
//			$('.to_date').datepicker('setStartDate', startDate);
//		});
//	$('.to_date')
//		.datepicker({
//			endDate: '+0d',
//			showButtonPanel: true,
//			changeMonth: true,
//			changeYear: true,
//			format: "dd/mm/yyyy",
//			language: langDatepicker
//		})
//		.on('changeDate', function(selected){
//			var FromEndDate = new Date(selected.date.valueOf());
//			$('.from_date').datepicker('setEndDate', FromEndDate);
//		});

	jQuery.validator.addClassRules("dateForm", {
		australianDate: true
	});

	jQuery.validator.addClassRules("maxlength128", {
		maxlength: 128
	});
	jQuery.validator.addClassRules("maxlength248", {
		maxlength: 248
	});
}

function loadResize()
{
	//  Вирівнювання по центру ".sign-up" на строінках входу, зміни паролю
	var windowH = $(window).height(),
		formBox = $(".form-box"),
		centered = $(".sign-up, .login"),
		centeredH = centered.innerHeight();
	centered.css("margin-top", (centeredH/-2) + "px");
	if(windowH < centeredH + 300){
		formBox.addClass("form-box-css");
		formBox.height(centeredH);
	} else{
		formBox.removeClass("form-box-css");
	}
	setTimeout(function() {
		centered.animate({opacity: 1}, 2000);
	}, 300);


	$('.mp_popup_rules').click(function () {
		openPopup("mp_popup_rules", undefined, 990);
		return false;
	});

	$('.mp_section_login').click(function () {
		openComponent("mp_section_login", function (status) {}, function () {
			loadResize();
			$("input, select").styler();
		});
	});
	$('.mp_section_recover_password').click(function () {
		openComponent("mp_section_recover_password", function (status) {
		}, function () {
			loadResize();
			$("input, select").styler();
		});
	});
	$('.mp_section_registration').click(function () {
		openComponent("mp_section_registration", function (status) {
		}, function () {
			loadResize();
			$("input, select").styler();
		});
	});
}

function openPopup(component, query, popupWidth)
{
	width = 460;
	url = "/json/dynamicData?additionalComponents={\""+component+"\":1}&rendering=1";
	if(query !== undefined)
	{
		url = url+"&"+query;
	}
	if(popupWidth !== undefined)
	{
		width = popupWidth;
	}
	kmcore.send(url, [], function (data) {
		$.fancybox({
			content     : data.response.result.popup,
			maxWidth	: width,
			fitToView	: false,
			width		: width+'px',
			height		: 'auto',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'fade',
			closeEffect	: 'fade',
			padding     : 0,
			closeBtn    : true,
			wrapCSS     : "spinner",
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
		updateStyler();
	});
}

function mpChangeStatus(form)
{
	var id = form.id;

	if (id) {
		var params = {}
		params.petId = $("#" + id + " input[name=petId]").val();
		params.status = $("#" + id + " input[name=status]:checked").val();

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#popup_status_dialog").empty();
					$("#popup_status_dialog").removeClass("error-msg");
					$("#popup_status_dialog").addClass("success-msg");
					$("#popup_status_dialog").html(data.message);
					$("#popup_status_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#popup_status_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#popup_status_dialog").empty()
							});
						}, 5000)
					});
				}
				//				openComponent("mp_container_pet_page_avatar");
				location.reload();
			}
			else {
				$("#popup_status_dialog").empty();
				$("#popup_status_dialog").removeClass("success-msg");
				$("#popup_status_dialog").addClass("error-msg");
				$("#popup_status_dialog").html(data.message);
				$("#popup_status_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#popup_status_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#popup_status_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function mpEditUserData(form) {
	var id = form.id;

	if (id) {
		var params = {}
		var socLinks = [];
		var phone = [];
		var skills = [];
		params.birthday = $("#" + id + " input[name=birthday]").val();

		var valCountry = $("#country").val();
		if(valCountry)
		{
			params.user_country = $('#country_list [value="'+valCountry+'"]').attr('id');
		}
		else
		{
			params.user_country = '';
		}

		var valRegion = $("#region").val();
		if(valRegion)
		{
			params.region = $('#region_list [value="'+valRegion+'"]').attr('id');
		}
		else
		{
			params.region = '';
		}

		var valCity = $("#city").val();
		if(valCity)
		{
			params.city = $('#city_list [value="'+valCity+'"]').attr('id');
			if(!params.city)
			{
				params.city = valCity;
			}
		}
		else
		{
			params.city = '';
		}

//		params.city = $("#" + id + " input[name=city]").val();
//		params.phone = $("#" + id + " input[name=phone]").val();
		$('#' + id + ' input[name="phone[]"]').each(function() {
			if($(this).val())
			{
				phone.push($(this).val());
			}
		});
		params.phone = phone;

		$('#' + id + ' input[name="socLinks[]"]').each(function() {
			if($(this).val())
			{
				socLinks.push($(this).val());
			}
		});
		params.about = $("#" + id + " input[name=about]").val();
		params.profession = $("#" + id + " select[name=profession] :selected").val();
		$('#' + id + ' input[name="skills[]"]').each(function() {
			if($(this).val())
			{
				skills.push($(this).val());
			}
		});
//		params.works = $("#" + id + " input[name=works]").val();
//		$('#' + id + ' input[name="works"]').each(function() {
//			params.works.push($(this).val());
//		});
		isSpecialist = $("#" + id + " input[name=isSpecialist]").prop( "checked" );
		if(isSpecialist === true)
		{
			params.isProf = 'yes';
		}
		params.about = $("#" + id + " textarea[name=about]").val();
		params.gender = $("#" + id + " select[name=gender] :selected").val();

		params.skills = skills;
		params.socLinks = socLinks;

		var works = {
			"organization": [],
			"specialization": [],
			"career": [],
		};

		var result = {};
		$("div").find(".organizations").each(function(){
			var id = $(this).attr('id');
			var _id = $("#"+id+" input[name=_id]").val();
			var name = $("#"+id+" input[name=name]").val();
			var birthday_s = $("#"+id+" input[name=birthday_s]").val();
			var birthday_e = $("#"+id+" input[name=birthday_e]").val();
			if(name)
			{
				if(_id)
				{
					works.organization.push({"_id": _id, "name": name, "birthday_s": birthday_s, "birthday_e": birthday_e});
				}
				else
				{
					works.organization.push({"name": name, "birthday_s": birthday_s, "birthday_e": birthday_e});
				}
			}
		});

		$("div").find(".specializations").each(function(){
			var id = $(this).attr('id');
			var _id = $("#"+id+" input[name=_id]").val();
			var name = $("#"+id+" input[name=name]").val();
			var birthday_s = $("#"+id+" input[name=birthday_s]").val();
			var birthday_e = $("#"+id+" input[name=birthday_e]").val();

			var type = $("#"+id+" input[name=type]").val();
			var breed = $("#"+id+" input[name=breed]").val();

//			params.type = $("#" + id + " select[name=type] :selected").val();
//			var valBreed = $("#breed_"+id+"").val();
//			params.breed = $('#breed_list_'+id+' [value="'+valBreed+'"]').attr('id');

			if(name)
			{
				if(_id)
				{
					works.specialization.push({"_id": _id, "name": name, "birthday_s": birthday_s, "birthday_e": birthday_e});
				}
				else
				{
					works.specialization.push({"name": name, "birthday_s": birthday_s, "birthday_e": birthday_e});
				}
			}
		});

		$("div").find(".careers").each(function(){
			var id = $(this).attr('id');
			var _id = $("#"+id+" input[name=_id]").val();
			var name = $("#"+id+" input[name=name]").val();
			var birthday_s = $("#"+id+" input[name=birthday_s]").val();
			var birthday_e = $("#"+id+" input[name=birthday_e]").val();
			var position = $("#"+id+" input[name=position]").val();
			if(name)
			{
				if(_id)
				{
					works.career.push({"_id": _id, "name": name, "birthday_s": birthday_s, "birthday_e": birthday_e, "position": position});
				}
				else
				{
					works.career.push({"name": name, "birthday_s": birthday_s, "birthday_e": birthday_e, "position": position});
				}
			}
		});
		params.works = works;

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#responseEdit").empty();
					$("#responseEdit").html(data.message);
					$("#responseEdit").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#responseEdit").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#responseEdit").empty()
							});
						}, 5000)
					});
				}

                kmcore.api.send("/clientApi/updateProgress", {}, function(data){window.location.replace("/user");});
//				kmcore.components.closeComponent('section_form_box_dialog');
//				kmcore.page.load('user', function (status) {
//				}, function () {
//					loadResize();
//					updateStyler();
//				});
//				kmcore.loggedIn = true;
//				window.location.replace("/user");
			}
			else {
				$("#responseEdit").empty();
				$("#responseEdit").html(data.message);
				$("#responseEdit").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#responseEdit").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#responseEdit").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function mpEditPetData(form) {
	var id = form.id;

	if (id) {
		var params = {};
		var owners = [];
		var vet = [];
		var groomer = [];
		var handler = [];
		var trainer = [];
		var other = [];
		var psychologist = [];
		var photographer = [];
		var felinologist = [];
		var cynologist = [];
		var breeder = [];
		params.id = $("#" + id + " input[name=petId]").val();
		params.hNick = $("#" + id + " input[name=hNick]").val();
		params.type = $("#" + id + " select[name=type] :selected").val();

		var valBreed = $("#breed").val();
		params.breed = $('#breed_list [value="'+valBreed+'"]').attr('id');

		params.birthday = $("#" + id + " input[name=birthday]").val();
		params.deathday = $("#" + id + " input[name=deathday]").val();

		var valCountry = $("#country").val();
		if(valCountry)
		{
			params.country = $('#country_list [value="'+valCountry+'"]').attr('id');
		}
		else
		{
			params.country = '';
		}

		var valRegion = $("#region").val();
		if(valRegion)
		{
			params.region = $('#region_list [value="'+valRegion+'"]').attr('id');
		}
		else
		{
			params.region = '';
		}

		var valCity = $("#city").val();
		if(valCity)
		{
			params.city = $('#city_list [value="'+valCity+'"]').attr('id');
			if(!params.city)
			{
				params.city = valCity;
			}
		}
		else
		{
			params.city = '';
		}

//		params.city = $("#" + id + " input[name=city]").val();
		params.signs = $("#" + id + " textarea[name=signs]").val();
		params.marks = $("#" + id + " input[name=marks]").val();
		params.gender = $("#" + id + " select[name=gender] :selected").val();

		params.nick = $("#" + id + " input[name=nick]").val();
		params.ancestry_code = $("#" + id + " input[name=ancestry_code]").val();
		params.chip = $("#" + id + " input[name=chip]").val();
		params.passport = $("#" + id + " input[name=passport]").val();
		params.color = $("#" + id + " input[name=color]").val();
		params.hair = $("#" + id + " input[name=hair]").val();
		params.size = $("#" + id + " input[name=size]").val();
		params.weight = $("#" + id + " input[name=weight]").val();
		params.about = $("#" + id + " textarea[name=about]").val();

		$('#' + id + ' input[name="owners[]"]').each(function() {
			if($(this).val())
			{
				owners.push($(this).val());
			}
		});
		params.owners = owners;

		params.ringleader = $("#" + id + " input[name=ringleader]").val();

		$('#' + id + ' input[name="vet[]"]').each(function() {
			if($(this).val())
			{
				vet.push($(this).val());
			}
		});
		params.vet = vet;

		$('#' + id + ' input[name="groomer[]"]').each(function() {
			if($(this).val())
			{
				groomer.push($(this).val());
			}
		});
		params.groomer = groomer;

		$('#' + id + ' input[name="handler[]"]').each(function() {
			if($(this).val())
			{
				handler.push($(this).val());
			}
		});
		params.handler = handler;

		$('#' + id + ' input[name="trainer[]"]').each(function() {
			if($(this).val())
			{
				trainer.push($(this).val());
			}
		});
		params.trainer = trainer;

		$('#' + id + ' input[name="other[]"]').each(function() {
			if($(this).val())
			{
				other.push($(this).val());
			}
		});
		params.other = other;

		$('#' + id + ' input[name="psychologist[]"]').each(function() {
			if($(this).val())
			{
				psychologist.push($(this).val());
			}
		});
		params.psychologist = psychologist;

		$('#' + id + ' input[name="photographer[]"]').each(function() {
			if($(this).val())
			{
				photographer.push($(this).val());
			}
		});
		params.photographer = photographer;

		$('#' + id + ' input[name="felinologist[]"]').each(function() {
			if($(this).val())
			{
				felinologist.push($(this).val());
			}
		});
		params.felinologist = felinologist;

		$('#' + id + ' input[name="cynologist[]"]').each(function() {
			if($(this).val())
			{
				cynologist.push($(this).val());
			}
		});
		params.cynologist = cynologist;

		$('#' + id + ' input[name="breeder[]"]').each(function() {
			if($(this).val())
			{
				breeder.push($(this).val());
			}
		});
		params.breeder = breeder;

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#responseEdit").empty();
					$("#responseEdit").html(data.message);
					$("#responseEdit").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#responseEdit").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#responseEdit").empty()
							});
						}, 5000)
					});
				}

                kmcore.api.send("/clientApi/updateProgress", {}, function(data){window.location.replace("/pet/"+petId);});
//				kmcore.components.closeComponent('section_form_box_dialog');
//				kmcore.page.load('user', function (status) {
//				}, function () {
//					loadResize();
//					updateStyler();
//				});
//				kmcore.loggedIn = true;
//				window.location.replace("/pet/"+petId);
			}
			else {
				$("#responseEdit").empty();
				$("#responseEdit").html(data.message);
				$("#responseEdit").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#responseEdit").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#responseEdit").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function mpAddNewOwner(form) {
	var id = form.id;

	if (id) {
		var params = {}
		params.petId = $("#" + id + " input[name=petId]").val();
		params.ownerId = $("#" + id + " input[name=ownerId]").val();

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#popup_status_dialog").empty();
					$("#popup_status_dialog").removeClass("error-msg");
					$("#popup_status_dialog").addClass("success-msg");
					$("#popup_status_dialog").html(data.message);
					$("#popup_status_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#popup_status_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#popup_status_dialog").empty()
							});
						}, 5000)
					});
				}
				//				openComponent("mp_container_pet_page_avatar");
				location.reload();
			}
			else {
				$("#popup_status_dialog").empty();
				$("#popup_status_dialog").removeClass("success-msg");
				$("#popup_status_dialog").addClass("error-msg");
				$("#popup_status_dialog").html(data.message);
				$("#popup_status_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#popup_status_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#popup_status_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function mpInviteNewOwner()
{
	var id = form.id;

	if (id) {
		var params = {}
		params.email = $("#" + id + " input[name=email]").val();

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#popup_status_dialog").empty();
					$("#popup_status_dialog").removeClass("error-msg");
					$("#popup_status_dialog").addClass("success-msg");
					$("#popup_status_dialog").html(data.message);
					$("#popup_status_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#popup_status_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#popup_status_dialog").empty()
							});
						}, 5000)
					});
				}
				//				openComponent("mp_container_pet_page_avatar");
				location.reload();
			}
			else {
				$("#popup_status_dialog").empty();
				$("#popup_status_dialog").removeClass("success-msg");
				$("#popup_status_dialog").addClass("error-msg");
				$("#popup_status_dialog").html(data.message);
				$("#popup_status_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#popup_status_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#popup_status_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function mpAddNewPet(form) {
	var id = form.id;

	if (id) {
		var params = {}
		params.hNick = $("#" + id + " input[name=hNick]").val();
		params.type = $("#" + id + " select[name=type] :selected").val();
		params.gender = $("#" + id + " select[name=gender] :selected").val();
		params.birthday = $("#" + id + " input[name=birthday]").val();

		var valBreed = $("#breed_popup").val();
		params.breed = $('#breed_list_popup [value="'+valBreed+'"]').attr('id');


		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#popup_status_dialog").empty();
					$("#popup_status_dialog").removeClass("error-msg");
					$("#popup_status_dialog").addClass("success-msg");
					$("#popup_status_dialog").html(data.message);
					$("#popup_status_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#popup_status_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#popup_status_dialog").empty()
							});
						}, 5000)
					});
				}

                kmcore.api.send("/clientApi/updateProgress", {}, function(data){location.reload();});
				//				openComponent("mp_container_pet_page_avatar");
				//location.reload();
			}
			else {
				$("#popup_status_dialog").empty();
				$("#popup_status_dialog").removeClass("success-msg");
				$("#popup_status_dialog").addClass("error-msg");
				$("#popup_status_dialog").html(data.message);
				$("#popup_status_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#popup_status_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#popup_status_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function selectRegions(country)
{
	var params = {}
	params.country = country;
	kmcore.send("/location/getRegionsByCountry", params, function (data) {
		var langName = kmcore.langName;
		$("#listRegion").empty();

		$("#listRegion").append('<input type="text" id="region" name="input_region" list="region_list">');
		$("#listRegion").append('<datalist name="region" id="region_list"></datalist>');
		$("#region_list").append('<option value="" class="toTranslate">'+mypet_translate.translateByKey("_t_not_selected_t_")+'</option>');
		for(key in data.response.regions)
		{
			var item = data.response.regions[key];
			if(item.hasOwnProperty(langName))
			{
				var title = item[langName];
			}
			else
			{
				var title = item.en;
			}

			$("#region_list").append('<option value="'+title+'" id="'+item._id+'"></option>');
		}
		$("#region_list").styler();
//		mypet_translate.findAndTranslate();
	});
}

function selectCities(region)
{
	var params = {}
	params.region = region;
	kmcore.send("/location/getCitiesByRegion", params, function (data) {
		var langName = kmcore.langName;
		$("#listCity").empty();

		$("#listCity").append('<input type="text" id="city" name="input_city" list="city_list">');
		$("#listCity").append('<datalist name="city" id="city_list"></datalist>');
		$("#city_list").append('<option value="" class="toTranslate">'+mypet_translate.translateByKey("_t_not_selected_t_")+'</option>');

		for(key in data.response.cities)
		{
			var item = data.response.cities[key];
			if(item.hasOwnProperty(langName))
			{
				var title = item[langName];
			}
			else
			{
				var title = item.en;
			}

			$("#city_list").append('<option value="'+title+'" id="'+item._id+'"></option>');
		}
		$("#city_list").styler();
//		mypet_translate.findAndTranslate();
	});
}

function selectBreeds(type, listBreed, breed_list, breed)
{
	var params = {}
	params.type = type;
	kmcore.send("/petApi/getBreedsByType", params, function (data) {
		var langName = kmcore.langName;
		$("#"+listBreed).empty();

		$("#"+listBreed).append('<input type="text" id="'+breed+'" list="'+breed_list+'">');
		$("#"+listBreed).append('<datalist id="'+breed_list+'"></datalist>');
		$("#"+breed_list).append('<option value="" class="toTranslate">'+mypet_translate.translateByKey("_t_not_selected_t_")+'</option>');

		for(key in data.response.breeds)
		{
			var item = data.response.breeds[key];
			if(item.hasOwnProperty(langName))
			{
				var title = item[langName];
			}
			else
			{
				var title = item.en;
			}

			$("#"+breed_list).append('<option value="'+title+'" id="'+item._id+'"></option>');
		}
		$("#"+breed_list).styler();
//		mypet_translate.findAndTranslate();
	});
}
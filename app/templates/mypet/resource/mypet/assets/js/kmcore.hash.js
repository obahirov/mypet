kmcore.hash = {
	component: function (callbackAfter, callbackBefore) {
		hash = window.location.hash;
		hash = hash.substring(1, hash.length);
		if (kmcore.strpos(hash, kmcore.prefix) !== false && hash.length > 0) {
			if (kmcore.animateLock == false) {
				kmcore.animateLock = true;
				kmcore.components.loadComponent(hash, function (status) {
					if (typeof callbackAfter == 'function') {
						kmcore.animateLock = false;
						window.location.hash = '';
						callbackAfter(status);
					}
				}, function (status) {
					if (typeof callbackBefore == 'function') {
						callbackBefore();
					}
				});  //status - fail
			}
		}
	},
	page: function (callbackAfter, callbackBefore) {
		hash = window.location.hash;
		hash = hash.substring(1, hash.length);
		if (hash.length > 0) {
			if (kmcore.animateLock == false) {
				kmcore.animateLock = true;
				kmcore.page.load(hash, function (status) {
					if (typeof callbackAfter == 'function') {
						kmcore.animateLock = false;
						window.location.hash = '';
						callbackAfter(status);
					}
				}, function () {
					if (typeof callbackBefore == 'function') {
						callbackBefore();
					}
				});  //status - fail
			}
		}
	}
}
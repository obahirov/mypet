/**
 * @author Maxim Tkach
 */
kmcore.api.friend = {
	addFriend: function(friendId, callback)
	{
			kmcore.api.send("/friendApi/add", {"friendId": friendId}, function(data){
				if (typeof callback == 'function') {
					callback(data);
				}
				if(data.success)
				{
					$("#action_buttons").empty().html('<a href="#" class="button green profile-message-send"><span><i class="icons"></i></span></a><a href="#" onclick=\'kmcore.api.friend.deleteFriend("'+friendId+'")\' class="button grey friend-status friend"><!-- friend or add or man --><span><i class="icons status-page"></i><i class="icons man"></i></span></a>');
				}
		});
	},
	deleteFriend: function(friendId, callback)
	{
			kmcore.api.send("/friendApi/delete", {"friendId": friendId}, function(data){
				if (typeof callback == 'function') {
					callback(data);
				}
				if(data.success)
				{
					$("#action_buttons").empty().html('<a href="#" class="button green profile-message-send"><span><i class="icons"></i></span></a><a href="#" onclick=\'kmcore.api.friend.addFriend("'+friendId+'")\' class="button grey friend-status add"><!-- friend or add or man --><span><i class="icons status-page"></i><i class="icons man"></i></span></a>');
				}
			});
	},
	confimFriend: function(friendId, callback)
	{
			kmcore.api.send("/friendApi/add", {"friendId": friendId}, function(data){
				if (typeof callback == 'function') {
					callback(data);
				}
				if(data.success)
				{
					$("#action_buttons").empty().html('<a href="#" class="button green profile-message-send"><span><i class="icons"></i></span></a><a href="#" onclick=\'kmcore.api.friend.deleteFriend("'+friendId+'")\' class="button grey friend-status friend"><!-- friend or add or man --><span><i class="icons status-page"></i><i class="icons man"></i></span></a>');
				}
			});
	}
}
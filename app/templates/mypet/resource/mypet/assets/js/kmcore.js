/**
 * @author Maxim Tkach
 */
var kmcore = {
	prefix: 'mp_',
	pageToLoadComponent: 'dynamicData',
	lock: false,
	send: function (url, params, functionSuccess) {
		if(kmcore.lock == false)
		{
			kmcore.lock = true;
			$.ajax({
				type: "POST",
				url: url,
				data: params,
				success: function(data)
				{
					kmcore.lock = false;
					if (typeof functionSuccess == 'function') {
						functionSuccess(data);
					}
				}
			});
		}
	},
	domAnimate: function (domId, value, callbackAfter, callbackBefore) {
		$("#" + domId + "").animate({
			opacity: 0
		}, 500, 'swing', function () {
			$("#" + domId + "").empty();
			$("#" + domId + "").html(value);

			if (typeof callbackBefore == 'function') {
				callbackBefore();
			}

			$("#" + domId + "").animate({
				opacity: 1
			}, 500, 'swing', function () {
				if (typeof callbackAfter == 'function') {
					callbackAfter(0);
				}
			});
		});
	},
	fastAnimate: function (domId, value, callbackAfter, callbackBefore) {
		$("#" + domId + "").animate({
			opacity: 0
		}, 0, 'swing', function () {
			$("#" + domId + "").empty();
			$("#" + domId + "").html(value);

			if (typeof callbackBefore == 'function') {
				callbackBefore();
			}

			$("#" + domId + "").animate({
				opacity: 1
			}, 0, 'swing', function () {
				if (typeof callbackAfter == 'function') {
					callbackAfter(0);
				}
			});
		});
	},
	strpos: function ( haystack, needle){	// Find position of first occurrence of a string
		var i = haystack.indexOf( needle, 0 ); // returns -1
		return i >= 0 ? i : false;
	},
    inArray: function (needle, haystack) {
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
            if(haystack[i] == needle) return true;
        }
        return false;
    },
	animateLock: false,
	rendering: 1,
	pageName: '',
	langName: '',
	loggedIn: false
}
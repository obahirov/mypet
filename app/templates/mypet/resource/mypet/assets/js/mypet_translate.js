var translate_data = {
	_t__t_: '',
	_t_details_t_: 'Read more',
	_t_connection_developers_t_: 'Feedback',
	_t_rules_t_: 'Terms',
	_t_is_dead_t_: 'Over the Rainbow Bridge',

	_t_looking_for_a_partner_t_: 'Looking for a mate',
	_t_need_help_t_: 'Need Help',
	_t_looking_owner_t_: 'Looking for owner',
	_t_waiting_for_offspring_t_: 'Expecting an offspring',
	_t_lost_t_: 'Lost',
	_t_none_t_: 'No status',

	_t_family_t_: 'Family',
	_t_friends_and_specialist_t_: 'Circles',
	_t_specialists_t_: 'Specialists',

	_t_not_selected_t_: 'Not selected',
	_t_cat_t_: 'Cat',
	_t_dog_t_: 'Dog',

	_t_gender_t_: 'Sex',
	_t_he_t_: 'Boy',
	_t_she_t_: 'Girl',

	_t_man_t_: 'Man',
	_t_woman_t_: 'Woman',

	_p__p_: '',
	_p_he_p_: 'Boy',
	_p_she_p_: 'Girl',

	_t_people_t_: 'People',
	_t_pet_t_: 'Pets',

	_t_organization_t_: 'Organization',
	_t_career_t_: 'Career',

	_t_type_t_: 'Type',
	_t_breed_t_: 'Breed',

	_t_other_t_: 'Other',
	_t_handler_t_: 'Handler',
	_t_trainer_t_: 'Trainer',
	_t_groomer_t_: 'Groomer',
	_t_vet_t_: 'Vet',
	_t_psychologist_t_: 'Psychologist',
	_t_photographer_t_: 'Photographer',
	_t_felinologist_t_: 'Felinologist',
	_t_cynologist_t_: 'Сynologist',
	_t_breeder_t_: 'Breeder',

	_t_datalistCountry_t_: 'Select the country',
	_t_datalistRegion_t_: 'Select the region',
	_t_australianDate_t_: 'Please enter a date in the format dd/mm/yyyy',
	_t_ancestry_add_relation_confirm_t_: 'User <a href="/user/%(user.id)s">%(user.first_name)s %(user.last_name)s</a> adds your pet <a href="%(petRelation.href)s">%(petRelation.href)s</a> to his pet\'s <a href="%(pet.href)s">%(pet.hNick)s</a> Pedigree chart',
	_t_ancestry_rem_relation_confirm_t_: 'User wants to break the link',
	_t_confirm_t_: 'Accept',
	_t_cancel_t_: 'Cancel',
	_t_yes_t_: 'Yes',
	_t_no_t_: 'No',

	_t_name_t_: 'Name',
	_t_work_place_t_: 'Work place',
	_t_position_t_: 'Position held',
	_t_period_t_: 'Period',
	_t_general_profession_t_: 'Main occupation',
	_t_skills_t_: 'Skills',
	_t_add_more_t_: 'Add more',
	_t_from_t_: 'from',
	_t_on_t_: 'to',

	_t_card_pet_t_: 'Pet’s card',
	_t_card_gag_favorite_t_: 'Temporary card',
	_t_owner_t_: 'Owner',
	_t_ringleader_t_: 'Breeder',
	_t_signs_t_: 'Titles and awards',
	_t_card_delete_t_: 'Delete card',
	_t_link_delete_t_: 'Remove link',
	_t_edit_card_t_: 'Edit card',

	_t_owners_t_: 'Owners',
	_t_pets_t_: 'Pets',

	_t_invite_confirm_t_: 'User <a href="/user/%(id)s">%(first_name)s %(last_name)s</a> adds you to Circles',

	_t_reject_t_: 'Reject',

	_t_father_t_: 'Father',
	_t_mother_t_: 'Mother',
	_t_brother_t_: 'Brother',
	_t_sister_t_: 'Sister',
	_t_daughter_t_: 'Daughter',
	_t_son_t_: 'Son',
	_t_brother_or_sister_t_: 'Brother or sister',
	_t_child_t_: 'Child',
	_t_blog_t_: 'Blog',
	_t_remove_me_t_: 'Remove me',
	_t_remove_my_pet_t_: 'Remove my pet',

	_t_cool_t_: 'Cool!',
	_t_send_t_: 'Send',
	_t_add_comment_t_: 'Add comment',
	_t_wassup_t_:'What\'s up?',
	_t_your_comment_t_:'Your comment',
	_t_share_post_t_: 'Share',
	_t_question_about_profesion_t_:'Your work is associated with pets',
	_t_specialization_t_:'Specialization',
	_t_main_information_t_:'Main information',
	_t_organizations_t_:'Organizations',
	_t_location_t_:'Location',
	_t_website_t_:'Website',
	_t_phone_number_t_:'Phone number',
	_t_tags_t_:'Tags',
	_t_work_place_palceholder_t_:'eg, Kennel "4 paws"',
	_t_tags_palceholder_t_:'enter your interests. eg, dogs, labrador, grooming, exhibitions, agility, veterinary',
	_t_website_palceholder_t_:'www.petspetspets.com',
	_t_organizations_palceholder_t_:'eg., КСУ, FCI',
	_t_edit_information_t_:'Edit information',
    _t_view_information_t_:'View information',
    _t_save_t_:'Save',
    _t_save_and_t_:'Save and',
    _t_im_not_a_specialist_t_:'I\'m not a specialist',
    _t_im_a_specialist_t_:'I\'m a specialist',
    _t_foto_t_:'Foto',
    _t_add_t_:'Add',
    _t_edit_t_:'Edit',
    _t_view_t_:'View',
    _t_photo_t_: 'Photo',


    _t_comment_photo_t_:'User <a href="/user/%(id)s">%(first_name)s %(last_name)s</a> send comment: "%(body)s"',
    _t_like_photo_t_:'User <a href="/user/%(id)s">%(first_name)s %(last_name)s</a> like your photo',
    _t_comment_post_t_:'User <a href="/user/%(id)s">%(first_name)s %(last_name)s</a> send comment: "%(body)s"',
    _t_like_post_t_:'User <a href="/user/%(id)s">%(first_name)s %(last_name)s</a> like your post',

	_f_user_fields_f_: 'User page',
	_f_pet_fields_f_: 'Pet page',
	_f_first_name_f_: 'First Name',
	_f_last_name_f_: 'Last Name',
	_f_phones_f_: 'Phone',
	_f_sites_f_: 'Site',
	_f_profession_f_: 'Profession',
	_f_hNick_f_: 'Nick',
	_f_photo_f_: 'Avatar',
	_f_location_f_:'Location',
	_f_gender_f_: 'Sex',
	_f_tags_f_:'Tags',
	_f_career_f_: 'Career',
	_f_breed_f_: 'Breed'
};
var mypet_translate = {
	findAndTranslate: function(){
		if(typeof translate_data != undefined)
		{
			for(key in translate_data)
			{
				$(document).find(".toTranslate").each(function(){
					var html = $(this).html();
					html = html.replace(new RegExp(key, 'g'), translate_data[key]);
					$(this).html(html);
				})
			}
		}
	},
	translateByKey: function(key)
	{
		if(typeof translate_data != undefined && translate_data.hasOwnProperty(key))
		{
			return translate_data[key];
		}
		else
		{
			return key;
		}
	}
};
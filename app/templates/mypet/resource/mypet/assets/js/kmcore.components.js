/** @author Maxim Tkach */
kmcore.components = {
	loadComponent: function (component, callbackAfter, callbackBefore, query) {

		if (typeof query == undefined) {
			query = '';
		}
		else
		{
			query = '&'+query;
		}
		kmcore.send("/json/" + kmcore.pageToLoadComponent + "?additionalComponents={\"" + component + "\":1}&rendering=" + kmcore.rendering+query, [], function (data) {
			data = data.response;
			if (($.isArray(data.result) && data.result.length > 0) || !$.isArray(data.result)) {
				for (var resultKey in data.result) {
					domId = resultKey;
					value = data.result[resultKey];
					kmcore.domAnimate(domId, value, callbackAfter, callbackBefore);
					kmcore.components.positions[domId] = component;
				}
			} else {
				if (typeof callbackAfter == 'function') {
					callbackAfter(1);
				}
			}
		});
	},
	loadComponents: function (components, callbackAfter, callbackBefore) {
		kmcore.send("/json/" + kmcore.pageToLoadComponent + "?additionalComponents=" + JSON.stringify(components) + "&rendering=" + kmcore.rendering, [], function (data) {
			data = data.response;
			if (($.isArray(data.result) && data.result.length > 0) || !$.isArray(data.result)) {
				for (var resultKey in data.result) {
					domId = resultKey;
					value = data.result[resultKey];
					kmcore.domAnimate(domId, value, callbackAfter, callbackBefore);
				}
			} else {
				if (typeof callbackAfter == 'function') {
					callbackAfter(1);
				}
			}
		})
	},
	closeComponent: function (domId) {
		$("#" + domId + "").animate({
			opacity: 0
		}, 500, 'swing', function () {
			$("#" + domId + "").empty();
			delete kmcore.components.positions[domId];
		});
	},
	toggleComponent: function (domId, component) {
		if (kmcore.components.positions.hasOwnProperty(domId)) {
			$("#" + domId + "").animate({
				opacity: 0
			}, 500, 'swing', function () {
				$("#" + domId + "").empty();
				delete kmcore.components.positions[domId];
			});
		}
		else {
			openComponent(component);
		}
	},
	checkPosition: function (domId) {
		if (kmcore.components.positions.hasOwnProperty(domId)) {
			return kmcore.components.positions[domId];
		}
		else false
	},
	positions: {}
}
/**
 * @author Maxim Tkach
 */
var tokens = {
    mypet:{
        userId: '',
        publicPass: '',
        sessionkey: '',
        expire: ''
    },
    fb: {
        accessToken: '',
        expiresIn:'',
        signedRequest:'',
        userID:''
    }
};
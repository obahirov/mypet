/**
 * @author Maxim Tkach
 */
kmcore.api.message = {
	send: function(dialogId, text, callback, sendRequest)
	{
			kmcore.api.send("/messageApi/send", {"dialogId": dialogId, "text": text, "sendRequest": sendRequest}, function(data){
				if (typeof callback == 'function') {
					callback(data);
				}

				if(data.success)
				{
					kmcore.send("/json/" + kmcore.pageToLoadComponent + "?additionalComponents={\"mp_block_message\":1}&rendering=1&dialogId="+dialogId, [], function (data) {
						data = data.response;
						if (($.isArray(data.result) && data.result.length > 0) || !$.isArray(data.result)) {
							for (var resultKey in data.result) {
								domId = resultKey;
								value = data.result[resultKey];
								kmcore.fastAnimate(domId, value);
								$("#text").val('');
								$("#text").height(56);
							}
						}
					});
				}
			});
	},
	create: function(friendId, callback)
	{
			kmcore.api.send("/dialogApi/create", {"friendId": friendId}, function(data){
				if (typeof callback == 'function') {
					callback(data);
				}
			});
	},
	deleteDialog: function(dialogId, callback)
	{
			kmcore.api.send("/dialogApi/delete", {"dialogId": dialogId}, function(data){
				if (typeof callback == 'function') {
					callback(data);
				}
				location.reload();
			});
	},
	get: function(){}
}
/**
 * @author Maxim Tkach
 */
kmcore.page = {
	load: function(pageName, callbackAfter, callbackBefore)
	{
		kmcore.send("/json/"+pageName+"?rendering="+kmcore.rendering, [], function(data){
			data = data.response;
			if(($.isArray(data.result) && data.result.length > 0) || !$.isArray(data.result))
			{
				for (var resultKey in data.result) {
					domId = resultKey;
					value = data.result[resultKey];

					kmcore.domAnimate(domId, value, callbackAfter, callbackBefore);
				}
			}
			else
			{
				if (typeof callbackAfter == 'function')
				{
					callbackAfter(1);
				}
			}
		});
	}
}
kmcore.api.ancestry = {
	id: '',
	type: '',
	parent: '',
	deleteType: '',
	baseId: '',
	profiles: {},
	isYou: false,
	getProfile: function(id)
	{
		if (kmcore.api.ancestry.profiles[id] != 'undefined') {
			if (kmcore.api.ancestry.profiles[id].hasOwnProperty('first_name') &&  kmcore.api.ancestry.profiles[id].hasOwnProperty('last_name')) {
					return {"first_name": kmcore.api.ancestry.profiles[id].first_name, "last_name": kmcore.api.ancestry.profiles[id].last_name};
				}
		}
		return {"first_name": "", "last_name": ""};
	},
	initAncestry: function(id, type, parent, isYou)
	{
		kmcore.api.ancestry.id = id;
		kmcore.api.ancestry.type = type;
		kmcore.api.ancestry.parent = parent;
		kmcore.api.ancestry.isYou = isYou;
	},
	addRelation: function (petId, petRelationId, gender, callback) {

		kmcore.api.send("/ancestryApi/addRelation", {"ancestryId": kmcore.api.ancestry.id, "petId": petId, "petRelationId": petRelationId, "type": kmcore.api.ancestry.type, "parent":kmcore.api.ancestry.parent, "gender":gender}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
	remRelation: function (petId, petRelationId, gender, callback) {
		kmcore.api.send("/ancestryApi/remRelation", {"ancestryId": kmcore.api.ancestry.id, "petId": petId, "petRelationId": petRelationId, "type": kmcore.api.ancestry.type, "parent":kmcore.api.ancestry.parent, "gender":gender}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
	addGag: function (petId, gagId, gender, hNick, birthday, deathday, signs, callback) {
		kmcore.api.send("/ancestryApi/addGag", {"ancestryId": kmcore.api.ancestry.id, "petId": petId, "gagId": gagId, "type": kmcore.api.ancestry.type, "parent":kmcore.api.ancestry.parent, "gender":gender, "hNick": hNick, "birthday": birthday, "deathday": deathday, "signs": signs}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
	remGag: function (gagId, callback) {
		kmcore.api.send("/ancestryApi/remGag", {"gagId": gagId}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
	editGag: function (gagId, hNick, birthday, deathday, signs, gender, callback) {
		kmcore.api.send("/ancestryApi/editGag", {"gagId": gagId, "hNick": hNick, "birthday": birthday, "deathday": deathday, "signs": signs, "gender":gender}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
}

function baseAncestryCallback(data)
{
	data = data.response;
	if (data.success) {
		if (data.message) {
			$("#popup_status_dialog").empty();
			$("#popup_status_dialog").removeClass("error-msg");
			$("#popup_status_dialog").addClass("success-msg");
			$("#popup_status_dialog").html(data.message);
			$("#popup_status_dialog").animate({
				opacity: 1
			}, 500, 'swing', function () {
				setTimeout(function () {
					$("#popup_status_dialog").animate({
						opacity: 0
					}, 500, 'swing', function () {
						$("#popup_status_dialog").empty()
					});
				}, 5000)
			});
		}
		//				openComponent("mp_container_pet_page_avatar");
		location.reload();
	}
	else {
		$("#popup_status_dialog").empty();
		$("#popup_status_dialog").removeClass("success-msg");
		$("#popup_status_dialog").addClass("error-msg");
		$("#popup_status_dialog").html(data.message);
		$("#popup_status_dialog").animate({
			opacity: 1
		}, 500, 'swing', function () {
			setTimeout(function () {
				$("#popup_status_dialog").animate({
					opacity: 0
				}, 500, 'swing', function () {
					$("#popup_status_dialog").empty()
				});
			}, 5000)
		});
	}
}

function openAncestryCardPopup(component, data, popupWidth)
{
//	if(data.hasOwnProperty('gag'))
//	{
	var gender = '';
	if (data.gender == 1)
	{
		var gender = '<i class="icons he"></i>';
	}
	else if(data.gender == 2)
	{
		var gender = '<i class="icons she"></i>';
	}

	if(data.hasOwnProperty("photo") && data.photo)
	{
		var photo = '<img class="base-avatar" alt="" src="'+data.photo+'">';
	}
	else
	{
		var photo = '';
	}

	var html = '<div class="pop-up-form card-pet">'
	html += '<h2 class="toTranslate">_t_card_pet_t_</h2>';
	html += '<form>';
	html += '<div class="section">';
	html += '<div class="row">';
	html += '<div class="col-l">';
	html += '<div class="avatar">';
	if(data.hasOwnProperty('gag'))
	{
		html += '<a href="#">';
	}
	else
	{
		html += '<a href="/pet/'+data.id+'">';
	}
	html += '<div class="base-avatar avatar-'+data.type+'">'+photo+'</div>';
	html += '</a>';
	html += '</div>';
	html += '</div>';
	html += '<div class="col-r">';
	html += '<ul class="card-pet-list">';
	if(data.hasOwnProperty('gag'))
	{
		html += '<li><div class="name">'+data.hNick+''+gender+'</div></li>';
	}
	else
	{
		html += '<li><a href="/pet/'+data.id+'"><div class="name">'+data.hNick+''+gender+'</div></a></li>';
	}
	html += '<li class="toTranslate">_t_'+data.type+'_t_</li>';
	if(data.breed_translate)
	{
		html += '<li>'+data.breed_translate+'</li>';
	}
	if(data.age)
	{
		html += '<li>'+data.age+'</li>';
	}
    if(data.hasOwnProperty('params'))
    {
        if(data.params.hasOwnProperty('owner') && data.params.owner)
        {
            html += '<li class="toTranslate">_t_owner_t_: '+data.params.owner+'</li>';
        }
        if(data.params.hasOwnProperty('ringleader') && data.params.ringleader)
        {
            html += '<li class="toTranslate">_t_ringleader_t_: '+data.params.ringleader+'</li>';
        }
    }
	else
	{
		if(data.hasOwnProperty('ringleader') && data.ringleader)
		{
			var user = kmcore.api.ancestry.getProfile(data.ringleader);
			if(user.hasOwnProperty('first_name') && user.hasOwnProperty('first_name'))
			{
				html += '<li class="toTranslate">_t_ringleader_t_: <a href="/user/'+data.ringleader+'">'+user.first_name+' '+user.last_name+'</a></li>';
			}
		}
		if(data.hasOwnProperty('ownerIds') && data.ownerIds)
		{
			for(key in data.ownerIds)
			{
				var item = data.ownerIds[key];
				var user = kmcore.api.ancestry.getProfile(item);
				if(user.hasOwnProperty('first_name') && user.hasOwnProperty('first_name'))
				{
					html += '<li class="toTranslate">_t_owner_t_: <a href="/user/'+item+'">'+user.first_name+' '+user.last_name+'</a></li>';
				}
			}
		}
	}


	if(data.signs)
	{
		html += '<li class="toTranslate">_t_signs_t_: '+data.signs+'</li>';
	}
	html += '</ul>';
	html += '</div>';
	html += '</div>';
	html += '<div class="row card-pet-link">';
	html += '<ul>';
	if(kmcore.api.ancestry.isYou)
	{
		if(data.hasOwnProperty('gag'))
		{
			html += '<li><a href="#" onclick=\'openPopup("mp_popup_ancestry_edit_pet", "gagId='+data.id+'", 570)\' class="toTranslate">_t_edit_card_t_</a></li>';
	//			if(!data.fatherId && !data.motherId)
	//			{
			html += '<li><a href="#" onclick=\'kmcore.api.ancestry.deleteType="gag";openPopup("mp_popup_ancestry_delete_pet", "petId='+data.id+'&gender='+data.gender+'");return false;\' class="toTranslate">_t_card_delete_t_</a></li>';
	//			}
		}
		else if(data.id != kmcore.api.ancestry.baseId)
		{
			html += '<li><a href="#" onclick=\'kmcore.api.ancestry.deleteType="relation";openPopup("mp_popup_ancestry_delete_pet", "petId='+data.id+'&gender='+data.gender+'");return false;\' class="toTranslate">_t_link_delete_t_</a></li>';
		}
	}
	html += '</ul>';
	html += '</div>';
	html += '</div>';
	html += '</form>';
	html += '</div>';

	width = 460;
	if(popupWidth !== undefined)
	{
		width = popupWidth;
	}

	$.fancybox({
		content     : html,
		maxWidth	: width,
		fitToView	: false,
		width		: width+'px',
		height		: 'auto',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'fade',
		closeEffect	: 'fade',
		padding     : 0,
		closeBtn    : true,
		wrapCSS     : "spinner",
		helpers: {
			overlay: {
				locked: false
			}
		}
	});
	updateStyler();
//	}
//	else
//	{
//		window.location.replace("/pet/"+data.id);
//	}
	mypet_translate.findAndTranslate();
}

function mpAncestryAddGag(form) {
	var id = form.id;
	if (id) {
		var params = {}
		params.petId = $("#" + id + " input[name=petId]").val();
		params.gender = $("#" + id + " select[name=gender] :selected").val();
		if(params.gender == undefined)
		{
			params.gender = $("#" + id + " input[name=gender]").val();
		}

		params.hNick = $("#" + id + " input[name=hNick]").val();
		params.birthday = $("#" + id + " input[name=birthday]").val();
		params.deathday = $("#" + id + " input[name=deathday]").val();
		params.signs = $("#" + id + " textarea[name=signs]").val();

		params.owner = $("#" + id + " input[name=owner]").val();
		params.ringleader = $("#" + id + " input[name=ringleader]").val();

		params.ancestryId = kmcore.api.ancestry.id;
		params.type = kmcore.api.ancestry.type;
		params.parent = kmcore.api.ancestry.parent;


		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#popup_status_dialog").empty();
					$("#popup_status_dialog").removeClass("error-msg");
					$("#popup_status_dialog").addClass("success-msg");
					$("#popup_status_dialog").html(data.message);
					$("#popup_status_dialog").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#popup_status_dialog").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#popup_status_dialog").empty()
							});
						}, 5000)
					});
				}
				//				openComponent("mp_container_pet_page_avatar");
				location.reload();
			}
			else {
				$("#popup_status_dialog").empty();
				$("#popup_status_dialog").removeClass("success-msg");
				$("#popup_status_dialog").addClass("error-msg");
				$("#popup_status_dialog").html(data.message);
				$("#popup_status_dialog").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#popup_status_dialog").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#popup_status_dialog").empty()
						});
					}, 5000)
				});
			}
		});
	}
}

function mpAncestryEditGag(form) {
	var id = form.id;
	if (id) {
		var params = {}
		params.gagId = $("#" + id + " input[name=gagId]").val();
//		params.gender = $("#" + id + " input[name=gender]").val();
		params.gender = $("#" + id + " select[name=gender] :selected").val();

		params.hNick = $("#" + id + " input[name=hNick]").val();
		params.birthday = $("#" + id + " input[name=birthday]").val();
		params.deathday = $("#" + id + " input[name=deathday]").val();
		params.signs = $("#" + id + " textarea[name=signs]").val();

		params.owner = $("#" + id + " input[name=owner]").val();
		params.ringleader = $("#" + id + " input[name=ringleader]").val();

		kmcore.send(form.action, params, function (data) {
			data = data.response;
			if (data.success) {
				if (data.message) {
					$("#responseEdit").empty();
					$("#responseEdit").html(data.message);
					$("#responseEdit").animate({
						opacity: 1
					}, 500, 'swing', function () {
						setTimeout(function () {
							$("#responseEdit").animate({
								opacity: 0
							}, 500, 'swing', function () {
								$("#responseEdit").empty()
							});
						}, 5000)
					});
				}

//				kmcore.components.closeComponent('section_form_box_dialog');
//				kmcore.page.load('user', function (status) {
//				}, function () {
//					loadResize();
//					updateStyler();
//				});
//				kmcore.loggedIn = true;
//				window.location.replace("/user");
			}
			else {
				$("#responseEdit").empty();
				$("#responseEdit").html(data.message);
				$("#responseEdit").animate({
					opacity: 1
				}, 500, 'swing', function () {
					setTimeout(function () {
						$("#responseEdit").animate({
							opacity: 0
						}, 500, 'swing', function () {
							$("#responseEdit").empty()
						});
					}, 5000)
				});
			}
		});
	}
}
kmcore.api.search = {
	user: function (value, callback) {
		kmcore.api.send("/searchApi/findUsers", {"query": value}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
	pet: function (value, callback) {
		kmcore.api.send("/searchApi/findPets", {"query": value}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
	find: function (value, callback) {
		kmcore.api.send("/searchApi/findMixed", {"query": value}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
	findFullDataReturn: function (value, callback) {
		kmcore.api.send("/searchHeperApi/findMixed", {"query": value}, function(data){
			if (typeof callback == 'function') {
				callback(data);
			}
		});
	},
}
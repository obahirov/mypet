/**
 * @author Maxim Tkach
 */
kmcore.helper = {
	intervalId: false,
	interval: 2000,
	updateBalanceStart: function () {
		if (kmcore.helper.intervalId == false) {
			kmcore.helper.intervalId = setInterval(function () {
				kmcore.send("/json/" + kmcore.pageToLoadComponent + "?additionalComponents={\""+kmcore.prefix+"block_user_panel\":1}", [], function (data) {
					$("#dynamic_balance").text(data.response.result.block_user_panel.balance)
				});
			}, kmcore.helper.interval);
		}
	},
	updateBalanceStop: function () {
		if (kmcore.helper.intervalId) {
			clearInterval(kmcore.helper.intervalId);
		}
	},
	replaceContent: function(text, params)
	{

	}
}

function openComponent(component, callbackAfter, callbackBefore, query) {
	if (kmcore.animateLock == false) {
		kmcore.animateLock = true;
		kmcore.components.loadComponent(component, function (status) {
			kmcore.animateLock = false;
			if (typeof callbackAfter == 'function') {
				callbackAfter(status);
			}
		}, function () {
			if (typeof callbackBefore == 'function') {
				callbackBefore();
			}
		}, query);  //status - fail
	}
}

function openPage(pageName, callbackAfter, callbackBefore) {
	if (kmcore.animateLock == false) {
		kmcore.animateLock = true;
		kmcore.page.load(pageName, function (status) {
			kmcore.animateLock = false;
			if (typeof callbackAfter == 'function') {
				callbackAfter(status);
			}
		}, function () {
			if (typeof callbackBefore == 'function') {
				callbackBefore();
			}
		});  //status - fail
	}
}
//
//$(document).ready(function () {
//	kmcore.hash.component(function (status) {
//	}, function () {
//	});
//});
//
//$(window).bind('hashchange', function () {
//	kmcore.hash.component(function (status) {
//	}, function () {
//	});
//});
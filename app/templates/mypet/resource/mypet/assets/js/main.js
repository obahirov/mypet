//function dump(obj) {
//    var out = '';
//    for (var i in obj) {
//        out += i + ": " + obj[i] + "\n";
//    }
//
//    alert(out);
//
//
//    var pre = document.createElement('pre');
//    pre.innerHTML = out;
//    document.body.appendChild(pre)
//
//
//            dump(e.target);
//}

$(function(){
    var centered = $(".sign-up, .login");
    centered.css("opacity", 0);
});

$(window).on('load',function() {

    //  Слайдер  --------------------------------------
    $(".bg-slider").responsiveSlides({
        "speed": 2000
    });
    var vw = $(window).width();
    $(".bg-slider li div").css("width", vw);

    $('input, textarea').placeholder();

    //  Перемикання мов у футері ----------------------
    $(".langvich").hover(function () {
        $(".switch-languages").show();
    }, function(){
        $(".switch-languages").hide();
    });

    //  Стилізація форм ----------------------------
    if($('input, select').length){
        $('input, select').styler();
    }

    //  Налаштування .header ----------------------
    $(".initSettingsMenu").hover(function () {
        $(".list-settings").show();
    }, function(){
        $(".list-settings").hide();
    });

    // Іконки поідомленнь -------------------------
    $(".letters, .alert").hover(function () {
        $(this).children().removeClass("wobble");
        $(this).children().addClass("bounceIn");
    }, function(){
        $(this).children().removeClass("bounceIn");
    });

    //   Кнопка close - закрити модуль в лівому сайдбарі ---
    $(".message .close").click(function () {
        var close = $(this).parents(".module");
        close.css("opacity", 0)
             .animate({
                height: 0
             }, 500,
             function(){
                close.remove();
             });
    });

    //  lightbox -------------------------------------
    if($(".lightbox").length){
        $(".lightbox").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 460,
                    fitToView	: false,
                    width		: '460px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    wrapCSS     : "spinner",
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
                $('input, select').styler({
                    filePlaceholder: "",
                    fileBrowse: ""
                });
//                $(".owner").autocomplete({source: availableOwner});
//                $(".birthday").mask("99/99/9999");
                $('textarea').autosize({
                    callback: function(){
                        $.fancybox.update({
                            helpers: {
                                overlay: {
                                    locked: true
                                }
                            }
                        });
                        if($(this).height() > 315){
                            $(this).trigger('autosize.destroy').height(315);
                        }
                    }
                });
            });
            return false;
        });
    }

    if($(".lightbox-2").length){
        $(".lightbox-2").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 990,
                    fitToView	: false,
                    width		: '990px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true
                });
            });
            return false;
        });
    }

    //  lightbox -------------------------------------
    if($(".lightbox-group").length){
        $(".lightbox-group").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 460,
                    fitToView	: false,
                    width		: '460px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    wrapCSS     : "spinner"
                });
                $('input, select').styler({
                    filePlaceholder: "",
                    fileBrowse: ""
                });
                $( ".country" ).autocomplete({source: availableCountry});
                $( ".city" ).autocomplete({source: availableCity});

                $('textarea').autosize({
                    callback: function(){
                        $.fancybox.update({
                            helpers: {
                                overlay: {
                                    locked: true
                                }
                            }
                        });
                        if($(this).height() > 315){
                            $(this).trigger('autosize.destroy').height(315);
                        }
                    }
                });
            });
            return false;
        });
    }

    if($(".add-new-photo").length){
        $(".add-new-photo").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 460,
                    fitToView	: false,
                    width		: '460px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    wrapCSS     : "spinner"
                });
                $('input, select').styler({
                    filePlaceholder: "",
                    fileBrowse: ""
                });
//                $(".owner").autocomplete({source: availableOwner});
//                $(".birthday").mask("99/99/9999");
                $('textarea').autosize({
                    callback: function(){
                        $.fancybox.update({
                            helpers: {
                                overlay: {
                                    locked: true
                                }
                            }
                        });
                        if($(this).height() > 315){
                            $(this).trigger('autosize.destroy').height(315);
                        }
                    }
                });
            });
            return false;
        });
    }

    //   маска на поле input - день народження --------------
//    if($(".birthday").length){
//        $(".birthday").mask("99/99/9999");
//    }
	var counter = 0;
    //   Кнорка клонування
     $(".add").click(function (e) {
        e.preventDefault();
        var parentAdd = $(this).siblings();
        var item = parentAdd.find(".item").clone(false).eq(0);
		var rnd = Math.floor((Math.random() * 1000) + 1);
		item.attr('id', item.attr('id')+''+rnd+''+counter);
        item.find("input").val("");
		item.find(".result").empty(); // Скидуємо пошук

//		item.find('.dateForm').each(function(){$(this).datepicker('remove');});
//		item.find('.from_date').each(function(){$(this).datepicker('remove');});
//		item.find('.to_date').each(function(){$(this).datepicker('remove');});

        var select = item.find('select');
        select.each(function(){
          var $this = $(this),
              name = $this.attr('name'),
              data = $this.html(),
              td = $this.parents('td:first'),
              s = $('<select></select>').attr('name', name).html(data);
            td.html(s);
            s.styler();
        });

//		 var datalist = item.find('.searchUser').each(function(){
//			var $this = $(this);
//			var list = $this.attr('list');
//			var rnd = Math.floor((Math.random() * 1000) + 1);
//			var new_list = list+''+rnd+''+counter;
//			$this.attr('list', new_list);
//			var listItem = $this.parent().find('datalist');
//			listItem.attr('id', new_list);
//			counter++;
//		 });

		 var rnd = Math.floor((Math.random() * 1000) + 1);
		 var date = item.find('.birthday').each(function(){
			 var id = $(this).attr('id');
			 $(this).datepicker("destroy");
			 var new_id = rnd+''+counter+'_'+id;
			 $(this).attr('id', new_id);
		 });
		 counter++;
//        var birthday = item.find('.birthday').removeClass('birthday');
//        birthday.unmask().mask("99/99/9999");
        item.appendTo(parentAdd);
		 updateStyler();

		 //  Кнорка видалення
		 $(".remove").click(function (e) {
			 e.preventDefault();
			 var quantity =  $(this).parent().parent().find(".item").length;
			 if(quantity > 1){
				 $(this).parents(".item").remove();
			 }
			 else
			 {
				 $(this).parents(".item").find("input").val("");
			 }
		 });
    });

    //  Кнорка видалення
    $(".remove").click(function (e) {
        e.preventDefault();
        var quantity =  $(this).parent().parent().find(".item").length;
        if(quantity > 1){
            $(this).parents(".item").remove();
        }
		else
		{
			$(this).parents(".item").find("input").val("");
		}
    });

    //   Якщо спеціаліст то показати форму  (форма редагування профілю)
    var sb  = $(".specialist-body"),
        isb = $(".specialist-checkbox input");
        isb.change(function() {
        if($(this).is(':checked')) {
            sb.slideDown();
        } else {
            sb.slideUp();
        }
    });

    //  іконка редагування головного зображення сторінки користувача
    $(".cover-image").hover(function () {
         $(this).find("a").stop().animate({
             opacity: 1
         }, 500);
    }, function(){
        $(this).find("a").stop().animate({
            opacity: 0
        }, 500);
    });

//    var availableCountry = [
//        "Україна",
//        "Білорусь",
//        "Хуйлостан"
//    ];
//    var availableCity = [
//        "Львів",
//        "Київ",
//        "Москвабад"
//    ];
//    var availableSpecies = [
//        "Британська короткошерста",
//        "Британська довгошерста"
//    ];
//    var availableOwner = [
//        "Оля",
//        "Юля",
//        "Роман",
//        "Роман1",
//        "Роман2",
//        "Роман3",
//        "Роман4",
//        "Роман5",
//        "Роман6",
//        "Роман7",
//        "Роман8",
//        "Роман9",
//        "Роман10",
//        "Роман11",
//        "Роман12",
//        "Роман13",
//        "Роман14",
//        "Роман15",
//        "Сергій16",
//        "Володя"
//    ];
//    var availableBreeder = [
//        "Оля",
//        "Юля",
//        "Роман",
//        "Сергій",
//        "Володя"
//    ];
//    $( ".country" ).autocomplete({source: availableCountry});
//    $( ".city" ).autocomplete({source: availableCity});
//    $(".species").autocomplete({source: availableSpecies});
//    $(".owner").autocomplete({
//        source: availableOwner,
//        open: function () {
//            $(this).data("autocomplete").menu.element.addClass("my_class");
//        }
//    });
//    $(".breeder").autocomplete({source: availableBreeder});

    //  tooltip
    if($(".tooltip").length){
        $('.tooltip').tooltipster();
    }

    //  Фотоальбом
    if($(".photo-album, .post-wall").length){
        $(".photo-pop-up").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 780,
                    fitToView	: false,
                    width		: '780px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    helpers: {
                        overlay: {
                            locked: true
                        }
                    }
                });
                $('input, select').styler();
                $('textarea').autosize();
            });
            return false;
        });

        $(".photo-item").hover(function () {
            $(this).find(".bottom").stop().animate({bottom: 0}, 300);
        }, function(){
            $(this).find(".bottom").stop().animate({bottom: -23}, 300);
        });
    }

    //  Автоматично міняємо висоту  textarea
    if($("textarea").length){
        $('textarea').autosize();
    }

    //  Змінюємо висто
    if($(".album-item").length){
        $(".album-item").hover(function () {
            $(this).find(".bottom").stop().animate({
                top: 0
            }, 100);
        }, function(){
            $(this).find(".bottom").stop().animate({
                top: 107
            }, 100);
        });
    }
    if($(".scroll-pane").length){
        $('.scroll-pane').jScrollPane({
            autoReinitialise: true,
            animateScroll: true
        });
    }

//  Сторінка пошуку - вкладки

    //    var tabs = $('ul.tabs');
    //    tabs.each(function(i) {
    //        var storage = localStorage.getItem('tab'+i);
    //        if (storage) $(this).find('li').eq(storage).addClass('current').siblings().removeClass('current')
    //            .parents('div.section').find('div.box').hide().eq(storage).show();
    //    });

    //    tabs.on('click', 'li:not(.current)', function() {
    //        $(this).addClass('current').siblings().removeClass('current')
    //            .parents('div.section').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();
    //        var ulIndex = $('ul.tabs').index($(this).parents('ul.tabs'));
    //        localStorage.removeItem('tab'+ulIndex);
    //        localStorage.setItem('tab'+ulIndex, $(this).index());
    //    });
    // if($(".children-page").length){
    //    $(".children-page").find(".children-item:nth-child(5n)").css("margin-right", "0");
    // }


    //  lightbox-t-card -------------------------------------
    if($(".lightbox-t-card").length){
        $(".lightbox-t-card").click(function(){
            var href= $(this).attr('href');
            $.get(href, function(d){
                $.fancybox({
                    content     : d,
                    maxWidth	: 570,
                    fitToView	: false,
                    width		: '570px',
                    height		: 'auto',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'fade',
                    closeEffect	: 'fade',
                    padding     : 0,
                    closeBtn    : true,
                    wrapCSS     : "lightbox-t-card",
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
                $('input, select').styler({
                    filePlaceholder: "",
                    fileBrowse: ""
                });
//                $(".owner").autocomplete({source: availableOwner});
//                $(".birthday").mask("99/99/9999");

            });
            return false;
        });
    }

    //noty - системні підказки
    var noty = $(".noty");
    var notyLi = noty.find("li");
    notyLi.find(".close").click(function(){
        var p =  $(this).parents("li");
            p.slideUp({
                duration: 300,
                easing: "easeInOutQuad",
                complete: function(){
                    p.remove();
                }
            });
    });
    var nonyC = notyLi.length;
    if(nonyC > 3){

        var h = 0 ;
        noty.find("ul").addClass("scroll-pane");
        noty.find("ul").find('li').each(function(i,e){
            h += $(this).innerHeight();
            //console.log(this, $(this).innerHeight());
            if(i == 2) return false;
        });
        noty.find("ul").height(h);
        $('.scroll-pane').jScrollPane({
            autoReinitialise: true,
            autoReinitialiseDelay: 1
        });

    }


});

$(window).on('load resize',function() {
    //  Вирівнювання по центру ".sign-up" на строінках входу, зміни паролю
    var windowH = $(window).height(),
        formBox = $(".form-box"),
        centered = $(".sign-up, .login"),
        centeredH = centered.innerHeight();
        centered.css("margin-top", (centeredH/-2) + "px");
        if(windowH < centeredH + 300){
            formBox.addClass("form-box-css");
            formBox.height(centeredH);
        } else{
            formBox.removeClass("form-box-css");
        }
        setTimeout(function() {
            centered.animate({opacity: 1}, 2000);
        }, 300);
});

/**
 * @author Maxim Tkach
 */
kmcore.api = {
	lockRemove: true,
	send: function(url, params, callback)
	{
		kmcore.send(url, params, callback);
	},

	removeMe: function(){
		if(kmcore.api.lockRemove === false)
		{
			kmcore.api.send("/userApi/removeUser", {}, function(data){window.location.replace("/");});
			kmcore.api.lockRemove = true;
		}
	},
	removeMyPet: function(petId){
		if(kmcore.api.lockRemove === false)
		{
			kmcore.api.send("/petApi/removePet", {"petId": petId}, function(data){
                kmcore.api.send("/clientApi/updateProgress", {}, function(data){ window.location.replace("/");});
            });
			kmcore.api.lockRemove = true;
		}
	},
	unlockRemove: function(){
		kmcore.api.lockRemove = false;
	}
}
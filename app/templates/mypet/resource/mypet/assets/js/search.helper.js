function showFindResult(data) {
	data = data.response;
	$("#search_item_user").empty();
	$("#search_item_pet").empty();
	$("#search_result").show();

	$("#search_group_user").hide();
	$("#search_pet_user").hide();

	var flag_user = false;
	var flag_pet = false;
	for (key in data.hits.hits) {
		var id = data.hits.hits[key]._id;
		var type = data.hits.hits[key]._type;
		var source = data.hits.hits[key]._source;

		if (source.hasOwnProperty("photo") && source.photo) {
			var photo = '<img class="search-avatar avatar-user" src="' + source.photo + '" alt=""/>';
		}
		else {
			var photo = '';
		}

		if (type == 'user') {
			var description = '';
			if (source.hasOwnProperty("location") && source.location.hasOwnProperty("formated"))
			{
				description = source.location.formated;
			}

			if(source.isProf === 'yes')
			{
				description += ' <block_trn class="toTranslate">_t_'+source.profession+'_t_</block_trn>';
			}
			var html = '<div class="search-item"><div class="result-item__photo"><a href="/user/' + id + '"><div class="search-avatar avatar-user">' + photo + '</div></a></div><div class="result-item__body"><h3><a href="/user/' + id + '">' + source.first_name + ' ' + source.last_name + '</a></h3><div class="result-item__description">'+description+'</div></div></div>';
			$("#search_item_user").append(html);
			flag_user = true;
		}
		else if (type == 'pet') {
			if(source.render_breed)
			{
				var description = source.render_breed;
				if(source.age)
				{
					description += ', '+source.age;
				}
			}
			else
			{
				var description = source.age;
			}
			var html = '<div class="search-item"><div class="result-item__photo"><a href="/pet/' + id + '"><div class="search-avatar avatar-' + source.type + '">' + photo + '</div></a></div><div class="result-item__body"><h3><a href="/pet/' + id + '">' + source.hNick + '</a></h3><div class="result-item__description">'+description+'</div></div></div>';
			$("#search_item_pet").append(html);
			flag_pet = true;
		}

		mypet_translate.findAndTranslate();
	}
	if (flag_user) {
		$("#search_group_user").show();
	}
	if (flag_pet) {
		$("#search_pet_user").show();
	}
}

$("#searchAllType").keyup(
	_.debounce(function () {
		var val = $(this).val();
		var arr = val.split(' ');
		var query = '';
		for (var i in arr) {
			if(arr[i])
			{
				query += arr[i]+' OR ';
			}
		}

		query = query.substring(0, query.length - 4);

		kmcore.api.search.findFullDataReturn(query, showFindResult);
	}, 1000)
);

$(".searchUser").keyup(_.debounce(function () {
	var typeSearch = $(this).attr('typeSearch');

	var val = $(this).val();
	var arr = val.split(' ');
	var query = '';
	for (var i in arr) {
		if(arr[i])
		{
			query += arr[i]+'* OR ';
		}
	}

	query = query.substring(0, query.length - 4);

	switch (typeSearch) {
		case 'user':
			query = query
			break
		case 'vet':
			query = query + ' AND profession:vet'
			break
		case 'groomer':
			query = query + ' AND profession:groomer'
			break
		case 'handler':
			query = query + ' AND profession:handler'
			break
		case 'trainer':
			query = query + ' AND profession:trainer'
			break
		case 'vet':
			query = query + ' AND profession:vet'
			break
		case 'psychologist':
			query = query + ' AND profession:psychologist'
			break
		case 'photographer':
			query = query + ' AND profession:photographer'
			break
		case 'felinologist':
			query = query + ' AND profession:felinologist'
			break
		case 'cynologist	':
			query = query + ' AND profession:cynologist'
			break
		case 'breeder':
			query = query + ' AND profession:breeder'
			break
		case 'other':
			query = query + ''
			break
	}

	var hidden = $(this).parent().find(".owner_hidden");
	if (val) {
		hidden.val(val);
	}
	var result = $(this).parent().find('.result');
	kmcore.api.search.user(query, function (data) {
		data = data.response;
		result.empty();
		for (key in data.hits.hits) {
			var id = data.hits.hits[key]._id;
			var type = data.hits.hits[key]._type;
			var source = data.hits.hits[key]._source;

			if (source.hasOwnProperty("photo") && source.photo) {
				var photo = '<img class="search-avatar" alt="" src="' + source.photo + '">';
			}
			else {
				var photo = '';
			}

			var html = '<div class="result-item"><div class="result-item__photo"><a class="sendRequest" href="#" id="' + source.id + '"><div class="search-avatar avatar-user">' + photo + '</div></a></div><div class="result-item__body"><h3><a class="sendRequest" href="#" id="' + source.id + '">' + source.first_name + ' ' + source.last_name + '</a></h3><div class="result-item__description toTranslate">_t_' + source.profession + '_t_</div></div></div>';

			var first_name = source.first_name;
			var last_name = source.last_name;

			result.append(html);
			mypet_translate.findAndTranslate();
		}

		$(".sendRequest").click(function () {
			var id = $(this).attr('id');
			var hidden = $(this).parent().parent().parent().parent().parent().find(".owner_hidden"); // FIXME Виправити на щось красивіше
			var current = $(this).parent().parent().parent().parent().parent().find(".owner"); // FIXME Виправити на щось красивіше
			if (id) {
				hidden.val(id);
				current.val(first_name+' '+last_name);
				result.empty();
			}
		});
	});
}, 1000));

$(".searchUserType").keyup(_.debounce(function (e) {
	var code = e.which; // recommended to use e.which, it's normalized across browsers
	if (code == 13) {
		var val = $(this).val();
		var query = '';

		if (val == '') {
			query = '*';
		}
		else {
			var arr = val.split(' ');
			for (var i in arr) {
				if(arr[i])
				{
					query += arr[i]+' OR ';
				}
			}

			query = query.substring(0, query.length - 4);
		}

		var specialist_query = '';
		var i = 0;
		$(".specialist input:checked").each(function () {
			if (i > 0) {
				specialist_query += ' OR ';
			}
			if ($(this).val() != 'other') {
				specialist_query += $(this).val();
			}
			i++;
		});

		var gender_query = '';
		$(".gender option:selected").each(function () {
			gender_query += $(this).val();
		});

		if (specialist_query) {
			query += ' AND profession:(' + specialist_query + ')';
		}
		if (gender_query) {
			query += ' AND gender:'+gender_query+'';
		}
        
		var searchBody = $('#search_body');
		openComponent('mp_page_search', function () {
			var ias = $.ias({
				container: ".container_post",
				item: ".item_post",
				pagination: "#pagination_post",
				next: ".next_post"
			});

			ias.extension(new IASSpinnerExtension());            // shows a spinner (a.k.a. loader)
			ias.extension(new IASTriggerExtension({offset: 3})); // shows a trigger after page 3
			ias.extension(new IASNoneLeftExtension({
				text: 'There are no more pages left to load.'      // override text when no pages left
			}));
		}, undefined, "type=user&query=" + query);

//	window.location.replace("/search/0?type=pet&query="+query);
//	kmcore.api.search.user(query, function(data)
//	{
//		data = data.response;
//		searchBody.empty();
//		for(key in data.hits.hits)
//		{
//			var id = data.hits.hits[key]._id;
//			var type = data.hits.hits[key]._type;
//			var source = data.hits.hits[key]._source;
//
////			searchBody.append('');
//		}
//
//	});
	}
}, 1000));

$(".searchPetType").keyup(_.debounce(function (e) {
	var code = e.which; // recommended to use e.which, it's normalized across browsers
	if (code == 13) {
		var status_query = '';
		var i = 0;
		$(".status input:checked").each(function () {
			if (i > 0) {
				status_query += ' OR ';
			}
			status_query += $(this).val();
			i++;
		});

		var pet_type_query = '';
		var i = 0;
		$(".pet_type option:selected").each(function () {
			if (i > 0) {
				pet_type_query += ' OR ';
			}
			pet_type_query += $(this).val();
			i++;
		});


		var gender_query = '';
		$(".gender option:selected").each(function () {
			gender_query += $(this).val();
		});

		var valBreed = $("#breed").val();
		var breed = $('#breed_list [value="'+valBreed+'"]').attr('id');

		var val = $(this).val();
		var query = '';

		if (val == '') {
			query = '*';
		}
		else {
			var arr = val.split(' ');
			for (var i in arr) {
				if(arr[i])
				{
					query += arr[i]+' OR ';
				}
			}

			query = query.substring(0, query.length - 4);
		}

		if (status_query) {
			query += ' AND status:(' + status_query + ')';
		}
		if (pet_type_query) {
			query += ' AND type:(' + pet_type_query + ')';
		}
		if (gender_query) {
			query += ' AND gender:'+gender_query+'';
		}
		if(breed)
		{
			query += ' AND breed:'+breed+'';
		}

		var searchBody = $('#search_body');

		openComponent('mp_page_search', undefined, undefined, "type=pet&query=" + query);
//	window.location.replace("/search/0?type=pet&query="+query);
//	kmcore.api.search.pet(query, function(data)
//	{
//		data = data.response;
//		searchBody.empty();
//		for(key in data.hits.hits)
//		{
//			var id = data.hits.hits[key]._id;
//			var type = data.hits.hits[key]._type;
//			var source = data.hits.hits[key]._source;
//
////			searchBody.append('');
//		}
//
//	});
	}
}, 1000));

$('#search_result').click(function (e) {
	//return false;
});

$(document).click(function (e) {
	$('#search_result').hide();
	//return false;
});
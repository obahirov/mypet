angular.module('myPet').factory('SearchService', ['$http', 'CONFIG','$location' ,function($http,CONFIG,$location){
  

   
///searchApi/findPets
//searchApi/findMixed

  function findUsers(outputData){
    var queryString = (outputData && outputData.query)? outputData.query : '*';
    if(outputData && outputData.professions.length) {
      queryString += ' AND profession:(' + outputData.professions.join(' OR ') + ')';
    }
    var query = {query: queryString};
    return $http.post('http://'+$location.$$host+'/searchApi/findUsers', query);
  }

  function findPets(outputData){
    var queryString = (outputData && outputData.query)? outputData.query : '*';
    if(outputData && outputData.petType !== 'all') {
      queryString += ' AND type:(' + outputData.petType + ')';
    }
    if(outputData && outputData.petStates.length) {
      queryString += ' AND status:(' + outputData.petStates.join(' OR ') + ')';
    }
    var query = {query: queryString};
    return $http.post('http://'+$location.$$host+'/searchApi/findPets', query);
  }

  var text ={
    question:mypet_translate.translateByKey('_t_question_about_profesion_t_'),
    specialization:mypet_translate.translateByKey('_t_specialization_t_'),
    yes:mypet_translate.translateByKey('_t_yes_t_'),
    no:mypet_translate.translateByKey('_t_no_t_'),
    main_information:mypet_translate.translateByKey('_t_main_information_t_'),
    organizations:mypet_translate.translateByKey('_t_organizations_t_'),
    organization:mypet_translate.translateByKey('_t_organization_t_'),
    work_place:mypet_translate.translateByKey('_t_work_place_t_'),
    location:mypet_translate.translateByKey('_t_location_t_'),
    website:mypet_translate.translateByKey('_t_website_t_'),
    phone_number:mypet_translate.translateByKey('_t_phone_number_t_'),
    tags:mypet_translate.translateByKey('_t_tags_t_'),
    work_place_palceholder:mypet_translate.translateByKey('_t_work_place_palceholder_t_'),
    tags_palceholder:mypet_translate.translateByKey('_t_tags_palceholder_t_'),
    website_palceholder:mypet_translate.translateByKey('_t_website_palceholder_t_'),
    organizations_palceholder:mypet_translate.translateByKey('_t_organizations_palceholder_t_'),
    edit_information:mypet_translate.translateByKey('_t_edit_information_t_'),
    view_information:mypet_translate.translateByKey('_t_view_information_t_'),
    save:mypet_translate.translateByKey('_t_save_t_'),
    save_and:mypet_translate.translateByKey('_t_save_and_t_'),
    im_not_a_specialist:mypet_translate.translateByKey('_t_im_not_a_specialist_t_'),
    im_a_specialist:mypet_translate.translateByKey('_t_im_a_specialist_t_')
  };

  
  return{
    text:text,
    findUsers:findUsers,
    findPets:findPets
  }

}]);
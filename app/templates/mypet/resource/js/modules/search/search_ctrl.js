
angular.module('myPet').controller('SearchCtrl',
  ['$scope','SearchService','$location','UserService',
  function($scope, SearchService, $location, UserService) {

    $scope.waitForData;
    $scope.inputData ={

    };

    $scope.types = ['users','pets'];
    $scope.petTypes = ['cat', 'dog'];

    function resetOutputData(type) {
      $scope.outputData = {
        professions:[],
        petType: 'all',
        petStates: [],
        query:'',
        type: type || $scope.types[0]
      };
    }

    $scope.setType = function(type) {
      resetOutputData(type);
      init();
    };

    $scope.checkThisName = function(){
      var data = $scope.outputData;
      if(data.query.length > 2 || data.query.length===0) {
        (data.type === 'users') ? getUsers(data) : getPets(data);
      }
    };

    $scope.addOrRemoveProfession = function(id, ev) {
      if ($(ev.target).hasClass('jq-checkbox')) {
        $(ev.target).toggleClass('checked');
        var profArray = $scope.outputData.professions;
        var isInArray = $scope.outputData.professions.indexOf(id);
        if (isInArray === -1) {
          profArray.push(id);
        } else {
          profArray.splice(isInArray, 1);
        }
        getUsers($scope.outputData);
      }
    };

    $scope.$watch('outputData.petType', function(newV, oldV) {
      getPets($scope.outputData);
      if(newV !== 'all') {
        getBreeds(newV);
      }
    });

    function errorFn(data){
      console.log(data);
    }

    function init() {
      if($scope.outputData.type === 'users'){
        getProfessions();
        getUsers();
      } else if($scope.outputData.type === 'pets'){
        getPets();
      } else {
        console.log('unknown type');
      }
    }

    function getUsers(outputData){
      $scope.waitForData = true;
      SearchService.findUsers(outputData)
      .success(function(data, status, headers, config){
        $scope.waitForData = false;
        $scope.inputData.total = data.response.hits.total
        $scope.inputData.hits = data.response.hits.hits
      })
      .error(errorFn);
    }
    
    function getPets(outputData){
      $scope.waitForData = true;
      SearchService.findPets(outputData)
      .success(function(data, status, headers, config){
        $scope.waitForData = false;
        $scope.inputData.total = data.response.hits.total
        $scope.inputData.hits = data.response.hits.hits
      })
      .error(errorFn);
    }

    function getProfessions(){
      UserService.getProfessions()
      .success(function(data, status, headers, config){
        $scope.professions = data.response.professions;
      })
      .error(errorFn);
    }

    function getBreeds(type){
      UserService.getBreedsByType(type)
      .success(function(data, status, headers, config){
        $scope.breeds = data.response.breeds;
      })
      .error(errorFn);
    }
    resetOutputData();
    init();
}]);
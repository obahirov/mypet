angular.module('myPet')
.directive('replies', [ function() {
  return {
    restrict: 'EA',
    controller: 'repliesCtrl',
    scope: {parent: '='},
    templateUrl: '/resource/js/modules/directives/replies.html',
    replace: true
  }
}])
.controller('repliesCtrl', 
  ['$scope','PostService','CONFIG','UserService',
  function($scope, PostService, CONFIG, UserService){

    $scope.$watch('parent', function(i) {
      checkComments();
    });

    $scope.text = PostService.text;
    $scope.currentUser = PostService.currentUser;

    function errorFn(data){
      console.log(data);
    }

    function checkComments() {
      PostService.getPosts($scope.parent.id)
      .success(function(data, status, headers, config){
        $scope.comments = data.response.reverse();
        $scope.parent.commentsCount = $scope.comments.length;
      })
      .error(errorFn);
    }


    $scope.addNewComment = function(){
      if($scope.newComment){
        $scope.newComment = PostService.makeMeBr($scope.newComment);
        var newComment = {
          type:$scope.parent.id,
          body:$scope.newComment,
          isComment:true
        };
        PostService.addPost(newComment)
        .success(function(data, status, headers, config){
          $scope.newComment='';
          checkComments();
        })
        .error(errorFn);
      }
    };

    $scope.likeThisPost = function(id,index){
      PostService.likeThisPost(id)
      .success(function(data, status, headers, config){
          $scope.comments[index].likes = data.response.likes;
      })
      .error(errorFn);
    };

    $scope.delThisPost = function(id){
      PostService.delThisPost(id)
      .success(function(data, status, headers, config){
        checkComments();
      })
      .error(errorFn);
    };

    $scope.iCanDelThis = function(item){
      return PostService.iCanDelThis(item);
    }

    checkComments();
}]);
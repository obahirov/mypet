
angular.module('myPet').controller('UserCtrl',
  ['$scope','UserService','$location','ngDialog','$interval','$timeout',
  function($scope, UserService, $location, ngDialog,$interval,$timeout) {

  UserService.getProfessions()
    .success(function(data, status, headers, config){
      $scope.professions = data.response.professions;
    })
    .error(errorFn);
  
  $scope.autocompleteOptions = null;
  $scope.current={};  
  $scope.text = UserService.text;
  $scope.isPageOwner = UserService.isYou;

  $scope.isProfy = function(state){
    $scope.userInfo.isProf = state;
    $scope.saveUser();
    if(!$scope.isEditMode){
      $scope.editMode();
    }
  };

  $scope.profy = function(state){
    $scope.userInfo.isProf = state;
    userInit();
    if(state ==='no'){
      clearProfyFields($scope.userInfo);
    }
    if(!$scope.isEditMode){
      $scope.editMode();
    }
  };

  function clearProfyFields(user){
    user.profession='';
    $scope.current.profession='';
    user.sites=[];
    user.phones = [];
    user.career = '';
  }

  $scope.editMode = function(){
    $scope.isEditMode = $scope.isEditMode ? false : true;
    if($scope.isEditMode){
      userInit();
    } else{
      $scope.saveUser();
    }
  };

  $scope.addItem = function(item){
    if(!$scope.userInfo[item].length || $scope.userInfo[item][$scope.userInfo[item].length-1].name){
      $scope.userInfo[item].push(new UserService.NewItem());
    }
  };

  $scope.removeItem = function(item, index){
    if($scope.userInfo[item].length>1){
      $scope.userInfo[item].splice(index, 1);
    }
  };  

  function errorFn(data){
    console.log(data);
  }

  function init(){
    UserService.getUser(UserService.user.id)
    .success(function(data, status, headers, config){
      $scope.userInfo = data.response.userInfo;
      $scope.loadView = true;
      $scope.currentLocation = {};
    })
    .error(errorFn);
  }

  $scope.changeLocationFn = function(){
    $scope.changeLocation = true;
  };

  $scope.saveUser = function(){
    $scope.changeLocation =false;
    $scope.userInfo = UserService.checkFields($scope.userInfo);
    $scope.userInfo.location = (!$scope.currentLocation.formated)? $scope.userInfo.location : $scope.currentLocation;
    console.log($scope.currentLocation,$scope.userInfo.location);
    console.log($scope.userInfo);
    UserService.setUser($scope.userInfo)
    .success(function(data, status, headers, config){
      if($scope.isEditMode){
        userInit();
      }
    })
    .error(errorFn);
  }

  function pushIfEmpty(arr){
    if(!arr.length){
      arr.push(new UserService.NewItem());
    }
  }

  function userInit(){
    pushIfEmpty($scope.userInfo.organizations);
     if($scope.userInfo.isProf === 'yes'){
      pushIfEmpty($scope.userInfo.sites);
      pushIfEmpty($scope.userInfo.phones);
    }
  }

  init();

}]);


angular.module('myPet').factory('UserService', ['$http', 'CONFIG','$location' ,function($http,CONFIG,$location){
  
  var user = {
    id:kmcore.api.userId
  };

  var isYou = kmcore.api.isYou;

  var pageHandlerId = kmcore.api.pageHandlerId;
  
  function giveMeId(){
	  console.log(pageHandlerId);
    if(pageHandlerId){
      return pageHandlerId;
    } else{
      return isYou && user.id;
    }
  }

  var text ={
    question:mypet_translate.translateByKey('_t_question_about_profesion_t_'),
    specialization:mypet_translate.translateByKey('_t_specialization_t_'),
    yes:mypet_translate.translateByKey('_t_yes_t_'),
    no:mypet_translate.translateByKey('_t_no_t_'),
    main_information:mypet_translate.translateByKey('_t_main_information_t_'),
    organizations:mypet_translate.translateByKey('_t_organizations_t_'),
    organization:mypet_translate.translateByKey('_t_organization_t_'),
    work_place:mypet_translate.translateByKey('_t_work_place_t_'),
    location:mypet_translate.translateByKey('_t_location_t_'),
    website:mypet_translate.translateByKey('_t_website_t_'),
    phone_number:mypet_translate.translateByKey('_t_phone_number_t_'),
    tags:mypet_translate.translateByKey('_t_tags_t_'),
    work_place_palceholder:mypet_translate.translateByKey('_t_work_place_palceholder_t_'),
    tags_palceholder:mypet_translate.translateByKey('_t_tags_palceholder_t_'),
    website_palceholder:mypet_translate.translateByKey('_t_website_palceholder_t_'),
    organizations_palceholder:mypet_translate.translateByKey('_t_organizations_palceholder_t_'),
    edit_information:mypet_translate.translateByKey('_t_edit_information_t_'),
    view_information:mypet_translate.translateByKey('_t_view_information_t_'),
    save:mypet_translate.translateByKey('_t_save_t_'),
    save_and:mypet_translate.translateByKey('_t_save_and_t_'),
    im_not_a_specialist:mypet_translate.translateByKey('_t_im_not_a_specialist_t_'),
    im_a_specialist:mypet_translate.translateByKey('_t_im_a_specialist_t_')
  };

  var lang = kmcore.langName;

  function getUser(userInfo){
    var query = {
      lang:lang,
      id:giveMeId()
    };
    return $http.post('http://'+$location.$$host+'/authApi/getUser',query);
  }

  function setUser(userInfo){
    if(isYou){
      var query = {userInfo:userInfo};
      return $http.post('http://'+$location.$$host+'/clientApi/setUser', query); 
    }
  }

  function getProfessions(){
    var query = {lang:lang};
    return $http.post('http://'+$location.$$host+'/authApi/getProfessions',query);
  }

  function getBreedsByType(type){
    var query = {type:type};
    return $http.post('http://'+$location.$$host+'/petApi/getBreedsByType',query);
  }

  function checkFields(userInfo){
    if(userInfo.organizations.length){
      userInfo.organizations = clearEmptyFields(userInfo.organizations);
    }
    if(userInfo.sites.length){
      userInfo.sites = clearEmptyFields(userInfo.sites);
    }
    if(userInfo.phones.length){
      userInfo.phones = clearEmptyFields(userInfo.phones);
    }

    return userInfo;
  }

  function clearEmptyFields(arr){
    var newArr = arr.filter(function(item){
      return item.name!=='';
    });
    return newArr;
  }

  function NewItem() {
    this.name = '';
  }

  return{
    getUser:getUser,
    setUser:setUser,
    getProfessions:getProfessions,
    getBreedsByType:getBreedsByType,
    user:user,
    isYou:isYou,
    NewItem:NewItem,
    checkFields:checkFields,
    text:text
  }

}]);
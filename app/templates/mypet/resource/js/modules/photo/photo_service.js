angular.module('myPet').factory('PhotoService', ['$http', 'CONFIG' ,'$location',function($http,CONFIG,$location){
  
  var currentUser= {
    id:kmcore.api.userId
  };
  var pageHandlerId = kmcore.api.pageHandlerId;

  var isPageOwner = currentUser.id === pageHandlerId;

  var text ={
    foto:mypet_translate.translateByKey('_t_foto_t_'),
    add:mypet_translate.translateByKey('_t_add_t_'),
    edit:mypet_translate.translateByKey('_t_edit_t_'),
    view:mypet_translate.translateByKey('_t_view_t_'),
    photo:mypet_translate.translateByKey('_t_photo_t_')
  };

  function getPhotos(id){
    var query = {type:id, albumId:pageHandlerId}
    return $http.post('http://'+$location.$$host+'/photoGetterApi/getAllPhotoInAlbum', query);
  }

  function addPhotos(arr){
    var query = {photos:arr}
    return $http.post('http://'+$location.$$host+'/photoApi/appendPhoto', query);
  }

  function likeThisPhoto(id){
    var query = {id:id}
    return $http.post('http://'+$location.$$host+'/photoApi/like', query);
  }

  function removePhoto(id){
    var query = {id:id}
    return $http.post('http://'+$location.$$host+'/photoApi/detachPhoto', query);
  }
  

  return{
    getPhotos:getPhotos,
    addPhotos:addPhotos,
    likeThisPhoto:likeThisPhoto,
    isPageOwner:isPageOwner,
    removePhoto:removePhoto,
    text:text
  }
}]);
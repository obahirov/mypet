
angular.module('myPet').controller('PhotoCtrl',
  ['$scope','PhotoService','FileUploader', '$location','ngDialog',
  function($scope, PhotoService, FileUploader, $location, ngDialog) {
  
  $scope.newPhotos =[];
  $scope.isPageOwner = PhotoService.isPageOwner;
  $scope.text = PhotoService.text;

  var uploader = $scope.uploader = new FileUploader({
    url: 'http://'+$location.$$host+'/uploadPhotoApi/upload'
  });

  uploader.filters.push({
    name: 'imageFilter',
    fn: function(item /*{File|FileLikeObject}*/, options) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  });

  uploader.onCompleteAll = function() {
    $.fancybox.close();
    uploader.clearQueue();
    addPhotos();
  };

  uploader.onSuccessItem = function(fileItem, response, status, headers) {
    $scope.newPhotos.push({photo:response.response.result[0]});
  };

  $scope.popapClose = function(){
    $.fancybox.close();
    uploader.clearQueue();
  }

  function addPhotos(){
    PhotoService.addPhotos($scope.newPhotos)
      .success(function(data, status, headers, config){
        init();
        $scope.newPhotos=[];
      })
      .error(errorFn);
  }

  function errorFn(data){
    console.log(data);
  }	

  function init(){
    PhotoService.getPhotos()
    .success(function(data, status, headers, config){
      $scope.photos = data.response;
    })
    .error(errorFn);
  }

	$scope.addPhotoDialog = function(){
		ngDialog.open({
		  template: 'http://'+$location.$$host+'/resource/partials/add_photo_dialog.html',
		  scope: $scope,
		  className: 'myModal'
		});
	};

  $scope.photoViewDialog = function(photo,index){
    ngDialog.open({
      template: 'http://'+$location.$$host+'/resource/partials/photo/photo_view_dialog.html',
      scope: $scope,
      className: 'photo_view'
    });
    $scope.currentPhoto = photo;
    $scope.currentPhotoIndex = index;
  };
  
  function setCurrentPhoto(newIndex){
    $scope.currentPhoto = $scope.photos[newIndex];
    $scope.currentPhotoIndex = newIndex;
  }

  $scope.prevPhoto = function(index){
    if (index > 0){
      var newIndex = index - 1;
      setCurrentPhoto(newIndex);
    }
  };

  $scope.nextPhoto = function(index){
    if (index < $scope.photos.length-1){
      var newIndex = index + 1;
      setCurrentPhoto(newIndex);
    }
  };

  $scope.likeThisPhoto = function(id,index){
    PhotoService.likeThisPhoto(id)
    .success(function(data, status, headers, config){
      $scope.photos[index].likes = data.response.likes;
    })
    .error(errorFn);
  };

  $scope.editMode = function(){
    $scope.isEditMode = $scope.isEditMode ? false : true;
  };

  $scope.removePhoto = function(id){
    PhotoService.removePhoto(id)
    .success(function(data, status, headers, config){
      init();
    })
    .error(errorFn);
  };

	init();

}]);


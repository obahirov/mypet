angular.module('myPet').factory('PostService', ['$http', 'CONFIG' ,'$location','UserService',
  function($http,CONFIG,$location,UserService){
  
  var text ={
    addComment:mypet_translate.translateByKey('_t_add_comment_t_'),
    send:mypet_translate.translateByKey('_t_send_t_'),
    watssup:mypet_translate.translateByKey('_t_wassup_t_'),
    cool:mypet_translate.translateByKey('_t_cool_t_'),
    yourComment:mypet_translate.translateByKey('_t_your_comment_t_'),
    sharePost:mypet_translate.translateByKey('_t_share_post_t_')
  };

  var currentUser= {
    id:kmcore.api.userId,
    photo: kmcore.api.photo
  };
  var wallHandlerId = kmcore.api.pageHandlerId;

  function getPosts(id){
    var id = id || wallHandlerId;
    var query = {type:id};
    return $http.post('http://'+$location.$$host+'/tapeToNewsApi/getPostsByType', query);
  }

  function addPost(query){
    return $http.post('http://'+$location.$$host+'/postApi/add', query);
  }

  function delThisPost(id){
    var query = {id:id}
    return $http.post('http://'+$location.$$host+'/postApi/remove', query);
  }

  function likeThisPost(id){
    var query = {id:id};
    return $http.post('http://'+$location.$$host+'/postApi/like', query);
  }

  function shareThisPost(id){
    var query = {id:id}
    return $http.post('http://'+$location.$$host+'/postApi/share', query);
  }

  function makeMeBr(str){
    return str && str.replace(/\n/g, "<br />");
  }

  function iCanDelThis(item){
    return currentUser.id === wallHandlerId || currentUser.id === item.userId;
  }

  return{
    currentUser:currentUser,
    wallHandlerId:wallHandlerId,
    getPosts:getPosts,
    addPost:addPost,
    delThisPost:delThisPost,
    likeThisPost:likeThisPost,
    shareThisPost:shareThisPost,
    text:text,
    makeMeBr:makeMeBr,
    iCanDelThis:iCanDelThis
  }

}]);


// angular.module('myPet')
// .factory('QuizResource', ['$resource', function( $resource) {
//   return $resource('/admin/assessments/:id/:property', 
//       { id:'@id', property:''},
//       { 
//         update: {method:'PUT'},
//         validate: {method:'POST', params: {id: '@id', property: '@property'}}
//       }
//   )
// }])

// .factory('CommentsResource', ['$resource', function( $resource) {
//   return $resource('/assessments/:id/comments',  { id:'@quiz_id'} )
// }])
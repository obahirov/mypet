
angular.module('myPet').controller('WallCtrl',
  ['$scope','PostService','FileUploader','$location','CONFIG',
  function($scope,PostService,FileUploader,$location,CONFIG) {
  
  $scope.newPost = {
    type:PostService.wallHandlerId,
    data:[]
  };

  $scope.emoji = CONFIG.EMOJI;

  $scope.text = PostService.text;

  //file uplodaer
  var uploader = $scope.uploader = new FileUploader({
    url: 'http://'+$location.$$host+'/uploadPhotoApi/upload',
    autoUpload: true
  });

  uploader.filters.push({
    name: 'imageFilter',
    fn: function(item /*{File|FileLikeObject}*/, options) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  });
  
  uploader.onSuccessItem = function(fileItem, response, status, headers) {
    console.log(response.response.result[0]);
    $scope.newPost.data.push(response.response.result[0]);
  };

  $scope.removeUplFile = function(index){
    uploader.queue[index].remove();
    $scope.newPost.data.splice(index,1);
  }
  // end uploader 

  function errorFn(data){
    console.log(data);
  }

  function init(){
    PostService.getPosts()
    .success(function(data, status, headers, config){
      $scope.posts = data.response;
    })
    .error(errorFn);
  }

  function clearNewPostData(){
    $scope.ShowEmoji = false;
    $scope.newPost.body='';
    $scope.newPost.data=[];
    uploader.queue =[];
  }

  $scope.addNewPost = function(){
    if($scope.newPost.body || $scope.newPost.data.length){
      $scope.newPost.body = PostService.makeMeBr($scope.newPost.body);
      PostService.addPost($scope.newPost)
      .success(function(data, status, headers, config){
        init();
        clearNewPostData();
      })
      .error(errorFn);
    }
  };

  $scope.likeThisPost = function(id,index){
    PostService.likeThisPost(id)
    .success(function(data, status, headers, config){
        $scope.posts[index].likes = data.response.likes;
    })
    .error(errorFn);
  };

  $scope.shareThisPost = function(id){
    PostService.shareThisPost(id)
    .success(function(data, status, headers, config){
      console.log('win');
    })
    .error(errorFn);
  };

  $scope.delThisPost = function(id){
    PostService.delThisPost(id)
    .success(function(data, status, headers, config){
      init();
    })
    .error(errorFn);
  };

  $scope.ShowHideEmojiFunc = function(){
    $scope.ShowEmoji = $scope.ShowEmoji ? false : true;
  };

  $scope.pickEmoji = function(index){
    $scope.newPost.body = $scope.newPost.body || '';
    $scope.newPost.body += ' '+$scope.emoji[index] + ' ';
    $('#postInput').focus();
  };

  $scope.iCanDelThis = function(item){
    return PostService.iCanDelThis(item);
  }

  init();
}]);


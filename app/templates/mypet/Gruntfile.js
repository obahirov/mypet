module.exports = function(grunt) {

	// To support SASS/SCSS or Stylus, just install
	// the appropriate grunt package and it will be automatically included
	// in the build process, Sass is included by default:
	//
	// * for SASS/SCSS support, run `npm install --save-dev grunt-contrib-sass`
	// * for Stylus/Nib support, `npm install --save-dev grunt-contrib-stylus`

	// var hasSass = npmDependencies['grunt-contrib-sass'] !== undefined;
	// var hasStylus = npmDependencies['grunt-contrib-stylus'] !== undefined;

	grunt.initConfig({

		// Watches for changes and runs tasks
		watch : {
			sass : {
				files : ['resource/scss/**/*.scss'],
				tasks : ['sass:dev'],
				options : {
					livereload : true
				}
			},
			js : {
				files : ['resource/js/**/*.js'],
				tasks : ['jshint'],
				options : {
					livereload : true
				}
			},
			twig : {
				files : ['**/*.twig'],
				options : {
					livereload : true
				}
			}
		},

		// JsHint your javascript
		jshint : {
			all : ['resource/js/*.js', '!js/modernizr.js', '!js/*.min.js', '!js/vendor/**/*.js'],
			options : {
				browser: true,
				curly: false,
				eqeqeq: false,
				eqnull: true,
				expr: true,
				immed: true,
				newcap: true,
				noarg: true,
				smarttabs: true,
				sub: true,
				undef: false
			}
		},

		// Dev and production build for sass
		sass : {
			production : {
				files : [
					{
						src : ['**/*.scss', '!**/_*.scss'],
						cwd : 'resource/scss',
						dest : 'resource/css',
						ext : '.css',
						expand : true
					}
				],
				options : {
					style : 'compressed'
				}
			},
			dev : {
				files : [
					{
						src : ['**/*.scss', '!**/_*.scss'],
						cwd : 'resource/scss',
						dest : 'resource/css',
						ext : '.css',
						expand : true
					}
				],
				options : {
					style : 'expanded'
				}
			}
		},

		// Require config
		concat: {
	      dist: {
	        src: ['js/**/*.js'],
	        dest: 'build/global.js',
	      }
	    },
	    uglify: {
	      	build: {
	            src: 'build/global.js',
	            dest: 'build/global.min.js'
	        }
      	},
		cssmin: {
            css:{
                src: 'css/global.css',
                dest: 'build/global.min.css'
            }
        },
		// Image min
		imagemin : {
			production : {
				files : [
					{
						expand: true,
						cwd: 'images',
						src: '**/*.{png,jpg,jpeg}',
						dest: 'images'
					}
				]
			}
		},

		// SVG min
		svgmin: {
			production : {
				files: [
					{
						expand: true,
						cwd: 'images',
						src: '**/*.svg',
						dest: 'images'
					}
				]
			}
		}

	});

	// Default task
	grunt.registerTask('default', ['watch']);

	grunt.registerTask('production', ['concat','uglify','imagemin','cssmin']);

	// Build task
	// grunt.registerTask('build', function() {
	// 	var arr = ['jshint'];

	// 	if (hasSass) {
	// 		arr.push('sass:production');
	// 	}

	// 	if (hasStylus) {
	// 		arr.push('stylus:production');
	// 	}

	// 	arr.push('imagemin:production', 'svgmin:production', 'requirejs:production');

	// 	return arr;
	// });

	// // Template Setup Task
	// grunt.registerTask('setup', function() {
	// 	var arr = [];

	// 	if (hasSass) {
	// 		arr.push['sass:dev'];
	// 	}

	// 	if (hasStylus) {
	// 		arr.push('stylus:dev');
	// 	}

	// 	arr.push('bower-install');
	// });

	// // Load up tasks
	// if (hasSass) {
	// 	grunt.loadNpmTasks('grunt-contrib-sass');
	// }

	// if (hasStylus) {
	// 	grunt.loadNpmTasks('grunt-contrib-stylus');
	// }
	
	require('load-grunt-tasks')(grunt);

	// Run bower install
	// grunt.registerTask('bower-install', function() {
	// 	var done = this.async();
	// 	var bower = require('bower').commands;
	// 	bower.install().on('end', function(data) {
	// 		done();
	// 	}).on('data', function(data) {
	// 		console.log(data);
	// 	}).on('error', function(err) {
	// 		console.error(err);
	// 		done();
	// 	});
	// });

};

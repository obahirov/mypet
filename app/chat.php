<?php
use MyPet\WebSockets\Events\Chat;
use MyPet\WebSockets\Events\Pusher;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

require(realpath(__DIR__ . '/bootstrap.php'));

set_time_limit(0);
//ini_set("memory_limit","2048M");
ini_set('default_socket_timeout', -1);

$server = IoServer::factory(
	new HttpServer(
		new WsServer(
			new Chat()
		)
	),
	8089
);

$server->run();
<?php
$conf['baseUrl'] = 'http://mypet.local';
/**
 * Название приложения. ВАЖНО!!!
 */
$conf['appSourceName'] = 'MyPet';

/**
 * Название админки. ВАЖНО!!!
 */
$conf['appAdminSourceName'] = 'CP';

/**
 * Название проекта в админке
 */
$conf['currentProject'] = '' . APP_NAME . ' ' . APP_VERSION . ' (' . APP_STATUS . ')';

/**
 * Название и путь к ресурсам отображения
 */
$conf['bundles'] = array(
	'KMCore' => VENDOR_PATH . '/gollariel/km-core/src', // Базовое ядро системы
	$conf['appSourceName'] => SRC_PATH // Путь к приложению
);

/**
 * Всевозможная соль
 */
$conf['salt'] = 'amxcgu-0)_(MU_XU#_XUR aejrhn37840xnxajhgt78';
$conf['snsigSalt'] = 'qwe234X()#8rmxpoafdsaf342879cnv(*@#&*^kjhsdflakjsdna390djlkaH432';
$conf['datasigSalt'] = 'QJaSmPLsDcSfdsafwe2hn6FSfdeouTdknTKGBQD0dN7bD9jIahgfdhgfVK9m5J7Pk';
$conf["authSalt"] = 'yxm2389xn98ynZ(*WEN3fSF2xDN4EkQ2_dsaIJ8Rq7J0a0VPPtgloGDV8zWdo56o6L';

$conf['xor'] = ['salt' => 'D8PE2.z3#UQnm7Hf$qVPHKp(0ktJX@7c', 'pass' => 'XubjNJ99PcmUYovWCb8SsDxCjbLUDiEV'];
$conf['sessionXorData'] = ['salt' => '2xDN4EkQ2_dsaIJ8nZ(*WEN3fSF', 'pass' => 'nZ(*WEN3f2879cnv(*@#&*^kj'];

/**
 * Тестовые хосты
 */
$conf['testHosts'] = array('mypet.local' => true, 'test.time4pets.com' => true);
$conf['isTestHost'] = isset($_SERVER['HTTP_HOST']) && !empty($conf['testHosts'])
	&& isset($conf['testHosts'][$_SERVER['HTTP_HOST']]);

/**
 * Тестовые UserAgents
 */
$conf['testUserAgents'] = array(
	"iphone"  => "Mozilla\/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit\/534.46 (KHTML, like Gecko) Mobile\/9A405 TEST",
	"ipad"    => "Mozilla\/5.0 (iPad; CPU OS 5_1 like Mac OS X) AppleWebKit\/534.46 (KHTML, like Gecko) Mobile\/9B176 TEST",
	"android" => "Mozilla\/5.0 (Linux; U; Android 2.2.2; en-us; HUAWEI-M835 Build\/HuaweiM835) AppleWebKit\/533.1 (KHTML, like Gecko) Version\/4.0 Mobile Safari\/533.1 TEST",
	"desktop" => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322 TEST)",
);
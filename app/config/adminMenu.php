<?php
/**
 * Меню админки
 */
$conf['menu'] = [
	'Users'          => [
		'Users' => 'MyPet_Users_Model_User',
		'SocUserModel'   => 'MyPet_Users_Model_SocUserModel',
		'WorkModel'      => 'MyPet_Users_Model_WorkModel',
	],
	'Pets'           => [
		'Pets' => 'MyPet_Pets_Model_PetModel',
		'Gags' => 'MyPet_Pets_Model_GagModel',
	],
	'CMS'            => [
		'Управление страницами'   => 'KMCore_CMS_Model_Pages',
		'Управление компонентами' => 'KMCore_CMS_Model_Components',
		'Transaction'             => 'KMCore_Transaction_TransactionModel',
		'SendEmailModel'          => 'MyPet_Notification_Email_Model_SendEmailModel',
		'FAQModel'                => 'MyPet_CMS_Classes_Plugins_FAQ_FAQModel',
		'HistoryConnectionDevelopers' => 'MyPet_Users_Model_HistoryConnectionDevelopers',
	],
	'Content' => [
		'PostModel'               => 'MyPet_Posts_Model_PostModel',
		'LikeHistory'             => 'MyPet_Users_Model_LikeHistory',
		'Альбоми' => 'MyPet_Photos_Model_AlbumModel',
		'Фото'    => 'MyPet_Photos_Model_PhotoModel',
		'Requests' => 'MyPet_Inbox_Model_Requests',
		'ProfessionModel' => 'MyPet_Users_Model_ProfessionModel',
		'BreedModel'      => 'MyPet_Pets_Model_BreedModel',
		'Country'         => 'MyPet_Location_Model_Country',
		'Region'          => 'MyPet_Location_Model_Region',
		'City'            => 'MyPet_Location_Model_City',
		'HistoryModel'                 => 'MyPet_Referrals_Model_HistoryModel',
		'Link'                 => 'MyPet_ShortLinks_Model_Link',
	],
	'Friends'        => [
		'FriendsModel' => 'MyPet_Friends_Model_FriendsModel',
		'MessageModel' => 'MyPet_Messages_Model_MessageModel',
		'DialogModel'  => 'MyPet_Messages_Model_DialogModel',
		'RulesModel'   => 'MyPet_Rules_Model_RulesModel',
	],
	'Админам'        => [
		'BaseConfig'                  => 'KMCore_Config_Model_BaseConfig',
		'Администраторы'              => 'KMCore_Admin_Model_AdminUser',
		'Лог действий админов'        => 'KMCore_Admin_Model_AdminActionLog',
		'ProfilingModel'              => 'KMCore_LogSystem_ProfilingModel',
	],
];

$conf['menuExtra'] = [
	'KMCore_Admin_Model_AdminUser' => [
		'Генератор хешей'         => 'AdminUser/generator',
		'Создать индексы всей БД' => 'AdminUser/createIndex'
	],
	'KMCore_CMS_Model_Pages'       => [
		'Генератор json контента' => 'Pages/generator'
	],
];
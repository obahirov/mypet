<?php
$conf['urlMap'] = [
	// Angular compatibility API
	'groupApi' => [
		'controller' => 'MyPet\Groups\Controller\GroupController'
	],
	'clientApi'       => [
		'controller' => 'MyPet\Users\Controller\ClientApiController'
	],
	'authApi'         => [
		'controller' => 'MyPet\Users\Controller\AuthApiController'
	],
	'userApi'         => [
		'controller' => 'MyPet\Users\Controller\UserController',
		'arguments'  => ['id' => null]
	],
	'friendApi'       => [
		'controller' => 'MyPet\Friends\Controller\FriendsController'
	],
	'petApi'          => [
		'controller' => 'MyPet\Pets\Controller\PetController'
	],
	'ancestryApi'     => [
		'controller' => 'MyPet\Pets\Controller\AncestryController'
	],
	'requestApi'      => [
		'controller' => 'MyPet\Inbox\Controller\RequestController'
	],
	'dialogApi'       => [
		'controller' => 'MyPet\Messages\Controller\DialogController'
	],
	'messageApi'      => [
		'controller' => 'MyPet\Messages\Controller\MessageController'
	],
	'searchApi'       => [
		'controller' => 'MyPet\Search\Controller\SearchController'
	],
	'searchHeperApi'  => [
		'controller' => 'MyPet\CMS\Controller\SearchHelper'
	],
	'postApi'         => [
		'controller' => 'MyPet\Posts\Controller\PostController',
	],
	'tapeToNewsApi'   => [
		'controller' => 'MyPet\Posts\Controller\TapeToNewsController',
	],
	'uploadPhotoApi'  => [
		'controller' => 'MyPet\Photos\Controller\UploadPhoto'
	],
	'photoApi'        => [
		'controller' => 'MyPet\Photos\Controller\PhotoController'
	],
	'photoGetterApi'        => [
		'controller' => 'MyPet\Photos\Controller\PhotoGetterController'
	],
	'ret'        => [
		'controller' => 'MyPet\RatAnalyst\Controller\RequestHandler'
	],

	// CMS PAGE
	'page'            => [
		'controller' => 'MyPet\CMS\Controller\Index'
	],
	'pet'             => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'pet',
		'arguments'  => ['petId' => null, 'action' => 'view']
	],
	'specialists'     => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'specialists',
		'arguments'  => ['petId' => null]
	],
	'ancestry'        => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'ancestry',
		'arguments'  => ['petId' => null, 'type' => 'parent']
	],
	'pets'            => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'pets',
		'arguments'  => ['userId' => null, 'page' => 0]
	],
	'familys'         => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'familys',
		'arguments'  => ['petId' => null]
	],
	'user'            => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'user',
		'arguments'  => ['userId' => null, 'action' => 'view']
	],
	'search'          => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'search',
		'arguments'  => ['page' => 0]
	],
	'friends'         => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'friends',
		'arguments'  => ['userId' => null, 'page' => 0]
	],
	'message'         => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'message',
		'arguments'  => ['dialogId' => null, 'page' => 0]
	],
	'portfolio'       => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'portfolio',
		'arguments'  => ['userId' => null, 'page' => 0]
	],
	'wall'            => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'wall',
		'arguments'  => ['userId' => null, 'page' => 0]
	],
	'photo'           => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'photo',
		'arguments'  => ['userId' => null, 'page' => 0]
	],
	'group'           => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'group',
		'arguments'  => ['groupId' => null, 'topicId' => null, 'page' => 0]
	],
	'news'           => [
		'controller' => 'MyPet\CMS\Controller\Index',
		'method'     => 'news',
		'arguments'  => ['page' => 0]
	],

	// CMS API
	'json'            => [
		'controller' => 'MyPet\CMS\Controller\Ajax'
	],
	'userAvatar'      => [
		'controller' => 'MyPet\FileUpload\Controller\UserAvatarUpload'
	],
	'userCover'       => [
		'controller' => 'MyPet\FileUpload\Controller\UserCoverUpload'
	],
	'petAvatar'       => [
		'controller' => 'MyPet\FileUpload\Controller\PetAvatarUpload'
	],
	'petCover'        => [
		'controller' => 'MyPet\FileUpload\Controller\PetCoverUpload'
	],
	'postPhotoUpload' => [
		'controller' => 'MyPet\FileUpload\Controller\PostPhotoUpload'
	],
	'location'        => [
		'controller' => 'MyPet\Location\Controller\LocationController'
	],
	'socialLogin'     => [
		'controller' => 'MyPet\Users\Controller\SocAuthApiController'
	],
	'referral'        => [
		'controller' => 'MyPet\Referrals\Controller\ReferralHandlerController'
	],
	'invite'          => [
		'controller' => 'MyPet\Referrals\Controller\InviteController'
	],
	'converter'       => [
		'controller' => 'MyPet\FileConverter\Controller\ConverterController'
	],

	'facebook'       => [
		'controller' => 'MyPet\FacebookCanvas\Controller\FacebookCanvas'
	],
	'sht'       => [
		'controller' => 'MyPet\ShortLinks\Controller\ShortLinksController',
		'method'     => 'r',
		'arguments'  => ['code' => null]
	],
	'link_generator'       => [
		'controller' => 'MyPet\ShortLinks\Controller\ShortLinkGeneratorController',
	],
];
$conf['defaultIdent'] = 'page';
$conf['defaultMethod'] = 'login';
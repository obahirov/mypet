<?php
$conf['baseUrl'] = 'time4pets.local';
/**
 * Базовый URL сайта. Если отсутсвует, при попытке получить через конфигурацию выдаст $_SERVER['HTTP_HOST']
 */
$conf['allowLocalLogin'] = true;

/**
 * Название хоста монги
 */
$conf['mongohostname'] = 'mongodb://127.0.0.1';
$conf['mongodbname'] = 'mypet';

//http://10.0.2.122:8081/
//rediska
//ch4SeZ13QVM4sEOw8mHE
//Пароль к базе данных редиса 2xW4P9e3pcYV585jOvTy
$conf['redishostname'] = '127.0.0.1';
$conf['redisport'] = 6379;
$conf['redisdb'] = 1;
$conf['redispass'] = '2xW4P9e3pcYV585jOvTy';
$conf['elasticksearchEnable'] = true;
$conf['elasticksearch']['hosts'] = [
//	'mp-db1.themypet.com:9200',         // IP + Port
	'test.time4pets.com:9200',         // IP + Port
];
//$conf['elasticksearch']['auth'] = array(
//	'elasticsearch',
//	'Eiw9hai4neiGhooW7mui5teer',
//	'Basic'
//);

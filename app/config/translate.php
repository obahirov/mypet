<?php
$conf['send_ancestry_pet_request'] = array(
    'en' => 'The request was sent to the user. After request confirmation the pet  will appear in your pet\'s Pedigree chart',
    'ru' => 'Пользователю был отправлен запрос. После его подтверждения, питомец появится в схеме родословной Вашего питомца',
    'ua' => 'Користувачу було надіслано запит. Після його підтвердження, улюбленець з\'явиться у схемі родоводу Вашого улюбленця'
);
$conf['send_request'] = array(
	'en' => 'Request send',
	'ru' => 'Запрос отправлен',
	'ua' => 'Запит надіслано'
);
$conf['success_update'] = array(
	'en' => 'Success update',
	'ru' => 'Успешная обновление',
	'ua' => 'Успішно оновлено'
);
$conf['wrong_email_or_password'] = array(
	'en' => 'Wrong email or password',
	'ru' => 'Неверные email или пароль',
	'ua' => 'Невірний email або пароль'
);
$conf['you_done_login'] = array(
	'en' => 'You have successfully logged in',
	'ru' => 'Вы успешно авторизировались',
	'ua' => 'Ви успішно авторизувались'
);
$conf['you_logged_user'] = array(
	'en' => 'You logged user',
	'ru' => 'Вы уже авторизированы',
	'ua' => 'Ви вже авторизовані'
);
$conf['you_not_logged_user'] = array(
	'en' => 'You not logged user',
	'ru' => 'Вы не авторизированы',
	'ua' => 'Ви не авторизовані'
);
$conf['passwords_do_not_match'] = array(
	'en' => 'Passwords do not match',
	'ru' => 'Пароли не совпадают',
	'ua' => 'Паролі не співпадають'
);
$conf['unsuccess_login'] = array(
	'en' => 'Unsuccess login',
	'ru' => 'Неуспешная авторизация',
	'ua' => 'Неуспішна авторизація'
);
$conf['successfully_completed'] = array(
	'en' => 'You have successfully completed',
	'ru' => 'Вы успешно завершили',
	'ua' => 'Ви успішно завершили'
);
$conf['email_already_exist'] = array(
	'en' => 'Email already exist',
	'ru' => 'Email уже существует',
	'ua' => 'Email вже існує'
);
$conf['wrong_create_user'] = array(
	'en' => 'Wrong create user',
	'ru' => 'Ошибка создания пользователя',
	'ua' => 'Помилка створення коритсувача'
);
$conf['payment_success'] = array(
	'en' => 'Payment success',
	'ru' => 'Оплата прошла успешно',
	'ua' => 'Оплата пройшла успішно'
);
$conf['payment_unsuccess'] = array(
	'en' => 'Payment unsuccess',
	'ru' => 'Оплата не прошла',
	'ua' => 'Оплата не пройшла'
);
$conf['payment_error'] = array(
	'en' => 'Payment error',
	'ru' => 'Ошибка платежа',
	'ua' => 'Помилка оплати'
);
$conf['not_connect_to_twitter'] = array(
	'en' => 'Could not connect to Twitter. Refresh the page or try again later',
	'ru' => 'Не могу подключиться к Twitter. Обновите страницу или повторите попытку позже',
	'ua' => 'Не можу підключитись до Twitter. Оновіть сторінку або спробуйте спробу пізніше'
);
$conf['error_get_user_data'] = array(
	'en' => 'Error get user data',
	'ru' => 'Ошибка получения данных пользователя',
	'ua' => 'Помилка отримання данних користувача'
);
$conf['registration_form_is_invalid'] = array(
	'en' => 'Registration form is invalid',
	'ru' => 'Регистрационная форма является недействительной',
	'ua' => 'Реєстраційна форма недійсна'
);
$conf['empty_password'] = array(
	'en' => 'Empty password',
	'ru' => 'Пустой пароль',
	'ua' => 'Пустий пароль'
);
$conf['email_strlen_error'] = array(
	'en' => 'Email is lenght error',
	'ru' => 'Email неверной длинны',
	'ua' => 'Email невірної довжини'
);
$conf['password_strlen_error'] = array(
	'en' => 'Password is lenght error',
	'ru' => 'Пароль неверной длинны',
	'ua' => 'Пароль невірної довжини'
);
$conf['not_have_permitted'] = array(
	'en' => 'Not have permitted',
	'ru' => 'Не хватает прав',
	'ua' => 'Не вистачає прав'
);
$conf['email_is_invalid'] = array(
	'en' => 'Email is invalid',
	'ru' => 'Почта недействительна',
	'ua' => 'Пошта недійсна'
);
$conf['not_found_correct_mail'] = array(
	'en' => 'Not found correct email',
	'ru' => 'Не найдена коректная почта',
	'ua' => 'Не знайдена коректна пошта'
);
$conf['not_found_correct_user'] = array(
	'en' => 'Not found correct usere',
	'ru' => 'Не найден пользователь',
	'ua' => 'Не знайденено користувача'
);
$conf['success_send_password'] = array(
	'en' => 'Success send',
	'ru' => 'Успешно отправлено',
	'ua' => 'Успішно надіслано'
);
$conf['success_logout'] = array(
	'en' => 'Success logout',
	'ru' => 'Успешно вышли',
	'ua' => 'Успішно вийшли'
);
$conf['success_save'] = array(
	'en' => 'Saved',
	'ru' => 'Изменения сохранены',
	'ua' => 'Зміни збережено'
);
$conf['double_email'] = array(
	'en' => 'Double email',
	'ru' => 'Такой email уже зарегистрирован',
	'ua' => 'Такий email вже зареєстровано'
);